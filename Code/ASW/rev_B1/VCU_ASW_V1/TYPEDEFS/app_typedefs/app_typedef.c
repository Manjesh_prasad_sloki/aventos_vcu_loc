/******************************************************************************
 *    FILENAME    : app_typedefs.c
 *    DESCRIPTION : source file contains definitions of timer counter.
 ******************************************************************************
 * Revision history
 *
 * Ver Author       Date               Description
 * 1   Sloki     21/11/2020		   Initial version
 ******************************************************************************
*/


#include "app_typedef.h"


volatile UINT32 _1msTimer_u32 = 0;
