﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : mcu_can_signals.c
|    Project        : VCU
|    Module         : motor signals data
|    Description    : This is the generated file from the DBC2CH Tool for motor.
|                     controller
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 04/05/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MCU_CAN_SIGNALS_C
#define MCU_CAN_SIGNALS_C

#include "mcu_can_signals.h"


/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 

 
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 
 
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
 uint32_t Deserialize_VCU_MCU_0x100_Tx(VCU_MCU_0x100_Tx_t* message, const uint8_t* data)
{
  message->Motor_Speed_Limit = (((data[1] & (SIGNLE_READ_Mask8)) << MCU_0X100_MOTOR_SPEED_LIMIT_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + MCU_0X100_CANID_MOTOR_SPEED_LIMIT_OFFSET;
  message->Max_Regeneration = ((data[3] & (SIGNLE_READ_Mask8))) + MCU_0X100_CANID_MAX_REGENERATION_OFFSET;
  message->Ride_mode_Request = ((data[7] & (SIGNLE_READ_Mask3))) + MCU_0X100_CANID_RIDE_MODE_REQUEST_OFFSET;
   return MCU_0X100_ID; 
}

uint32_t Serialize_VCU_MCU_0x100_Tx(VCU_MCU_0x100_Tx_t* message, uint8_t* data)
{
  message->Motor_Speed_Limit = (message->Motor_Speed_Limit  - MCU_0X100_CANID_MOTOR_SPEED_LIMIT_OFFSET);
  message->Max_Regeneration = (message->Max_Regeneration  - MCU_0X100_CANID_MAX_REGENERATION_OFFSET);
  message->Ride_mode_Request = (message->Ride_mode_Request  - MCU_0X100_CANID_RIDE_MODE_REQUEST_OFFSET);
  data[0] = (message->Motor_Speed_Limit & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->Motor_Speed_Limit >> MCU_0X100_MOTOR_SPEED_LIMIT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->Max_Regeneration & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->Ride_mode_Request & (SIGNLE_READ_Mask3)) ;
   return MCU_0X100_ID; 
}

/*----------------------------------------------------------------------------*/

 uint32_t Deserialize_MCU_VCU_0x150_Rx(MCU_VCU_0x150_Rx_t* message, const uint8_t* data)
{
  message->Motor_Speed = (((data[1] & (SIGNLE_READ_Mask8)) << MCU_0X150_MOTOR_SPEED_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + MCU_0X150_CANID_MOTOR_SPEED_OFFSET;
  message->Ride_Mode_Actual = (((data[7] >> MCU_0X150_RIDE_MODE_ACTUAL_MASK0) & (SIGNLE_READ_Mask3))) + MCU_0X150_CANID_RIDE_MODE_ACTUAL_OFFSET;
   return MCU_0X150_ID; 
}

 uint32_t Serialize_MCU_0x150(MCU_VCU_0x150_Rx_t* message, uint8_t* data)
{
  message->Motor_Speed = (message->Motor_Speed  - MCU_0X150_CANID_MOTOR_SPEED_OFFSET);
  message->Ride_Mode_Actual = (message->Ride_Mode_Actual  - MCU_0X150_CANID_RIDE_MODE_ACTUAL_OFFSET);
  data[0] = ((int16_t)(message->Motor_Speed) & (SIGNLE_READ_Mask8)) ;
  data[1] = (((int16_t)(message->Motor_Speed) >> MCU_0X150_MOTOR_SPEED_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->Ride_Mode_Actual & (SIGNLE_READ_Mask3)) << MCU_0X150_RIDE_MODE_ACTUAL_MASK0) ;
  return MCU_0X150_ID; 
}
/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_MCU_VCU_0x250_Rx(MCU_VCU_0x250_Rx_t* message, const uint8_t* data)
{
  message->Motor_Temp = (((data[1] & (SIGNLE_READ_Mask2)) << MCU_0X250_MOTOR_TEMP_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + MCU_0X250_CANID_MOTOR_TEMP_OFFSET;
  message->Controller_Temp = (((data[3] & (SIGNLE_READ_Mask2)) << MCU_0X250_CONTROLLER_TEMP_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + MCU_0X250_CANID_CONTROLLER_TEMP_OFFSET;
  message->Motor_Rotation_Number = ((((uint32_t)data[7] & (SIGNLE_READ_Mask8)) << MCU_0X250_MOTOR_ROTATION_NUMBER_MASK0) | (((uint32_t)data[6] & (SIGNLE_READ_Mask8)) << MCU_0X250_MOTOR_ROTATION_NUMBER_MASK1) | (((uint32_t)data[5] & (SIGNLE_READ_Mask8)) << MCU_0X250_MOTOR_ROTATION_NUMBER_MASK2) | ((uint32_t)data[4] & (SIGNLE_READ_Mask8))) + MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_OFFSET;
  return MCU_0X250_ID; 
}

 uint32_t Serialize_MCU_0x250(MCU_VCU_0x250_Rx_t* message, uint8_t* data)
{
  message->Motor_Temp = (message->Motor_Temp  - MCU_0X250_CANID_MOTOR_TEMP_OFFSET);
  message->Controller_Temp = (message->Controller_Temp  - MCU_0X250_CANID_CONTROLLER_TEMP_OFFSET);
  message->Motor_Rotation_Number = (message->Motor_Rotation_Number  - MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_OFFSET);
  data[0] = (message->Motor_Temp & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->Motor_Temp >> MCU_0X250_MOTOR_TEMP_MASK0) & (SIGNLE_READ_Mask2)) ;
  data[2] = (message->Controller_Temp & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->Controller_Temp >> MCU_0X250_CONTROLLER_TEMP_MASK0) & (SIGNLE_READ_Mask2)) ;
  data[4] = (message->Motor_Rotation_Number & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->Motor_Rotation_Number >> MCU_0X250_MOTOR_ROTATION_NUMBER_MASK2) & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->Motor_Rotation_Number >> MCU_0X250_MOTOR_ROTATION_NUMBER_MASK1) & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->Motor_Rotation_Number >> MCU_0X250_MOTOR_ROTATION_NUMBER_MASK0) & (SIGNLE_READ_Mask8)) ;
   return MCU_0X250_ID; 
}
/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_VCU_MCU_0x400_Tx(VCU_MCU_0x400_Tx_t* message, const uint8_t* data)
{
  message->Broadcas_Rate = ((data[0] & (SIGNLE_READ_Mask8))) + MCU_0X400_CANID_BROADCAS_RATE_OFFSET;
  message->Broadcast_Mode = ((data[1] & (SIGNLE_READ_Mask1))) + MCU_0X400_CANID_BROADCAST_MODE_OFFSET;
   return MCU_0X400_ID; 
}

 uint32_t Serialize_VCU_MCU_0x400_Tx(VCU_MCU_0x400_Tx_t* message, uint8_t* data)
{
  message->Broadcas_Rate = (message->Broadcas_Rate  - MCU_0X400_CANID_BROADCAS_RATE_OFFSET);
  message->Broadcast_Mode = (message->Broadcast_Mode  - MCU_0X400_CANID_BROADCAST_MODE_OFFSET);
  data[0] = (message->Broadcas_Rate & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Broadcast_Mode & (SIGNLE_READ_Mask1)) ;
   return MCU_0X400_ID; 
}

 uint32_t Deserialize_MCU_0x200(MCU_0x200_t* message, const uint8_t* data)
{
  message->Throttle_out_p = ((data[0] & (SIGNLE_READ_Mask8))) + MCU_0X200_CANID_THROTTLE_OUT_P_OFFSET;
  message->Throttle_reference = ((data[1] & (SIGNLE_READ_Mask8))) + MCU_0X200_CANID_THROTTLE_REFERENCE_OFFSET;
  message->Battery_voltage = ((data[7] & (SIGNLE_READ_Mask8))) + MCU_0X200_CANID_BATTERY_VOLTAGE_OFFSET;
   return MCU_0X200_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_MCU_0x200(MCU_0x200_t* message, uint8_t* data)
{
  message->Throttle_out_p = (message->Throttle_out_p  - MCU_0X200_CANID_THROTTLE_OUT_P_OFFSET);
  message->Throttle_reference = (message->Throttle_reference  - MCU_0X200_CANID_THROTTLE_REFERENCE_OFFSET);
  message->Battery_voltage = (message->Battery_voltage  - MCU_0X200_CANID_BATTERY_VOLTAGE_OFFSET);
  data[0] = (message->Throttle_out_p & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Throttle_reference & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->Battery_voltage & (SIGNLE_READ_Mask8)) ;
   return MCU_0X200_ID; 
}

/*----------------------------------------------------------------------------*/

#endif /* MCU_CAN_SIGNALS_C */