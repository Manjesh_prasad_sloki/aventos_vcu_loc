﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : cluster_can_signals.c
|    Project        : VCU
|    Module         : cluster signals data
|    Description    : This is the generated file from the DBC2CH Tool for HMI.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 12/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CLUSTER_CAN_SIGNALS_C
#define CLUSTER_CAN_SIGNALS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "cluster_can_signals.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Eve_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Eve_1_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize__i_HMI_Tx_Eve_1(_i_HMI_Tx_Eve_1_t *message, uint8_t *data)
{
  message->_i_Reverse_Mode_u8 = (message->_i_Reverse_Mode_u8 - _I_HMI_TX_EVE_1_CANID__I_REVERSE_MODE_U8_OFFSET);
  message->_i_Sports_Mode_u8 = (message->_i_Sports_Mode_u8 - _I_HMI_TX_EVE_1_CANID__I_SPORTS_MODE_U8_OFFSET);
  message->_i_Eco_Mode_u8 = (message->_i_Eco_Mode_u8 - _I_HMI_TX_EVE_1_CANID__I_ECO_MODE_U8_OFFSET);
  message->_i_Neutral_Mode_u8 = (message->_i_Neutral_Mode_u8 - _I_HMI_TX_EVE_1_CANID__I_NEUTRAL_MODE_U8_OFFSET);
  message->_i_Kill_Switch_u8 = (message->_i_Kill_Switch_u8 - _I_HMI_TX_EVE_1_CANID__I_KILL_SWITCH_U8_OFFSET);
  message->_i_Right_Indicator_u8 = (message->_i_Right_Indicator_u8 - _I_HMI_TX_EVE_1_CANID__I_RIGHT_INDICATOR_U8_OFFSET);
  message->_i_Left_Indicator_u8 = (message->_i_Left_Indicator_u8 - _I_HMI_TX_EVE_1_CANID__I_LEFT_INDICATOR_U8_OFFSET);
  message->_i_Safe_Mode_u8 = (message->_i_Safe_Mode_u8 - _I_HMI_TX_EVE_1_CANID__I_SAFE_MODE_U8_OFFSET);
  message->_i_Motor_Fault_u8 = (message->_i_Motor_Fault_u8 - _I_HMI_TX_EVE_1_CANID__I_MOTOR_FAULT_U8_OFFSET);
  message->_i_Battery_Fault_u8 = (message->_i_Battery_Fault_u8 - _I_HMI_TX_EVE_1_CANID__I_BATTERY_FAULT_U8_OFFSET);
  message->_i_High_Beam_u8 = (message->_i_High_Beam_u8 - _I_HMI_TX_EVE_1_CANID__I_HIGH_BEAM_U8_OFFSET);
  message->_i_Low_Beam_u8 = (message->_i_Low_Beam_u8 - _I_HMI_TX_EVE_1_CANID__I_LOW_BEAM_U8_OFFSET);
  message->_i_Ignition_u8 = (message->_i_Ignition_u8 - _I_HMI_TX_EVE_1_CANID__I_IGNITION_U8_OFFSET);
  message->_i_Battery_Status_u8 = (message->_i_Battery_Status_u8 - _I_HMI_TX_EVE_1_CANID__I_BATTERY_STATUS_U8_OFFSET);
  message->_i_Warning_Symbol_u8 = (message->_i_Warning_Symbol_u8 - _I_HMI_TX_EVE_1_CANID__I_WARNING_SYMBOL_U8_OFFSET);
  message->_i_Service_Indicator_u8 = (message->_i_Service_Indicator_u8 - _I_HMI_TX_EVE_1_CANID__I_SERVICE_INDICATOR_U8_OFFSET);
  message->_i_Side_Stand_u8 = (message->_i_Side_Stand_u8 - _I_HMI_TX_EVE_1_CANID__I_SIDE_STAND_U8_OFFSET);
  message->_i_Hours_Time_u8 = (message->_i_Hours_Time_u8 - _I_HMI_TX_EVE_1_CANID__I_HOURS_TIME_U8_OFFSET);
  message->_i_Minutes_Time_u8 = (message->_i_Minutes_Time_u8 - _I_HMI_TX_EVE_1_CANID__I_MINUTES_TIME_U8_OFFSET);
  message->_i_Cluster_Use_Place_u8 = (message->_i_Cluster_Use_Place_u8 - _I_HMI_TX_EVE_1_CANID__I_CLUSTER_USE_PLACE_U8_OFFSET);
  message->_i_Eve_1_Counter_u8 = (message->_i_Eve_1_Counter_u8 - _I_HMI_TX_EVE_1_CANID__I_EVE_1_COUNTER_U8_OFFSET);
  data[0] = (message->_i_Reverse_Mode_u8 & (SIGNAL_READ_Mask2)) | ((message->_i_Sports_Mode_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_SPORTS_MODE_U8_MASK0) | ((message->_i_Eco_Mode_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_ECO_MODE_U8_MASK0) | ((message->_i_Neutral_Mode_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_NEUTRAL_MODE_U8_MASK0);
  data[1] = (message->_i_Kill_Switch_u8 & (SIGNAL_READ_Mask2)) | ((message->_i_Right_Indicator_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_RIGHT_INDICATOR_U8_MASK0) | ((message->_i_Left_Indicator_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_LEFT_INDICATOR_U8_MASK0) | ((message->_i_Safe_Mode_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_SAFE_MODE_U8_MASK0);
  data[2] = (message->_i_Motor_Fault_u8 & (SIGNAL_READ_Mask2)) | ((message->_i_Battery_Fault_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_BATTERY_FAULT_U8_MASK0) | ((message->_i_High_Beam_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_HIGH_BEAM_U8_MASK0) | ((message->_i_Low_Beam_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_LOW_BEAM_U8_MASK0);
  data[3] = (message->_i_Ignition_u8 & (SIGNAL_READ_Mask1)) | ((message->_i_Battery_Status_u8 & (SIGNAL_READ_Mask1)) << _I_HMI_TX_EVE_1__I_BATTERY_STATUS_U8_MASK0) | ((message->_i_Warning_Symbol_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_WARNING_SYMBOL_U8_MASK0) | ((message->_i_Service_Indicator_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_SERVICE_INDICATOR_U8_MASK0) | ((message->_i_Side_Stand_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_EVE_1__I_SIDE_STAND_U8_MASK0);
  data[4] = (message->_i_Hours_Time_u8 & (SIGNAL_READ_Mask8));
  data[5] = (message->_i_Minutes_Time_u8 & (SIGNAL_READ_Mask8));
  data[6] = ((message->_i_Cluster_Use_Place_u8 & (SIGNAL_READ_Mask1)) << _I_HMI_TX_EVE_1__I_CLUSTER_USE_PLACE_U8_MASK0);
  data[7] = ((message->_i_Eve_1_Counter_u8 & (SIGNAL_READ_Mask4)) << _I_HMI_TX_EVE_1__I_EVE_1_COUNTER_U8_MASK0);
  return _I_HMI_TX_EVE_1_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Per_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Per_1_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize_HMI_Tx_Per_1(HMI_Tx_Per_1_t *message, uint8_t *data)
{
  message->ODO_u32 = (message->ODO_u32 - HMI_TX_PER_1_CANID_ODO_U32_OFFSET);
  message->Vehicle_Speed_u8 = (message->Vehicle_Speed_u8 - HMI_TX_PER_1_CANID_VEHICLE_SPEED_U8_OFFSET);
  message->Mileage_u8 = (message->Mileage_u8 - HMI_TX_PER_1_CANID_MILEAGE_U8_OFFSET);
  message->Per_1_Counter_u8 = (message->Per_1_Counter_u8 - HMI_TX_PER_1_CANID_PER_1_COUNTER_U8_OFFSET);
  data[0] = ((message->ODO_u32 >> HMI_TX_PER_1_ODO_U32_MASK0) & (SIGNAL_READ_Mask8));
  data[1] = ((message->ODO_u32 >> HMI_TX_PER_1_ODO_U32_MASK1) & (SIGNAL_READ_Mask8));
  data[2] = ((message->ODO_u32 >> HMI_TX_PER_1_ODO_U32_MASK2) & (SIGNAL_READ_Mask8));
  data[3] = (message->ODO_u32 & (SIGNAL_READ_Mask8));
  data[4] = (message->Vehicle_Speed_u8 & (SIGNAL_READ_Mask8));
  data[5] = (message->Mileage_u8 & (SIGNAL_READ_Mask8));
  data[7] = ((message->Per_1_Counter_u8 & (SIGNAL_READ_Mask4)) << HMI_TX_PER_1_PER_1_COUNTER_U8_MASK0);
  return HMI_TX_PER_1_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Per_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Per_1_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize__i_HMI_Tx_Per_1(_i_HMI_Tx_Per_1_t *message, uint8_t *data)
{
  message->_i_ODO_u32 = (message->_i_ODO_u32 - _I_HMI_TX_PER_1_CANID__I_ODO_U32_OFFSET);
  message->_i_Vehicle_Speed_u8 = (message->_i_Vehicle_Speed_u8 - _I_HMI_TX_PER_1_CANID__I_VEHICLE_SPEED_U8_OFFSET);
  message->_i_Mileage_u8 = (message->_i_Mileage_u8 - _I_HMI_TX_PER_1_CANID__I_MILEAGE_U8_OFFSET);
  message->_i_Per_1_Counter_u8 = (message->_i_Per_1_Counter_u8 - _I_HMI_TX_PER_1_CANID__I_PER_1_COUNTER_U8_OFFSET);
  data[0] = ((message->_i_ODO_u32 >> _I_HMI_TX_PER_1__I_ODO_U32_MASK0) & (SIGNAL_READ_Mask8));
  data[1] = ((message->_i_ODO_u32 >> _I_HMI_TX_PER_1__I_ODO_U32_MASK1) & (SIGNAL_READ_Mask8));
  data[2] = ((message->_i_ODO_u32 >> _I_HMI_TX_PER_1__I_ODO_U32_MASK2) & (SIGNAL_READ_Mask8));
  data[3] = (message->_i_ODO_u32 & (SIGNAL_READ_Mask8));
  data[4] = (message->_i_Vehicle_Speed_u8 & (SIGNAL_READ_Mask8));
  data[5] = (message->_i_Mileage_u8 & (SIGNAL_READ_Mask8));
  data[7] = ((message->_i_Per_1_Counter_u8 & (SIGNAL_READ_Mask4)) << _I_HMI_TX_PER_1__I_PER_1_COUNTER_U8_MASK0);
  return _I_HMI_TX_PER_1_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Per_2
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Per_2_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize_HMI_Tx_Per_2(HMI_Tx_Per_2_t *message, uint8_t *data)
{
  message->Power_Consumption_u16 = (message->Power_Consumption_u16 - HMI_TX_PER_2_CANID_POWER_CONSUMPTION_U16_OFFSET);
  message->Range_Km_u16 = (message->Range_Km_u16 - HMI_TX_PER_2_CANID_RANGE_KM_U16_OFFSET);
  message->Battery_SOC_u8 = (message->Battery_SOC_u8 - HMI_TX_PER_2_CANID_BATTERY_SOC_U8_OFFSET);
  message->BLE_Icon_u8 = (message->BLE_Icon_u8 - HMI_TX_PER_2_CANID_BLE_ICON_U8_OFFSET);
  message->Regen_Braking_u8 = (message->Regen_Braking_u8 - HMI_TX_PER_2_CANID_REGEN_BRAKING_U8_OFFSET);
  message->Per_2_Counter_u8 = (message->Per_2_Counter_u8 - HMI_TX_PER_2_CANID_PER_2_COUNTER_U8_OFFSET);
  data[0] = ((message->Power_Consumption_u16 >> HMI_TX_PER_2_POWER_CONSUMPTION_U16_MASK0) & (SIGNAL_READ_Mask8));
  data[1] = (message->Power_Consumption_u16 & (SIGNAL_READ_Mask8));
  data[2] = ((message->Range_Km_u16 >> HMI_TX_PER_2_RANGE_KM_U16_MASK0) & (SIGNAL_READ_Mask8));
  data[3] = (message->Range_Km_u16 & (SIGNAL_READ_Mask8));
  data[4] = (message->Battery_SOC_u8 & (SIGNAL_READ_Mask8));
  data[5] = ((message->BLE_Icon_u8 & (SIGNAL_READ_Mask2)) << HMI_TX_PER_2_BLE_ICON_U8_MASK0) | ((message->Regen_Braking_u8 & (SIGNAL_READ_Mask2)) << HMI_TX_PER_2_REGEN_BRAKING_U8_MASK0);
  data[7] = ((message->Per_2_Counter_u8 & (SIGNAL_READ_Mask4)) << HMI_TX_PER_2_PER_2_COUNTER_U8_MASK0);
  return HMI_TX_PER_2_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize__i_HMI_Tx_Per_2
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : _i_HMI_Tx_Per_2_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize__i_HMI_Tx_Per_2(_i_HMI_Tx_Per_2_t *message, uint8_t *data)
{
  message->_i_Power_Consumption_u16 = (message->_i_Power_Consumption_u16 - _I_HMI_TX_PER_2_CANID__I_POWER_CONSUMPTION_U16_OFFSET);
  message->_i_Range_Km_u16 = (message->_i_Range_Km_u16 - _I_HMI_TX_PER_2_CANID__I_RANGE_KM_U16_OFFSET);
  message->_i_Battery_SOC_u8 = (message->_i_Battery_SOC_u8 - _I_HMI_TX_PER_2_CANID__I_BATTERY_SOC_U8_OFFSET);
  message->_i_BLE_Icon_u8 = (message->_i_BLE_Icon_u8 - _I_HMI_TX_PER_2_CANID__I_BLE_ICON_U8_OFFSET);
  message->_i_Regen_Braking_u8 = (message->_i_Regen_Braking_u8 - _I_HMI_TX_PER_2_CANID__I_REGEN_BRAKING_U8_OFFSET);
  message->_i_Per_2_Counter_u8 = (message->_i_Per_2_Counter_u8 - _I_HMI_TX_PER_2_CANID__I_PER_2_COUNTER_U8_OFFSET);
  data[0] = ((message->_i_Power_Consumption_u16 >> _I_HMI_TX_PER_2__I_POWER_CONSUMPTION_U16_MASK0) & (SIGNAL_READ_Mask8));
  data[1] = (message->_i_Power_Consumption_u16 & (SIGNAL_READ_Mask8));
  data[2] = ((message->_i_Range_Km_u16 >> _I_HMI_TX_PER_2__I_RANGE_KM_U16_MASK0) & (SIGNAL_READ_Mask8));
  data[3] = (message->_i_Range_Km_u16 & (SIGNAL_READ_Mask8));
  data[4] = (message->_i_Battery_SOC_u8 & (SIGNAL_READ_Mask8));
  data[5] = ((message->_i_BLE_Icon_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_PER_2__I_BLE_ICON_U8_MASK0) | ((message->_i_Regen_Braking_u8 & (SIGNAL_READ_Mask2)) << _I_HMI_TX_PER_2__I_REGEN_BRAKING_U8_MASK0);
  data[7] = ((message->_i_Per_2_Counter_u8 & (SIGNAL_READ_Mask4)) << _I_HMI_TX_PER_2__I_PER_2_COUNTER_U8_MASK0);
  return _I_HMI_TX_PER_2_ID;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Serialize_HMI_Tx_Eve_1
*   Description   : This function updates the signals data to the Sending CAN 
*		    data.
*   Parameters    : HMI_Tx_Eve_1_t* message, uint8_t* data 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Serialize_HMI_Tx_Eve_1(HMI_Tx_Eve_1_t *message, uint8_t *data)
{

   message->Reverse_Mode_u8 = (message->Reverse_Mode_u8  - HMI_TX_EVE_1_CANID_REVERSE_MODE_U8_OFFSET);
  message->Sports_Mode_u8 = (message->Sports_Mode_u8  - HMI_TX_EVE_1_CANID_SPORTS_MODE_U8_OFFSET);
  message->Eco_Mode_u8 = (message->Eco_Mode_u8  - HMI_TX_EVE_1_CANID_ECO_MODE_U8_OFFSET);
  message->Neutral_Mode_u8 = (message->Neutral_Mode_u8  - HMI_TX_EVE_1_CANID_NEUTRAL_MODE_U8_OFFSET);
  message->Kill_Switch_u8 = (message->Kill_Switch_u8  - HMI_TX_EVE_1_CANID_KILL_SWITCH_U8_OFFSET);
  message->Right_Indicator_u8 = (message->Right_Indicator_u8  - HMI_TX_EVE_1_CANID_RIGHT_INDICATOR_U8_OFFSET);
  message->Left_Indicator_u8 = (message->Left_Indicator_u8  - HMI_TX_EVE_1_CANID_LEFT_INDICATOR_U8_OFFSET);
  message->Safe_Mode_u8 = (message->Safe_Mode_u8  - HMI_TX_EVE_1_CANID_SAFE_MODE_U8_OFFSET);
  message->Motor_Fault_u8 = (message->Motor_Fault_u8  - HMI_TX_EVE_1_CANID_MOTOR_FAULT_U8_OFFSET);
  message->Battery_Fault_u8 = (message->Battery_Fault_u8  - HMI_TX_EVE_1_CANID_BATTERY_FAULT_U8_OFFSET);
  message->High_Beam_u8 = (message->High_Beam_u8  - HMI_TX_EVE_1_CANID_HIGH_BEAM_U8_OFFSET);
  message->Low_Beam_u8 = (message->Low_Beam_u8  - HMI_TX_EVE_1_CANID_LOW_BEAM_U8_OFFSET);
  message->Ignition_u8 = (message->Ignition_u8  - HMI_TX_EVE_1_CANID_IGNITION_U8_OFFSET);
  message->Battery_Status_u8 = (message->Battery_Status_u8  - HMI_TX_EVE_1_CANID_BATTERY_STATUS_U8_OFFSET);
  message->Warning_Symbol_u8 = (message->Warning_Symbol_u8  - HMI_TX_EVE_1_CANID_WARNING_SYMBOL_U8_OFFSET);
  message->Service_Indicator_u8 = (message->Service_Indicator_u8  - HMI_TX_EVE_1_CANID_SERVICE_INDICATOR_U8_OFFSET);
  message->Side_Stand_u8 = (message->Side_Stand_u8  - HMI_TX_EVE_1_CANID_SIDE_STAND_U8_OFFSET);
  message->Hours_Time_u8 = (message->Hours_Time_u8  - HMI_TX_EVE_1_CANID_HOURS_TIME_U8_OFFSET);
  message->Minutes_Time_u8 = (message->Minutes_Time_u8  - HMI_TX_EVE_1_CANID_MINUTES_TIME_U8_OFFSET);
  message->TimeSetting_u8 = (message->TimeSetting_u8  - HMI_TX_EVE_1_CANID_TIMESETTING_U8_OFFSET);
  message->Eve_1_Counter_u8 = (message->Eve_1_Counter_u8  - HMI_TX_EVE_1_CANID_EVE_1_COUNTER_U8_OFFSET);
  data[0] = (message->Reverse_Mode_u8 & (SIGNLE_READ_Mask2)) | ((message->Sports_Mode_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_SPORTS_MODE_U8_MASK0) | ((message->Eco_Mode_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_ECO_MODE_U8_MASK0) | ((message->Neutral_Mode_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_NEUTRAL_MODE_U8_MASK0) ;
  data[1] = (message->Kill_Switch_u8 & (SIGNLE_READ_Mask2)) | ((message->Right_Indicator_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_RIGHT_INDICATOR_U8_MASK0) | ((message->Left_Indicator_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_LEFT_INDICATOR_U8_MASK0) | ((message->Safe_Mode_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_SAFE_MODE_U8_MASK0) ;
  data[2] = (message->Motor_Fault_u8 & (SIGNLE_READ_Mask2)) | ((message->Battery_Fault_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_BATTERY_FAULT_U8_MASK0) | ((message->High_Beam_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_HIGH_BEAM_U8_MASK0) | ((message->Low_Beam_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_LOW_BEAM_U8_MASK0) ;
  data[3] = (message->Ignition_u8 & (SIGNLE_READ_Mask1)) | ((message->Battery_Status_u8 & (SIGNLE_READ_Mask1)) << HMI_TX_EVE_1_BATTERY_STATUS_U8_MASK0) | ((message->Warning_Symbol_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_WARNING_SYMBOL_U8_MASK0) | ((message->Service_Indicator_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_SERVICE_INDICATOR_U8_MASK0) | ((message->Side_Stand_u8 & (SIGNLE_READ_Mask2)) << HMI_TX_EVE_1_SIDE_STAND_U8_MASK0) ;
  data[4] = (message->Hours_Time_u8 & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->Minutes_Time_u8 & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->TimeSetting_u8 & (SIGNLE_READ_Mask4)) | ((message->Eve_1_Counter_u8 & (SIGNLE_READ_Mask4)) << HMI_TX_EVE_1_EVE_1_COUNTER_U8_MASK0) ;
   return HMI_TX_EVE_1_ID; 

}

#endif /* CLUSTER_CAN_SIGNALS_C */
/*---------------------- End of File -----------------------------------------*/
