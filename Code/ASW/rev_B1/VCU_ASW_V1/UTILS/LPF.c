#include "LPF.h"

uint16_t U16_LPF_U16(uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16)
{
	uint16_t DiffVar_u16 		= 0;
	uint16_t OffsetVal_u16  	= 0;
	
	// If the current measeured value is not equal to the input
	if(Input_u16 != *Output_pu16)
	{
		// Compute the filter output by using the formula for the first order low pass filter
        	// y[i] = y[i-1] + alpha(x[i] - y[i-1])
		if(Input_u16 > *Output_pu16)
		{
			DiffVar_u16 = (uint16_t)(Input_u16 - *Output_pu16);
			
			if(PosConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)PosConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		else
		{
			DiffVar_u16 = (uint16_t)(*Output_pu16 - Input_u16);
			
			if(NegConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)NegConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		
		// If the step value is 0 then set it to 1
		if(OffsetVal_u16 == 0)
		{
			OffsetVal_u16 = 1;
		}
		else
		{
			;
		}
		
		if(Input_u16 > *Output_pu16)
		{
			// Input curve is moving up, hence step up the result.
			*Output_pu16 += OffsetVal_u16;
		}
		else
		{
			// Input curve is moving down, hence step down the result.
			*Output_pu16 -= OffsetVal_u16;
		}
	}
	else
	{
		;
	}
	
	return (*Output_pu16);
}