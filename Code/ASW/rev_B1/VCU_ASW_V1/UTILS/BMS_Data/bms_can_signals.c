﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms_can_signals.c
|    Project        : VCU
|    Module         : BMS can signals data
|    Description    : This is the generated file from the DBC2CH Tool for BMS.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 08/07/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_CAN_SIGNALS_C
#define BMS_CAN_SIGNALS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "bms_can_signals.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
 uint32_t Deserialize_BMS_Rx_0x2AA(BMS_VCU_0x2AA_Rx_St_t* message, const uint8_t* data)
{
  message->SoC = ((data[0] & (SIGNLE_READ_Mask8))) + TPD02_CANID_SOC_OFFSET;
  message->Full_CAP = (((data[2] & (SIGNLE_READ_Mask8)) << TPD02_FULL_CAP_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + TPD02_CANID_FULL_CAP_OFFSET;
  message->SoH = ((data[7] & (SIGNLE_READ_Mask8))) + TPD02_CANID_SOH_OFFSET;
  message->Full_CAP = message->Full_CAP *0.125;
   return TPD02_ID; 
}
   

/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_Rx_0x3AA(BMS_VCU_0x3AA_Rx_St_t* message, const uint8_t* data)
{
   
  message->Battery_Current = (((data[1] & (SIGNLE_READ_Mask8)) << TPD03_BATTERY_CURRENT_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + TPD03_CANID_BATTERY_CURRENT_OFFSET;
 // message->Battery_Current*=TPD03_BATTERY_CURRENTFACTOR;
  message->Battery_Voltage = (((data[3] & (SIGNLE_READ_Mask8)) << TPD03_BATTERY_VOLTAGE_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + TPD03_CANID_BATTERY_VOLTAGE_OFFSET;
  message->Max_DCHG_Current = (((data[5] & (SIGNLE_READ_Mask8)) << TPD03_MAX_DCHG_CURRENT_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + TPD03_CANID_MAX_DCHG_CURRENT_OFFSET;
  message->SOP = ((data[6] & (SIGNLE_READ_Mask8))) + TPD03_CANID_SOP_OFFSET;
  message->Battery_State = ((data[7] & (SIGNLE_READ_Mask8))) + TPD03_CANID_BATTERY_STATE_OFFSET;

  

  return TPD03_ID; 
}


/*----------------------------------------------------------------------------*/

 uint32_t Deserialize_BMS_Rx_0x3AB(BMS_VCU_0x3AB_Rx_St_t* message, const uint8_t* data)
{
  message->BMSTemperature = ((data[0] & (SIGNLE_READ_Mask8))) + TPDO7_CANID_BMSTEMPERATURE_OFFSET;
  message->PDUTemperature = ((data[1] & (SIGNLE_READ_Mask8))) + TPDO7_CANID_PDUTEMPERATURE_OFFSET;
  if(message->PDUTemperature == 0)
  {
    message->PDUTemperature = 25;
  }
   return TPDO7_ID; 
}


/*----------------------------------------------------------------------------*/
#if (OLD_BMS_CM == TRUE)
 uint32_t Deserialize_BMS_Rx_0x4AA(BMS_VCU_0x4AA_Rx_St_t* message, const uint8_t* data)
{
  message->High_Module_Temp = (((data[7] & (SIGNLE_READ_Mask8)) << TPD04_HIGH_MODULE_TEMP_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + TPD04_CANID_HIGH_MODULE_TEMP_OFFSET;
   return TPD04_ID; 
}
 #endif
 
#if (NEW_BMS_CM == TRUE)
uint32_t Deserialize_TPDO4(TPDO4_t* message, const uint8_t* data)
{
  message->LOW_CELL_VOLT = (((data[1] & (SIGNLE_READ_Mask8)) << TPDO4_LOW_CELL_VOLT_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + TPDO4_CANID_LOW_CELL_VOLT_OFFSET;
  message->HIGH_CELL_VOLT = (((data[3] & (SIGNLE_READ_Mask8)) << TPDO4_HIGH_CELL_VOLT_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + TPDO4_CANID_HIGH_CELL_VOLT_OFFSET;
  message->LOW_CELL_TEMP = ((data[4] & (SIGNLE_READ_Mask8))) + TPDO4_CANID_LOW_CELL_TEMP_OFFSET;
  message->HIGH_CELL_TEMP = ((data[5] & (SIGNLE_READ_Mask8))) + TPDO4_CANID_HIGH_CELL_TEMP_OFFSET;
  message->HIGH_CT_INDEX = ((data[6] & (SIGNLE_READ_Mask3))) + TPDO4_CANID_HIGH_CT_INDEX_OFFSET;
  message->HIGH_CV_INDEX = (((data[6] >> TPDO4_HIGH_CV_INDEX_MASK0) & (SIGNLE_READ_Mask5))) + TPDO4_CANID_HIGH_CV_INDEX_OFFSET;
  message->LOW_CT_INDEX = ((data[7] & (SIGNLE_READ_Mask3))) + TPDO4_CANID_LOW_CT_INDEX_OFFSET;
  message->LOW_CV_INDEX = (((data[7] >> TPDO4_LOW_CV_INDEX_MASK0) & (SIGNLE_READ_Mask5))) + TPDO4_CANID_LOW_CV_INDEX_OFFSET;
   return TPDO4_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_TPDO4(TPDO4_t* message, uint8_t* data)
{
  message->LOW_CELL_VOLT = (message->LOW_CELL_VOLT  - TPDO4_CANID_LOW_CELL_VOLT_OFFSET);
  message->HIGH_CELL_VOLT = (message->HIGH_CELL_VOLT  - TPDO4_CANID_HIGH_CELL_VOLT_OFFSET);
  message->LOW_CELL_TEMP = (message->LOW_CELL_TEMP  - TPDO4_CANID_LOW_CELL_TEMP_OFFSET);
  message->HIGH_CELL_TEMP = (message->HIGH_CELL_TEMP  - TPDO4_CANID_HIGH_CELL_TEMP_OFFSET);
  message->HIGH_CT_INDEX = (message->HIGH_CT_INDEX  - TPDO4_CANID_HIGH_CT_INDEX_OFFSET);
  message->HIGH_CV_INDEX = (message->HIGH_CV_INDEX  - TPDO4_CANID_HIGH_CV_INDEX_OFFSET);
  message->LOW_CT_INDEX = (message->LOW_CT_INDEX  - TPDO4_CANID_LOW_CT_INDEX_OFFSET);
  message->LOW_CV_INDEX = (message->LOW_CV_INDEX  - TPDO4_CANID_LOW_CV_INDEX_OFFSET);
  data[0] = (message->LOW_CELL_VOLT & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->LOW_CELL_VOLT >> TPDO4_LOW_CELL_VOLT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->HIGH_CELL_VOLT & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->HIGH_CELL_VOLT >> TPDO4_HIGH_CELL_VOLT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->LOW_CELL_TEMP & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->HIGH_CELL_TEMP & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->HIGH_CT_INDEX & (SIGNLE_READ_Mask3)) | ((message->HIGH_CV_INDEX & (SIGNLE_READ_Mask5)) << TPDO4_HIGH_CV_INDEX_MASK0) ;
  data[7] = (message->LOW_CT_INDEX & (SIGNLE_READ_Mask3)) | ((message->LOW_CV_INDEX & (SIGNLE_READ_Mask5)) << TPDO4_LOW_CV_INDEX_MASK0) ;
   return TPDO4_ID; 
}
 #endif

/*----------------------------------------------------------------------------*/

 uint32_t Deserialize_BMS_Rx_0x4AB(BMS_VCU_0x4AB_Rx_St_t* message, const uint8_t* data)
{
  message->_1_Cycle_Charge_Capacity = (((data[1] & (SIGNLE_READ_Mask8)) << TPD08__1_CYCLE_CHARGE_CAPACITY_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + TPD08_CANID__1_CYCLE_CHARGE_CAPACITY_OFFSET;
  message->_2_Cycle_discharge_capacity = (((data[3] & (SIGNLE_READ_Mask8)) << TPD08__2_CYCLE_DISCHARGE_CAPACITY_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + TPD08_CANID__2_CYCLE_DISCHARGE_CAPACITY_OFFSET;
  message->_3_Available_Capacity = (((data[5] & (SIGNLE_READ_Mask8)) << TPD08__3_AVAILABLE_CAPACITY_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + TPD08_CANID__3_AVAILABLE_CAPACITY_OFFSET;
  message->_4_Available_Energy = (((data[7] & (SIGNLE_READ_Mask8)) << TPD08__4_AVAILABLE_ENERGY_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + TPD08_CANID__4_AVAILABLE_ENERGY_OFFSET;
   return TPD08_ID; 
}

/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_Rx_0x4AC(BMS_VCU_0x4AC_Rx_St_t* message, const uint8_t* data)
{
  message->_1_Equivalent_Cycle_Count = (((data[1] & (SIGNLE_READ_Mask8)) << TPD09__1_EQUIVALENT_CYCLE_COUNT_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + TPD09_CANID__1_EQUIVALENT_CYCLE_COUNT_OFFSET;
  message->_2_Lifetime_Charge_capacity = (((data[4] & (SIGNLE_READ_Mask8)) << TPD09__2_LIFETIME_CHARGE_CAPACITY_MASK0) | ((data[3] & (SIGNLE_READ_Mask8)) << TPD09__2_LIFETIME_CHARGE_CAPACITY_MASK1) | (data[2] & (SIGNLE_READ_Mask8))) + TPD09_CANID__2_LIFETIME_CHARGE_CAPACITY_OFFSET;
  message->_3_Lifetime_Discharge_capacity = (((data[7] & (SIGNLE_READ_Mask8)) << TPD09__3_LIFETIME_DISCHARGE_CAPACITY_MASK0) | ((data[6] & (SIGNLE_READ_Mask8)) << TPD09__3_LIFETIME_DISCHARGE_CAPACITY_MASK1) | (data[5] & (SIGNLE_READ_Mask8))) + TPD09_CANID__3_LIFETIME_DISCHARGE_CAPACITY_OFFSET;
   return TPD09_ID; 
}

uint32_t Deserialize_HMI_VCU_0x7EB_RX(HMI_VCU_0x7EB_RX_St_t* message, const uint8_t* data)
{
  message->Button_status = ((data[0] & (SIGNLE_READ_Mask8))) + HMI_VCU_0X7EB_RX_CANID_BUTTON_STATUS_OFFSET;
   return HMI_VCU_0X7EB_RX_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_HMI_VCU_0x7EB_RX(HMI_VCU_0x7EB_RX_St_t* message, uint8_t* data)
{
  message->Button_status = (message->Button_status  - HMI_VCU_0X7EB_RX_CANID_BUTTON_STATUS_OFFSET);
  data[0] = (message->Button_status & (SIGNLE_READ_Mask8)) ;
   return HMI_VCU_0X7EB_RX_ID; 
}

/*----------------------------------------------------------------------------*/



#endif /* BMS_CAN_SIGNALS_C */