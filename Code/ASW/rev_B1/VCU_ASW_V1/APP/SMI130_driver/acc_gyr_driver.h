/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	        : acc_gyr_driver.h
|    Project	    : SMI130 Driver
|    Module         : SMI130 Acclerometer and Gyroscope
|    Description    : This file contains the export variables and functions to
|                     which SMI130 Drivers implemented in the H file.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                               Company
| --------      ---------------------     --------------------------------------
| 23/11/2022        Mutturaj H               Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef ACC_GYR_DRIVER_H
#define ACC_GYR_DRIVER_H


/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "SPI_Driver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define Acc_resolution_1024 		    (0x03)
#define Acc_resolution_512 		        (0x05)
#define Acc_resolution_256 		        (0x08)
#define Acc_resolution_128 		        (0x0C)

#define Accelerometer_g_range_2 	    (2)
#define Accelerometer_g_range_4 	    (4)
#define Accelerometer_g_range_8 	    (8)
#define Accelerometer_g_range_16 	    (16)

#define Rate_Measurement_Range_2000     (0x00)
#define Rate_Measurement_Range_1000     (0x01)
#define Rate_Measurement_Range_500      (0x02)
#define Rate_Measurement_Range_250      (0x03)
#define Rate_Measurement_Range_125      (0x04)

#define Software_reset                  (0xB6)

#define array_2_byte                    (2)

#define acc_x_LSB                       (0x02)  
#define acc_x_MSB                       (0x03)  
#define acc_y_LSB                       (0x04)  
#define acc_y_MSB                       (0x05)  
#define acc_z_LSB                       (0x06)  
#define acc_z_MSB                       (0x07) 

#define Lower_Byte                      (0)
#define Higher_Byte                     (1)

#define Left_shift_4_bit                (4)
#define Right_shift_4_bit               (4)
#define Left_shift_8_bit                (8)
 
#define acc_g_range_2                   (2)
#define acc_g_range_4                   (4)
#define acc_g_range_8                   (8)
#define acc_g_range_16                  (16)
#define acc_resolution_3                (3)
#define acc_resolution_5                (5)
#define acc_resolution_8                (8)
#define acc_resolution_0C               (0x0C)
#define Software_reset_address          (0x14)
#define Acc_Temprature_reg              (0X08)
#define Gyr_Temprature_reg              (0X08)


/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

#pragma pack(1)
typedef struct
{
    uint16_t acc_x;
    uint16_t acc_y;
    uint16_t acc_z;
} acc_data_struct;
#pragma pack(1)
typedef struct
{
    uint16_t gyr_x;
    uint16_t gyr_y;
    uint16_t gyr_z;
} gyro_data_struct;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Sample_Function
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void Acc_gyro_proc(void);

/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Sample_Function
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void Sample_Function(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_acc_data
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : structure contains x_axis,y_axis,z_axis of accelerometer
*******************************************************************************/ 
extern acc_data_struct Read_acc_data(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Write_acc_resolution
*   Description   : This function write acclerometer resolution values.
*   Parameters    : Range to set the resolution
*   Return Value  : None
*******************************************************************************/ 
extern void Write_acc_resolution(uint8_t g_range);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_acc_resolution
*   Description   : This function read present acclerometer resolution values.
*   Parameters    : None
*   Return Value  : return resolution number 
*******************************************************************************/ 
extern uint8_t Read_acc_resolution(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : acc_SoftwareReset
*   Description   : reset the accelerometer configuration to default configursaton.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void acc_SoftwareReset(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyr_SoftwareReset
*   Description   : reset the gryoscope configuration to default configursaton.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern void gyr_SoftwareReset(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_gyr_rate_measure_range
*   Description   : Read the guroscope rate of measuring range
*   Parameters    : None
*   Return Value  : rate of measuring range
*******************************************************************************/ 
extern uint8_t Read_gyr_rate_measure_range(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : write_gyr_rate_measure_range
*   Description   : writing guroscope rate of measuring range
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
extern void write_gyr_rate_measure_range(uint8_t gyr_range);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Sample_Function
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
extern gyro_data_struct Read_Gyroscope(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : accelerometer_ini
*   Description   : initializing the accelerometer
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
extern void Accelerometer_init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyroscope_init
*   Description   : initializing the gyroscope
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
extern void Gyroscope_init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : acc_temp_read
*   Description   : Read accelerometer temperature
*   Parameters    : None
*   Return Value  : return accelerometer temperature value
*******************************************************************************/
extern uint8_t acc_temp_read(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyr_temp_read
*   Description   : Read gyroscope temperature
*   Parameters    : None
*   Return Value  : return gyroscope temperature value
*******************************************************************************/
extern uint8_t gyr_temp_read(void);

#endif /* ACC_GYR_DRIVER_H */
/*---------------------- End of File -----------------------------------------*/

