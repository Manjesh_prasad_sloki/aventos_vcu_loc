/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	        : I2C_Driver.c
|    Project	    : SMI130 Driver
|    Module         : SMI130 Acclerometer and Gyroscope
|    Description    : This file contains the variables and functions to
|                     which SMI130 Drivers implemented in the C file.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	             Name                         Company
| ----------            ---------------      -----------------------------------
| 23/11/2022              Mutturaj H          Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef I2C_DRIVER_C
#define I2C_DRIVER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_CSIH2.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "r_cg_cgc.h"
#include "acc_gyr_driver.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 



/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : I2C_acc_Write
*   Description   : This function is to read accelerometer values throuth I2C.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void I2C_acc_Write(uint8_t add, uint8_t data)
{
    //write code here
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : I2C_acc_Read
*   Description   : This function is to read accelerometer values throuth I2C.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void I2C_acc_Read(uint8_t add, uint8_t *data)
{
    //write code here
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : I2C_gyr_Write
*   Description   : This function is to write gyroscope register throuth I2C.
*   Parameters    : Register address,data to be written.
*   Return Value  : None
*******************************************************************************/ 
void I2C_gyr_Write(uint8_t add, uint8_t data)
{
    //write code here
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : I2C_gyr_Read
*   Description   : This function is to read gyroscope values throuth I2C.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void I2C_gyr_Read(uint8_t add, uint8_t *data)
{
    //write code here
}
#endif /* ACC_GYR_DRIVER_C */
/*---------------------- End of File -----------------------------------------*/
