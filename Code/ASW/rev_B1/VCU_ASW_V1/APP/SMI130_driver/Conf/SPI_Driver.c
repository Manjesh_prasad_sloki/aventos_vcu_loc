/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	        : SPI_Driver.c
|    Project	    : SMI130 Driver
|    Module         : SMI130 Acclerometer and Gyroscope
|    Description    : This file contains the variables and functions to
|                     which SMI130 Drivers implemented in the C file.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 23/11/2022       Mutturaj H         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SPI_DRIVER_C
#define SPI_DRIVER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "Config_CSIH2.h"
#include "Config_PORT.h"
//#include "csih_user.h"
#include "SPI_Driver.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 



/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_acc_Write
*   Description   : This function is to read accelerometer values throuth SPI.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void SPI_acc_Write(uint8_t add, uint8_t data)
{
    uint16_t SPI_RX_data = 0;
    uint16_t SPI_TX_data = Write_reg(add, data);
//    if( ( CSIH0.CFG0 & 0x00030000 ) != _CSIH_PHASE_SELECTION_TYPE4)
//    {
//	    R_Config_CSIH0_Stop();
//	    CSIH0.CFG0 = CSIH0.CFG0 & 0xFFFCFFFF;
//	    CSIH0.CFG0 = CSIH0.CFG0 | _CSIH_PHASE_SELECTION_TYPE4;
//	    /* Synchronization processing */
//	    g_cg_sync_read = CSIH0.CTL1;
//	    __syncp();
//	    R_Config_CSIH0_Start();
//    }
    //PORT.P0 &= ~_PORT_Pn8_OUTPUT_HIGH;
    //Recive_flag = false;
    Recive_flag_spi2 = false;
    R_Config_CSIH2_Send_Receive((uint16_t *)&SPI_TX_data, Write_data_length_1, (uint16_t *)&SPI_RX_data, _CSIH_SELECT_CHIP_0);
    while (Recive_flag_spi2 == false)
    // while (Recive_flag == false)
        ;
    Recive_flag_spi2 = false;
    //PORT.P0 |= _PORT_Pn8_OUTPUT_HIGH;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_acc_Read
*   Description   : This function is to read accelerometer values throuth SPI.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void SPI_acc_Read(uint8_t add, uint8_t *data)
{
    uint16_t SPI_RX_data = 0;
    uint16_t SPI_TX_data_f = 0X80;
    uint16_t SPI_TX_data = Read_reg(add);
    
    R_Config_CSIH2_Send_Receive((uint16_t *)&SPI_TX_data_f, Write_data_length_1, (uint16_t *)&SPI_RX_data, _CSIH_SELECT_CHIP_0);
    while (Recive_flag_spi2 == false);
    Recive_flag_spi2 = false;
//    if( ( CSIH0.CFG0 & 0x00030000 ) != _CSIH_PHASE_SELECTION_TYPE4)
//    {
//	    R_Config_CSIH0_Stop();
//	    CSIH0.CFG0 = CSIH0.CFG0 & 0xFFFCFFFF;
//	    CSIH0.CFG0 = CSIH0.CFG0 | _CSIH_PHASE_SELECTION_TYPE4;
//	    /* Synchronization processing */
//	    g_cg_sync_read = CSIH0.CTL1;
//	    __syncp();
//	    R_Config_CSIH0_Start();
//    }
    //PORT.P0 &= ~_PORT_Pn8_OUTPUT_HIGH;
    Recive_flag_spi2= false;
    // Recive_flag = 0;
    R_Config_CSIH2_Send_Receive((uint16_t *)&SPI_TX_data, Write_data_length_1, (uint16_t *)&SPI_RX_data, _CSIH_SELECT_CHIP_0);
    // while (Recive_flag == false)
    while (Recive_flag_spi2 == false)
        ;
    Recive_flag_spi2 = false;
    //PORT.P0 |= _PORT_Pn8_OUTPUT_HIGH;
    data[0] = (uint8_t)SPI_RX_data;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_gyr_Write
*   Description   : This function is to write gyroscope register throuth SPI.
*   Parameters    : Register address,data to be written.
*   Return Value  : None
*******************************************************************************/ 
void SPI_gyr_Write(uint8_t add, uint8_t data)
{
    uint16_t SPI_RX_data = 0;
    uint16_t SPI_TX_data = Write_reg(add, data);
//    if( ( CSIH0.CFG0 & 0x00030000 ) != _CSIH_PHASE_SELECTION_TYPE4)
//    {
//	    R_Config_CSIH0_Stop();
//	    CSIH0.CFG0 = CSIH0.CFG0 & 0xFFFCFFFF;
//	    CSIH0.CFG0 = CSIH0.CFG0 | _CSIH_PHASE_SELECTION_TYPE4;
//	    /* Synchronization processing */
//	    g_cg_sync_read = CSIH0.CTL1;
//	    __syncp();
//	    R_Config_CSIH0_Start();
//    }
//    PORT.P0 &= ~_PORT_Pn9_OUTPUT_HIGH;
    Recive_flag_spi2 = 0;
    R_Config_CSIH2_Send_Receive((uint16_t *)&SPI_TX_data, Write_data_length_1, (uint16_t *)&SPI_RX_data, _CSIH_SELECT_CHIP_0);
    while (Recive_flag_spi2 == false)
        ;
    Recive_flag_spi2 = false;
    //PORT.P0 |= _PORT_Pn9_OUTPUT_HIGH;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_gyr_Read
*   Description   : This function is to read gyroscope values throuth SPI.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
void SPI_gyr_Read(uint8_t add, uint8_t *data)
{
    uint16_t SPI_RX_data = 0;
    uint16_t SPI_TX_data = Read_reg(add);
//    if( ( CSIH0.CFG0 & 0x00030000 ) != _CSIH_PHASE_SELECTION_TYPE4)
//    {
//	    R_Config_CSIH0_Stop();
//	    CSIH0.CFG0 = CSIH0.CFG0 & 0xFFFCFFFF;
//	    CSIH0.CFG0 = CSIH0.CFG0 | _CSIH_PHASE_SELECTION_TYPE4;
//	    /* Synchronization processing */
//	    g_cg_sync_read = CSIH0.CTL1;
//	    __syncp();
//	    R_Config_CSIH0_Start();
//    }
//    PORT.P0 &= ~_PORT_Pn9_OUTPUT_HIGH;
    Recive_flag_spi2 = 0;
    R_Config_CSIH2_Send_Receive((uint16_t *)&SPI_TX_data, Write_data_length_1, (uint16_t *)&SPI_RX_data, _CSIH_SELECT_CHIP_0);
    while (Recive_flag_spi2 == false)
        ;
    Recive_flag_spi2 = false;
    //PORT.P0 |= _PORT_Pn9_OUTPUT_HIGH;
    data[0] = (uint8_t)SPI_RX_data;
}




#endif /* SPI_DRIVER_C */
/*---------------------- End of File -----------------------------------------*/
