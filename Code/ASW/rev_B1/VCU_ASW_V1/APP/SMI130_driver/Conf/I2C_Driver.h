/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	        : SPI_Driver.h
|    Project	    : SMI130 Driver
|    Module         : SMI130 Acclerometer and Gyroscope
|    Description    : This file contains the export variables and functions to
|                     which SMI130 Drivers implemented in the H file.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                               Company
| --------      ---------------------     --------------------------------------
| 23/11/2022        Mutturaj H               Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef I2C_DRIVER_H
#define I2C_DRIVER_H


/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define Read_reg(reg) (0x8000 | ((uint16_t)(reg << 8)))
#define Write_reg(reg, data) (((uint16_t)(reg << 8)) | ((uint16_t)(data)))
#define Write_data_length_1             (1)

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_acc_Write
*   Description   : This function is to write accelerometer register  throuth SPI.
*   Parameters    : Register address,data to be written.
*   Return Value  : None
*******************************************************************************/ 
extern void I2C_acc_Write(uint8_t add, uint8_t data);
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_acc_Read
*   Description   : This function is to read accelerometer values throuth SPI.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
extern void I2C_acc_Read(uint8_t add, uint8_t *data);
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_gyr_Write
*   Description   : This function is to write gyroscope register throuth SPI.
*   Parameters    : Register address,data to be written.
*   Return Value  : None
*******************************************************************************/ 
extern void I2C_gyr_Write(uint8_t add, uint8_t data);
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SPI_gyr_Read
*   Description   : This function is to read gyroscope values throuth SPI.
*   Parameters    : Register address ,Register address to get the values
*   Return Value  : None
*******************************************************************************/ 
extern void I2C_gyr_Read(uint8_t add, uint8_t *data);

#endif /* I2C_DRIVER_H */
/*---------------------- End of File -----------------------------------------*/

