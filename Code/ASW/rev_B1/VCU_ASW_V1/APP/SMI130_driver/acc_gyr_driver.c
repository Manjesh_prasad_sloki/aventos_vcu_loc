/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	        : acc_gyr_driver.c
|    Project	    : SMI130 Driver
|    Module         : SMI130 Acclerometer and Gyroscope
|    Description    : This file contains the variables and functions to
|                     which SMI130 Drivers implemented in the C file.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 23/11/2022       Mutturaj H         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef ACC_GYR_DRIVER_C
#define ACC_GYR_DRIVER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_CSIH2.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "r_cg_cgc.h"
#include "acc_gyr_driver.h"
#include "communicator.h"
#if(controller == 0)
#include "SPI_Driver.h"
#endif
#if(controller == 1)
#include "I2C_Driver.h"
#endif
#include "SPI_Driver.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 
//uint16_t Data_Rx[20] = {0};
uint32_t delay = 0;
uint16_t recived_data = 0;
uint8_t value=0;
acc_data_struct acc_data_st;   
gyro_data_struct gyro_data_st;
uint8_t can_data[8]={0};

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_acc_data
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : structure contains x_axis,y_axis,z_axis of accelerometer
*******************************************************************************/ 
void Acc_gyro_proc(void)
{
	static uint32_t time_caputre_variable = 0;
	static bool time_capture_flag = 0;
	if(time_capture_flag == false)
	{
		time_caputre_variable = (GET_VCU_TIME_MS() + 1000);
		time_capture_flag = true;
	}
	else if(time_caputre_variable <= GET_VCU_TIME_MS())
	{
		Read_acc_resolution();
	    acc_data_st = Read_acc_data();
        can_data[0]=(uint8_t)((acc_data_st.acc_x & 0xFF00)>>8);
	    can_data[1]=(uint8_t)((acc_data_st.acc_x & 0x00FF));
	    can_data[2]=(uint8_t)((acc_data_st.acc_y & 0xFF00)>>8);
	    can_data[3]=(uint8_t)((acc_data_st.acc_y & 0x00FF));
	    can_data[4]=(uint8_t)((acc_data_st.acc_z & 0xFF00)>>8);
	    can_data[5]=(uint8_t)((acc_data_st.acc_z & 0x00FF));
        data_acc_st.accXaxisH = (uint8_t)((acc_data_st.acc_x & 0xFF00)>>8);
        data_acc_st.accXaxisL = (uint8_t)((acc_data_st.acc_x & 0x00FF));
        data_acc_st.accYaxisH = (uint8_t)((acc_data_st.acc_y & 0xFF00)>>8);
	    data_acc_st.accYaxisL = (uint8_t)((acc_data_st.acc_y & 0x00FF));
        data_acc_st.accZaxisH = (uint8_t)((acc_data_st.acc_z & 0xFF00)>>8);
		data_acc_st.accZaxisL = (uint8_t)((acc_data_st.acc_z & 0x00FF));
        

		CAN1_Transmit(0x600,8,&can_data[0]); 
		gyro_data_st = Read_Gyroscope();
        can_data[0]=(uint8_t)((gyro_data_st.gyr_x & 0xFF00)>>8);
		can_data[1]=(uint8_t)((gyro_data_st.gyr_x & 0x00FF));
		can_data[2]=(uint8_t)((gyro_data_st.gyr_y & 0xFF00)>>8);
		can_data[3]=(uint8_t)((gyro_data_st.gyr_y & 0x00FF));
		can_data[4]=(uint8_t)((gyro_data_st.gyr_z & 0xFF00)>>8);
		can_data[5]=(uint8_t)((gyro_data_st.gyr_z & 0x00FF));

        data_gro_st.gyroXaxisH = (uint8_t)((gyro_data_st.gyr_x & 0xFF00)>>8);
        data_gro_st.gyroXaxisL = (uint8_t)((gyro_data_st.gyr_x & 0x00FF));
        data_gro_st.gyroYaxisH =  (uint8_t)((gyro_data_st.gyr_y & 0xFF00)>>8);
        data_gro_st.gyroYaxisL =  (uint8_t)((gyro_data_st.gyr_y & 0x00FF));
        data_gro_st.gyroZaxisH =  (uint8_t)((gyro_data_st.gyr_z & 0xFF00)>>8);
        data_gro_st.gyroZaxisL =  (uint8_t)((gyro_data_st.gyr_z & 0x00FF));

		CAN1_Transmit(0x601,8,&can_data[0]);
		time_capture_flag = false;
	}
	
}



/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_acc_data
*   Description   : This function is a demo function.
*   Parameters    : None
*   Return Value  : structure contains x_axis,y_axis,z_axis of accelerometer
*******************************************************************************/ 
acc_data_struct Read_acc_data(void)
{
    uint8_t x_axis_array [ array_2_byte ] = { 0 };
    uint8_t y_axis_array [ array_2_byte ] = { 0 };
    uint8_t z_axis_array [ array_2_byte ] = { 0 };
    acc_data_struct acc_data_st;
    #if(controller == 0)
    
    SPI_acc_Read(acc_x_LSB, (uint8_t *)&x_axis_array[Lower_Byte ]);
    SPI_acc_Read(acc_x_MSB, (uint8_t *)&x_axis_array[Higher_Byte]);
    SPI_acc_Read(acc_y_LSB, (uint8_t *)&y_axis_array[Lower_Byte ]);
    SPI_acc_Read(acc_y_MSB, (uint8_t *)&y_axis_array[Higher_Byte]);
    SPI_acc_Read(acc_z_LSB, (uint8_t *)&z_axis_array[Lower_Byte ]);
    SPI_acc_Read(acc_z_MSB, (uint8_t *)&z_axis_array[Higher_Byte]);
    #endif
    #if(controller == 1)
    I2C_acc_Read(acc_x_LSB, (uint8_t *)&x_axis_array[Lower_Byte ]);
    I2C_acc_Read(acc_x_MSB, (uint8_t *)&x_axis_array[Higher_Byte]);
    I2C_acc_Read(acc_y_LSB, (uint8_t *)&y_axis_array[Lower_Byte ]);
    I2C_acc_Read(acc_y_MSB, (uint8_t *)&y_axis_array[Higher_Byte]);
    I2C_acc_Read(acc_z_LSB, (uint8_t *)&z_axis_array[Lower_Byte ]);
    I2C_acc_Read(acc_z_MSB, (uint8_t *)&z_axis_array[Higher_Byte]);
    #endif
    acc_data_st.acc_x = ((((uint8_t)(x_axis_array[Higher_Byte]) << Left_shift_4_bit) | ((uint8_t)(x_axis_array[Lower_Byte]) >> Right_shift_4_bit)) & 0x0FFF);
    acc_data_st.acc_y = ((((uint8_t)(y_axis_array[Higher_Byte]) << Left_shift_4_bit) | ((uint8_t)(y_axis_array[Lower_Byte]) >> Right_shift_4_bit)) & 0x0FFF);
    acc_data_st.acc_z = ((((uint8_t)(z_axis_array[Higher_Byte]) << Left_shift_4_bit) | ((uint8_t)(z_axis_array[Lower_Byte]) >> Right_shift_4_bit)) & 0x0FFF);
    return acc_data_st;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_Gyroscope
*   Description   : This function read the gyroscope values
*   Parameters    : None
*   Return Value  : structure contains x_axis,y_axis,z_axis of gyroscope
*******************************************************************************/ 
gyro_data_struct Read_Gyroscope(void)
{
    uint8_t x_gyr_array [ array_2_byte ] = { 0 };
    uint8_t y_gyr_array [ array_2_byte ] = { 0 };
    uint8_t z_gyr_array [ array_2_byte ] = { 0 };
    gyro_data_struct gyro_data_st;
    Read_gyr_rate_measure_range();
    #if(controller == 0)
    SPI_gyr_Read(acc_x_LSB, (uint8_t *)&x_gyr_array[Lower_Byte ]);
    SPI_gyr_Read(acc_x_MSB, (uint8_t *)&x_gyr_array[Higher_Byte]);
    SPI_gyr_Read(acc_y_LSB, (uint8_t *)&y_gyr_array[Lower_Byte ]);
    SPI_gyr_Read(acc_y_MSB, (uint8_t *)&y_gyr_array[Higher_Byte]);
    SPI_gyr_Read(acc_z_LSB, (uint8_t *)&z_gyr_array[Lower_Byte ]);
    SPI_gyr_Read(acc_z_MSB, (uint8_t *)&z_gyr_array[Higher_Byte]);
    #endif
    #if(controller == 1)
    I2C_gyr_Read(acc_x_LSB, (uint8_t *)&x_gyr_array[Lower_Byte ]);
    I2C_gyr_Read(acc_x_MSB, (uint8_t *)&x_gyr_array[Higher_Byte]);
    I2C_gyr_Read(acc_y_LSB, (uint8_t *)&y_gyr_array[Lower_Byte ]);
    I2C_gyr_Read(acc_y_MSB, (uint8_t *)&y_gyr_array[Higher_Byte]);
    I2C_gyr_Read(acc_z_LSB, (uint8_t *)&z_gyr_array[Lower_Byte ]);
    I2C_gyr_Read(acc_z_MSB, (uint8_t *)&z_gyr_array[Higher_Byte]);
    #endif
    gyro_data_st.gyr_x = ((((uint8_t)(x_gyr_array[ Higher_Byte ]) << Left_shift_8_bit) + ((uint8_t)(x_gyr_array[ Lower_Byte ]))) & 0xFFFF);
    gyro_data_st.gyr_y = ((((uint8_t)(y_gyr_array[ Higher_Byte ]) << Left_shift_8_bit) + ((uint8_t)(y_gyr_array[ Lower_Byte ]))) & 0xFFFF);
    gyro_data_st.gyr_z = ((((uint8_t)(z_gyr_array[ Higher_Byte ]) << Left_shift_8_bit) + ((uint8_t)(z_gyr_array[ Lower_Byte ]))) & 0xFFFF);
    return gyro_data_st;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_acc_resolution
*   Description   : This function read present acclerometer resolution values.
*   Parameters    : None
*   Return Value  : return resolution number 
*******************************************************************************/ 
uint8_t Read_acc_resolution(void)
{
    uint8_t acc_resolution = 0;
    #if(controller == 0)
    SPI_acc_Read(0x0F, (uint8_t *)&acc_resolution);
    if ((acc_resolution & 0x0F) == acc_resolution_3 )
    {
        return acc_g_range_2 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_5 )
    {
        return acc_g_range_4 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_8 )
    {
        return acc_g_range_8 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_0C)
    {
        return acc_g_range_16;
    }
    else
    {
        return (acc_resolution & 0x0F);
    }
    #endif
    #if(controller == 1)
    SPI_acc_Read(0x0F, (uint8_t *)&acc_resolution);
    if ((acc_resolution & 0x0F) == acc_resolution_3 )
    {
        return acc_g_range_2 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_5 )
    {
        return acc_g_range_4 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_8 )
    {
        return acc_g_range_8 ;
    }
    else if ((acc_resolution & 0x0F) == acc_resolution_0C)
    {
        return acc_g_range_16;
    }
    else
    {
        return (acc_resolution & 0x0F);
    }
    #endif
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Write_acc_resolution
*   Description   : This function write acclerometer resolution values.
*   Parameters    : Range to set the resolution
*   Return Value  : None
*******************************************************************************/ 
void Write_acc_resolution(uint8_t g_range)
{
    uint8_t Write_data = 0;
    if (g_range == acc_g_range_2 )
    {
        Write_data = Acc_resolution_1024;
    }
    else if (g_range == acc_g_range_4 )
    {
        Write_data = Acc_resolution_512;
    }
    else if (g_range == acc_g_range_8 )
    {
        Write_data = Acc_resolution_256;
    }
    else if (g_range == acc_g_range_16)
    {
        Write_data = Acc_resolution_128;
    }
    else
    {
        return ;
    }
    #if(controller == 0)
    SPI_acc_Write(0x0F, Write_data);
    #endif
    #if(controller == 1)
    I2C_acc_Write(0x0F, Write_data);
    #endif
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : acc_SoftwareReset
*   Description   : reset the accelerometer configuration to default configursaton.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void acc_SoftwareReset(void)
{
    
    #if(controller == 0)
    SPI_acc_Write(Software_reset_address, Software_reset);
    #endif
    #if(controller == 1)
    I2C_acc_Write(Software_reset_address, Software_reset);
    #endif
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyr_SoftwareReset
*   Description   : reset the gryoscope configuration to default configursaton.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void gyr_SoftwareReset(void)
{
    #if(controller == 0)
    SPI_gyr_Write(Software_reset_address, Software_reset);
    #endif
    #if(controller == 1)
    I2C_gyr_Write(Software_reset_address, Software_reset);
    #endif
    
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_gyr_rate_measure_range
*   Description   : Read the guroscope rate of measuring range
*   Parameters    : None
*   Return Value  : rate of measuring range
*******************************************************************************/ 
uint8_t Read_gyr_rate_measure_range(void)
{
    uint8_t x_gyr_array=0;
    #if(controller == 0)
    SPI_gyr_Read(0x0f, (uint8_t *)&x_gyr_array);
    #endif
    #if(controller == 1)
    I2C_gyr_Read(0x0f, (uint8_t *)&x_gyr_array);
    #endif
    
    return x_gyr_array;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : write_gyr_rate_measure_range
*   Description   : writing guroscope rate of measuring range
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
void write_gyr_rate_measure_range(uint8_t gyr_range)
{
   // uint8_t x_gyr_array=0;
    #if(controller == 0)
    SPI_gyr_Write(0x0f, gyr_range);
    #endif
    #if(controller == 1)
    I2C_gyr_Write(0x0f, gyr_range);
    #endif
    
    
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : accelerometer_ini
*   Description   : initializing the accelerometer
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
void Accelerometer_init(void)
{
    //PORT.P9 |= _PORT_Pn0_OUTPUT_HIGH;
    acc_SoftwareReset();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyroscope_init
*   Description   : initializing the accelerometer
*   Parameters    : Rate of measuring range
*   Return Value  : None
*******************************************************************************/ 
void Gyroscope_init(void)
{
    //PORT.P0 |= _PORT_Pn9_OUTPUT_HIGH;
    gyr_SoftwareReset();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : acc_temp_read
*   Description   : Read accelerometer temperature
*   Parameters    : None
*   Return Value  : return accelerometer temperature value
*******************************************************************************/ 
uint8_t acc_temp_read(void)
{
    uint8_t current_acc_temp_value=0;

    #if(controller == 0)
    SPI_acc_Read(Acc_Temprature_reg, (uint8_t *)&current_acc_temp_value);
    #endif
    #if(controller == 1)
    I2C_acc_Read(Acc_Temprature_reg, (uint8_t *)&current_acc_temp_value);
    #endif

    return current_acc_temp_value;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : gyr_temp_read
*   Description   : Read gyroscope temperature
*   Parameters    : None
*   Return Value  : return gyroscope temperature value
*******************************************************************************/ 
uint8_t gyr_temp_read(void)
{
    uint8_t current_gyr_temp_value=0;
//    static uint8_t i=0;
   // uint8_t one[8] = {0};
   // uint8_t two[8] = {0};
   // uint8_t carry = 1;
    value = 0;
    #if(controller == 0)
    SPI_gyr_Read(Acc_Temprature_reg, (uint8_t *) & current_gyr_temp_value);
    #endif
    #if(controller == 1)
    I2C_gyr_Read(Acc_Temprature_reg, (uint8_t *) & current_gyr_temp_value);
    #endif
    // current_gyr_temp_value = 0x5;
    // for(i = 0; i < 8; i++)
    // {
    //     if((current_gyr_temp_value & (1<<i)) == 0){
    //         one[i] = 1;
    //     }
    //     else if((current_gyr_temp_value & (1<<i)) == 1)
    //     {
    //         one[i] = 0;
    //     }
    // }
    // //for(i=0;i<8;i++)
    // //{
    // //   printf("%d",one[i]);
    // //}
    // //printf("\n");
    // for(i = 0; i <8 ; i++){
    //     if(one[i] == 0 && carry ==0 ){
    //         carry =0;
    //         two[i] =0;
    //     }
    //     else if(one[i] == 0 && carry ==1 ){
    //         carry =0;
    //         two[i] =1;
    //     }
    //     else if(one[i] == 1 && carry ==0 ){
    //         carry =0;
    //         two[i] =1;
    //     }
    //     else if(one[i] == 1 && carry ==1 ){
    //         carry =1;
    //         two[i] = 0;
    //     }
    //     else{
    //         NOP();
    //     }
    // }
    // for(i=8;i=0;i--)
    // {
    //     //printf("%d",two[i]);
    //     if(two[i-1] == 1){
    //         value = ( value | (1 << (i-1)));
    //     }
    // }
    // //printf("\n%x",value);
    // return value;
    return current_gyr_temp_value;
}
#endif /* ACC_GYR_DRIVER_C */
/*---------------------- End of File -----------------------------------------*/
