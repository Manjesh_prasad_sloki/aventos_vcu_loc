
/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : motor_control.c
|    Project        : VCU
|    Module         : motor control
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the motor control functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 29/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MOTOR_CONTROL_C
#define MOTOR_CONTROL_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "motor_control.h"
#include "Communicator.h"
#include "cil_can_conf.h"
//#include "temp.h"
#include "ODOmeterCal.h"
#include "powerConsum.h"
#include "LPF.h"
#include "DataBank.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/

/******************************************************************************/

/* -----------------------------------------------------------------------------
*  MOTOR CONTROL TX SET GET DATA 
*  ---------------------------------------------------------------------------*/


// #define Set_Reserved_for_0x300_LTVS_u64(x) (Vehicle_0x300_Tx.Reserved_for_LTVS_u64 = x)

// #define Get_Reserved_for_0x300_LTVS_u64() (Vehicle_0x300_Tx.Reserved_for_LTVS_u64)

#define Set_Broadcast_Mode(x) (VCU_MCU_0x400_Tx.Broadcast_Mode = x)
#define Set_Broadcast_rate(x) (VCU_MCU_0x400_Tx.Broadcas_Rate = x)
#define Set_Speed_Limit(x)    (VCU_MCU_0x100_Tx.Motor_Speed_Limit =x)
#define Set_Max_Regeneration(a) (VCU_MCU_0x100_Tx.Max_Regeneration = a)
#define Set_Ride_mode_Request(x) (VCU_MCU_0x100_Tx.Ride_mode_Request = x)

#define Get_Motor_speed()		(MCU_VCU_0x150_Rx.Motor_Speed)
#define Get_Ride_mode_actual	(MCU_VCU_0x150_Rx.Ride_Mode_Actual)
#define Get_Motor_temp()			(MCU_0x250_MotorTemp)
#define Get_Controller_temp()		(MCU_0x250_ControllerTemp)
#define Get_MRN()                    (MCU_0x250_MRN)

 #define MC_150_BIT    (0)
 #define MC_250_BIT    (1)
 #define MCU_COMM_BITS  (0x03)

 #define  MAX_MOTOR_TEMP (145) // in celicius
 #define MOTOR_TEMP_EXIT (135) // in celicius
 
 #define  MAX_CONTROLLER_TEMP (85) // in celicius
 #define  CONTROLLER_TEMP_EXIT (75) // in celicius exit from the safe mode
 
// #define Get_Broadcast_Mode() (Vehicle_0x400_Tx.Broadcast_Mode_u8)
#define Get_Broadcast_rate() (VCU_MCU_0x400_Tx.Broadcas_Rate)


#define LPF_POS_CONST   1000
#define LPF_NEG_CONST   1000

 #define SET_MCU_COMM_BIT(X)    (MCU_Comm_u8 |= (1<<X) )
 #define CLEAR_MCU_COMM_BIT(X)  (MCU_Comm_u8 &= (~(1<<X)) )
 #define GET_MCU_COMM_BVAL()	(MCU_Comm_u8)


/******************************************************************************/

// #define NEUTRAL 0U
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static MotorControl_RunTime_St_t MotorControl_RunTime_St;
static float MCU_0x250_MotorTemp;
static int16_t MCU_0x250_ControllerTemp;
static uint32_t MCU_0x250_MRN;

uint8_t  data_0x711[8];
uint8_t  data_0x712[8];
uint8_t  data_0x719[8];
uint8_t  data_0x720[8];


static VCU_MCU_0x100_Tx_t VCU_MCU_0x100_Tx;
static VCU_MCU_0x400_Tx_t VCU_MCU_0x400_Tx;

static MCU_0x200_t MCU_VCU_0x200_Rx;
static MCU_VCU_0x150_Rx_t MCU_VCU_0x150_Rx;
static MCU_VCU_0x250_Rx_t MCU_VCU_0x250_Rx;


static bool Msg_200_rx_flag = 0;
static bool Msg_250_rx_flag = 0;
static bool Msg_100_tx_flag = 0;

static bool Send_Once_b = false;
static float MCU_BatteryCurrent = 0;
static float MCU_BatteryVoltage = 0;
static int16_t MCU_Motor_Speed_RPM = 0;

static uint16_t Vehicle_Speed_Kmph_Cal = 0;
static uint32_t Counter = 0;
static uint16_t Speed_to_display = 0;
static uint32_t ODO_in_meters = 0;


static void CheckMcuComm(void);
static void CheckMcuTemperature(void);
static void ComputeODO(void);
static void ComputeWhkm(void);

uint8_t MCU_Comm_u8 = 0;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_StateMachine_proc
*   Description   : This function implements MotorControl State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void MotorControl_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Start_Signal
*   Description   : This function implements MotorControl Start Signal 
*		    and stop operation.
*   Parameters    : MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En
*   Return Value  : None
*******************************************************************************/
static void MotorControl_Start_Signal(MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_Start_Signal
*   Description   : This function implements get Start Signal 
*		    and stop data.
*   Parameters    : None
*   Return Value  : MotorControl_Kill_Signal_En_t
*******************************************************************************/
static MotorControl_Kill_Signal_En_t MotorControl_Get_Start_Signal(void);

//static void check_vechile_charging(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Init
*   Description   : This function implements MotorControl initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_Init(void)
{
		
	MotorControl_RunTime_St.MotorControl_State_En = MOTOR_CONTROL_STANDBY_STATE_E;
	MotorControl_RunTime_St.MotorControl_Event_En = MOTOR_CONTROL_STANDBY_EVENT_E;
	MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
	MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
	MotorControl_RunTime_St.MotorControl_Start_b = false;
	//MotorControl_RunTime_St.Regen_state_b = true;
	Set_Regen_State(THREE);
	Set_Broadcast_rate(ZERO);
	Send_Once_b = false;
	MCU_BatteryCurrent = 0;
	MCU_BatteryVoltage = 0;
	MCU_Motor_Speed_RPM = 0;
	MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
	CLEAR_MC_COMM_STATUS();
	/*Add other module Initialisation*/
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Proc
*   Description   : This function implements MotorControl Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_Proc(void)
{
	
	Counter++;
	MotorControl_StateMachine_proc();
	if ((MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_START_STATE_E) ||
		(MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_RUN_STATE_E) ||
		(MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_SAFE_MODE_STATE_E))
	{
		if (0 == (Counter % 1))
		{
			static float PrevMotorRPM_f32 = 0;

			if(PrevMotorRPM_f32 != MCU_Motor_Speed_RPM)
			{
				Vehicle_Speed_Kmph_Cal = CalculateVehicleSpeed((int16_t)MCU_Motor_Speed_RPM);
				PrevMotorRPM_f32 = MCU_Motor_Speed_RPM;
			}
			//Calculate_DistanceTravelled(Get_MRN());
			Speed_to_display = Vehicle_Speed_Kmph_Cal ;//VehicleSpeedFilter(Vehicle_Speed_Kmph_Cal);
			//Vehicle_Throttle_u8 = (uint8_t)U16_LPF_U16((uint16_t)UserInputSig_St.Throttle_u8,&Filter_Throttle_Out_u16,LPF_POS_CONST,LPF_NEG_CONST); // todo Manjesh
		}
	}
	else if (MotorControl_RunTime_St.MotorControl_State_En == MOTOR_CONTROL_SHUT_DOWN_STATE_E)
	{
		//ResetTheSpeedFilterData();
	}
	else
	{
		/* code */
	}

	
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_StateMachine_proc
*   Description   : This function implements MotorControl State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_StateMachine_proc(void)
{
	MotorControl_State_En_t MotorControl_State_En = (MotorControl_State_En_t)GET_VCU_STATE();

	MotorControl_RunTime_St.MotorControl_State_En = MotorControl_State_En;

	 if(MCU_COMM_BITS == GET_MCU_COMM_BVAL())
	 {
		SET_MC_COMM_STATUS();	
		NOP();
	 }
	 else
	 {
	 	CLEAR_MC_COMM_STATUS();
	 }
	//SET_MILAGE_WH_KM(Calculate_Wh_Km());
	switch (MotorControl_RunTime_St.MotorControl_State_En)
	{
	case MOTOR_CONTROL_INIT_STATE_E:
	{
		MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
		MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		MotorControl_RunTime_St.MotorControl_Start_b = true;
		Set_Speed_Limit(22937); // todo Manjesh
		//MotorControl_RunTime_St.Regen_state_b = true;
		Set_Regen_State(THREE);
		SET_REGEN_STATUS(255);
		Set_Max_Regeneration(255);
		VCU_MCU_0x100_Tx.Ride_mode_Request = NEUTRAL;
		Set_Broadcast_rate(13);
		Set_Broadcast_Mode(1);
		Send_Once_b = false;
		Restore_ODO_From_FEE();
		break;
	}
	case MOTOR_CONTROL_START_STATE_E:
	{
		CheckMcuTemperature();
		CheckMcuComm();
		ComputeWhkm();
		break;
	}
	case MOTOR_CONTROL_RUN_STATE_E:
	{
		CheckMcuComm();
		ComputeODO();
		ComputeWhkm();
		CheckMcuTemperature();

		if (MOTOR_CONTROL_KILL_ON_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_OFF_E);
		}
		break;
	}
	case MOTOR_CONTROL_SHUT_DOWN_STATE_E:
	{
		if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		}
		MotorControl_RunTime_St.MotorControl_Start_b = false;
		Store_ODO_To_FEE();
		ResetTheODOEstimationData();
		break;
	}
	case MOTOR_CONTROL_SAFE_MODE_STATE_E:
	{
		CheckMcuComm();
		CheckMcuTemperature();
		ComputeWhkm();
		ComputeODO();
		MotorControl_RunTime_St.Regen_state_b = false;
		break;
	}
	case MOTOR_CONTROL_STANDBY_STATE_E:
	{
		MotorControl_RunTime_St.MotorControl_Start_b = false;
		CLEAR_MC_COMM_STATUS();
		if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Get_Start_Signal())
		{
			MotorControl_Start_Signal(MOTOR_CONTROL_KILL_ON_E);
		}
		break;
	}

	default:
	{
		break;
	}
	}
	VCU_MCU_0x100_Tx.Motor_Speed_Limit = Get_Motor_Speed_Limit();
	VCU_MCU_0x100_Tx.Ride_mode_Request = Get_Ride_mode_Request();
//	Motor_0x150_SetRx = Motor_0x150_Rx;  /* TODO: Jeevan*/
}

void CheckMcuTemperature(void)
{
	if ((false == MotorControl_RunTime_St.MCU_SafeMode_MCU_temp_b) ||
			(false == MotorControl_RunTime_St.MCU_SafeMode_Motor_temp_b))
		{
			if (Get_Motor_temp() > MAX_MOTOR_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MOTOR_TEM_E, VEHICLE_SAFE_MODE_ON_E); 
				MotorControl_RunTime_St.MCU_SafeMode_Motor_temp_b = true;
			}
			else
			{
				/* code */
			}
			if (Get_Controller_temp() > MAX_CONTROLLER_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MOTOR_TEM_E, VEHICLE_SAFE_MODE_ON_E); 
				MotorControl_RunTime_St.MCU_SafeMode_MCU_temp_b = false; 
			}
			else
			{
				/* code */
			}
		}
		else
		{
			if((true == MotorControl_RunTime_St.MCU_SafeMode_MCU_temp_b  )  ||(true == MotorControl_RunTime_St.MCU_SafeMode_Motor_temp_b) )
			{
				if(Get_Motor_temp() < MOTOR_TEMP_EXIT ) 
				{
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_MOTOR_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
					MotorControl_RunTime_St.MCU_SafeMode_Motor_temp_b = false;	
				}
				if (Get_Controller_temp() < CONTROLLER_TEMP_EXIT)
				{
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_MOTOR_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
					MotorControl_RunTime_St.MCU_SafeMode_MCU_temp_b = false;
				}
			}
			/* code */
		}
	return ;
}

void CheckMcuComm(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_InitCommCheck_b)
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
			}
			else if ((false == GET_MC_COMM_STATUS() &&
					 (MC_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - MotorControl_RunTime_St.Timer_u32))))
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MC_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				Vehicle_Speed_Kmph_Cal = 0;
			}
			else
			{
				/* code */
			}
		}
		else if (true == MotorControl_RunTime_St.MotorControl_LateCommCheck_b)
		{
			MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			if(true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
			}
		}
		else
		{
			if (true == GET_MC_COMM_STATUS())
			{
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = false;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
			}
			else
			{
				MotorControl_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				MotorControl_RunTime_St.MotorControl_InitCommCheck_b = true;
				MotorControl_RunTime_St.MotorControl_LateCommCheck_b = false;
			}
		}
	return;
}

void ComputeODO(void)
{
	ODO_in_meters = Estimate_ODO(MCU_0x250_MRN);
	return;
}

float netEnergy ;
uint32_t ooodo;
float avaaaaenergy;
static uint32_t pre_odo;
float init_avaaaaenergy;

void ComputeWhkm(void)
{
	uint8_t milage;
	avaaaaenergy = Available_Energy_1;
	init_avaaaaenergy = Available_Energy_Init_1;
	ooodo = Get_ODO_meters();
	//flaot qqq=0;
	if((Get_ODO_meters()- pre_odo  >= 100))
	{	
		
		
		if((Available_Energy_Init_1 - Available_Energy_1)!=0)
		{
			netEnergy = ((Available_Energy_Init_1 - Available_Energy_1)/ (Get_ODO_meters()/1000.00));
			pre_odo = Get_ODO_meters();
		}
		else
		{
			NOP();
		}
	}
	if(netEnergy !=0)
	{
		milage =  (netEnergy );
	}
	SET_MILAGE_WH_KM(milage);
	return;
}





/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Speed_Display
*   Description   : This function returns the present speed to be display on  
					the cluster.
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Speed_Display(void)
{
	return Speed_to_display;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Vehicle_Speed_kmph
*   Description   : This function returns the vehicle speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Vehicle_Speed_kmph(void)
{
	return Vehicle_Speed_Kmph_Cal;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_ODO_meters
*   Description   : This function returns the vehicle ODO in meters
*   Parameters    : None
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Get_ODO_meters(void)
{
	return ODO_in_meters;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Regen_State
*   Description   : This function returns the Regenerative Breaking Status
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool Get_Regen_State(void)
{
	return MotorControl_RunTime_St.Regen_state_b;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : set_Regen_State
*   Description   : This function returns the Regenerative Breaking Status
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
void Set_Regen_State(uint8_t setvalue)
{
	MotorControl_RunTime_St.Regen_state_b = setvalue;
	//return MotorControl_RunTime_St.Regen_state_b;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Actual_MC_Ride_Mode
*   Description   : This function Gets Actaul Ride mode from Motor.
*   Parameters    : None, 
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t Get_Actual_MC_Ride_Mode(void)
{
	return MCU_VCU_0x150_Rx.Ride_Mode_Actual;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x100_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x100_TxMsgCallback(void)
{
	if ((true == MotorControl_RunTime_St.MotorControl_Start_b) && (false == VechileCharging_b  ))
	{
 		uint8_t data[EIGHT] = {ZERO};
		uint8_t i = ZERO;
		CAN_MessageFrame_St_t Can_Applidata_St;
		VCU_MCU_0x100_Tx.Max_Regeneration = REGEN_STATUS();
		VCU_MCU_0x100_Tx.Motor_Speed_Limit = MOTOR_SPEED_LIMIT; // todo Manjesh (the value is hard codded should check if the value has to be changed)
		Msg_100_tx_flag = true;
		Serialize_VCU_MCU_0x100_Tx(&VCU_MCU_0x100_Tx, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
			data_0x720[i] = data[i];
		}
		Can_Applidata_St.DataLength_u8 = EIGHT;
		Can_Applidata_St.MessageType_u8 = STD_E;
		CIL_CAN_Tx_AckMsg(CIL_MC_0x100_TX_E, Can_Applidata_St);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x300_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x300_TxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		//uint8_t data[EIGHT] = {ZERO};
		uint8_t i = ZERO;
		//CAN_MessageFrame_St_t Can_Applidata_St;
		//Serialize_Vehicle_0x300_Tx(&Vehicle_0x300_Tx, &data[ZERO]);
		for (i = ZERO; i < EIGHT; i++)
		{
			//Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
		//Can_Applidata_St.DataLength_u8 = EIGHT;
		//Can_Applidata_St.MessageType_u8 = STD_E;
//		CIL_CAN_Tx_AckMsg(CIL_MC_0x300_TX_E, Can_Applidata_St);
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x711_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t* MotorControl_0x711_TxMsgCallback(void)
{
		return (uint8_t*)&data_0x711[0];
	
}




/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x712_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t* MotorControl_0x712_TxMsgCallback(void)
{
	
		
		
		if(Msg_200_rx_flag == true)
		{
			Msg_200_rx_flag = false;
		
			// for (i = ZERO; i < EIGHT; i++)
			// {
			// 	Can_Applidata_St.DataBytes_au8[i] = data_0x712[i];
			// }
			// Can_Applidata_St.DataLength_u8 = EIGHT;
			// Can_Applidata_St.MessageType_u8 = STD_E;
			//CIL_CAN_Tx_AckMsg(CIL_MC_0x711_TX_E, Can_Applidata_St);
		}
		return &data_0x712[0];
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x712_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t* MotorControl_0x719_TxMsgCallback(void)
{
	
		
		if(Msg_250_rx_flag == true)
		{
			Msg_250_rx_flag = false;
			// Serialize_MCU_0x250(&MCU_VCU_0x719_Tx, &data[ZERO]);
			// for (i = ZERO; i < EIGHT; i++)
			// {
			// 	Can_Applidata_St.DataBytes_au8[i] = data[i];
			// }
			// Can_Applidata_St.DataLength_u8 = EIGHT;
			// Can_Applidata_St.MessageType_u8 = STD_E;
			//CIL_CAN_Tx_AckMsg(CIL_MC_0x719_TX_E, Can_Applidata_St);
		}
		
	return data_0x719;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x711_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t* MotorControl_0x720_TxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if(Msg_100_tx_flag == true)
		{
			Msg_100_tx_flag = false;
		}
		return data_0x720;
	}
	return 0;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x400_TxMsgCallback
*   Description   : This function Send Motor controller data over CAN.
*   Parameters    : None  
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x400_TxMsgCallback(void)
{
	if ((true == MotorControl_RunTime_St.MotorControl_Start_b) && (false == VechileCharging_b))
	{
		if ((ZERO != Get_Broadcast_rate()) || (true == Send_Once_b))
		{
			uint8_t data[EIGHT] = {ZERO};
			uint8_t i = ZERO;
			CAN_MessageFrame_St_t Can_Applidata_St;
			Send_Once_b = false;
			Serialize_VCU_MCU_0x400_Tx(&VCU_MCU_0x400_Tx, &data[ZERO]);
			for (i = ZERO; i < EIGHT; i++)
			{
				Can_Applidata_St.DataBytes_au8[i] = data[i];
			}
			Can_Applidata_St.DataLength_u8 = EIGHT;
			Can_Applidata_St.MessageType_u8 = STD_E;
			CIL_CAN_Tx_AckMsg(CIL_MC_0x400_TX_E, Can_Applidata_St);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/

void MotorControl_0x150_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	uint8_t index =0;
	
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x150_RX_E == CIL_SigName_En)
		{
			SET_MCU_COMM_BIT(MC_150_BIT) ; 
			for(index = 0; index <8 ;index++)
			{
				data_0x711[index] = Can_Applidata_St->DataBytes_au8[index];
			}
			Deserialize_MCU_VCU_0x150_Rx(&MCU_VCU_0x150_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
			MCU_Motor_Speed_RPM =(int16_t) (MCU_VCU_0x150_Rx.Motor_Speed );	
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x200_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	uint8_t index = 0;
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		Msg_200_rx_flag = true;
	
		if (CIL_MC_0x200_RX_E == CIL_SigName_En)
		{
			for(index = 0; index <8 ;index++)
			{
				data_0x712[index] = Can_Applidata_St->DataBytes_au8[index];
			}
			Deserialize_MCU_0x200(&MCU_VCU_0x200_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
			
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x250_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	uint8_t index ;
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		if (CIL_MC_0x250_RX_E == CIL_SigName_En)
		{
			Msg_250_rx_flag = true;
			SET_MCU_COMM_BIT(MC_250_BIT);
			for(index = 0; index <8 ;index++)
			{
				data_0x719[index] = Can_Applidata_St->DataBytes_au8[index];
			}
			
			Deserialize_MCU_VCU_0x250_Rx(&MCU_VCU_0x250_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
			//MCU_VCU_0x719_Tx  = MCU_VCU_0x250_Rx ;
			MCU_0x250_MotorTemp = MCU_VCU_0x250_Rx.Motor_Temp * 0.390625  ;
			MCU_0x250_ControllerTemp = MCU_VCU_0x250_Rx.Controller_Temp * 0.292969; 
			MCU_0x250_MRN = MCU_VCU_0x250_Rx.Motor_Rotation_Number;
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x650_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
//		if (CIL_MC_0x650_RX_E == CIL_SigName_En)
		{
			//SET_MC_COMM_STATUS();
			//Deserialize_Motor_0x650_Rx(&Motor_0x650_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_RxMsgCallback
*   Description   : This function Receive Motor controller data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x750_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
//		if (CIL_MC_0x750_RX_E == CIL_SigName_En)
		{
			//SET_MC_COMM_STATUS();
			//Deserialize_Motor_0x750_Rx(&Motor_0x750_Rx, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x150_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x150_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MCU_COMM_BIT(MC_150_BIT);
		
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x200_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x200_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		//CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x250_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x250_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		//CLEAR_MCU_COMM_BIT(MC_250_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x650_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x650_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		CLEAR_MC_COMM_STATUS();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_0x750_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void MotorControl_0x750_TimeOut_RxMsgCallback(void)
{
	if (true == MotorControl_RunTime_St.MotorControl_Start_b)
	{
		//CLEAR_MC_COMM_BIT();
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Start_Signal
*   Description   : This function implements MotorControl Start Signal 
*		    		and stop operation.
*   Parameters    : MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En
*   Return Value  : None
*******************************************************************************/
void MotorControl_Start_Signal(MotorControl_Kill_Signal_En_t MotorControl_Kill_Signal_En)
{
	if (MOTOR_CONTROL_KILL_ON_E == MotorControl_Kill_Signal_En)
	{
		//PORT.P9&=0xFFFE;
		if (ONE == PORT.P9 | 0x01U)
		{
			PORT.P9 &= 0xFFFE;
		}
	}
	else if (MOTOR_CONTROL_KILL_OFF_E == MotorControl_Kill_Signal_En)
	{
		if (ZERO == PORT.P9 | 0x01U)
		{
			PORT.P9 |= 0x01U;
		}
	}
	else
	{
		/* code */
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_Start_Signal
*   Description   : This function implements get Start Signal 
*		    and stop data.
*   Parameters    : None
*   Return Value  : MotorControl_Kill_Signal_En_t
*******************************************************************************/
MotorControl_Kill_Signal_En_t MotorControl_Get_Start_Signal(void)
{

	if (PORT.P9 &= 0xFFFE)
	{
		return MOTOR_CONTROL_KILL_ON_E;
	}
	else
	{
		return MOTOR_CONTROL_KILL_OFF_E;
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryVoltage
*   Description   : This function returns the Battery Volatge value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
float MotorControl_Get_BatteryVoltage(void)
{
	return MCU_BatteryVoltage;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : MotorControl_Get_BatteryCurrent
*   Description   : This function returns the Battery Current value recevied
*		            the MCU over CAN
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/
float MotorControl_Get_BatteryCurrent(void)
{
	return MCU_BatteryCurrent;
}


uint32_t Get_Motor_MRN(void)
{
	return Get_MRN();
}
int16_t Get_Motor_Temp(void)
{
 return Get_Motor_temp();
 
}

int16_t Get_Controller_Temp()
{
	return Get_Controller_temp();
}

int16_t Get_Motor_Speed()
{
	return Get_Motor_speed();
}
#endif /* MOTOR_CONTROL_C */
/*---------------------- End of File -----------------------------------------*/