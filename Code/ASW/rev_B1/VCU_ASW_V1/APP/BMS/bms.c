/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms.c
|    Project        : VCU
|    Module         : bms module 
|    Description    : This file contains the variables and functions                    
|                     to initialize and Operate the bms functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 20/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_C
#define BMS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "bms.h"
#include "bms_can_signals.h"
#include "Communicator.h"
#include "cil_can_conf.h"
#include "RangeMileage.h"
#include "DataBank.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/
#ifdef TESTING_SOC_LOWERLT
#define Get_SOC()  		(10)
#else
#define Get_SOC()  		(SOC_u8)
#endif
#define Get_SoH()		(SOH_u8)

#define GET_BATTERY_VOLTAGE()	   (BMS_voltage)
#define GET_BATTERY_CURRENT()	   (BMS_Current)
#define GET_AVAILABLE_CAPACITY()  (AvailableCapacity)
#define Get_AVAILABLE_ENERGY()    (Available_Energy)

#define  GET_PDU_TEMP()  (25)  // BMS_VCU_0x3AB_Rx_St.BMSTemperature
#define  GET_BMS_TEMP()   (25) // BMS_VCU_0x3AB_Rx_St.PDUTemperature

#define PDU_temp()      (25)
#define BMS_temp()		(25) 

#if (OLD_BMS_CM == TRUE)
#define Get_high_module_temp()	(BMS_VCU_0x4AA_Rx_St.High_Module_Temp )
 #endif
 #if (NEW_BMS_CM == TRUE)
 #ifdef TESTING_BMS_trg1
 #define Get_high_cell_temp() (High_Cell_temp1_testing)//(High_Cell_temp)
 #else
    #define Get_high_cell_temp() (High_Cell_temp)
 #endif
 #endif


 #define BMS_4AA_BIT          (0) 
 #define BMS_4AB_BIT          (1)
 #define BMS_3AA_BIT          (2)
 #define BMS_3AB_BIT          (3)
 #define BMS_2AA_BIT          (4)
 #define BMS_4AC_BIT          (5)
 #define BMS_COMM_BITS        (0x04)

#define BATTERY_MAX_TEMP   (50)
 #define BATTERY_MAX_TEMP_UPPER     (50) // in celicius
 #define BATTERY_MAX_TEMP_LOWER      (48)

 #define PDU_MAX_TEMP         (65) // in celicius

 #define BMS_MAX_TEMP         (50)// in celicius
 #define BATTERY_MAX_TEMP_EXT  (45)
 #define PDU_MAX_TEMP_EXT      (60)
#define  BMS_MAX_TEMP_EXT      (50)


 #define SET_BMS_COMM_BIT(X)   (BMS_Comm_u8 |= (1<<X) )
 #define CLEAR_BMS_COMM_BIT(X)  (BMS_Comm_u8 &= (~(1<<X)) )
 #define GET_BMS_COMM_BVAL()	(BMS_Comm_u8)

uint8_t  SOC_u8 = 0;
uint8_t SOH_u8 = 0;
uint8_t BMS_Comm_u8 =  0; 


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static Bms_RunTime_St_t Bms_RunTime_St;


static BMS_VCU_0x2AA_Rx_St_t BMS_VCU_0x2AA_Rx_St;
static BMS_VCU_0x3AA_Rx_St_t BMS_VCU_0x3AA_Rx_St;
static BMS_VCU_0x3AB_Rx_St_t BMS_VCU_0x3AB_Rx_St;
 uint8_t safe_Temp();
static void CheckBmsComm(void);
static void CheckBmsTemp(void);
static void checkBmsCharging(void);
static void	ComputeMilage(void);
static void CheckBmsRegen(void);
//static void CheckBmsSoc(void); 
#if (OLD_BMS_CM == TRUE)
static BMS_VCU_0x4AA_Rx_St_t BMS_VCU_0x4AA_Rx_St;
#endif

#if (NEW_BMS_CM == TRUE)
static TPDO4_t BMS_VCU_0x4AA_Rx_St;
 #endif
static BMS_VCU_0x4AB_Rx_St_t BMS_VCU_0x4AB_Rx_St;
static BMS_VCU_0x4AC_Rx_St_t BMS_VCU_0x4AC_Rx_St;
static HMI_VCU_0x7EB_RX_St_t HMI_VCU_0x7EB_RX_St;

static float BMS_BatteryVoltage = 0;
static float AvailableCapacity = 0;
static float BMS_voltage =0;
static float BMS_Current = 0;
static float High_Cell_temp = 0;
 #ifdef TESTING_BMS_trg1
static float High_Cell_temp1_testing = 0; 
#endif
float AvailableEnergy=0;
static uint16_t Power_Consum_Bars_u16 = 0;
// static EstimatedRangeMileage_St_t EstimatedRangeMileage_St;
uint32_t count=1,safetemp=1,temp_bat=0;
uint32_t bat_temp=45,safe=0;

uint32_t countbms=0;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void BMS_StateMachine_Proc(void);
static void computePowerConsumtion(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Init
*   Description   : This function implements BMS initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Init(void)
{
	Bms_RunTime_St.Bms_State_En = BMS_STANDBY_STATE_E;
	Bms_RunTime_St.Bms_Event_En = BMS_STOP_EVENT_E;
	Bms_RunTime_St.BMS_InitCommCheck_b = false;
	Bms_RunTime_St.BMS_LateCommCheck_b = false;
	Bms_RunTime_St.BMS_Charging_b = false;
	Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = false;
	Bms_RunTime_St.Timer_u32 = 0;
	Bms_RunTime_St.Bms_Start_b = false;
	SOC_u8 = ZERO;
	SOH_u8 = ZERO;
	CLEAR_BMS_COMM_STATUS();

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Proc
*   Description   : This function implements BMS Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Proc(void)
{
	BMS_StateMachine_Proc();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_StateMachine_Proc(void)
{

	Bms_State_En_t Bms_State_En = (Bms_State_En_t)GET_VCU_STATE();
	Bms_RunTime_St.Bms_State_En = Bms_State_En;
	

	 if( BMS_COMM_BITS == GET_BMS_COMM_BVAL())
	 {
		SET_BMS_COMM_STATUS();
		NOP();
	 }
	 else
	 {
	 	CLEAR_BMS_COMM_STATUS();
		NOP();
	 }

	switch (Bms_RunTime_St.Bms_State_En)
	{
	case BMS_INIT_STATE_E:
	{
		Bms_RunTime_St.BMS_InitCommCheck_b = true;
		Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		Bms_RunTime_St.Bms_State_En = BMS_START_STATE_E;
		Bms_RunTime_St.Bms_Start_b = true;
		Restore_Wh_Km_from_FEE();
		Compute_Wh_On_VehicleStart(AvailableCapacity, BMS_BatteryVoltage);
		break;
	}
	case BMS_START_STATE_E:
	{
		//CheckBmsSoc();
		CheckBmsComm();
		CheckBmsTemp();
		checkBmsCharging();
		ComputeMilage();
		CheckBmsRegen();
		computePowerConsumtion();
		break;
	}
	case BMS_RUN_STATE_E:
	{
		//CheckBmsSoc();    ////
		CheckBmsComm();
		CheckBmsTemp();
		ComputeMilage();
		checkBmsCharging();
		CheckBmsRegen();
		computePowerConsumtion();
		NOP();
		break;
	}
	case BMS_SHUT_DOWN_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		Store_Wh_Km_To_FEE();
		break;
	}
	case BMS_SAFE_MODE_STATE_E:
	{
		CheckBmsComm();
		CheckBmsTemp();
		checkBmsCharging();
		ComputeMilage();
		CheckBmsRegen();
		computePowerConsumtion();
		NOP();
		break;
	}
	case BMS_STANDBY_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		CLEAR_BMS_COMM_STATUS();
		break;
	}

	default:
	{
		break;
	}
	}

	/*Add other module Initialisation*/
}





void CheckBmsRegen(void)
{
	static uint8_t RegenPrevState_u8 = TURN_OFF;
	static  bool timecapture_b ;
	static  uint32_t timer_u32;
	if ((NEUTRAL == Get_Actual_Ride_Mode()) && (false == TimeSetting_ActualValue_St.timeSetFalg))
	{
		if (RegenPrevState_u8 != UserInputSig_St.ReGen_SwitchState_u8)
		{
			RegenPrevState_u8 = UserInputSig_St.ReGen_SwitchState_u8;
			if (TURNED_ON_E == UserInputSig_St.ReGen_SwitchState_u8)
			{
				if (Get_Regen_State() == THREE)
				{
					Set_Regen_State(false);
					SET_REGEN_STATUS(0);
				}
				else if (Get_Regen_State() == false)
				{
					Set_Regen_State(THREE);
					SET_REGEN_STATUS(255);
				}
			}
		
		}
	}
	if(BMS_Current > 0)
	{
		Bms_RunTime_St.BMS_Regen_b = true;
	}
	if((Get_Regen_State() == THREE) && (true == Bms_RunTime_St.BMS_Regen_b ) && (false == timecapture_b))
	{
		timer_u32 = GET_VCU_TIME_MS() + 2000;
		timecapture_b = true;
		Bms_RunTime_St.BMS_Regen_b = false;
	}
	else if(true == timecapture_b )
	{
		Set_Regen_State(ONE);
		SET_REGEN_STATUS(255);
		if(timer_u32 < GET_VCU_TIME_MS())
		{
			Set_Regen_State(THREE);
			timecapture_b = false;
			//SET_REGEN_STATUS(0);
		}
	}
	return;
}


void CheckBmsComm(void)
{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				
			}
			else if (false == GET_BMS_COMM_STATUS() &&
					 (BMS_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
          			Bms_RunTime_St.BMS_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
		return;
}

float test_bms_temp;
void CheckBmsTemp(void)
{
#ifdef 	TESTING_BMS_trg1
	static uint8_t temp_var_testing = false; 
	if((Get_Vehicle_Speed() >= 25) || (temp_var_testing == true))
	{
		High_Cell_temp1_testing = 0;
		temp_var_testing = false;
	}
#endif
test_bms_temp = Get_high_cell_temp();
	if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
	{
		if ((Get_high_cell_temp() >= BATTERY_MAX_TEMP_LOWER) && (UserInputSig_St.BmsTempLowerTri_u8 == false) && (Get_high_cell_temp() <= BATTERY_MAX_TEMP_UPPER))
		{
			UserInputSig_St.BmsTempLowerTri_u8 = true;
			NOP();
		}
		else if(eeprom_data_Tx.temp_flag == true)
		{
			UserInputSig_St.BmsTempLowerTri_u8 = true;
		}
 		if ((Get_high_cell_temp() >= BATTERY_MAX_TEMP_UPPER) || (BMS_temp()>= BMS_MAX_TEMP) || (PDU_temp()>= PDU_MAX_TEMP)||(eeprom_data_Tx.temp_flag == 1))
		{
			eeprom_data_Tx.temp_flag = true;
			FDL_write(0x00000,sizeof(eeprom_data_Tx),(uint8_t*)&eeprom_data_Tx);
			SafeMode_SetEvent(VEHICLE_SAFE_MODE_BATT_TEMP_E, VEHICLE_SAFE_MODE_ON_E); 
			Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true; 
			UserInputSig_St.BmsTempLowerTri_u8 = false;
		
		}
		else
		{
			;/* code */
		}
		
	}
	// else if(true == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
	// {
	// 	if ((Get_high_cell_temp() <= BATTERY_MAX_TEMP_LOWER))
	// 	{
	// 		UserInputSig_St.BmsTempLowerTri_u8 = false;
			
	// 	}
	// 	if ((Get_high_cell_temp() < BATTERY_MAX_TEMP_EXT) && (BMS_temp() < BMS_MAX_TEMP_EXT) && (PDU_temp() < PDU_MAX_TEMP_EXT)/* && (safetemp==0)*/)
	// 		{
	// 			SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
	// 			Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;
	// 			//safetemp = 1;
	// 		}
	// 		else
	// 		{
	// 			/* code */
	// 		}
	// }
		if(eeprom_data_Tx.temp_flag == true)
		{
			if (Get_high_cell_temp() < BATTERY_MAX_TEMP_EXT)
			{
				eeprom_data_Tx.temp_flag = false;
				UserInputSig_St.BmsTempLowerTri_u8 = false;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BATT_TEMP_E,VEHICLE_SAFE_MODE_OFF_E);
				//SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
				//SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
				//SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
				FDL_write(0x00000,sizeof(eeprom_data_Tx),(uint8_t*)&eeprom_data_Tx);
			}
		}
}

void checkBmsCharging(void)
{
	static uint8_t timecaptureflag = false;
	static uint32_t timerValue = 0;
	if(1 >= Get_Vehicle_Speed())
	{
		if(BMS_Current > 0)
		{
			if(false == timecaptureflag )
			{
				timecaptureflag = true;
				timerValue = GET_VCU_TIME_MS();
			}
			else if((GET_VCU_TIME_MS() - timerValue) > 10000)
			{
 				Bms_RunTime_St.BMS_Charging_b = true;
			}
		}
		else 
		{
			Bms_RunTime_St.BMS_Charging_b = false;
			timecaptureflag = false;
		}	
	}
	else
	{
		Bms_RunTime_St.BMS_Charging_b = false;
		timecaptureflag = false;
	}
	return;
}


/*
void  CheckBmsSoc(void)
{
	if(true == Bms_RunTime_St.BMSCommunStart_b)
	{
        if (Get_SOC() <= 20)
		{
			//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_ON_E);
			;//NOP();
		}
		else if (Get_SOC()> 20)
		{
			//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_OFF_E);
			;//NOP();
		}
	}
		return;
}
*/


void ComputeMilage(void)
{
		float  BMSsoc;
		BMSsoc = Get_BMS_SOC();
		if(true == CalculateRangeMileage_b)
		{
			CalculateRangeMileage_b = false;
			BMS_BatteryVoltage = GET_BATTERY_VOLTAGE();	
			if(SPORT == Get_Actual_Ride_Mode())
			{
				NOP();
				EstimatedRangeMileage_St.Esti_Range_Km_u16 = (1627.92 *(BMSsoc/100))/MASTER_WH_KM_SPORT; //((38*50.4*0.85*(BMSsoc/100)))/25;
				NOP();
			}
			else
			{
				EstimatedRangeMileage_St.Esti_Range_Km_u16 =  (1627.92 *(BMSsoc/100))/MASTER_WH_KM_ECO;
				NOP();
			}
			
			
		}
		else
		{
			//if(true == EstimatedRangeMileage_St.Esti_Range_Km_b)
			{
				if(SPORT == Get_Actual_Ride_Mode())
				{
					EstimatedRangeMileage_St.Esti_Range_Km_b = false;
					BMS_VCU_0x4AB_Rx_St.data_received_b = false;
					EstimatedRangeMileage_St.Esti_Range_Km_u16 = (1627.92 *(BMSsoc/100))/MASTER_WH_KM_SPORT; 
					NOP();
				}
				else
				{
					EstimatedRangeMileage_St.Esti_Range_Km_u16 =  (1627.92 *(BMSsoc/100))/MASTER_WH_KM_ECO;
				}
			}
			/* code */
		}
 return;
}


uint8_t temp_test = 0;
void computePowerConsumtion(void)
{
	float power_consumption = BMS_Current; 
	int16_t current = (int16_t)BMS_Current;
	float temp_factor= 0;
	if(power_consumption <0)
	{
		power_consumption *= -1;
		current *=-1;
	}
	if(power_consumption != 0)
	{
		Power_Consum_Bars_u16 = power_consumption/5;
		power_consumption = power_consumption/5;
		temp_factor = power_consumption -  Power_Consum_Bars_u16;
		if(temp_factor >= 0.5)
		{
			Power_Consum_Bars_u16 +=1;
		}
	}
	else 
	{
		Power_Consum_Bars_u16 = 0;
	}
	
	return;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Power_consum_bar
*   Description   : This function returns the power_consumption bars speed in kmph
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Power_consum_bar(void)
{
	return Power_Consum_Bars_u16;
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x2AA_RX_E == CIL_SigName_En)
		{
 			//SET_BMS_COMM_BIT(BMS_2AA_BIT);
			Bms_RunTime_St.BMSCommunStart_b = true;
			Deserialize_BMS_Rx_0x2AA(&BMS_VCU_0x2AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			SOC_u8 = BMS_VCU_0x2AA_Rx_St.SoC;
			SOH_u8  = BMS_VCU_0x2AA_Rx_St.SoH;
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AA_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_4AA_BIT);
			#if (OLD_BMS_CM == TRUE)
			Deserialize_BMS_Rx_0x4AA(&BMS_VCU_0x4AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);

			 #endif
			#if (NEW_BMS_CM == TRUE)
			Deserialize_TPDO4(&BMS_VCU_0x4AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			High_Cell_temp = BMS_VCU_0x4AA_Rx_St.HIGH_CELL_TEMP ;
			
			#endif
		}
	}
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AC_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AC_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AC_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_4AC_BIT);
			Deserialize_BMS_Rx_0x4AC(&BMS_VCU_0x4AC_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
	
}
void BMS_0x7EB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	 
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x7EB_RX_E == CIL_SigName_En)
		{
			Deserialize_HMI_VCU_0x7EB_RX(&HMI_VCU_0x7EB_RX_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			hmi_switch_u8  = HMI_VCU_0x7EB_RX_St.Button_status;

		}
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	static uint8_t init_flag = false;
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AB_RX_E == CIL_SigName_En)
		{
			
			Deserialize_BMS_Rx_0x4AB(&BMS_VCU_0x4AB_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			BMS_VCU_0x4AB_Rx_St.data_received_b = true;
			Available_Energy_1 = (float)BMS_VCU_0x4AB_Rx_St._4_Available_Energy/8 ;
			AvailableCapacity = (float)BMS_VCU_0x4AB_Rx_St._3_Available_Capacity /8;  /* TODO: Jeevan*/
			if(false == init_flag)
			{
				init_flag = TRUE;
				Available_Energy_Init_1 = (float)BMS_VCU_0x4AB_Rx_St._4_Available_Energy/8 ;
			}
			NOP();
			
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_RxMsgCallback
*   Description    : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x3AA_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_BIT(BMS_3AA_BIT);
			Deserialize_BMS_Rx_0x3AA(&BMS_VCU_0x3AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
 			BMS_Current = (float)BMS_VCU_0x3AA_Rx_St.Battery_Current/16;
			BMS_voltage =(float) BMS_VCU_0x3AA_Rx_St.Battery_Voltage/1024;
		}
	}

}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x3AB_RX_E == CIL_SigName_En)
		{
			
			Deserialize_BMS_Rx_0x3AB(&BMS_VCU_0x3AB_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			
		}
	}

}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		//CLEAR_BMS_COMM_BIT(BMS_2AA_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		//CLEAR_BMS_COMM_BIT(BMS_4AA_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		//CLEAR_BMS_COMM_BIT(BMS_4AB_BIT);
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AC_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AC_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
	//CLEAR_BMS_COMM_BIT(BMS_4AC_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_TimeOut_RxMsgCallback(void)
{
	//uint8_t static time_out_counter = 0;
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		
		CLEAR_BMS_COMM_BIT(BMS_3AA_BIT);
		
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AB_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		//CLEAR_BMS_COMM_BIT(BMS_3AB_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BMS_SOC
*   Description   : This function will Returns the Present Battery SOC.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t Get_BMS_SOC(void)
{
	return Get_SOC();
	
}

uint8_t Get_BMS_SOH(void)
{
	return Get_SoH();
	
}

float Get_BMS_AvaE (void)
{
	return Available_Energy_1;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Charging_state
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool BMS_Get_Charging_state(void)
{
	return Bms_RunTime_St.BMS_Charging_b;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Voltage
*   Description   : This function provides the voltage.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

float BMS_Get_Voltage(void)
{
	return GET_BATTERY_VOLTAGE();
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Current
*   Description   : This function provides the Current.
*   Parameters    : None
*   Return Value  : int16_t
*******************************************************************************/

float BMS_Get_Current(void)
{
	
	return GET_BATTERY_CURRENT();  //testing
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Availability_Capacity
*   Description   : This function provides the Battery Availability Capacity.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

int16_t BMS_Get_Availability_Capacity(void)
{
 return GET_AVAILABLE_CAPACITY();
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Availabe_Energy
*   Description   : This function provides the Battery Availabe Energy.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

 float BMS_Get_Availabe_Energy(void)
{
	
	//return Available_Energy;
	return 89;
}



/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Mileage_Wh_Km
*   Description   : This function returns the Estimated Mileage(Wh/Km)
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t BMS_Get_Mileage_Wh_Km(void)
{
	return EstimatedRangeMileage_St.Esti_Mileage_Wh_Km_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Range_Km
*   Description   : This function returns the Estimated Range(Km)
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t BMS_Get_Range_Km(void)
{
	if(0 == EstimatedRangeMileage_St.Esti_Range_Km_u16)
	{
		return  MASTER_WH_KM_ECO;
	}
	else
	{
		return EstimatedRangeMileage_St.Esti_Range_Km_u16;
	}
	
}

float Get_PDU_Temperature(void)
{
	return GET_PDU_TEMP();
}
float Get_BMS_Temperature(void)
{
	return GET_BMS_TEMP();
}

float Get_BMS_high_cell_Temp(void)
{
	return Get_high_cell_temp();
}


 uint8_t safe_Temp()
 {
			if(temp_bat == 0)
			{
			temp_bat = (GET_VCU_TIME_MS() + 1000);
			}
			else if(temp_bat >= GET_VCU_TIME_MS())
			{
				temp_bat=0;
				
				count++;
				CAN1_Transmit(0x333, 2, count);
			}
			if(count == 10)
			{
				return 1;
			}
			return 0;
 }

#endif /* BMS_C */
/*---------------------- End of File -----------------------------------------*/
