/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : sdcard.h
|    Project        : VCU
|    Module         : sdcard
|    Description    :       
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 29/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SD_CARD_H
#define SD_CARD_H
/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h" 
#include "fatfs.h"
#include "fatfs_sd.h"
#include "string.h"
#include "Config_CSIH0.h"


#define NUM_INT_DATA    3
#define NUM_FLOAT_DATA  2
#define END_SPACE       1000 
typedef struct 
{
    FIL        file_pointer;
    FRESULT    Fresult;
    FATFS fs;
    uint16_t br;
    char_t buffer_ua8[200];
    uint8_t buffer_temp_ua8[50];
    uint16_t length_u16;
    uint16_t length_temp_u16;
    
}Sd_opertion_St_t;

typedef struct
{
   char_t folderName[20];
   uint16_t priority; 
}Folder_Info_St_t;

typedef struct
{
  char_t folder[20];
  uint32_t index;
}demo_folder;

typedef enum
{
	ABSENT=0,
	PRESENT,
}sdCard_pre;

typedef enum
{
    Jan_En = 1,
    Feb_En,
    Mar_En,
    Apr_En,
    May_En,
    Jun_En,
    Jul_En,
    Aug_En,
    Sep_En,
    Oct_En,
    Nov_En,
    Dec_En,
}month_of_year_ET;

typedef enum
{
	zero=0,
	one,
	two,
	three,
	four,
	five,
	six,
	seven,
	eight,
	nine,
	ten,
}Folder_index;

typedef struct 
{
    uint32_t data_ua8[10];
    //float    float_data_fa32[10];
}
SdCard_Data_temp_st_t ;

/************************************************************
GLOBAL VARIABLE
*************************************************************/
extern SdCard_Data_temp_st_t SdCard_Data_temp_st;
extern Sd_opertion_St_t Sd_opertion_St;
void Sd_Card_Init(void);
uint8_t* File_open(int8_t *File_Name);
//void File_open(uint8_t *File_name);
void File_write(char_t *data ,uint_t length );
void SD_Card_proc(void);

void write_sd_card(char_t seconds,char_t minutes,char_t hours,uint16_t CAN_ID, char_t *data );
//void File_close(void);
void SD_card_write(void);


#endif /* SD_CARD_H */
/*---------------------- End of File -----------------------------------------*/