 
/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : sdcard.c
|    Project        : VCU
|    Module         : sdcard
|    Description    : 
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 29/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SD_CARD_C
#define SD_CARD_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "sdcard.h"
#include "r_cg_macrodriver.h"
#include "Communicator.h"
#include"rtc_iic.h"

 /******************************************************************************/

static void Int_to_string(uint16_t data_au8 , uint8_t flag);
static void clearbuffer(uint8_t*,uint8_t );

char_t  string_reverse_data[8];


char_t* integer_string_8(char_t data);
char_t* integer_string_16(uint16_t data);
void get_ascii_time(char_t seconds,char_t minutes,char_t hours,char_t *string);
char_t* can_data_string(char_t *can_data);
uint8_t remainder_maping(uint8_t data);
void SdcardFile_naming(void);
void Folder_create(void);
void year_append(void);
char_t* FolderCounter(void);
void check_sdcard(void);
void check_SD_card_size(void);
uint32_t size_of_SD_card();
uint32_t SD_Card_present();
char_t* Store_sub_folder(char_t *);
void delete_sub_folder(char_t *);
void delete_parent_folder(char_t *);
void sd_card_full_overwrite();



Sd_opertion_St_t Sd_opertion_St;
SdCard_Data_temp_st_t SdCard_Data_temp_st;
FRESULT fresult2,fresult1,res;
FIL fil5;
FATFS fs1;
uint_t br1 =0 ;
uint8_t *File_return;
DWORD fre_clust ;
char_t  filename[30];

//////////////////////////////////////////////////////////////////////
FATFS fs; // File system object
FRESULT res;
FILINFO fileInfo;
FIL file;
FRESULT result;
DIR dir;
DWORD freeSpace, totalSpace;
uint32_t  free_space;
uint32_t  total_space;
uint32_t sdCard_avilable_space;
FILINFO fno;
////////////////////////////////////////////////////////////////////////

void SD_Card_proc(void)
{
	f_mount(NULL, "", 0);
	Sd_Card_Init();	
}


void Sd_Card_Init(void)
{
	MX_FATFS_Init();


			
	//fresult2 = f_mount(&fs1, "", 0);
	//SdcardFile_naming();
	//res = f_open(&fil5, filename,  FA_OPEN_APPEND | FA_WRITE);
	if(SD_Card_present())
	//if(SD_Card_Present_b== true)
	{
	        check_SD_card_size();
		fresult2 = f_mount(&fs1, "", 0);
		//check_SD_card_size();	
		SdcardFile_naming();
		res = f_open(&fil5, filename,  FA_OPEN_APPEND | FA_WRITE);
	}
        
}

/***********************************************************************************************************************
* Function Name: SD_Card_present
* Description  : This function notice,in VCU SD card present or not 
* Arguments    : None
* Return Value : uint32_t 
***********************************************************************************************************************/

uint32_t SD_Card_present()
{
FATFS fs; 
FRESULT result;
DWORD state;

    
    result = f_mount(&fs, "0:", 1);
    if (result != FR_OK) 
    {
        return ABSENT;
	//NOP();
    }

   
     state = disk_status(0);
    if (state & STA_NOINIT)
    {
        return ABSENT;
	//NOP();
    }
    if (state & STA_NODISK) 
    {
        return ABSENT;
	//NOP();
    }

       return PRESENT;
}

/***********************************************************************************************************************
* Function Name: check_SD_card_size
* Description  : This function is get the available storage in the SD card 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void check_SD_card_size()
{
	
	sdCard_avilable_space = size_of_SD_card();
	

 	if(sdCard_avilable_space <= END_SPACE)
	{
		sd_card_full_overwrite();
	}
}

/***********************************************************************************************************************
* Function Name: size_of_SD_card
* Description  : find the size of SD card 
* Arguments    : uint8_t *
* Return Value : None
***********************************************************************************************************************/
uint32_t size_of_SD_card()
{
	res = f_mount(&fs, "", 1);
	if (res == FR_OK) 
	{
 	 	res = f_getfree("", &freeSpace, &fs);
	}
	if (res == FR_OK)
	{	  
	        	total_space =  (fs.n_fatent - 2) * fs.csize / 2;
		  free_space = freeSpace * fs.csize / 2;
		  
	}
	return free_space;
}

/***********************************************************************************************************************
* Function Name: sd_card_full_overwrite
* Description  : when sd card available space is below limit space 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void sd_card_full_overwrite()
{
	char_t* m_folder;
	char_t parent_folder_sd[100];

	m_folder = FolderCounter();
	strcpy(parent_folder_sd,"/");
	strcat(parent_folder_sd,m_folder);


	delete_parent_folder(parent_folder_sd);
	check_SD_card_size();
}

/***********************************************************************************************************************
* Function Name: FolderCounter
* Description  : this for store the month folders
* Arguments    : None
* Return Value : uint8_t* folderInfo[one].folderName
***********************************************************************************************************************/

demo_folder DemoFolder[12]={{"Jan",1},{"Feb",2},{"Mar",3},{"Apr",4},{"May",5},{"Jun",6},{"Jul",7},{"Aug",8},{"Sep",9},{"Oct",10},{"Nov",11},{"Dec",12}};
char_t last_2_char[three] ;

char_t* FolderCounter(void)
{
//int folder_count = 0;
FRESULT res1;
uint32_t monthIndex= 0;
uint32_t storeMonthIndex= 0;
DIR dir;
FILINFO fno;
uint32_t length;
Folder_Info_St_t folderInfo[200];
res1 = f_mount(&fs, "", 1);
res1=f_opendir(&dir, "");
    if (res1 != FR_OK) 
    {
      	NOP();   
    }

    while (f_readdir(&dir, &fno) == FR_OK && fno.fname[0] != 0) 
    {
        if (fno.fattrib & AM_DIR)                     // f_attrib is AM_DIR if the entry is a directory
        {
           if (strcmp(fno.fname, ".") != 0 && strcmp(fno.fname, "..") != 0)   // Ignore "." and ".." directories
      	   {
                for(monthIndex=0; monthIndex<12; monthIndex++)
                {
			    if((strncmp(fno.fname,DemoFolder[monthIndex].folder,3) == 0 ) && (fno.fname[three]=='_') )
			        {
			      
			      	length=strlen(fno.fname);
				    strcpy(last_2_char,fno.fname + length - 2 );
				    last_2_char[two]='\0';
							
			            if(((last_2_char[zero] >= '0') && (last_2_char[zero] <='9')) &&  ((last_2_char[one] >='0' )&& (last_2_char[one] <='9')) && (length == 6))
			            {
				    	strcpy(folderInfo[storeMonthIndex].folderName,fno.fname ); 
				    	storeMonthIndex++;
				    }

			        }
	       }
            }						
					
        }
    } 
f_closedir(&dir);
return (char_t*)(folderInfo[one].folderName);
}

/***********************************************************************************************************************
* Function Name: Store_sub_folder
* Description  : this for stores the date folders
* Arguments    : uint8_t* parent
* Return Value : uint8_t* parent_folder_path
***********************************************************************************************************************/
  char_t parent_folder_path[100];
 char_t* Store_sub_folder(char_t *parent)
{
    uint32_t dateindex=0;
   //Sub_Folder subfolder[31];
    char_t parent_folder[100];
    char_t dup_parent_folder_path[100];
    FILINFO fno;
    DIR dir;
    FRESULT res;
	
    strcpy(parent_folder,parent);
    strcat(parent_folder_path,"/");
    strcat(parent_folder_path,parent_folder);
    strcpy(dup_parent_folder_path,parent_folder_path);
  
 

    res = f_opendir(&dir, parent_folder_path);
    if (res != FR_OK) 
    {        
        f_mount(NULL, "", 0);
    }

    while (1) 
    {
        res = f_readdir(&dir, &fno);
        if (res != FR_OK || fno.fname[0] == 0)
	{
		break;
	} 

        if (fno.fattrib & AM_DIR)
        {
            if (strcmp(fno.fname, ".") != 0 && strcmp(fno.fname, "..") != 0) 
	    {
			strcat(parent_folder_path,"/");
			strcat(parent_folder_path,fno.fname);
		
                dateindex++;
		break;
         }
		 
        }
	
    }
    f_closedir(&dir);
 return parent_folder_path;
}

/***********************************************************************************************************************
* Function Name: delete_sub_folder
* Description  : delete the sub folder when sd card is full 
* Arguments    : uint8_t *
* Return Value : None
***********************************************************************************************************************/
void delete_sub_folder(char_t* folderName)
{
FATFS fs;
DIR dir;
FILINFO fileInfo;
FRESULT res;
char_t path[100] = "\0";
char_t path_copy[100] = "\0";
//char pathRoot[100];
strcpy(path,folderName);

res = f_mount(&fs, "", 0);       // Mount the file system
if (res != FR_OK) 
{
        NOP();                  // Handle error 
}

res = f_opendir(&dir, path);    // Open the folder
if (res != FR_OK) 
{  
        NOP();   // Handle error
}

while(f_readdir(&dir, &fileInfo)== FR_OK && fileInfo.fname != 0)                 // Read the folder contents
{
        strcpy(path_copy,path);
        strcat(path_copy,"/");		
        strcat(path_copy,fileInfo.fname);
        res=f_unlink(path_copy);

        if(res !=  FR_OK )
        {
                break;//   NOP();
        }
}


f_closedir(&dir);     // Close the folder

res = f_unlink(path);

f_mount(NULL, "", 0);  // Unmount the file system

return;
}

/***********************************************************************************************************************
* Function Name: delete_parent_folder
* Description  : delete the parent folder when sd card is full 
* Arguments    : uint8_t *
* Return Value : None
***********************************************************************************************************************/
void delete_parent_folder(char_t *parent_folder)
{
DIR dir;
FILINFO fileInfo;
uint32_t folderEmpty;
FATFS fs;
char_t parent_folder_path[20];
strcpy(parent_folder_path, parent_folder);
folderEmpty = 1;
f_mount(&fs, "", 1);
f_opendir(&dir, parent_folder_path);
while ((f_readdir(&dir, &fileInfo)) == FR_OK)
{
		if (fileInfo.fname[0] == 0)
		{
				break;
		}
		if (strcmp(fileInfo.fname, ".") == 0 || strcmp(fileInfo.fname, "..") == 0)
		{
				continue;
		}
		folderEmpty = 0;
		break;
}
f_closedir(&dir);
if (folderEmpty == 1)
{
	 f_unlink(parent_folder_path);
}
return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

void File_write(char_t *data ,uint_t  length )
{	
	Sd_opertion_St.br  =0;
	br1 =0 ; 
	Sd_opertion_St.Fresult =   f_write(&fil5, data, length, &br1);
	
}



char_t check_var[100];
uint8_t sec ;
uint8_t min;
uint8_t hou;
uint16_t cid;
uint8_t canData[8];
uint8_t len = 0;
 char_t  time[100] = {1,2,3,4,5,6,7,8};
void write_sd_card(char_t seconds,char_t minutes,char_t hours,uint16_t CAN_ID, char_t *data )
{
	
    
	char string[100];
	static uint8_t sync_counter = 0;
	sec = seconds;
	min = minutes;
	hou = hours; 
	cid = CAN_ID;
	data[0] = 0xc5;
	data[1]= 0xd7;
	data[2] =0xc6;
	canData[0] = data[0];
	canData[1] = data[1];
	canData[2] = data[2];
	canData[3] = data[3];
	canData[4] = data[4];
	canData[5] = data[5];
	canData[6] = data[6];
	canData[7] = data[7];
	
	if(false == SD_Card_Present_b)
	{
		return;
	}
	if(CAN_ID == 0x709)
	{
		sync_counter++;
	}
	
	get_ascii_time(seconds, minutes, hours,string);
	strcpy(&time[0],string);
	integer_string_16(CAN_ID);
	strncpy(&time[9],&string_reverse_data[0],3);
	time[12] = ',';
	strncpy(&time[13],can_data_string(&data[0]),23);
	time[37] = '\r';
	time[38] = '\n';
	len = strlen(time);
	if(len != 36)
	{
		NOP();
	}
	strncpy(check_var,time,40);
	check_var[37] = '\r';
	check_var[38] = '\n';
	check_sdcard();
	File_write(&time[0],38);
	if(sync_counter >= 10)
	{
		f_sync(&fil5);
		sync_counter = 0;
	}
}

void check_sdcard(void)
{
	
 	if(check_var[2]  ==  ':')
	{

	}
	else
	{
		NOP();
	}
	if(check_var[5]  ==  ':')
	{

	}
	else
	{
 		NOP();
	}
	if(check_var[8]  ==  ',')
	{

	}
	else
	{
		NOP();
	}
	if(check_var[12] == ',')
	{

	}
	else
	{
		NOP();
	}
	if(check_var[37] == '\r')
	{

	}
	else
	{
		NOP();
	}
	if(check_var[38] == '\n')
	{

	}
	else
	{
		NOP();
	}

	return;
}



void get_ascii_time(char_t seconds,char_t minutes,char_t hours, char_t *string)
{
	strncpy(&string[0] ,integer_string_8(hours),2);
	string[2] = ':';
	strncpy(&string[3],integer_string_8(minutes),2);
	string[5] = ':';
	strncpy(&string[6] ,integer_string_8(seconds),2);
	string[8] = ',';
	string[9] = '\n';
	//return time_string;
}

char_t string_1[5];
char_t* integer_string_8(char_t data)
{
	
	if(data == 0 )
	{
		string_1[0] = '0';
		string_1[1] =  '0';
	}
	else if(data < 10)
	{
		string_1[0] = '0';
		string_1[1] = (data%10) + '0';
	}
	else if((data>= 10) && (data <= 99))
	{
		string_1[1] = (data%10) + '0';
		data = data/10;
		string_1[0] = (data%10) + '0';
	}
	else if((data> 99) && (data <= 256))
	{
		string_1[2] = data%10;
		data = data/10;
		string_1[1] = data%10;
		data = data/10;
		string_1[0] = data%10;

	}
	else
	{
		string_1[0] = '0';
		string_1[1] =  '0';
	}
	string_1[2] =  '\n';
	return string_1;
}


char_t* integer_string_16(uint16_t data)
{
	uint8_t  string_data[8];
	uint8_t index =0;
	uint8_t remainder =0; 
	uint8_t temp = 0;
	 while(data)
	 {
		remainder = data%16;
		if(remainder <= 9)
		{
			temp = remainder + '0';
		}
		else if(remainder == 10)
		{
			temp = 'A';
		}
		else if(remainder == 11)
		{
			temp = 'B';
		}
		else if(remainder == 12)
		{
			temp = 'C';
		}
		else if(remainder == 13)
		{
			temp = 'D';
		}
		else if(remainder == 14)
		{
			temp = 'E';
		}
		else if(remainder == 15)
		{
			temp = 'F';
		}
		string_data[index++] = temp;
		data = data/16;
	 }
	
	switch(index)
	{
		case 0:
		{
			string_reverse_data[0] = '0';
			string_reverse_data[1] = '0';
			string_reverse_data[2] = '0';
			break;
		}
		case 1:
		{
			string_reverse_data[0] = '0';
			string_reverse_data[1] = '0';
			string_reverse_data[2] = string_data[0];
			break;
		}
		case 2:
		{
			string_reverse_data[0] = '0';
			string_reverse_data[1] = string_data[0];
			string_reverse_data[2] = string_data[1];
			break;
		}
		case 3:
		{
			string_reverse_data[0] = string_data[2];
			string_reverse_data[2] = string_data[0];
			string_reverse_data[1] = string_data[1];
			break;
		}
		
		default :
		{

		}
		string_reverse_data[3] = '\n';
		
	}
	return string_reverse_data;	
}

uint8_t remainder_= 0;
char_t string[3];
uint8_t test_var =0;
uint8_t test_var1 =0 ;
uint8_t test_var2 =0 ;
char_t* hex_string_8(uint8_t data)
{
	uint8_t i =0 ;
	test_var = data;
	if(data == 0 )
	{
		string[0] = '0';
		string[1] =  '0';
	}
	else if(data < 0x0f)
	//else if(test_var < 0x0f)
	{
		NOP();
		string[0] = '0';
		remainder_ = data%16;
		test_var1 = remainder_;
		test_var2 = 0xc5%16;
		string[1] = remainder_maping(remainder_);
		
	}
	else if((test_var >= 16) && (test_var <= 0xff))
	{
		NOP();
		remainder_ = data%16;
		string[1] = remainder_maping(remainder_);
		data = data/16;
		remainder_ = data%16;
		
		string[0] = remainder_maping(remainder_);
	}
	
	else
	{
		string[0] = '0';
		string[1] =  '0';
	}
	for(i =0 ;i <2;i++)
	{
		if(string[i] == '\n' || string[i] == '\r')
		{
			NOP();
		}
	}
	string[2] =  '\n';
	return string;
}

uint8_t remainder_maping(uint8_t data)
{
	uint8_t temp_data;
	switch(data)
	{
		case 10:
		{
			temp_data = 'A';
			break ; 
		}
		case 11:
		{
			temp_data = 'B';
			break ; 
		}
		case 12:
		{
			temp_data = 'C';
			break ; 
		}
		case 13:
		{
			temp_data = 'D';
			break ; 
		}
		case 14:
		{
			temp_data = 'E';
			break ; 
		}
		case 15:
		{
			temp_data = 'F';
			break ; 
		}
		default :
		{
			temp_data = data+'0';
		}
	}
	return temp_data;
}

uint8_t data_temp[8] = {0};
char_t candata_string[50];
uint8_t test =0; 
char_t* can_data_string(char_t *can_data)
{
	
	uint8_t i =0;
	
	uint8_t ind =0;
	
	for(i= 0;i<8;i++)
	{
		strncpy(&candata_string[ind],hex_string_8(can_data[i]),2);
		ind = ind+2;
		candata_string[ind] = ' ';
		ind++;
	}
	for(test =0; test < ind;test++)
	{
		if(candata_string[test] == '\n')
		{
			NOP();
		}

	}
	candata_string[ind] = '\n';
	return candata_string;
}

void SdcardFile_naming(void)
{
	char_t string_filename[20];
	RealTimeData_St_t RealTimeData_St_1;
	GetRealTime(&RealTimeData_St);
	
	RealTimeData_St_1.Date_u8 =   (RealTimeData_St.Date_u8);
	RealTimeData_St_1.Month_u8 =  (RealTimeData_St.Month_u8);
	RealTimeData_St_1.Year_u8 =   (RealTimeData_St.Year_u8);
	RealTimeData_St_1.Hour_u8 =   (RealTimeData_St.Hour_u8);
	RealTimeData_St_1.Minute_u8 = (RealTimeData_St.Minute_u8);
	NOP();
	strncpy(&string_filename[0],integer_string_8(RealTimeData_St_1.Date_u8),2);
	string_filename[2] = '_';
	strncpy(&string_filename[3],integer_string_8(RealTimeData_St_1.Month_u8),2);
	string_filename[5] = '_';
	strncpy(&string_filename[6],integer_string_8(RealTimeData_St_1.Year_u8),2);
	string_filename[8] = '_';
	strncpy(&string_filename[9],integer_string_8(RealTimeData_St_1.Hour_u8),2);
	string_filename[11] = '_';
	strncpy(&string_filename[12],integer_string_8(RealTimeData_St_1.Minute_u8),2);
	string_filename[14] = '_';
	string_filename[15] = '.';
	string_filename[16] = 'c';
	string_filename[17] = 's';
	string_filename[18] = 'v';
	string_filename[19] = '\n';
	strcpy(&filename[0],&string_filename[0] );

	Folder_create();
	year_append();
	return;
}


char_t folderName[30] ;
void Folder_create(void)
{
	
	switch(RealTimeData_St.Month_u8)
	{
		case Jan_En:
		{
			strncpy(&folderName[0],"Jan_",4);
			break;
		}
		case Feb_En:
		{
			strncpy(&folderName[0],"Feb_",4);
			break;
		}
		case Mar_En:
		{
			strncpy(&folderName[0],"Mar_",4);
			break;
		}
		case Apr_En:
		{
			strncpy(&folderName[0],"Apr_",4);
			break;
		}
		case May_En:
		{
			strncpy(&folderName[0],"May_",4);
			break;
		}
		case Jun_En:
		{
			strncpy(&folderName[0],"Jun_",4);
			break;
		}
		case Jul_En:
		{
			strncpy(&folderName[0],"Jul_",4);
			break;
		}
		case Aug_En:
		{
			strncpy(&folderName[0],"Aug_",4);
			break;
		}
		case Sep_En:
		{
			strncpy(&folderName[0],"Sep_",4);
			break;
		}
		case Oct_En:
		{
			strncpy(&folderName[0],"Oct_",4);
			break;
		}
		case Nov_En:
		{
			strncpy(&folderName[0],"Nov_",4);
			break;
		}
		case Dec_En:
		{
			strncpy(&folderName[0],"Dec_",4);
			break;
		}
	}
	
	return;
}

void year_append(void)
{
	strncpy(&folderName[4],integer_string_8(RealTimeData_St.Year_u8),2);
	folderName[6] = '\n' ;
	f_mkdir(folderName);
	folderName[6] = '/' ;
	strncpy(&folderName[7],integer_string_8(RealTimeData_St.Date_u8),2);
	folderName[9] = '\n' ;
	folderName[9]= '_';
	strncpy(&folderName[10],integer_string_8(RealTimeData_St.Month_u8),2);
	folderName[12] = '\n' ;
	folderName[12] = '_';
	strncpy(&folderName[13],integer_string_8(RealTimeData_St.Year_u8),2);
	folderName[15] = '\n' ;
	f_mkdir(folderName);
	folderName[15] = '/' ;
	folderName[16] = '\n' ;
	strcpy(&folderName[16],filename);
	strcpy(filename,folderName);
	
	return;
}



	

/**
void FolderCounter(void)
{
    int folder_count = 0;
    int i =0;
    char_t DirName[20][20];
    DIR dir;
    FILINFO fno;

    if (f_opendir(&dir, "/") != FR_OK) 
	{
        printf("Failed to open directory\n");
        return ;
    }

    while (f_readdir(&dir, &fno) == FR_OK && fno.fname[0] != 0) 
	{
        if (fno.fattrib & AM_DIR) 
		{
            // f_attrib is AM_DIR if the entry is a directory
            if (strcmp(fno.fname, ".") != 0 && strcmp(fno.fname, "..") != 0) 
			{
                // Ignore "." and ".." directories
                folder_count++;
				strcpy(DirName[i],fno.fname);
				i++;
            }
        }
    }

    f_closedir(&dir);

    printf("Number of folders: %d\n", folder_count);

    return ;
}

    
*/
   

 


/***********************************************************************************************************************
* Function Name: string_reverse
* Description  : This function reverse the string.
* Arguments    : uint8_t *
* Return Value : None
***********************************************************************************************************************/



void Int_to_string(uint16_t data_au8 , uint8_t flag)
{

}


uint8_t temp_data[10];

void user_SdCard_write(uint16_t canid_u16 ,uint8_t *data_ua8)
{
	uint8_t data_index;
	Sd_opertion_St.length_u16 = 0;
	Sd_opertion_St.length_temp_u16 = 0;
	clearbuffer(Sd_opertion_St.buffer_temp_ua8,50);
	
	for(data_index = 0;  data_index < 8 ;data_index++ )
	{
		temp_data[data_index] = data_ua8[data_index];
	}
	Int_to_string(canid_u16 , true);
	for(data_index = 2;  data_index < 10 ;data_index++ )
	{
		Int_to_string(data_ua8[data_index],false);
	}

	Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = '\r';
	Sd_opertion_St.length_u16++;
	Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = '\n';
	File_write(Sd_opertion_St.buffer_ua8,Sd_opertion_St.length_u16 );
}
void clearbuffer(uint8_t *buff,uint8_t size)
{
	uint8_t index_u8 =0;
	for(index_u8 = 0; index_u8 < size; index_u8++)
	{
		buff[index_u8] = '\n';
	}
}




#endif /*SD_CARD_C */
/*---------------------- End of File -----------------------------------------*/