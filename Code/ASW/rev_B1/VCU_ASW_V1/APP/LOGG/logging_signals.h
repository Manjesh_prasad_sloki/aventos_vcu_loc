﻿#include <stdint.h>
#include "r_cg_macrodriver.h"


#ifndef LOGG_SIG_H
#define LOGG_SIG_H




#define DATA_709__3_BATTERY_CURRENT_MASK0  8U
#define DATA_709__6_AVAILABLE_ENERGY_MASK0  8U




#define DATA_711_MCU_150_MOTOR_SPEED_MASK0  8U
#define DATA_711_MCU_150_BATTERY_CURRENT_MASK0  8U
#define DATA_711_MCU_150_POSITION_SENSOR_ERROR_MASK0  6U
#define DATA_711_MCU_150_THROTTLE_DISABLE_STATUS_MASK0  7U
#define DATA_711_MCU_150_PHERIPHERY_SUPPLY_FAILURE_MASK0  1U
#define DATA_711_MCU_150_MOTOR_TEMPERATURE_WARNING_MASK0  2U
#define DATA_711_MCU_150_CONTROLLER_TEMP_WARNING_MASK0  3U
#define DATA_711_MCU_150_OVER_VOLTAGE_ERROR_MASK0  4U
#define DATA_711_MCU_150_UNDER_VOLTAGE_ERROR_MASK0  5U
#define DATA_711_MCU_150_OVER_CURRENT_ERROR_MASK0  6U
#define DATA_711_MCU_150_TIMEOUT_ERROR_MASK0  7U
#define DATA_711_MCU_150_DRIVING_DIRECTION_ACTUAL_MASK0  1U
#define DATA_711_MCU_150_RIDE_MODE_ACTUAL_MASK0  2U
#define DATA_711_MCU_150_THROTTLE_MAP_ACTUAL_MASK0  5U




#define DATA_713__2_BATTERY_CAPACITY_MASK0  8U
#define DATA_713__4_M_WH_KM_MASK0  8U


#define DATA_715__2_AVAILABLE_CAPACITY_MASK0  8U
#define DATA_715__4_IGN_WH_MASK0  8U
#define DATA_715__5_IGN_WH_KM_MASK0  8U


#define DATA_716__1_WH_KM_ECO_MASK0  8U
#define DATA_716__2_WH_KM_SPORT_MASK0  8U
#define DATA_716_BATTERY_AVAILABLE_ENERGY_MASK0  8U






#define DATA_719_MCU_250_MOTOR_TEMPERATURE_MASK0  8U
#define DATA_719_MCU_250_CONTROLLER_TEMPERATURE_MASK0  8U
#define DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK0  24U
#define DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK1  16U
#define DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK2  8U


#define DATA_720_MCU_100__1_MOTOR_SPEED_LIMIT_MASK0  8U
#define DATA_720_MCU_100__4_LTVS_MASK0  16U
#define DATA_720_MCU_100__4_LTVS_MASK1  8U
#define DATA_720_MCU_100__6_THROTTLE_MAP_SELECT_MASK0  3U
#define DATA_720_MCU_100__7_MOTOR_STOP_MASK0  6U
#define DATA_720_MCU_100__8_DRIVING_DIRECTION_MASK0  7U


#define DATA_721_DISTANCECOVERED_MASK0  8U
#define DATA_721_NO_OF_100M_MASK0  8U
#define DATA_721_AVAILABLE_ENERGY_BMS_MASK0  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 













/* def @DATA_709 CAN Message                                   (1801) */
#define DATA_709_ID                                            (1801U)
#define DATA_709_IDE                                           (0U)
#define DATA_709_DLC                                           (8U)


#define DATA_709__1_BATTERY_VOLTAGEFACTOR                                   (0.25)
#define DATA_709_CANID__1_BATTERY_VOLTAGE_STARTBIT                           (0)
#define DATA_709_CANID__1_BATTERY_VOLTAGE_OFFSET                             (0)
#define DATA_709_CANID__1_BATTERY_VOLTAGE_MIN                                (0)
#define DATA_709_CANID__1_BATTERY_VOLTAGE_MAX                                (63.75)
#define DATA_709__2_CURRENT_DIRECTIONFACTOR                                   (1)
#define DATA_709_CANID__2_CURRENT_DIRECTION_STARTBIT                           (8)
#define DATA_709_CANID__2_CURRENT_DIRECTION_OFFSET                             (0)
#define DATA_709_CANID__2_CURRENT_DIRECTION_MIN                                (0)
#define DATA_709_CANID__2_CURRENT_DIRECTION_MAX                                (255)
#define DATA_709__3_BATTERY_CURRENTFACTOR                                   (0.0625)
#define DATA_709_CANID__3_BATTERY_CURRENT_STARTBIT                           (16)
#define DATA_709_CANID__3_BATTERY_CURRENT_OFFSET                             (0)
#define DATA_709_CANID__3_BATTERY_CURRENT_MIN                                (0)
#define DATA_709_CANID__3_BATTERY_CURRENT_MAX                                (4095.94)
#define DATA_709__4_VEHICLE_SPEEDFACTOR                                   (1)
#define DATA_709_CANID__4_VEHICLE_SPEED_STARTBIT                           (32)
#define DATA_709_CANID__4_VEHICLE_SPEED_OFFSET                             (0)
#define DATA_709_CANID__4_VEHICLE_SPEED_MIN                                (0)
#define DATA_709_CANID__4_VEHICLE_SPEED_MAX                                (255)
#define DATA_709__5_BATTERY_SOCFACTOR                                   (1)
#define DATA_709_CANID__5_BATTERY_SOC_STARTBIT                           (40)
#define DATA_709_CANID__5_BATTERY_SOC_OFFSET                             (0)
#define DATA_709_CANID__5_BATTERY_SOC_MIN                                (0)
#define DATA_709_CANID__5_BATTERY_SOC_MAX                                (255)
#define DATA_709__6_AVAILABLE_ENERGYFACTOR                                   (0.1)
#define DATA_709_CANID__6_AVAILABLE_ENERGY_STARTBIT                           (48)
#define DATA_709_CANID__6_AVAILABLE_ENERGY_OFFSET                             (0)
#define DATA_709_CANID__6_AVAILABLE_ENERGY_MIN                                (0)
#define DATA_709_CANID__6_AVAILABLE_ENERGY_MAX                                (6553.5)

#pragma pack (1)
typedef struct
{
  uint8_t _1_Battery_Voltage;
  uint8_t _2_Current_direction;
  uint16_t _3_Battery_current;
  uint8_t _4_Vehicle_Speed;
  uint8_t _5_Battery_SoC;
  uint16_t _6_Available_Energy;
}Data_709_t;


/* def @DATA_710 CAN Message                                   (1808) */
#define DATA_710_ID                                            (1808U)
#define DATA_710_IDE                                           (0U)
#define DATA_710_DLC                                           (8U)


#define DATA_710_KILL_SWITCHFACTOR                                   (1)
#define DATA_710_CANID_KILL_SWITCH_STARTBIT                           (0)
#define DATA_710_CANID_KILL_SWITCH_OFFSET                             (0)
#define DATA_710_CANID_KILL_SWITCH_MIN                                (0)
#define DATA_710_CANID_KILL_SWITCH_MAX                                (255)
#define DATA_710_KICK_STANDFACTOR                                   (1)
#define DATA_710_CANID_KICK_STAND_STARTBIT                           (8)
#define DATA_710_CANID_KICK_STAND_OFFSET                             (0)
#define DATA_710_CANID_KICK_STAND_MIN                                (0)
#define DATA_710_CANID_KICK_STAND_MAX                                (255)
#define DATA_710_TORQUE_ZEROFACTOR                                   (1)
#define DATA_710_CANID_TORQUE_ZERO_STARTBIT                           (16)
#define DATA_710_CANID_TORQUE_ZERO_OFFSET                             (0)
#define DATA_710_CANID_TORQUE_ZERO_MIN                                (0)
#define DATA_710_CANID_TORQUE_ZERO_MAX                                (255)
#define DATA_710_MOTOR_KILLFACTOR                                   (1)
#define DATA_710_CANID_MOTOR_KILL_STARTBIT                           (24)
#define DATA_710_CANID_MOTOR_KILL_OFFSET                             (0)
#define DATA_710_CANID_MOTOR_KILL_MIN                                (0)
#define DATA_710_CANID_MOTOR_KILL_MAX                                (255)
#define DATA_710_MOTOR_TEMPERATUREFACTOR                                   (1)
#define DATA_710_CANID_MOTOR_TEMPERATURE_STARTBIT                           (32)
#define DATA_710_CANID_MOTOR_TEMPERATURE_OFFSET                             (0)
#define DATA_710_CANID_MOTOR_TEMPERATURE_MIN                                (0)
#define DATA_710_CANID_MOTOR_TEMPERATURE_MAX                                (255)
#define DATA_710_CONTROLLER_TEMPERATUREFACTOR                                   (1)
#define DATA_710_CANID_CONTROLLER_TEMPERATURE_STARTBIT                           (40)
#define DATA_710_CANID_CONTROLLER_TEMPERATURE_OFFSET                             (0)
#define DATA_710_CANID_CONTROLLER_TEMPERATURE_MIN                                (0)
#define DATA_710_CANID_CONTROLLER_TEMPERATURE_MAX                                (255)
#define DATA_710_CELL_HIGH_TEMPERATUREFACTOR                                   (1)
#define DATA_710_CANID_CELL_HIGH_TEMPERATURE_STARTBIT                           (48)
#define DATA_710_CANID_CELL_HIGH_TEMPERATURE_OFFSET                             (0)
#define DATA_710_CANID_CELL_HIGH_TEMPERATURE_MIN                                (0)
#define DATA_710_CANID_CELL_HIGH_TEMPERATURE_MAX                                (255)
#define DATA_710_PDU_TEMPERATUREFACTOR                                   (1)
#define DATA_710_CANID_PDU_TEMPERATURE_STARTBIT                           (56)
#define DATA_710_CANID_PDU_TEMPERATURE_OFFSET                             (0)
#define DATA_710_CANID_PDU_TEMPERATURE_MIN                                (0)
#define DATA_710_CANID_PDU_TEMPERATURE_MAX                                (255)

#pragma pack (1)
typedef struct
{
  uint8_t Kill_Switch;
  uint8_t Kick_stand;
  uint8_t Torque_zero;
  uint8_t Motor_kill;
  uint8_t Motor_temperature;
  uint8_t Controller_temperature;
  uint8_t Cell_High_temperature;
  uint8_t PDU_Temperature;
}
Data_710_t;


/* def @DATA_711_MCU_150 CAN Message                                   (1809) */
#define DATA_711_MCU_150_ID                                            (1809U)
#define DATA_711_MCU_150_IDE                                           (0U)
#define DATA_711_MCU_150_DLC                                           (8U)


#define DATA_711_MCU_150_MOTOR_SPEEDFACTOR                                   (0.305176)
#define DATA_711_MCU_150_CANID_MOTOR_SPEED_STARTBIT                           (0)
#define DATA_711_MCU_150_CANID_MOTOR_SPEED_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_MOTOR_SPEED_MIN                                (-10000)
#define DATA_711_MCU_150_CANID_MOTOR_SPEED_MAX                                (9999.7)
#define DATA_711_MCU_150_BATTERY_CURRENTFACTOR                                   (0.012695)
#define DATA_711_MCU_150_CANID_BATTERY_CURRENT_STARTBIT                           (16)
#define DATA_711_MCU_150_CANID_BATTERY_CURRENT_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_BATTERY_CURRENT_MIN                                (-415.99)
#define DATA_711_MCU_150_CANID_BATTERY_CURRENT_MAX                                (415.977)
#define DATA_711_MCU_150_POSITION_SENSOR_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_STARTBIT                           (46)
#define DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_MAX                                (1)
#define DATA_711_MCU_150_THROTTLE_DISABLE_STATUSFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_STARTBIT                           (47)
#define DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_MIN                                (0)
#define DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_MAX                                (1)
#define DATA_711_MCU_150_MOTOR_ENABLE_STATUSFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_STARTBIT                           (48)
#define DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_MIN                                (0)
#define DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_MAX                                (1)
#define DATA_711_MCU_150_PHERIPHERY_SUPPLY_FAILUREFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_STARTBIT                           (49)
#define DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_MIN                                (0)
#define DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_MAX                                (1)
#define DATA_711_MCU_150_MOTOR_TEMPERATURE_WARNINGFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_STARTBIT                           (50)
#define DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_MIN                                (0)
#define DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_MAX                                (1)
#define DATA_711_MCU_150_CONTROLLER_TEMP_WARNINGFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_STARTBIT                           (51)
#define DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_MIN                                (0)
#define DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_MAX                                (1)
#define DATA_711_MCU_150_OVER_VOLTAGE_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_STARTBIT                           (52)
#define DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_MAX                                (1)
#define DATA_711_MCU_150_UNDER_VOLTAGE_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_STARTBIT                           (53)
#define DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_MAX                                (1)
#define DATA_711_MCU_150_OVER_CURRENT_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_STARTBIT                           (54)
#define DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_MAX                                (1)
#define DATA_711_MCU_150_TIMEOUT_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_TIMEOUT_ERROR_STARTBIT                           (55)
#define DATA_711_MCU_150_CANID_TIMEOUT_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_TIMEOUT_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_TIMEOUT_ERROR_MAX                                (1)
#define DATA_711_MCU_150_ANALOG_THROTTLE_ERRORFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_STARTBIT                           (56)
#define DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_MIN                                (0)
#define DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_MAX                                (1)
#define DATA_711_MCU_150_DRIVING_DIRECTION_ACTUALFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_STARTBIT                           (57)
#define DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_MIN                                (0)
#define DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_MAX                                (1)
#define DATA_711_MCU_150_RIDE_MODE_ACTUALFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_STARTBIT                           (58)
#define DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_MIN                                (0)
#define DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_MAX                                (7)
#define DATA_711_MCU_150_THROTTLE_MAP_ACTUALFACTOR                                   (1)
#define DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_STARTBIT                           (61)
#define DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_OFFSET                             (0)
#define DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_MIN                                (0)
#define DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_MAX                                (7)

#pragma pack (1)
typedef struct
{
  int16_t Motor_speed;
  int16_t Battery_current;
  int8_t Position_sensor_error;
  int8_t Throttle_disable_status;
  int8_t Motor_enable_status;
  int8_t Pheriphery_supply_failure;
  int8_t Motor_temperature_warning;
  int8_t Controller_Temp_warning;
  int8_t Over_voltage_error;
  int8_t under_voltage_error;
  int8_t over_current_error;
  int8_t Timeout_error;
  int8_t Analog_throttle_error;
  int8_t Driving_direction_actual;
  uint8_t Ride_mode_actual;
  uint8_t Throttle_map_actual;
}
Data_711_MCU_150_t;


/* def @DATA_712_MCU_200 CAN Message                                   (1810) */
#define DATA_712_MCU_200_ID                                            (1810U)
#define DATA_712_MCU_200_IDE                                           (0U)
#define DATA_712_MCU_200_DLC                                           (8U)


#define DATA_712_MCU_200_THROTTLE_OUT_PFACTOR                                   (0.390625)
#define DATA_712_MCU_200_CANID_THROTTLE_OUT_P_STARTBIT                           (0)
#define DATA_712_MCU_200_CANID_THROTTLE_OUT_P_OFFSET                             (0)
#define DATA_712_MCU_200_CANID_THROTTLE_OUT_P_MIN                                (0)
#define DATA_712_MCU_200_CANID_THROTTLE_OUT_P_MAX                                (99.6094)
#define DATA_712_MCU_200_THROTTLE_REFERENCEFACTOR                                   (0.019531)
#define DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_STARTBIT                           (8)
#define DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_OFFSET                             (0)
#define DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_MIN                                (0)
#define DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_MAX                                (4.98041)
#define DATA_712_MCU_200_BATTERY_VOLTAGEFACTOR                                   (0.45098)
#define DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_STARTBIT                           (56)
#define DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_OFFSET                             (0)
#define DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_MIN                                (0)
#define DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_MAX                                (115)

#pragma pack (1)
typedef struct
{
  uint8_t Throttle_out_p;
  uint8_t Throttle_reference;
  uint8_t Battery_voltage;
}
Data_712_MCU_200_t;


/* def @DATA_713 CAN Message                                   (1811) */
#define DATA_713_ID                                            (1811U)
#define DATA_713_IDE                                           (0U)
#define DATA_713_DLC                                           (8U)


#define DATA_713__1_BATTERY_VOLTAGEFACTOR                                   (0.25)
#define DATA_713_CANID__1_BATTERY_VOLTAGE_STARTBIT                           (0)
#define DATA_713_CANID__1_BATTERY_VOLTAGE_OFFSET                             (0)
#define DATA_713_CANID__1_BATTERY_VOLTAGE_MIN                                (0)
#define DATA_713_CANID__1_BATTERY_VOLTAGE_MAX                                (63.75)
#define DATA_713__2_BATTERY_CAPACITYFACTOR                                   (0.125)
#define DATA_713_CANID__2_BATTERY_CAPACITY_STARTBIT                           (8)
#define DATA_713_CANID__2_BATTERY_CAPACITY_OFFSET                             (0)
#define DATA_713_CANID__2_BATTERY_CAPACITY_MIN                                (0)
#define DATA_713_CANID__2_BATTERY_CAPACITY_MAX                                (8191.88)
#define DATA_713__3_DISTANCE_FROM_STARTFACTOR                                   (0.5)
#define DATA_713_CANID__3_DISTANCE_FROM_START_STARTBIT                           (24)
#define DATA_713_CANID__3_DISTANCE_FROM_START_OFFSET                             (0)
#define DATA_713_CANID__3_DISTANCE_FROM_START_MIN                                (0)
#define DATA_713_CANID__3_DISTANCE_FROM_START_MAX                                (127.5)
#define DATA_713__4_M_WH_KMFACTOR                                   (0.03125)
#define DATA_713_CANID__4_M_WH_KM_STARTBIT                           (32)
#define DATA_713_CANID__4_M_WH_KM_OFFSET                             (0)
#define DATA_713_CANID__4_M_WH_KM_MIN                                (0)
#define DATA_713_CANID__4_M_WH_KM_MAX                                (2047.97)
#define DATA_713__5_INSTANT_MILEAGEFACTOR                                   (0.25)
#define DATA_713_CANID__5_INSTANT_MILEAGE_STARTBIT                           (48)
#define DATA_713_CANID__5_INSTANT_MILEAGE_OFFSET                             (0)
#define DATA_713_CANID__5_INSTANT_MILEAGE_MIN                                (0)
#define DATA_713_CANID__5_INSTANT_MILEAGE_MAX                                (63.75)
#define DATA_713__6_HISTORIC_MILEAGEFACTOR                                   (0.24)
#define DATA_713_CANID__6_HISTORIC_MILEAGE_STARTBIT                           (56)
#define DATA_713_CANID__6_HISTORIC_MILEAGE_OFFSET                             (0)
#define DATA_713_CANID__6_HISTORIC_MILEAGE_MIN                                (0)
#define DATA_713_CANID__6_HISTORIC_MILEAGE_MAX                                (61.2)

#pragma pack (1)
typedef struct
{
  uint8_t _1_Battery_Voltage;
  uint16_t _2_Battery_Capacity;
  uint8_t _3_Distance_from_start;
  uint16_t _4_M_Wh_Km;
  uint8_t _5_Instant_Mileage;
  uint8_t _6_Historic_Mileage;
}
Data_713_t;


/* def @DATA_715 CAN Message                                   (1813) */
#define DATA_715_ID                                            (1813U)
#define DATA_715_IDE                                           (0U)
#define DATA_715_DLC                                           (8U)


#define DATA_715__1_BATTERY_VOLTAGEFACTOR                                   (25)
#define DATA_715_CANID__1_BATTERY_VOLTAGE_STARTBIT                           (0)
#define DATA_715_CANID__1_BATTERY_VOLTAGE_OFFSET                             (0)
#define DATA_715_CANID__1_BATTERY_VOLTAGE_MIN                                (0)
#define DATA_715_CANID__1_BATTERY_VOLTAGE_MAX                                (6375)
#define DATA_715__2_AVAILABLE_CAPACITYFACTOR                                   (0.125)
#define DATA_715_CANID__2_AVAILABLE_CAPACITY_STARTBIT                           (8)
#define DATA_715_CANID__2_AVAILABLE_CAPACITY_OFFSET                             (0)
#define DATA_715_CANID__2_AVAILABLE_CAPACITY_MIN                                (0)
#define DATA_715_CANID__2_AVAILABLE_CAPACITY_MAX                                (8191.88)
#define DATA_715__3_DISTANCE_KMFACTOR                                   (0.5)
#define DATA_715_CANID__3_DISTANCE_KM_STARTBIT                           (24)
#define DATA_715_CANID__3_DISTANCE_KM_OFFSET                             (0)
#define DATA_715_CANID__3_DISTANCE_KM_MIN                                (0)
#define DATA_715_CANID__3_DISTANCE_KM_MAX                                (127.5)
#define DATA_715__4_IGN_WHFACTOR                                   (0.03125)
#define DATA_715_CANID__4_IGN_WH_STARTBIT                           (32)
#define DATA_715_CANID__4_IGN_WH_OFFSET                             (0)
#define DATA_715_CANID__4_IGN_WH_MIN                                (0)
#define DATA_715_CANID__4_IGN_WH_MAX                                (2047.97)
#define DATA_715__5_IGN_WH_KMFACTOR                                   (0.03125)
#define DATA_715_CANID__5_IGN_WH_KM_STARTBIT                           (48)
#define DATA_715_CANID__5_IGN_WH_KM_OFFSET                             (0)
#define DATA_715_CANID__5_IGN_WH_KM_MIN                                (0)
#define DATA_715_CANID__5_IGN_WH_KM_MAX                                (2047.97)

#pragma pack (1)
typedef struct
{
  uint8_t _1_Battery_voltage;
  uint16_t _2_Available_Capacity;
  uint8_t _3_Distance_km;
  uint16_t _4_IGN_WH;
  uint16_t _5_IGN_WH_KM;
}
Data_715_t;


/* def @DATA_716 CAN Message                                   (1814) */
#define DATA_716_ID                                            (1814U)
#define DATA_716_IDE                                           (0U)
#define DATA_716_DLC                                           (8U)


#define DATA_716__1_WH_KM_ECOFACTOR                                   (0.03125)
#define DATA_716_CANID__1_WH_KM_ECO_STARTBIT                           (0)
#define DATA_716_CANID__1_WH_KM_ECO_OFFSET                             (0)
#define DATA_716_CANID__1_WH_KM_ECO_MIN                                (0)
#define DATA_716_CANID__1_WH_KM_ECO_MAX                                (2047.97)
#define DATA_716__2_WH_KM_SPORTFACTOR                                   (0.03125)
#define DATA_716_CANID__2_WH_KM_SPORT_STARTBIT                           (16)
#define DATA_716_CANID__2_WH_KM_SPORT_OFFSET                             (0)
#define DATA_716_CANID__2_WH_KM_SPORT_MIN                                (0)
#define DATA_716_CANID__2_WH_KM_SPORT_MAX                                (2047.97)
#define DATA_716__3_RANGEFACTOR                                   (1)
#define DATA_716_CANID__3_RANGE_STARTBIT                           (32)
#define DATA_716_CANID__3_RANGE_OFFSET                             (0)
#define DATA_716_CANID__3_RANGE_MIN                                (0)
#define DATA_716_CANID__3_RANGE_MAX                                (255)
#define DATA_716_BATTERY_AVAILABLE_ENERGYFACTOR                                   (0.1)
#define DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_STARTBIT                           (48)
#define DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_OFFSET                             (0)
#define DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_MIN                                (0)
#define DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_MAX                                (6553.5)

#pragma pack (1)
typedef struct
{
  uint16_t _1_WH_KM_ECO;
  uint16_t _2_WH_KM_SPORT;
  uint8_t _3_Range;
  uint16_t Battery_available_Energy;
}
Data_716_t;


/* def @DATA_717 CAN Message                                   (1815) */
#define DATA_717_ID                                            (1815U)
#define DATA_717_IDE                                           (0U)
#define DATA_717_DLC                                           (8U)


#define DATA_717__1_ACCELERATIONANGLE_XFACTOR                                   (1)
#define DATA_717_CANID__1_ACCELERATIONANGLE_X_STARTBIT                           (0)
#define DATA_717_CANID__1_ACCELERATIONANGLE_X_OFFSET                             (0)
#define DATA_717_CANID__1_ACCELERATIONANGLE_X_MIN                                (0)
#define DATA_717_CANID__1_ACCELERATIONANGLE_X_MAX                                (255)
#define DATA_717__2_ACCELERATIONANGLE_YFACTOR                                   (1)
#define DATA_717_CANID__2_ACCELERATIONANGLE_Y_STARTBIT                           (8)
#define DATA_717_CANID__2_ACCELERATIONANGLE_Y_OFFSET                             (0)
#define DATA_717_CANID__2_ACCELERATIONANGLE_Y_MIN                                (0)
#define DATA_717_CANID__2_ACCELERATIONANGLE_Y_MAX                                (255)

#pragma pack (1)
typedef struct
{
  uint8_t _1_AccelerationAngle_X;
  uint8_t _2_AccelerationAngle_Y;
}
Data_717_t;


/* def @DATA_718 CAN Message                                   (1816) */
#define DATA_718_ID                                            (1816U)
#define DATA_718_IDE                                           (0U)
#define DATA_718_DLC                                           (8U)


#define DATA_718_MCUTRANSMISSIONSTATUSFACTOR                                   (1)
#define DATA_718_CANID_MCUTRANSMISSIONSTATUS_STARTBIT                           (56)
#define DATA_718_CANID_MCUTRANSMISSIONSTATUS_OFFSET                             (0)
#define DATA_718_CANID_MCUTRANSMISSIONSTATUS_MIN                                (0)
#define DATA_718_CANID_MCUTRANSMISSIONSTATUS_MAX                                (255)

#pragma pack (1)
typedef struct
{
  uint8_t MCUTransmissionStatus;
}
Data_718_t;


/* def @DATA_719_MCU_250 CAN Message                                   (1817) */
#define DATA_719_MCU_250_ID                                            (1817U)
#define DATA_719_MCU_250_IDE                                           (0U)
#define DATA_719_MCU_250_DLC                                           (8U)


#define DATA_719_MCU_250_MOTOR_TEMPERATUREFACTOR                                   (0.390625)
#define DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_STARTBIT                           (0)
#define DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_OFFSET                             (0)
#define DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_MIN                                (-200)
#define DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_MAX                                (199.609)
#define DATA_719_MCU_250_CONTROLLER_TEMPERATUREFACTOR                                   (0.292969)
#define DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_STARTBIT                           (16)
#define DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_OFFSET                             (0)
#define DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_MIN                                (-150)
#define DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_MAX                                (149.707)
#define DATA_719_MCU_250_MOTOR_ROTATION_NUMBERFACTOR                                   (1)
#define DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_STARTBIT                           (32)
#define DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_OFFSET                             (0)
#define DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_MIN                                (0)
#define DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_MAX                                (4294970000)

#pragma pack (1)
typedef struct
{
  int16_t Motor_temperature;
  int16_t Controller_temperature;
  uint32_t Motor_Rotation_Number;
}
Data_719_MCU_250_t;


/* def @DATA_720_MCU_100 CAN Message                                   (1824) */
#define DATA_720_MCU_100_ID                                            (1824U)
#define DATA_720_MCU_100_IDE                                           (0U)
#define DATA_720_MCU_100_DLC                                           (8U)


#define DATA_720_MCU_100__1_MOTOR_SPEED_LIMITFACTOR                                   (0.305176)
#define DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_STARTBIT                           (0)
#define DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_MIN                                (-10000)
#define DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_MAX                                (9999.7)
#define DATA_720_MCU_100__2_LTVSFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__2_LTVS_STARTBIT                           (16)
#define DATA_720_MCU_100_CANID__2_LTVS_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__2_LTVS_MIN                                (0)
#define DATA_720_MCU_100_CANID__2_LTVS_MAX                                (255)
#define DATA_720_MCU_100__3_MAX_REGENERATIONFACTOR                                   (0.392)
#define DATA_720_MCU_100_CANID__3_MAX_REGENERATION_STARTBIT                           (24)
#define DATA_720_MCU_100_CANID__3_MAX_REGENERATION_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__3_MAX_REGENERATION_MIN                                (0)
#define DATA_720_MCU_100_CANID__3_MAX_REGENERATION_MAX                                (99.96)
#define DATA_720_MCU_100__4_LTVSFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__4_LTVS_STARTBIT                           (32)
#define DATA_720_MCU_100_CANID__4_LTVS_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__4_LTVS_MIN                                (-8388610)
#define DATA_720_MCU_100_CANID__4_LTVS_MAX                                (8388610)
#define DATA_720_MCU_100__5_RIDE_MODE_REQUESTFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_STARTBIT                           (56)
#define DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_MIN                                (0)
#define DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_MAX                                (7)
#define DATA_720_MCU_100__6_THROTTLE_MAP_SELECTFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_STARTBIT                           (59)
#define DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_MIN                                (0)
#define DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_MAX                                (7)
#define DATA_720_MCU_100__7_MOTOR_STOPFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__7_MOTOR_STOP_STARTBIT                           (62)
#define DATA_720_MCU_100_CANID__7_MOTOR_STOP_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__7_MOTOR_STOP_MIN                                (0)
#define DATA_720_MCU_100_CANID__7_MOTOR_STOP_MAX                                (1)
#define DATA_720_MCU_100__8_DRIVING_DIRECTIONFACTOR                                   (1)
#define DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_STARTBIT                           (63)
#define DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_OFFSET                             (0)
#define DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_MIN                                (0)
#define DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_MAX                                (1)

#pragma pack (1)
typedef struct
{
  int16_t _1_Motor_Speed_Limit;
  uint8_t _2_LTVS;
  uint8_t _3_Max_Regeneration;
  int32_t _4_LTVS;
  uint8_t _5_Ride_Mode_Request;
  uint8_t _6_Throttle_Map_select;
  int8_t _7_Motor_Stop;
  int8_t _8_Driving_Direction;
}
Data_720_MCU_100_t;


/* def @DATA_721 CAN Message                                   (1825) */
#define DATA_721_ID                                            (1825U)
#define DATA_721_IDE                                           (0U)
#define DATA_721_DLC                                           (8U)


#define DATA_721_DISTANCECOVEREDFACTOR                                   (1)
#define DATA_721_CANID_DISTANCECOVERED_STARTBIT                           (0)
#define DATA_721_CANID_DISTANCECOVERED_OFFSET                             (0)
#define DATA_721_CANID_DISTANCECOVERED_MIN                                (0)
#define DATA_721_CANID_DISTANCECOVERED_MAX                                (65535)
#define DATA_721_NO_OF_100MFACTOR                                   (1)
#define DATA_721_CANID_NO_OF_100M_STARTBIT                           (16)
#define DATA_721_CANID_NO_OF_100M_OFFSET                             (0)
#define DATA_721_CANID_NO_OF_100M_MIN                                (0)
#define DATA_721_CANID_NO_OF_100M_MAX                                (65535)
#define DATA_721_AVAILABLE_ENERGY_BMSFACTOR                                   (0.1)
#define DATA_721_CANID_AVAILABLE_ENERGY_BMS_STARTBIT                           (48)
#define DATA_721_CANID_AVAILABLE_ENERGY_BMS_OFFSET                             (0)
#define DATA_721_CANID_AVAILABLE_ENERGY_BMS_MIN                                (0)
#define DATA_721_CANID_AVAILABLE_ENERGY_BMS_MAX                                (6553.5)

#pragma pack (1)
typedef struct
{
  uint16_t DistanceCovered;
  uint16_t No_of_100m;
  uint16_t Available_Energy_BMS;
}
Data_721_t;


 

 extern uint32_t Deserialize_Data_709(Data_709_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_709(Data_709_t* message, uint8_t* data);
 
 extern uint32_t Deserialize_Data_710(Data_710_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_710(Data_710_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_711_MCU_150(Data_711_MCU_150_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_711_MCU_150(Data_711_MCU_150_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_712_MCU_200(Data_712_MCU_200_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_712_MCU_200(Data_712_MCU_200_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_713(Data_713_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_713(Data_713_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_715(Data_715_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_715(Data_715_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_716(Data_716_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_716(Data_716_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_717(Data_717_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_717(Data_717_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_718(Data_718_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_718(Data_718_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_719_MCU_250(Data_719_MCU_250_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_719_MCU_250(Data_719_MCU_250_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_720_MCU_100(Data_720_MCU_100_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_720_MCU_100(Data_720_MCU_100_t* message, uint8_t* data);
 extern uint32_t Deserialize_Data_721(Data_721_t* message, const uint8_t* data);
 extern uint32_t Serialize_Data_721(Data_721_t* message, uint8_t* data);
#endif /*LOGG_SIG_H*/