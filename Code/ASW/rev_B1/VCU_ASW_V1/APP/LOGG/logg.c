/***********************************************************************************************************************
* File Name    : logg.c
* Version      : 01
* Description  : 
* Created By   : 
* Creation Date: 
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "logg.h"
#include "Communicator.h"
#include "can_if.h"
#include "cil_can_conf.h"
#include  "logging_signals.h"
#include "sdcard.h"
#include"rtc_iic.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/



/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t  counter_ = 0 ;

void logging_Proc()
{
	static uint8_t st_machine = 0;
	switch(st_machine)
	{
		case 0:
		{
			Logg_0x709_TxMsgCallback();
			st_machine = 1;
			break;
		}
		case 1:
		{
			Logg_0x710_TxMsgCallback();
			st_machine = 2;
			break;
		}
		case 2:
		{
			Logg_0x711_TxMsgCallback();
			st_machine = 3;
			break;
		}
		case 3:
		{
			Logg_0x712_TxMsgCallback();
			st_machine = 4;
			break;
		}
		case 4:
		{
			Logg_0x713_TxMsgCallback();
			st_machine = 5;
			break;
		}
		case 5:
		{
			Logg_0x715_TxMsgCallback();
			st_machine = 6;
			break;
		}
		case 6:
		{
			Logg_0x716_TxMsgCallback();
			st_machine = 7;
			break;
		}
		case 7:
		{
			Logg_0x717_TxMsgCallback();
			st_machine = 8;
			break;
		}
		case 8:
		{
			Logg_0x718_TxMsgCallback();
			st_machine = 9;
			break;
		}
		case 9:
		{
			Logg_0x719_TxMsgCallback();
			st_machine = 10;
			break;
		}
		case 10:
		{
			Logg_0x720_TxMsgCallback();
			st_machine = 11;
			break;
		}
		case 11:
		{
			Logg_0x721_TxMsgCallback();
			st_machine = 0;
			break;
		}
		default :
		{
			st_machine = 0;
			break ;
		}
	}
	return ;
}
Data_709_t Data_709_struture;
uint8_t  data_709[8],i_709;
CAN_MessageFrame_St_t Can_Applidata_St_709;
void Logg_0x709_TxMsgCallback()
{
    Data_709_struture = Update_0x709_struture();
	Serialize_Data_709(&Data_709_struture, data_709 );
	for (i_709 = ZERO; i_709 < EIGHT; i_709++)
		{
			Can_Applidata_St_709.DataBytes_au8[i_709] = data_709[i_709];
		}
	Can_Applidata_St_709.DataLength_u8 = EIGHT;
	Can_Applidata_St_709.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x709_TX_E, Can_Applidata_St_709);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x709,(char_t*)&data_709[0]);	
}

/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
Data_710_t Data_710_struture;
uint8_t  data_710[8],i_710;
CAN_MessageFrame_St_t Can_Applidata_St_710;
void Logg_0x710_TxMsgCallback(void)
{
	Data_710_struture = Update_0x710_struture();
	Serialize_Data_710(&Data_710_struture, data_710 );
	for (i_710 = ZERO; i_710 < EIGHT; i_710++)
		{
			Can_Applidata_St_710.DataBytes_au8[i_710] = data_710[i_710];
		}
	Can_Applidata_St_710.DataLength_u8 = EIGHT;
	Can_Applidata_St_710.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x710_TX_E, Can_Applidata_St_710);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x710,(char_t*)&data_710[0]);	

}

/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint8_t  *data_711,i_711;
CAN_MessageFrame_St_t Can_Applidata_St_711;
void Logg_0x711_TxMsgCallback(void)
{
	
	
	data_711 = (uint8_t*)MotorControl_0x711_TxMsgCallback();
	for (i_711 = ZERO; i_711 < EIGHT; i_711++)
		{
			Can_Applidata_St_711.DataBytes_au8[i_711] = data_711[i_711];
		}
 	Can_Applidata_St_711.DataLength_u8 = EIGHT;
	Can_Applidata_St_711.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x711_TX_E, Can_Applidata_St_711);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x711,(char_t*)&data_711[0]);	
}

uint8_t  *data_712,i_712;
CAN_MessageFrame_St_t Can_Applidata_St_712;
void Logg_0x712_TxMsgCallback(void)
{
	
	data_712 = (uint8_t*)MotorControl_0x712_TxMsgCallback();
	for (i_712 = ZERO; i_712 < EIGHT; i_712++)
		{
			Can_Applidata_St_712.DataBytes_au8[i_712] = data_712[i_712];
		}
	Can_Applidata_St_712.DataLength_u8 = EIGHT;
	Can_Applidata_St_712.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x712_TX_E, Can_Applidata_St_712);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x712,(char_t*)&data_712[0]);	
}


Data_713_t Data_713_struture;
uint8_t  data_713[8],i_713;
	CAN_MessageFrame_St_t Can_Applidata_St; 
void Logg_0x713_TxMsgCallback(void)
{

	Data_713_struture = Update_0x713_struture();
	Serialize_Data_713(&Data_713_struture, data_713 );
	for (i_713 = ZERO; i_713 < EIGHT; i_713++)
		{
			Can_Applidata_St.DataBytes_au8[i_713] = data_713[i_713];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x713_TX_E, Can_Applidata_St);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x713,(char_t*)&data_713[0]);	
}


Data_715_t Data_715_struture;
uint8_t  data_715[8],i_715;
CAN_MessageFrame_St_t Can_Applidata_St_715;
void Logg_0x715_TxMsgCallback(void)
{
	
	Data_715_struture = Update_0x715_struture();
	Serialize_Data_715(&Data_715_struture, data_715 );
	for (i_715 = ZERO; i_715 < EIGHT; i_715++)
	{
		Can_Applidata_St_715.DataBytes_au8[i_715] = data_715[i_715];
	}
	Can_Applidata_St_715.DataLength_u8 = EIGHT;
	Can_Applidata_St_715.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x715_TX_E, Can_Applidata_St_715);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x715,(char_t*)&data_715[0]);	
}


	Data_716_t Data_716_struture;
	uint8_t  data_716[8],i_716;
	CAN_MessageFrame_St_t Can_Applidata_St_716;
void Logg_0x716_TxMsgCallback(void)
{

	Data_716_struture = Update_0x716_struture();
	Serialize_Data_716(&Data_716_struture, data_716 );
	for (i_716 = ZERO; i_716 < EIGHT; i_716++)
		{
			Can_Applidata_St_716.DataBytes_au8[i_716] = data_716[i_716];
		}
	Can_Applidata_St_716.DataLength_u8 = EIGHT;
	Can_Applidata_St_716.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x716_TX_E, Can_Applidata_St);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x716,(char_t*)&data_716[0]);	
}


    Data_717_t Data_717_struture;
	uint8_t  data_717[8],i_717;
	CAN_MessageFrame_St_t Can_Applidata_St_717;
void Logg_0x717_TxMsgCallback(void)
{
	Data_717_struture = Update_0x717_struture();
	// Serialize_Data_717(&Data_717_struture, data_717 );
	// for (i_717 = ZERO; i_717 < EIGHT; i_717++)
	// 	{
	// 		Can_Applidata_St_717.DataBytes_au8[i_717] = data_717[i_717];
	// 	}
	Can_Applidata_St_717.DataBytes_au8[0] = data_acc_st.accXaxisH;
	Can_Applidata_St_717.DataBytes_au8[1] = data_acc_st.accXaxisL;
	Can_Applidata_St_717.DataBytes_au8[2] = data_acc_st.accYaxisH;
	Can_Applidata_St_717.DataBytes_au8[3] = data_acc_st.accYaxisL;
	Can_Applidata_St_717.DataBytes_au8[4] = data_acc_st.accZaxisH;
	Can_Applidata_St_717.DataBytes_au8[5] = data_acc_st.accZaxisL;
	Can_Applidata_St_717.DataBytes_au8[6] = 0;
	Can_Applidata_St_717.DataBytes_au8[7] = 0;
	Can_Applidata_St_717.DataLength_u8 = EIGHT;
	Can_Applidata_St_717.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x717_TX_E, Can_Applidata_St_717);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x717,(char_t*)&data_717[0]);	
}

Data_718_t Data_718_struture;
uint8_t  data_718[8],i_718;
CAN_MessageFrame_St_t Can_Applidata_St_718;
void Logg_0x718_TxMsgCallback(void)
{
	Data_718_struture = Update_0x718_struture();
	// Serialize_Data_718(&Data_718_struture, data_718 );
	// for (i_718= ZERO; i_718 < EIGHT; i_718++)
	// 	{
	// 		Can_Applidata_St_718.DataBytes_au8[i_718] = data_718[i_718];
	// 	}
    Can_Applidata_St_718.DataBytes_au8[0]    = data_gro_st.gyroXaxisH;
    Can_Applidata_St_718.DataBytes_au8[1]    = data_gro_st.gyroXaxisL;
    Can_Applidata_St_718.DataBytes_au8[2]    = data_gro_st.gyroYaxisH;
    Can_Applidata_St_718.DataBytes_au8[3]    = data_gro_st.gyroYaxisL;
    Can_Applidata_St_718.DataBytes_au8[4]    = data_gro_st.gyroZaxisH;
    Can_Applidata_St_718.DataBytes_au8[5]    = data_gro_st.gyroZaxisL;
    Can_Applidata_St_718.DataBytes_au8[6]   = 0;
    Can_Applidata_St_718.DataBytes_au8[7]   = 0;
	Can_Applidata_St_718.DataLength_u8 = EIGHT;
	Can_Applidata_St_718.MessageType_u8 = STD_E;
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x718,(char_t*)&data_718[0]);	
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x718_TX_E, Can_Applidata_St_718);
}


uint8_t  *data_719,i_719;
CAN_MessageFrame_St_t Can_Applidata_St_719;
void Logg_0x719_TxMsgCallback(void)
{
	
	data_719 = (uint8_t*)MotorControl_0x719_TxMsgCallback();
	for (i_719 = ZERO; i_719 < EIGHT; i_719++)
	{
		Can_Applidata_St_719.DataBytes_au8[i_719] = data_719[i_719];
	}
	Can_Applidata_St_719.DataLength_u8 = EIGHT;
	Can_Applidata_St_719.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x719_TX_E, Can_Applidata_St_719);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x719,(char_t*)&data_719[0]);	
}

uint8_t  *data_720,i_720;
CAN_MessageFrame_St_t Can_Applidata_St_720;
void Logg_0x720_TxMsgCallback(void)
{
	
	
	data_720 = (uint8_t*)MotorControl_0x720_TxMsgCallback();
	for (i_720 = ZERO; i_720 < EIGHT; i_720++)
		{
			Can_Applidata_St_720.DataBytes_au8[i_720] = data_720[i_720];
		}
	Can_Applidata_St_720.DataLength_u8 = EIGHT;
	Can_Applidata_St_720.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x720_TX_E, Can_Applidata_St_720);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x720,(char_t*)&data_720[0]);	
}

Data_721_t Data_721_struture;
uint8_t  data_721[8],i_721;
CAN_MessageFrame_St_t Can_Applidata_St_721;
void Logg_0x721_TxMsgCallback(void)
{
	
	
	Data_721_struture = Update_0x721_struture();
	Serialize_Data_721(&Data_721_struture, data_721 );
	for (i_721 = ZERO;i_721 < EIGHT; i_721++)
		{
			Can_Applidata_St_721.DataBytes_au8[i_721] = data_721[i_721];
		}
	Can_Applidata_St_721.DataLength_u8 = EIGHT;
	Can_Applidata_St_721.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x721_TX_E, Can_Applidata_St_721);
	write_sd_card(RealTimeData_St.Second_u8,RealTimeData_St.Minute_u8,RealTimeData_St.Hour_u8,0x721,(char_t*)&data_721[0]);	
}

/********************************************************EOF***********************************************************/