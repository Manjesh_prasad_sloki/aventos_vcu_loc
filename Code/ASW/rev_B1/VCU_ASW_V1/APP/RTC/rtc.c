/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : rtc.c
|    Project        : VCU
|    Module         : RTC
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the vehicle switch signals 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef RTC_C
#define RTC_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "switch_signals.h"
#include "Communicator.h"
#include "DataAquire.h"
#include "DataBank.h"
#include "rtc.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

rtc_counter_value_t rtc_counter_value;
rtc_counter_value_t rtc_counter_value1;

static uint8_t BCDtoDECIMAL(uint8_t BCD_Num_u8);
static uint8_t DECtoBCD(uint8_t Dec_u8) ;
void RTC_Proc(void)
{
	static uint8_t rtc_counter = 0;
	rtc_counter++;
	if( 0 == (rtc_counter%5))
	{
		R_Config_RTCA0_Get_CounterBufferValue(&rtc_counter_value1);
		NOP();
	}
	

}

void RTC_0x188_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	rtc_counter_value.sec = DECtoBCD(Can_Applidata_St->DataBytes_au8[0]);
	rtc_counter_value.min = DECtoBCD(Can_Applidata_St->DataBytes_au8[1]);
	rtc_counter_value.hour = DECtoBCD(Can_Applidata_St->DataBytes_au8[2]);
	rtc_counter_value.day = DECtoBCD(Can_Applidata_St->DataBytes_au8[3]);
	rtc_counter_value.week = DECtoBCD(Can_Applidata_St->DataBytes_au8[4]);
	rtc_counter_value.month = DECtoBCD(Can_Applidata_St->DataBytes_au8[5]);
	rtc_counter_value.year = DECtoBCD(Can_Applidata_St->DataBytes_au8[6]);
	R_Config_RTCA0_Set_CounterValue(rtc_counter_value);

	NOP();
}

void RTC_0x189_TXMsgCallback()
{ 
	CAN_MessageFrame_St_t Can_Applidata_St;
	Can_Applidata_St.DataBytes_au8[0] =  BCDtoDECIMAL(rtc_counter_value1.sec);
	Can_Applidata_St.DataBytes_au8[1] =  BCDtoDECIMAL(rtc_counter_value1.min);
	Can_Applidata_St.DataBytes_au8[2] =  BCDtoDECIMAL(rtc_counter_value1.hour);
	Can_Applidata_St.DataBytes_au8[3] =  BCDtoDECIMAL(rtc_counter_value1.day);
	Can_Applidata_St.DataBytes_au8[4] =  BCDtoDECIMAL(rtc_counter_value1.week);
	Can_Applidata_St.DataBytes_au8[5] =  BCDtoDECIMAL(rtc_counter_value1.month);
	Can_Applidata_St.DataBytes_au8[6] =  BCDtoDECIMAL(rtc_counter_value1.year);
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	CIL_CAN_Tx_AckMsg( CIL_RTC_0x189_TX_E, Can_Applidata_St);
	//CAN0_Transmit(0x123,8,&tx_buff[0]);
}

uint8_t BCDtoDECIMAL(uint8_t BCD_Num_u8)
{
    uint8_t Decimal_Num_u8 = 0;
    Decimal_Num_u8 = ((BCD_Num_u8>>4)) *10;
    Decimal_Num_u8 = Decimal_Num_u8 + (BCD_Num_u8&0x0f);
    return Decimal_Num_u8;
}

uint8_t DECtoBCD(uint8_t Dec_u8) 
{
    uint8_t BcdValue_u8 = 0;
    BcdValue_u8 = ((Dec_u8/10) << 4);
    BcdValue_u8 = (BcdValue_u8 | (Dec_u8%10));
    return BcdValue_u8;
}
#endif /* RTC_C */
	   /*---------------------- End of File -----------------------------------------*/