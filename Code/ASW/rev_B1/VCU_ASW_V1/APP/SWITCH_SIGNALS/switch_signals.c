/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : switch_signals.c
|    Project        : VCU
|    Module         : Switch Signals 
|    Description    : This file contains the variables and functions to         
|                     initialize and Operate the vehicle switch signals 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SWITCH_SIGNALS_C
#define SWITCH_SIGNALS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "switch_signals.h"
#include "Communicator.h"
#include "DataAquire.h"
#include "DataBank.h"
#include "r_cg_port.h"
#include "app_typedef.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/
#define TURNO_STATE          (0)
#define ADC_READ_STATE       (1)
#define INDICATOR_WRK        (2)
#define  INDICATOR_NOT_WRK   (3)
#define INDICATOR_CUT_OFF    (80)

#define LEFT_IND_ST      (0x10)
#define RIGHT_IND_ST     (0x01)
#define LEFT_RIGHT_ST    (0x11)
#define LEFT_RIGHT_OFF    (0x00)
#define WRK_BLINK_SPEED   (330)

#define LEFT_INDICATOR (0)
#define RIGHT_INDICATOR (1)
#define LEFT_RIGHT_INDICATOR (2)
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static Turn_RunTime_St_t Turn_RunTime_St;
static Break_RunTime_St_t Break_RunTime_St;
static Light_RunTime_St_t Light_RunTime_St;
static Kill_RunTime_St_t Kill_RunTime_St;
static uint8_t time_capture_falg_Lt = false;
static uint32_t time_capture_variable_Lt = 0;

static uint8_t time_capture_falg_Rt = false;
static uint32_t time_capture_variable_Rt = 0;

static uint8_t time_capture_falg_RLt = false;
static uint32_t time_capture_variable_RLt = 0;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Break_StateMachine_proc
*   Description   : This function implements Vehicle Break Event operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Break_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Kill_StateMachine_proc
*   Description   : This function implements Vehicle Kill State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Kill_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleTurn_StateMachine_proc
*   Description   : This function implements Vehicle Turn State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VehicleTurn_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleLights_StateMachine_proc
*   Description   : This function implements Vehicle Lights State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VehicleLights_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : B_Low_StateMachine_proc
*   Description   : This function implements Vehicle LOW BATTERY State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void B_Low_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Regen_StateMachine_Proc
*   Description   : This function implements Vehicle ReGen State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void Regen_StateMachine_Proc(void);
static void Kick_standMachine_proc(void);

static void blink_indicator(uint8_t indicator );
static void blink_indicator_fast(uint8_t indicator );
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Init
*   Description   : This function implements Switch Signals initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void SwitchSignals_Init(void)
{
	Break_RunTime_St.BreakSwitch_Event_En = BREAK_SWITCH_LOW_EVENT_E;
	Break_RunTime_St.BreakSwitch_State_En = BREAK_SWITCH_LOW_STATE_E;
	SET_BREAK_STATUS(false);
	Turn_RunTime_St.LeftTurnSwitch_State_En = LEFT_TURN_SWITCH_OFF_STATE_E;
	Turn_RunTime_St.RightTurnSwitch_State_En = RIGHT_TURN_SWITCH_OFF_STATE_E;
	Turn_RunTime_St.RightTurnSwitch_Event_En = RIGHT_TURN_SWITCH_OFF_EVENT_E;
	Turn_RunTime_St.LeftTurnSwitch_Event_En = LEFT_TURN_SWITCH_OFF_EVENT_E;
	Light_RunTime_St.HeadLight_State_En = LOW_BEAM_ON_STATE_E;
	Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
	Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
	Light_RunTime_St.LightSwitch_State_En = LIGHT_SWITCH_OFF_STATE_E;
	Light_RunTime_St.HLB_FirstEntry_b = true;
	Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_ON_EVENT_E;
	Kill_RunTime_St.KillSwitch_State_En = KILL_SWITCH_ON_STATE_E;
	Kill_RunTime_St.Kill_FirstEntry_b = true;
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchSignals_Proc
*   Description   : This function implements Switch Signals Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void SwitchSignals_Proc(void)
{
	Break_StateMachine_proc();
	VehicleTurn_StateMachine_proc();
	VehicleLights_StateMachine_proc();
	Kill_StateMachine_proc();
	B_Low_StateMachine_proc();
	Regen_StateMachine_Proc();
	Kick_standMachine_proc();
	EnterSafemodeCheck();

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Break_StateMachine_proc
*   Description   : This function implements Vehicle Break State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Break_StateMachine_proc(void)
{
	BreakSwitch_State_En_t BreakSwitch_State_En = (BreakSwitch_State_En_t)(UserInputSig_St.Brake_SwitchState_Front_u8); 
							/*| UserInputSig_St.Brake_SwitchState_Back_u8)*/;

	Break_RunTime_St.BreakSwitch_State_En = BreakSwitch_State_En;

	switch (Break_RunTime_St.BreakSwitch_State_En)
	{
	case BREAK_SWITCH_LOW_STATE_E:
	{
		if (true == BREAK_STATUS())
		{
			SET_BREAK_STATUS(false);
		}
		/* code */
		break;
	}
	case BREAK_SWITCH_HIGH_STATE_E:
	{
		if (false == BREAK_STATUS())
		{
			SET_BREAK_STATUS(true);
		}
		/* code */
		break;
	}
	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Kill_StateMachine_proc
*   Description   : This function implements Vehicle Kill State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Kill_StateMachine_proc(void)
{
	KillSwitch_State_En_t KillSwitch_State_En = (KillSwitch_State_En_t)UserInputSig_St.Kill_SwitchState_u8;
	uint8_t ActiveMode_u8;
	Kill_RunTime_St.KillSwitch_State_En = KillSwitch_State_En;
	

	switch (Kill_RunTime_St.KillSwitch_State_En)
	{
	case KILL_SWITCH_ON_STATE_E:
	{
		if ((KILL_SWITCH_OFF_EVENT_E == Kill_RunTime_St.KillSwitch_Event_En) ||
			(true == Kill_RunTime_St.Kill_FirstEntry_b))
		{
			Kill_RunTime_St.Kill_FirstEntry_b = false;
			Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_ON_EVENT_E;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, THREE);
		//	VcuOutputs_St.TorqueZero_u8 = TURNED_ON_E;
		}
		else
		{
			/* code */
		}

		break;
	}
	
	case KILL_SWITCH_OFF_STATE_E:
	{
		if ((KILL_SWITCH_ON_EVENT_E == Kill_RunTime_St.KillSwitch_Event_En) ||
			(true == Kill_RunTime_St.Kill_FirstEntry_b))
		{
			Kill_RunTime_St.Kill_FirstEntry_b = false;
			Kill_RunTime_St.KillSwitch_Event_En = KILL_SWITCH_OFF_EVENT_E;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, ZERO);
			//VcuOutputs_St.TorqueZero_u8 = TURNED_OFF_E;
		}
		else
		{
			/* code */
		}
		
		break;
	}
	default:
	{
		break;
	}
	
	}

	if(KILL_SWITCH_ON_EVENT_E == Kill_RunTime_St.KillSwitch_Event_En)
	{
		if (TEN >= Get_Vehicle_Speed())
		{
			
			if(ZERO == Get_SafeMode_Data())
			{
				ActiveMode_u8 = Get_Actual_Ride_Mode();
				if(ActiveMode_u8 == SAFE_MODE)
				{
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
					NOP();
				}
			}
			Clear_Drive_Mode(NEUTRAL_MODE_E);
		}
	}
}


 void Kick_standMachine_proc(void)
 {
	Vechile_KickStand_En_t Vechile_KickStand_En = (Vechile_KickStand_En_t) UserInputSig_St.KickStand_SwitchState_u8;
	//KillSwitch_State_En_t KillSwitch_State_En = (KillSwitch_State_En_t)UserInputSig_St.Kill_SwitchState_u8;
	uint8_t ActiveMode_u8;
	if((TURNED_ON_E == Vechile_KickStand_En )/*|| (TURNED_ON_E == KillSwitch_State_En)*/)
	{
		//VcuOutputs_St.TorqueZero_u8 = TURNED_OFF_E;
		if (TEN >= Get_Vehicle_Speed())
		{
			if(ZERO == Get_SafeMode_Data())
			{
				ActiveMode_u8 = Get_Actual_Ride_Mode();
				if(ActiveMode_u8 == SAFE_MODE)
				{
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
				}
			}
			Clear_Drive_Mode(NEUTRAL_MODE_E);
		}
	}
 }


   void EnterSafemodeCheck(void)
  {
	static uint32_t counter;
	if((NEUTRAL == Get_Actual_Ride_Mode())||(SAFE_MODE == Get_Actual_Ride_Mode()))
	{
			if (Get_BMS_SOC() <= LOWSOCCUTOFF)
			{
				if ((UserInputSig_St.ReGen_SwitchState_u8 == TURNED_ON_E) && (UserInputSig_St.Brake_SwitchState_Front_u8 == TURNED_ON_E))
				{
					if (GET_VCU_TIME_MS() >= counter)
					{
						if(TURNED_ON_E == UserInputSig_St.Safe_mode_Entery_exit_u8 )
						{
							UserInputSig_St.Safe_mode_Entery_exit_u8 = TURNED_OFF_E;
							counter = GET_VCU_TIME_MS() + 2000;
						}
						else if(TURNED_OFF_E == UserInputSig_St.Safe_mode_Entery_exit_u8  )
						{
							UserInputSig_St.Safe_mode_Entery_exit_u8 = TURNED_ON_E;
							counter = GET_VCU_TIME_MS() + 2000;
						}
					}
				}
				else
				{
					counter = GET_VCU_TIME_MS() + 2000;
				}
			}
			else
			{
				counter = GET_VCU_TIME_MS() + 2000;
			}
	}
	else
	{
		counter = GET_VCU_TIME_MS() + 2000;
	}
	return;
  }
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleTurn_StateMachine_proc
*   Description   : This function implements Vehicle Turn State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/

void VehicleTurn_StateMachine_proc(void)
{
	
	static uint8_t counter_rt = 0;
	static uint8_t counter_lft = 0;
	static uint8_t counter_rlt = 0;
	static uint8_t  temp_RTstate_machine = 0;
	static uint8_t  temp_LTstate_machine = 0;
	static uint8_t  temp_LTRTstate_machine = 0;
	static uint8_t   indicator_u8= 0;
	static uint8_t   state_counter_u8= 0;
	static uint8_t   state_counter_1_u8= 0;
	
	RightTurnSwitch_State_En_t RightTurnSwitch_State_En =
					(RightTurnSwitch_State_En_t)UserInputSig_St.RightTurn_SwitchState_u8;
	LeftTurnSwitch_State_En_t LeftTurnSwitch_State_En = 
					(LeftTurnSwitch_State_En_t)UserInputSig_St.LeftTurn_SwitchState_u8;

	Turn_RunTime_St.RightTurnSwitch_State_En = RightTurnSwitch_State_En;
	Turn_RunTime_St.LeftTurnSwitch_State_En = LeftTurnSwitch_State_En;

 	
	if((Get_Actual_Ride_Mode() == REVERSE) ||(UserInputSig_St.Hazard_SwitchState_u8 == TURNED_ON_E ))
	{
		Turn_RunTime_St.RightTurnSwitch_State_En = RIGHT_TURN_SWITCH_ON_STATE_E;
		Turn_RunTime_St.LeftTurnSwitch_State_En  = LEFT_TURN_SWITCH_ON_STATE_E;
	}

	if(Turn_RunTime_St.RightTurnSwitch_State_En)
	{
		indicator_u8 |= 0x01;
	}
	else
	{
		indicator_u8 &= ~(0x01);
		temp_RTstate_machine =  TURNO_STATE;
		VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
		SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
		
	}
	if(Turn_RunTime_St.LeftTurnSwitch_State_En )
	{
		indicator_u8 |= 0x10;
	}
	else
	{
		indicator_u8 &= ~(0x10);
		temp_LTstate_machine =  TURNO_STATE;
		VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
		SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
	}
	if((!Turn_RunTime_St.RightTurnSwitch_State_En)  && (!Turn_RunTime_St.LeftTurnSwitch_State_En))
	{
		indicator_u8 = 0x00;
	}
	



	switch(indicator_u8)
	{
	  
		case LEFT_IND_ST :
		{
			switch(temp_LTstate_machine)
			{
				case TURNO_STATE:
				{
					counter_lft++;
					SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
					VcuOutputs_St.LeftIndLight_u8 = TURNED_ON_E;
					if(counter_lft == 3)
					{
						temp_LTstate_machine = ADC_READ_STATE;
						counter_lft = 0;
					}
					break;
				}
				case ADC_READ_STATE:
				{
					if(UserInputSig_St.LeftTurn_FeedBack_u16 >  INDICATOR_CUT_OFF)
					{
						temp_LTstate_machine = INDICATOR_WRK;
					}
					else
					{
						temp_LTstate_machine = INDICATOR_NOT_WRK;
					}
					break;
				}
				case INDICATOR_WRK:
				{
					if(false == time_capture_falg_Lt)
					{
						time_capture_falg_Lt = true;
						time_capture_variable_Lt = (GET_VCU_TIME_MS() + WRK_BLINK_SPEED);
					}
					blink_indicator(LEFT_INDICATOR);
					if(TURNED_ON_E == VcuOutputs_St.LeftIndLight_u8)
					{
						if (UserInputSig_St.LeftTurn_FeedBack_u16 < INDICATOR_CUT_OFF)
						{
							state_counter_u8++;
							if (state_counter_u8 >= 3)
							{
								temp_LTstate_machine = INDICATOR_NOT_WRK;
								state_counter_u8 = 0;
							}
						}
						else
						{
							state_counter_u8 = 0;
						}
					}
					break;
				}
				case INDICATOR_NOT_WRK:
				{
					if(false == time_capture_falg_Lt)
					{
						time_capture_falg_Lt = true;
						time_capture_variable_Lt = (GET_VCU_TIME_MS() + WRK_BLINK_SPEED);
					}
					blink_indicator_fast(LEFT_INDICATOR);
					if(TURNED_ON_E == VcuOutputs_St.LeftIndLight_u8)
					{
						if (UserInputSig_St.LeftTurn_FeedBack_u16 > INDICATOR_CUT_OFF)
						{
							temp_LTstate_machine = INDICATOR_WRK;
						}
					}
					break;
				}
			}
			break;
		}
  
		case RIGHT_IND_ST:
		{
			switch(temp_RTstate_machine)
		 	{
		 		case TURNO_STATE:
		 		{
		 			counter_rt++;
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
		 			if(counter_rt == 3)
		 			{
		 				temp_RTstate_machine = ADC_READ_STATE;
		 				counter_rt = 0;
		 			}
		 			break;
		 		}
		 		case ADC_READ_STATE:
		 		{
					
		 			if(UserInputSig_St.RightTurn_FeedBack_u16 >  INDICATOR_CUT_OFF)
					{
						temp_RTstate_machine = INDICATOR_WRK;
					}
					else
					{
						temp_RTstate_machine = INDICATOR_NOT_WRK;
					}
					break;
				}
				case INDICATOR_WRK:
				{
					if(false == time_capture_falg_Rt)
					{
						time_capture_falg_Rt = true;
						time_capture_variable_Rt = (GET_VCU_TIME_MS() + WRK_BLINK_SPEED);
					}
		 			blink_indicator(RIGHT_INDICATOR);
		 			if(TURNED_ON_E ==  VcuOutputs_St.RightIndLight_u8)
		 			{
						if (UserInputSig_St.RightTurn_FeedBack_u16 < INDICATOR_CUT_OFF)
						{
							state_counter_1_u8++;
							if (state_counter_1_u8 >= 3)
							{
								temp_RTstate_machine = INDICATOR_NOT_WRK;
								NOP();
								state_counter_1_u8 = 0;
							}
						}
						else
						{
							state_counter_1_u8 = 0;
						}
		 			}
		 			break;
		 		}
				case INDICATOR_NOT_WRK:
		 		{
						
					if(false == time_capture_falg_Rt)
					{
						time_capture_falg_Rt = true;
						time_capture_variable_Rt = (GET_VCU_TIME_MS() + WRK_BLINK_SPEED);
					}
					blink_indicator_fast(RIGHT_INDICATOR);
					if(TURNED_ON_E ==  VcuOutputs_St.RightIndLight_u8)
					{
						if(UserInputSig_St.RightTurn_FeedBack_u16 > INDICATOR_CUT_OFF)
						{
							temp_RTstate_machine = INDICATOR_WRK;
						}
					}
					break;
				}
			}	
			break;
		}
		case LEFT_RIGHT_ST :
		{
			
			switch (temp_LTRTstate_machine)
			{
				case TURNO_STATE:
				{
					counter_rlt++;
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
					if(counter_rlt >= 3)
					{
						temp_LTRTstate_machine = ADC_READ_STATE;
						counter_rlt = 0;
					}
					break;
				}
				case ADC_READ_STATE:
				{
					temp_LTRTstate_machine = INDICATOR_WRK;
					break;
				}
				case INDICATOR_WRK:
				{
					if(false == time_capture_falg_RLt)
					{
						time_capture_falg_RLt = true;
						time_capture_variable_RLt = (GET_VCU_TIME_MS() + WRK_BLINK_SPEED);
					}
					blink_indicator(LEFT_RIGHT_INDICATOR);
					break;
				}
			}
			break;
		}
		
		case LEFT_RIGHT_OFF:
		{
			temp_RTstate_machine =  TURNO_STATE;
			temp_LTstate_machine =  TURNO_STATE;
			VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
			VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = DEACTIVATE;
			if( TURNED_ON_E != VcuOutputs_St.modechange_b )
			{
				VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
			}
			
			SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);	
			SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
			break;
		}
	}	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VehicleLights_StateMachine_proc
*   Description   : This function implements Vehicle Lights State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VehicleLights_StateMachine_proc(void)
{
	Vehicle_State_En_t Vehicle_State_En = GET_VCU_STATE();
    LightSwitch_State_En_t LightSwitch_State_En =  (LightSwitch_State_En_t)LIGHT_SWITCH_ON_STATE_E;// Manjesh changed
	HeadLight_State_En_t HeadLight_State_En =
		(HeadLight_State_En_t)UserInputSig_St.HighBeam_SwitchState_u8;

	Light_RunTime_St.HeadLight_State_En = HeadLight_State_En;
	Light_RunTime_St.LightSwitch_State_En = LightSwitch_State_En;
 
	if((Vehicle_State_En == VEHICLE_START_STATE_E) ||
			(Vehicle_State_En == VEHICLE_RUN_STATE_E) ||
						(Vehicle_State_En == VEHICLE_SAFE_MODE_STATE_E))
	{
		switch (Light_RunTime_St.HeadLight_State_En)
		{
		case LOW_BEAM_ON_STATE_E:
		{
			if ((LIGHT_SWITCH_ON_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
				((HIGH_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En) || (true == Light_RunTime_St.HLB_FirstEntry_b)))
			{
				Light_RunTime_St.HLB_FirstEntry_b = false;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, THREE);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_ON_EVENT_E;
				//Todo: VCU must transmit this information via CAN to the HMI in
				//order to illuminate the “Headlight” icon in the HMI.
			}
			else if ((LIGHT_SWITCH_OFF_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
					 (LOW_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En)&&
							(LIGHT_SWITCH_ON_EVENT_E == Light_RunTime_St.LightSwitch_Event_En))
			{
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = LOW_BEAM_ON_EVENT_E;
				Light_RunTime_St.HLB_FirstEntry_b = true;
			}
			else
			{
				;/* code */
			}

			/* code */
			break;
		}
		case HIGH_BEAM_ON_STATE_E:
		{
			if ((LIGHT_SWITCH_ON_STATE_E == Light_RunTime_St.LightSwitch_State_En) &&
				((LOW_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En)||((true == Light_RunTime_St.HLB_FirstEntry_b))))
			{
				Light_RunTime_St.HLB_FirstEntry_b = false;
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, THREE);
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = HIGH_BEAM_ON_EVENT_E;
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_ON_EVENT_E;
				//Todo: VCU must transmit this information via CAN to the HMI in
				//order to illuminate the “Headlight” icon in the HMI.
			}
			else if ((LIGHT_SWITCH_OFF_STATE_E == Light_RunTime_St.LightSwitch_State_En)&&
					 (HIGH_BEAM_ON_EVENT_E == Light_RunTime_St.HeadLight_Event_En) &&
							(LIGHT_SWITCH_ON_EVENT_E == Light_RunTime_St.LightSwitch_Event_En))
			{
				Light_RunTime_St.LightSwitch_Event_En = LIGHT_SWITCH_OFF_EVENT_E;
				SET_CLUSTER_DATA(CLUSTER_LOW_BEAM_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_HIGH_BEAM_E, ZERO);
				Light_RunTime_St.HeadLight_Event_En = HIGH_BEAM_ON_EVENT_E;
				Light_RunTime_St.HLB_FirstEntry_b = true;
			}

			/* code */
			break;
		}
		default:
		{
			break;
		}
		}
	}
}


void B_Low_StateMachine_proc()
{
	B_LOW_Switch_State_En_t B_LOW_Switch_State_En = (B_LOW_Switch_State_En_t)UserInputSig_St.B_Low_SwitchState_u8;
	switch(B_LOW_Switch_State_En)
	{
		case VEHICLE_SAFE_MODE_OFF_E:
		{ 
			break;
		}
		case VEHICLE_SAFE_MODE_ON_E :
		{
		//	if (DRIVER_NEUTRAL_MODE_RUN_STATE_E == Get_DriverMode())
			{
				//if (Get_SOC() <= 20)
				{
				//	SafeMode_SetEvent(VEHICLE_   _E,VEHICLE_SAFE_MODE_ON_E);
				}
			}
			break;
		}
			default :
			{
				; 
			}
	}
	
	
}
 void Regen_StateMachine_Proc()
 {
	
	
}
 


 

 void blink_indicator(uint8_t indicator_u8 )
 {
	 static uint8_t counter_u8 = 0;
	 counter_u8++;

	 switch(indicator_u8)
	 {
		case LEFT_INDICATOR:
		{
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
			
			if(time_capture_variable_Lt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_Lt = false;
				if(TURNED_ON_E == VcuOutputs_St.LeftIndLight_u8)
				{
					VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
				}
				else
				{
					VcuOutputs_St.LeftIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, THREE);
				}
			}
			break;
		}
		case RIGHT_INDICATOR:
		 {
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
		 	if(time_capture_variable_Rt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_Rt = false;
				if (TURNED_ON_E ==  VcuOutputs_St.RightIndLight_u8)
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8  = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
					SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
				}
				else
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8  = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
					SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, THREE);
				}
		 	}
			break;
		}
		case LEFT_RIGHT_INDICATOR:
		{
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
			if(time_capture_variable_RLt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_RLt = false;
				if(TURNED_ON_E == VcuOutputs_St.RightIndLight_u8)
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
					SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
					
				}
				else
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.LeftIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
					SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, THREE);
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, THREE);
				}
			}
			break;
		}
	 }

	 
 }
 
 	
      
  
  void blink_indicator_fast(uint8_t indicator_u8 )
 {
	 static uint8_t cluster_counter_u8 = 0;
	 static uint8_t temp_data = 0;
	  switch(indicator_u8)
	 {
		case LEFT_INDICATOR:
		{
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
			if(time_capture_variable_Lt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_Lt = false;
				if(TURNED_ON_E == VcuOutputs_St.LeftIndLight_u8)
				{
					VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
					NOP();
				}
				else
				{
					VcuOutputs_St.LeftIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
					SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, THREE);
					NOP();
				}
				cluster_counter_u8++;
			}
			if (cluster_counter_u8 >= 2)
			{
				cluster_counter_u8 = 0;
				if (temp_data)
				{
				SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
				temp_data = 0;
				NOP();
				}
				else
				{
				SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, THREE);
				temp_data = 1;
				NOP();
				}
			}
			break;
		}
		case RIGHT_INDICATOR:
		{
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
			if(time_capture_variable_Rt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_Rt = false;
				if (TURNED_ON_E == VcuOutputs_St.RightIndLight_u8)
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
				}
				else
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
				}
				cluster_counter_u8++;
			}
			if (cluster_counter_u8 >= 2)
			{
				cluster_counter_u8 = 0;
				if (temp_data)
				{
				SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
				temp_data = 0;
				}
				else
				{
				SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, THREE);
				temp_data = 1;
				}
			}
			break;
		}
		case LEFT_RIGHT_INDICATOR:
		{
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].BuzzStatus_En = ACTIVATE;
			if(time_capture_variable_RLt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_RLt = false;
				if (TURNED_ON_E ==  VcuOutputs_St.LeftIndLight_u8)
				{
					 VcuOutputs_St.LeftIndLight_u8 = TURNED_OFF_E;
					 VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
				}
				else
				{
					 VcuOutputs_St.LeftIndLight_u8 = TURNED_ON_E;
					 VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
				}
			}
			if(time_capture_variable_RLt <= GET_VCU_TIME_MS() )
			{
				time_capture_falg_RLt = false;
				if (TURNED_ON_E == VcuOutputs_St.RightIndLight_u8)
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_OFF_E;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false ;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true ;
				}
				else
				{
					VcuOutputs_St.RightIndLight_u8 = TURNED_ON_E;
					VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
					Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
				}

				cluster_counter_u8++;
			}

			if (cluster_counter_u8 >= 2)
			{
				cluster_counter_u8 = 0;
				if (temp_data)
				{
				SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, ZERO);
				temp_data = 0;
				}
				else
				{
				SET_CLUSTER_DATA(CLUSTER_LEFT_INDICATOR_E, THREE);
				SET_CLUSTER_DATA(CLUSTER_RIGHT_INDICATOR_E, THREE);
				temp_data = 1;
				}
			}
			break;
		}
	 }
	 
 }
 
#endif /* SWITCH_SIGNALS_C */
	   /*---------------------- End of File -----------------------------------------*/