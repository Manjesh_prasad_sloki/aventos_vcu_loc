 /*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : driver_modes.c
|    Project        : VCU
|    Module         : driver modes
|    Description    : This file contains the variables and functions to
|                     initialize and Operate the vehicle driver mode
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 15/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef DRIVER_MODES_C
#define DRIVER_MODES_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "diag_typedefs.h"
#include "r_cg_macrodriver.h"
#include "driver_modes.h"
#include "Communicator.h"
#include "DataBank.h"
#include "DataAquire.h"
#include "r_cg_port.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/
//  #define FALSE 0
//  #define TRUE  1
#define IDLE_CHECK_ENABLE true

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static DriverMode_RunTime_St_t DriverMode_RunTime_St;
uint32_t VehicleIdleTime_u32 = 0;
bool sound_modechange = 0;
//uint8_t count=0,safetemp=1;
//uint8_t bat_temp=45;
void VehicleIdleCheck(void);
//void temp_bms_safemode(void);
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
 *  FUNCTION DECLERATION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : DriverMode_StateMachine_proc
 *   Description   : This function implements Driver Mode State operation.
 *   Parameters    : None
 *   Return Value  : None
 *******************************************************************************/
//static void DriverMode_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
 *  FUNCTION DECLERATION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : Cluster_Driver_Mode_Set
 *   Description   : This function Sets Driver Mode in the HMI(cluster).
 *   Parameters    : uint8_t Driver_Mode_Data_u8
 *   Return Value  : None
 *******************************************************************************/
static void Cluster_Driver_Mode_Set(uint8_t Driver_Mode_Data_u8);
static void buzeer_bing(void);
/* -----------------------------------------------------------------------------
 *  FUNCTION DECLERATION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : Cluster_Driver_Mode_Move
 *   Description   : This function Gets Driver Mode movement (joystic)
 *					in the HMI(cluster).
 *   Parameters    : uint8_t Driver_Mode_Data_u8
 *   Return Value  : None
 *******************************************************************************/
//static void Cluster_Driver_Mode_Move(uint8_t Driver_Mode_Data_u8);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
 *  FUNCTION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : DriverMode_Init
 *   Description   : This function implements Driver Mode initialisation.
 *   Parameters    : None
 *   Return Value  : None
 *******************************************************************************/
void DriverMode_Init(void)
{
	DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
	DriverMode_RunTime_St.SafeMode_TurnOn_b = false;
	DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
	DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
	DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
	DriverMode_RunTime_St.DriverMode_State_En = DRIVER_IDLE_MODE_STATE_E;
	DriverMode_RunTime_St.SafeMode_data_u32 = 0;
	VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
	Clear_Drive_Mode(NEUTRAL_MODE_E);
	Set_Ride_mode_Request(NEUTRAL);
	Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_TOGGLE | REVERSE_TOGGLE);
	SafeMode_ClearEvent();
	/*Add other module Initialisation*/
	//temp_bms_safemode();
}
/* -----------------------------------------------------------------------------
 *  FUNCTION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : DriverMode_Proc
 *   Description   : This function implements Driver Mode Scheduler.
 *   Parameters    : None
 *   Return Value  : None
 *******************************************************************************/
void DriverMode_Proc(void)
{
	//DriverMode_StateMachine_proc();
	Driver_machine_state();
	/*Add other module Initialisation*/
}

/***********************************new implementation ******************************************************/
driving_current_Other_st driving_Neutral_Other[DRI_TOTAL_MODE_E]=
{
	{DRI_REQ_MODE_NEUTRAL_E, true,BRAKE_CHECK_FALSE,    SPEED_CHECK_FALSE, SPEED_CHECK_FALSE,  DRIVER_MODE_TRANSITION_TIME },
	{DRI_REQ_MODE_ECO_E,	 true,BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,  SPEED_5kmh,        DRIVER_MODE_TRANSITION_TIME },
	{DRI_REQ_MODE_SPORTS_E,   true,BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,  SPEED_5kmh,        DRIVER_MODE_TRANSITION_TIME },
	{DRI_REQ_MODE_REVERSE_E, true,BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,  SPEED_5kmh,        DRIVER_MODE_TRANSITION_TIME },
	{DRI_REQ_MODE_SAFE_E,	 true,BRAKE_CHECK_TRUE,     SPEED_CHECK_TRUE,   SPEED_5kmh,        DRIVER_MODE_TRANSITION_TIME}
};

driving_current_Other_st driving_ECO_Other[DRI_TOTAL_MODE_E]=
{
	{DRI_REQ_MODE_NEUTRAL_E,	 true, BRAKE_CHECK_FALSE,    SPEED_CHECK_TRUE,   SPEED_10kmh,       DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_ECO_E,	 	 true, BRAKE_CHECK_FALSE,	SPEED_CHECK_FALSE,  SPEED_CHECK_FALSE, DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SPORTS_E,      true, BRAKE_CHECK_FALSE,	SPEED_CHECK_FALSE,  SPEED_CHECK_FALSE, DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_REVERSE_E,     true, BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,   SPEED_5kmh,        DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SAFE_E,		 true, BRAKE_CHECK_FALSE,    SPEED_CHECK_TRUE,  SPEED_25kmh,        DRIVER_MODE_TRANSITION_TIME}
};

driving_current_Other_st driving_Sport_Other[DRI_TOTAL_MODE_E]=
{
	{DRI_REQ_MODE_NEUTRAL_E,	 true, BRAKE_CHECK_FALSE,    SPEED_CHECK_TRUE,   SPEED_10kmh,        DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_ECO_E,		 true, BRAKE_CHECK_FALSE,	SPEED_CHECK_FALSE,   SPEED_CHECK_FALSE,        DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SPORTS_E,      true, BRAKE_CHECK_FALSE,	SPEED_CHECK_FALSE,  SPEED_CHECK_FALSE,  DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_REVERSE_E,     true, BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,   SPEED_5kmh,         DRIVER_MODE_TRANSITION_TIME},
    {DRI_REQ_MODE_SAFE_E,	     true, BRAKE_CHECK_FALSE,     SPEED_CHECK_FALSE, SPEED_CHECK_FALSE,      DRIVER_MODE_TRANSITION_TIME}
};
driving_current_Other_st driving_Reverse_Other[DRI_TOTAL_MODE_E]=
{
	{DRI_REQ_MODE_NEUTRAL_E,	true,BRAKE_CHECK_FALSE,    	SPEED_CHECK_TRUE,   SPEED_5kmh,           DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_ECO_E,		true,BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,   SPEED_5kmh,           DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SPORTS_E,     true,BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,   SPEED_5kmh,           DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_REVERSE_E,    true,BRAKE_CHECK_FALSE,	SPEED_CHECK_FALSE,  SPEED_CHECK_FALSE,    DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SAFE_E,	    true,BRAKE_CHECK_FALSE,     SPEED_CHECK_FALSE, SPEED_CHECK_FALSE,DRIVER_MODE_TRANSITION_TIME}
};
driving_current_Other_st driving_Safe_Other[DRI_TOTAL_MODE_E]=
{
	{DRI_REQ_MODE_NEUTRAL_E,	false, BRAKE_CHECK_FALSE,    	SPEED_CHECK_TRUE,   SPEED_10kmh,           DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_ECO_E,		true, BRAKE_CHECK_FALSE,		SPEED_CHECK_FALSE,  SPEED_CHECK_FALSE,   DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SPORTS_E,     false, BRAKE_CHECK_TRUE,		SPEED_CHECK_TRUE,   SPEED_5kmh,           DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_REVERSE_E,    false, BRAKE_CHECK_TRUE,	    SPEED_CHECK_TRUE,   SPEED_5kmh,    		  DRIVER_MODE_TRANSITION_TIME},
	{DRI_REQ_MODE_SAFE_E,	    false, BRAKE_CHECK_TRUE,     SPEED_CHECK_TRUE,    SPEED_5kmh,      DRIVER_MODE_TRANSITION_TIME}
};

 driving_mode_condtion_st driving_mode_condtion[DRI_TOTAL_MODE_E] = 
 {
 	{DRI_CURRENT_MODE_NEUTRAL_E,    driving_Neutral_Other,    &hmi_netural_other},
 	{DRI_CURRENT_MODE_ECO_E,  		driving_ECO_Other,        &hmi_eco_other},	
 	{DRI_CURRENT_MODE_SPORTS_E,  	driving_Sport_Other,      &hmi_sports_other},	
 	{DRI_CURRENT_MODE_REVERSE_E,    driving_Reverse_Other,    &hmi_reverse_other},
	{DRI_CURRENT_MODE_SAFE_E,       driving_Safe_Other,       &hmi_safe_other},
};


RunTimeDriverMode_st RunTimeDriverMode;
uint8_t teting =0;
uint8_t dummySandeep =0;
uint8_t test_new_mode =0;
#ifdef TESTING_USER_MODE_SW
	uint8_t testing_var2_ua8[8];
#endif
void Driver_machine_state(void)
{
	uint8_t index = 0;
	uint8_t temp =0;
	uint8_t actualride_mode_temp =0; 
	static bool safe_entery_flag = 0 ;
	
	#ifdef TESTING_USER_MODE_SW
	testing_var2_ua8[0] = Get_Actual_Ride_Mode();
	testing_var2_ua8[1] = RunTimeDriverMode.CurrentMode;
	testing_var2_ua8[2] = RunTimeDriverMode.NewlyReqMode;
	testing_var2_ua8[3] = 0;
	testing_var2_ua8[4] = 0;
	testing_var2_ua8[5] = 0;
	testing_var2_ua8[6] = 0;
	testing_var2_ua8[7] = 0;
	CAN0_Transmit(0x111,8,&testing_var2_ua8[0]);
	#endif
	buzeer_bing();
	VehicleIdleCheck();
	RunTimeDriverMode.CurrentMode = Get_Actual_Ride_Mode();
	if(TURNED_ON_E == UserInputSig_St.Safe_mode_Entery_exit_u8 )
	{
		Set_Ride_mode_Request(SAFE_MODE);
		Clear_Drive_Mode(SAFE_MODE_E);
		UserInputSig_St.BootUpflag_b = true;
		UserInputSig_St.Neutral_Mode_Sel_u8 = 0;
		UserInputSig_St.Economy_Mode_Sel_u8 = 0;
		UserInputSig_St.Sports_Mode_Sel_u8 = 0;
		UserInputSig_St.Safe_Mode_Sel_u8 = TURN_ON;
		safe_entery_flag = true;
	}
	else if((TURNED_OFF_E == UserInputSig_St.Safe_mode_Entery_exit_u8 ) && (true == safe_entery_flag))
	{
		safe_entery_flag = false;
		Set_Ride_mode_Request(ECO);
		Clear_Drive_Mode(ECONOMY_MODE_E);
	}
	if ((driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[RunTimeDriverMode.NewlyReqMode].transAllflag == false) ||(UserInputSig_St.BmsTempLowerTri_u8 == true))
	{
		
		if(UserInputSig_St.BmsTempLowerTri_u8 == true)
		{
			
			if(SPORT == Get_Actual_Ride_Mode())
			{
				Clear_Drive_Mode(ECONOMY_MODE_E);
				Set_Ride_mode_Request(ECO);
				NOP();
			}
			else if(SPORT != Get_Actual_Ride_Mode())
			{
				SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E,ONE);
				if(UserInputSig_St.Sports_Mode_Sel_u8 == TURNED_ON_E)
				{
					UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
					switch(Get_Actual_Ride_Mode())
					{
						case NEUTRAL:
						{
							UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
							break;
						}
						case ECO :
						{
							UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
							break;	
						}
						case REVERSE :
						{
							UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
							break;	
						}
					}
				}
			}

		}

		else if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[RunTimeDriverMode.NewlyReqMode].transAllflag == false)
		{
			RunTimeDriverMode.NewlyReqMode = RunTimeDriverMode.CurrentMode;
			switch (RunTimeDriverMode.NewlyReqMode)
			{
				case NEUTRAL:
				{
					Clear_Drive_Mode(NEUTRAL_MODE_E);
					break;
				}
				case ECO:
				{
					Clear_Drive_Mode(ECONOMY_MODE_E);
					break;
				}
				case SPORT:
				{
					Clear_Drive_Mode(SPORTS_MODE_E);
					break;
				}
				case REVERSE:
				{
					Clear_Drive_Mode(REVERSE_MODE_E);
					break;
				}
				case SAFE_MODE:
				{
					Clear_Drive_Mode(SAFE_MODE_E);
					break;
				}
			}
		}
	}
	

	if((false == UserInputSig_St.BootUpflag_b )&&(SAFE_MODE != Get_Actual_Ride_Mode()))
	{
		if(UserInputSig_St.Sports_Mode_Sel_u8 == TURNED_ON_E)
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
		}
	}
		
	if (((ZERO < Get_SafeMode_Data()) || (TRUE == RunTimeDriverMode.Safe_mode_flag) /*|| (SAFE_MODE == RunTimeDriverMode.CurrentMode)*/) )
	{
		UserInputSig_St.BootUpflag_b = true;
		safe_mode_func();
	}
	else if ((TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8) &&
		(TURNED_OFF_E == UserInputSig_St.Fall_SwitchState_u8) && (TURNED_OFF_E == UserInputSig_St.Kill_SwitchState_u8 && (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8)))
	{
		
		if (false == RunTimeDriverMode.NewRideModeProcessing_flag) 
		{
			RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_NEUTRAL_E] = UserInputSig_St.Neutral_Mode_Sel_u8;
  			RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_ECO_E] = UserInputSig_St.Economy_Mode_Sel_u8;
			RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_SPORTS_E] = UserInputSig_St.Sports_Mode_Sel_u8;
			RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_REVERSE_E] = UserInputSig_St.Reverse_Mode_Sel_u8;
			
			RunTimeDriverMode.NewlyReqMode = GetNewlyRequestedMode();
			if ((RunTimeDriverMode.NewlyReqMode != Get_Actual_Ride_Mode()) /*&&( 4 != Get_Actual_Ride_Mode())*/)
			{
				RunTimeDriverMode.NewRideModeProcessing_flag = true;
				if(true == RunTimeDriverMode.safeMode_exit_b)
				{
					RunTimeDriverMode.OldMode = ECO;
				}
				else
				{
					RunTimeDriverMode.OldMode = Get_Actual_Ride_Mode();
				}
			}

		}
		
		else if (TRUE == RunTimeDriverMode.NewRideModeProcessing_flag)
		{
			switch (RunTimeDriverMode.NewModeRequestStatus_En)
			{
			case REQ_INIT:
			{
				if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].DriverCurrentMode_En == RunTimeDriverMode.CurrentMode)
				{
					for (index = DRI_REQ_MODE_E; index < DRI_REQ_MODE_END_E; index++)
					{
						if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].DriverRequestedMode_En == RunTimeDriverMode.NewlyReqMode)
						{
							RunTimeDriverMode.CaputuredTime_u32 = (GET_VCU_TIME_MS() + driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].transition_time);
							if ((driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].BrakeCheck_flag == RunTimeDriverMode.BrakeState_flag) ||
								(driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].BrakeCheck_flag == BRAKE_CHECK_FALSE))
							{
								RunTimeDriverMode.NewModeRequestStatus_En = REQ_BRAKE_PASS;
								if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].SpeedCheck_flag == SPEED_CHECK_TRUE)
								{
									if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].speed_u8 >= Get_Vehicle_Speed())
									{
										RunTimeDriverMode.NewModeRequestStatus_En = REQ_SPEED_BRAKE_PASS;
									}
									else
									{
										RunTimeDriverMode.NewModeRequestStatus_En = REQ_SPEED_WAIT;
										RunTimeDriverMode.SpeedConditionCapture = driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].speed_u8;
									}
								}
								else if (driving_mode_condtion[RunTimeDriverMode.CurrentMode].driving_current_Other[index].SpeedCheck_flag == SPEED_CHECK_FALSE)
								{
									RunTimeDriverMode.NewModeRequestStatus_En = REQ_SPEED_BRAKE_PASS;
								}
							}
							else
							{
								RunTimeDriverMode.NewModeRequestStatus_En = REQ_BRAKE_FAIL;
							}
							break;
						}
					}
				}
				break;
			}
			case REQ_BRAKE_FAIL:
			{
				if (RunTimeDriverMode.CaputuredTime_u32 <= (GET_VCU_TIME_MS()))
				{
					RunTimeDriverMode.NewModeRequestStatus_En = REQ_REJECTED;
					RunTimeDriverMode.first_enteryflag = false;
				}
				else if(RunTimeDriverMode.first_enteryflag == false)
				{
					RunTimeDriverMode.first_enteryflag = true;
					actualride_mode_temp = Get_Actual_Ride_Mode();
					switch (actualride_mode_temp)
					{
						case NEUTRAL:
						{
							Clear_Drive_Mode(NEUTRAL_MODE_E);
							break;
						}
						case ECO:
						{
							Clear_Drive_Mode(ECONOMY_MODE_E);
							break;
						}
						case SPORT:
						{
							Clear_Drive_Mode(SPORTS_MODE_E);
							break;
						}
						case REVERSE:
						{
							Clear_Drive_Mode(REVERSE_MODE_E);
							break;
						}
						case SAFE_MODE:
						{
							Clear_Drive_Mode(SAFE_MODE_E);
							break;
						}

					}
				}
				else
				{
					RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_NEUTRAL_E] = UserInputSig_St.Neutral_Mode_Sel_u8;
  					RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_ECO_E] = UserInputSig_St.Economy_Mode_Sel_u8;
					RunTimeDriverMode.ModeChangePinState[DRI_CURRENT_MODE_REVERSE_E] = UserInputSig_St.Reverse_Mode_Sel_u8;
					
					teting = GetNewlyRequestedMode();
					if(teting != Get_Actual_Ride_Mode())
					{
						RunTimeDriverMode.CaputuredTime_u32 =0;
						RunTimeDriverMode.NewRideModeProcessing_flag = false;
						RunTimeDriverMode.first_enteryflag = false;
						RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
					}
				}
				
				break;
			}
			case REQ_SPEED_BRAKE_PASS:
			{
				// HMI blink on newly requested mode and sloid on the current mode;
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_MCU_WAIT;
				Set_Ride_mode_Request(RunTimeDriverMode.NewlyReqMode); 
				break;
			}
			case REQ_SPEED_WAIT:
			{
				if (RunTimeDriverMode.CaputuredTime_u32 <= (GET_VCU_TIME_MS()))
				{
					RunTimeDriverMode.NewModeRequestStatus_En = REQ_REJECTED;
					NOP();
				}
				else if (RunTimeDriverMode.SpeedConditionCapture >= Get_Vehicle_Speed())
				{
					RunTimeDriverMode.NewModeRequestStatus_En = REQ_SPEED_BRAKE_PASS;
				}
				break;
			}
			case REQ_SPEED_FAIL:
			{
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_REJECTED;
				NOP();
				// RunTimeDriverMode.NewRideModeProcessing_flag = false;
				break;
			}

			case REQ_ACCEPTED:
			{
				
				break;
			}
			case REQ_REJECTED:
			{
				
				break;
			}
			case REQ_MCU_WAIT:
			{
				if (RunTimeDriverMode.CaputuredTime_u32 <= (GET_VCU_TIME_MS()))
				{
					RunTimeDriverMode.NewModeRequestStatus_En = REQ_REJECTED;
				}
				else
				{
					if (RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
					{
						RunTimeDriverMode.NewModeRequestStatus_En = REQ_ACCEPTED;
						sound_modechange = true;
					}
				}
				break;
			}
			}
		}
		else if(NEUTRAL== Get_Actual_Ride_Mode())
		{
				EnterSafemodeCheck();
		}
	}
	else if(TURNED_ON_E == UserInputSig_St.Kill_SwitchState_u8 || (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8))
	{
		UserInputSig_St.BootUpflag_b = true;
		temp = Get_Actual_Ride_Mode();
		if (TEN >= Get_Vehicle_Speed())
		{
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			Set_Ride_mode_Request(NEUTRAL);
		}
		if(NEUTRAL == temp)
		{
			Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
		}
		
	}

	if((driving_mode_condtion[RunTimeDriverMode.OldMode].Driver_mode_callback != NULL) && ( (false == RunTimeDriverMode.Safe_mode_flag)))
	{
		driving_mode_condtion[RunTimeDriverMode.OldMode].Driver_mode_callback();
	}
	return;
}

uint8_t ReturnValue_u8;
uint8_t GetNewlyRequestedMode(void)
{
	uint8_t index ;
	for(index = DRI_REQ_MODE_E; index <DRI_REQ_MODE_END_E ; index++)
	{
		if(TURNED_ON_E == RunTimeDriverMode.ModeChangePinState[index])
		{
			
			ReturnValue_u8 = index;
			RunTimeDriverMode.BrakeState_flag = BREAK_STATUS();
			break;
		}
	}
	return ReturnValue_u8;
}

void hmi_netural_other()
{
	switch(RunTimeDriverMode.NewModeRequestStatus_En)
	{
		case REQ_BRAKE_FAIL:
		{
			if (UserInputSig_St.BootUpflag_b == false)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_TOGGLE);
				NOP();
			}
			else if (UserInputSig_St.BootUpflag_b == true)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
			}
			else
			{
			}
			break;
		}
		case REQ_REJECTED:
		{
			Set_Ride_mode_Request(NEUTRAL);
			Clear_Drive_Mode(NEUTRAL_MODE_E);
			if (UserInputSig_St.BootUpflag_b == false)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_TOGGLE);
			}
			else if (UserInputSig_St.BootUpflag_b == true)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
			}
			else
			{
			}
			RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
			RunTimeDriverMode.NewRideModeProcessing_flag = false;
			break;
		}
		case REQ_ACCEPTED:
		{
			Set_Ride_mode_Request(RunTimeDriverMode.NewlyReqMode);
			if(RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
			{
				if( ECO == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
					UserInputSig_St.BootUpflag_b = true;
				}
				else if((SPORT == RunTimeDriverMode.NewlyReqMode))
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
				}
				else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
					UserInputSig_St.BootUpflag_b = true;
				}
				else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				}
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
			}
			break;
		}
		case REQ_MCU_WAIT:
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				if (UserInputSig_St.BootUpflag_b == false)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_TOGGLE);
				}
				else if (UserInputSig_St.BootUpflag_b == true)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				}
				else
				{
				}
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			
			break;
		}
		case REQ_SPEED_WAIT :
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			
		}
	}
	
	if(false == UserInputSig_St.BootUpflag_b)
	{
		Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_TOGGLE);
	}
	return ;
}

void hmi_eco_other(void)
{
	switch(RunTimeDriverMode.NewModeRequestStatus_En)
	{
		case REQ_BRAKE_FAIL:
		{
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_OFF);
			break;
		}
		case REQ_REJECTED:
		{
			Clear_Drive_Mode(ECONOMY_MODE_E);
			Set_Ride_mode_Request(ECO);
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
			RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
			RunTimeDriverMode.NewRideModeProcessing_flag = false;
			break;
		}
		case REQ_ACCEPTED:
		{
			Set_Ride_mode_Request(RunTimeDriverMode.NewlyReqMode);
			if(RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
			{
				if( ECO == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
				}
				else if(SPORT == RunTimeDriverMode.NewlyReqMode) 
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
				}
				else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
				}
				else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				}
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
			}
			
			break;
		}
		case REQ_MCU_WAIT:
		{
			if( NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_ON | REVERSE_OFF);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_TOGGLE);
			}
			break;
		}
		case REQ_SPEED_WAIT :
		{
			if( NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_ON | REVERSE_OFF);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_ON | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_TOGGLE);
			}
			break;
		}
		default:
		{
			if(ECO == Get_Actual_Ride_Mode())
			{
				if(0 ==(UserInputSig_St.Neutral_Mode_Sel_u8 || UserInputSig_St.Economy_Mode_Sel_u8 ||  UserInputSig_St.Sports_Mode_Sel_u8 || UserInputSig_St.Reverse_Mode_Sel_u8))
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
				}
			}
			
		}
	}
	return ;
}

void hmi_sports_other(void)
{
	switch(RunTimeDriverMode.NewModeRequestStatus_En)
	{
		case REQ_BRAKE_FAIL:
		{
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			break;
		}
		case REQ_REJECTED:
		{
			Clear_Drive_Mode(SPORTS_MODE_E);
			Set_Ride_mode_Request(SPORT);
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
			RunTimeDriverMode.NewRideModeProcessing_flag = false;
			break;
		}
		case REQ_ACCEPTED:
		{
			if(RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
			{
				if( ECO == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
				}
				else if(SPORT == RunTimeDriverMode.NewlyReqMode) 
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
				}
				else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
				}
				else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				}
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
			}
			break;
		}
		case REQ_MCU_WAIT:
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
			}
			else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			break;
		}
		case REQ_SPEED_WAIT :
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
			}
			else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_OFF);
			}
			break;
		}
	}
	return ;
}
void hmi_reverse_other(void)
{
	switch(RunTimeDriverMode.NewModeRequestStatus_En)
	{
		case REQ_BRAKE_FAIL:
		{
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_TOGGLE);
			break;
		}
		case REQ_REJECTED:
		{
			Clear_Drive_Mode(REVERSE_MODE_E);
			Set_Ride_mode_Request(REVERSE);
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
			RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
			RunTimeDriverMode.NewRideModeProcessing_flag = false;
			break;
		}
		case REQ_ACCEPTED:
		{
			if(RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
			{
				if( ECO == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);	
				}
				else if(SPORT == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
				}
				else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
					
				}
				else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
				}
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
			}
			break;
		}
		case REQ_MCU_WAIT:
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_TOGGLE | REVERSE_ON);
			}
			else if(SPORT == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_ON);
			}
			else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_OFF | ECO_OFF | REVERSE_ON);
			}
			else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_TOGGLE | ECO_OFF | REVERSE_ON);
			}
			
			break;
		}
	}
	return ;
}


void hmi_safe_other(void)
{
	switch(RunTimeDriverMode.NewModeRequestStatus_En)
	{
		case REQ_BRAKE_FAIL:
		{
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			break;
		}
		case REQ_REJECTED:
		{
			Clear_Drive_Mode(SAFE_MODE_E);
			Set_Ride_mode_Request(SAFE_MODE);
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
			RunTimeDriverMode.NewRideModeProcessing_flag = false;
			break;
		}
		case REQ_ACCEPTED:
		{
			if(RunTimeDriverMode.NewlyReqMode == Get_Actual_Ride_Mode())
			{
				
				if( ECO == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E,ZERO);	
				}
				else if(SPORT == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E,ZERO);	
				}
				else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_ON);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E,ZERO);	
					NOP();
				}
				else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_ON | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
					SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E,ZERO);	
					
				}
				else if(SAFE_MODE == RunTimeDriverMode.NewlyReqMode)
				{
					Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_OFF);
				}
				RunTimeDriverMode.NewModeRequestStatus_En = REQ_INIT;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
			}
			break;
		}
		case REQ_MCU_WAIT:
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			}
			
			break;
		}
		case REQ_SPEED_WAIT:
		{
			if( ECO == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_TOGGLE | REVERSE_OFF);
			}
			else if(REVERSE == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_ON | ECO_OFF | REVERSE_TOGGLE);
			}
			else if(NEUTRAL == RunTimeDriverMode.NewlyReqMode)
			{
				Cluster_Driver_Mode_Set(NEUTRAL_TOGGLE | SPORTS_ON | ECO_OFF | REVERSE_OFF);
			}
			
	 		break;
	 	}
	}
	 if(TURNED_ON_E == UserInputSig_St.Safe_mode_Entery_exit_u8 )
	 {
		Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
		SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, THREE);
	 }
	 return ;
}

void safe_mode_func(void)
{
	if (ZERO < Get_SafeMode_Data())
	{
		if ((false == DriverMode_RunTime_St.SafeMode_TurnOn_b) ||
			(ZERO == DriverMode_RunTime_St.SafeMode_data_u32))
		{
			Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_OFF | REVERSE_OFF);
			SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, THREE);
			Set_Ride_mode_Request(SAFE_MODE);
			Set_Regen_State(ZERO);
			SET_REGEN_STATUS(0);
			DriverMode_RunTime_St.SafeMode_TurnOn_b = TRUE;
			RunTimeDriverMode.Safe_mode_flag = true;
		}
		else
		{
			/* code */
		}
		DriverMode_RunTime_St.SafeMode_data_u32 = Get_SafeMode_Data();
		if ((false == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
			(ZERO < (Get_SafeMode_Data() &
					 (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
		{
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ONE);
			SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ONE);
			DriverMode_RunTime_St.SafeMode_MC_Set_b = true;
		}
		else if ((true == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
				 (ZERO == (Get_SafeMode_Data() &
						   (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
		{
			SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ZERO);
			DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
		}
		else
		{
			/* code */
		}

		if ((false == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
			(ZERO < (Get_SafeMode_Data() &
					 (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
					  SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM | SAFE_MODE_BMS_SOC |SAFE_MODE_BMS_HIGH_MODULE_TEMP))))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE); // todo manjesh changed from 3 to 1
			SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ONE);
			DriverMode_RunTime_St.SafeMode_BMS_Set_b = true;
		}
		
		else if ((true == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
				 (ZERO == (Get_SafeMode_Data() &
						   (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
							SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM | SAFE_MODE_BMS_SOC | SAFE_MODE_BMS_HIGH_MODULE_TEMP))))
		{
			if (ZERO == Get_SafeMode_Data())
			{
				SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
				SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
			}

			SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
		}
		else
		{
			/* code */
		}
		
		if (ZERO != (Get_SafeMode_Data() &
					 (SAFE_MODE_MC_COMM | SAFE_MODE_BMS_COMM)))
		{
			// set vehicle speed to zero
			Set_Motor_Speed_Limit(ZERO);
		}

		else
		{
			/* code */
		}
		Clear_Drive_Mode(NEUTRAL_MODE_E);
	}
	
 	else if (ZERO == Get_SafeMode_Data())
	{
		if (true == UserInputSig_St.Economy_Mode_Sel_u8)
		{
			
			Clear_Drive_Mode(ECONOMY_MODE_E);
			SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ZERO);
			SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ZERO);
			Set_Ride_mode_Request(ECO);
			DriverMode_RunTime_St.DriverMode_State_En =
				DRIVER_IDLE_MODE_STATE_E;
			DriverMode_RunTime_St.SafeMode_TurnOn_b = false;
			if(ECO == Get_Actual_Ride_Mode())
			{
				RunTimeDriverMode.Safe_mode_flag = false;
				RunTimeDriverMode.OldMode  = ECO;
				RunTimeDriverMode.NewRideModeProcessing_flag = false;
				Cluster_Driver_Mode_Set(NEUTRAL_OFF | SPORTS_OFF | ECO_ON | REVERSE_OFF);
			}
			
		}
		else
		{
			SET_CLUSTER_DATA(CLUSTER_SAFE_MODE_E, ONE);
			SET_CLUSTER_DATA(CLUSTER_ECO_MODE_E, ONE);
		}

		if ((true == DriverMode_RunTime_St.SafeMode_MC_Set_b) &&
			(ZERO == (Get_SafeMode_Data() &
					  (SAFE_MODE_MOTOR_TEM | SAFE_MODE_CONTROLLER_TEM | SAFE_MODE_MC_COMM))))
		{
			if (ZERO == Get_SafeMode_Data())
			{
				SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
			}

			SET_CLUSTER_DATA(CLUSTER_MOTOR_FAULT_E, ZERO);
			DriverMode_RunTime_St.SafeMode_MC_Set_b = false;
		}
		else
		{
			/* code */
		}

		if ((true == DriverMode_RunTime_St.SafeMode_BMS_Set_b) &&
			(ZERO == (Get_SafeMode_Data() &
					  (SAFE_MODE_BMS_COMM | SAFE_MODE_MODULE_TEM |
					   SAFE_MODE_PDU_TEM | SAFE_MODE_BMS_TEM))))
		{
			if (ZERO == Get_SafeMode_Data())
			{
				SET_CLUSTER_DATA(CLUSTER_WARNING_SYMBOL_E, ZERO);
			}
			SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			DriverMode_RunTime_St.SafeMode_BMS_Set_b = false;
		}
		else
		{
			/* code */
		}
		DriverMode_RunTime_St.SafeMode_data_u32 = ZERO;
	}
	else
	{
		/* code */
	}
}

/***********************************new implementation ******************************************************/

/* -----------------------------------------------------------------------------
 *  FUNCTION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : Cluster_Driver_Mode_Set
 *   Description   : This function Sets Driver Mode in the HMI(cluster).
 *   Parameters    : uint8_t Driver_Mode_Data_u8
 *   Return Value  : None
 *******************************************************************************/
static void Cluster_Driver_Mode_Set(uint8_t Driver_Mode_Data_u8)
{
	if(Driver_Mode_Data_u8 == 0)
	{
		NOP();
	}
	SET_CLUSTER_DATA(CLUSTER_NEUTRAL_MODE_E, ((Driver_Mode_Data_u8 >> 6) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_SPORTS_MODE_E, ((Driver_Mode_Data_u8 >> 4) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_ECO_MODE_E, ((Driver_Mode_Data_u8 >> 2) & 0x03U));
	SET_CLUSTER_DATA(CLUSTER_REVERSE_MODE_E, ((Driver_Mode_Data_u8 >> 0) & 0x03U));
}

/* -----------------------------------------------------------------------------
 *  FUNCTION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : DriverSafe_Mode_State
 *   Description   : This function Gets Driver Safe Mode State.
 *   Parameters    : None
 *   Return Value  : bool
 *******************************************************************************/
bool DriverSafe_Mode_State(void)
{
	return DriverMode_RunTime_St.SafeMode_TurnOn_b;
}
/* -----------------------------------------------------------------------------
 *  FUNCTION DESCRIPTION
 *  -----------------------------------------------------------------------------
 *   Function Name : Get_DriverMode
 *   Description   : This function Gets Driver Mode State operation.
 *   Parameters    : None
 *   Return Value  : DriverMode_State_En_t
 *******************************************************************************/
DriverMode_State_En_t Get_DriverMode(void)
{
	return DriverMode_RunTime_St.DriverMode_State_En;
}
uint8_t rrr_ridemode =0 ;
void VehicleIdleCheck(void)
{

	if((NEUTRAL == Get_Actual_Ride_Mode()) || (SAFE_MODE== Get_Actual_Ride_Mode()))
	{
		VehicleIdleTime_u32 = 0;
		NOP();
		return ;
		
	}
	if (ONE >= Get_Vehicle_Speed())
	{
		if (false == DriverMode_RunTime_St.Vehicle_Idle_Check_b)
		{
			DriverMode_RunTime_St.Vehicle_Idle_Check_b = true;
			 DriverMode_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			VehicleIdleTime_u32 = 0;
		}
		else if ((true == DriverMode_RunTime_St.Vehicle_Idle_Check_b) &&
				 (VEHICLE_IDLE_TIME <=
				  (VehicleIdleTime_u32)))
		{
			Set_Ride_mode_Request(NEUTRAL); // todo: check manjesh
			if (NEUTRAL == Get_Actual_Ride_Mode())
			{
				DriverMode_RunTime_St.DriverMode_State_En = DRIVER_NEUTRAL_MODE_RUN_STATE_E;
				DriverMode_RunTime_St.DriverMode_Event_En = DRIVER_MODE_NEUTRAL_MODE_EVENT_E;
				DriverMode_RunTime_St.Vehicle_Idle_Check_b = false;
				Clear_Drive_Mode(NEUTRAL_MODE_E); // todo: check manjesh
			}
		}
		else
		{
			VehicleIdleTime_u32 += 5;
			if(VehicleIdleTime_u32 >= VEHICLE_IDLE_TIME)
			{
				NOP();
			}
			/* code */
		}
		
	}
	else
	{
		DriverMode_RunTime_St.Vehicle_Idle_Check_b =0;
		VehicleIdleTime_u32 = 0;
	}
	return;
}


static bool  lowSocBuzz_b = false;
static uint32_t  lowsoctimecapture_u32 = 0;
static uint32_t  HighTemptimecapture_u32 = 0;
static bool  lowsoctimecapture_b  = 0;
uint32_t pre_odo ;
uint8_t counter_LowSoc =0;

  void buzeer_bing(void)
  {
        static bool off_flag_b = 0;
		static uint32_t timecapture_u32= 0;
		static bool high_temp_timecapture_b =0;

		if(true == sound_modechange)
		{
			if(false == off_flag_b )
			{
				timecapture_u32 = GET_VCU_TIME_MS();
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].BuzzStatus_En = ACTIVATE;
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].TurnON_Time_b = true;
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].TurnOFF_Time_b = false;
				off_flag_b = true;
				//VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
				
			}
			else if((GET_VCU_TIME_MS() - timecapture_u32 ) > 100)
			{
				sound_modechange = false;
				off_flag_b =  false;
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].BuzzStatus_En = DEACTIVATE;
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].TurnON_Time_b = false;
				Buzzer_Functionality_St[BUZZ_MODE_CHANGE_E].TurnOFF_Time_b = true;
				timecapture_u32 = GET_VCU_TIME_MS();
				//VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
			}
		}

		else if(LOWSOCCUTOFF >= Get_BMS_SOC())
		{
			
			if(((Get_ODO_meters()- pre_odo) >= 100) && (Get_ODO_meters()!=0) && (false == lowSocBuzz_b))
			{
				pre_odo = Get_ODO_meters();
				lowSocBuzz_b  = true;
				VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
				Buzzer_Functionality_St[BUZZ_LOWSOC_E].BuzzStatus_En = ACTIVATE;
			}
			if((true  == lowSocBuzz_b ) && (false == lowsoctimecapture_b))
			{
				lowsoctimecapture_b = true;
 				lowsoctimecapture_u32  = GET_VCU_TIME_MS();
			}
			else if(((GET_VCU_TIME_MS() -  lowsoctimecapture_u32) > 100) && (true == lowsoctimecapture_b) && (TURNED_ON_E == VcuOutputs_St.buzzer_u8))
			{
				VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
				VcuOutputs_St.modechange_b = TURNED_OFF_E;
				Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnON_Time_b = false;
				Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnOFF_Time_b = true;
				lowsoctimecapture_b = false;
				counter_LowSoc++;  
				if(counter_LowSoc >= 2)
				{
					counter_LowSoc = 0;
					lowSocBuzz_b = 0;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					VcuOutputs_St.modechange_b = TURNED_OFF_E;
					Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnOFF_Time_b = false;
					Buzzer_Functionality_St[BUZZ_LOWSOC_E].BuzzStatus_En = DEACTIVATE;
				}
			}
			else if(((GET_VCU_TIME_MS() -  lowsoctimecapture_u32) > 500) && (true == lowsoctimecapture_b) && (TURNED_OFF_E == VcuOutputs_St.buzzer_u8))
			{
				VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
				VcuOutputs_St.modechange_b = TURNED_ON_E;
				Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnON_Time_b = true;
				Buzzer_Functionality_St[BUZZ_LOWSOC_E].TurnOFF_Time_b = false;
				lowsoctimecapture_b = false;
			}
		}
		else if(Get_SafeMode_Data() & (1<<VEHICLE_SAFE_MODE_BMS_TEM_E))
		{
			if(false == high_temp_timecapture_b )
			{
    				high_temp_timecapture_b = true;
				HighTemptimecapture_u32 = GET_VCU_TIME_MS()+2000;
			}
			if(HighTemptimecapture_u32 < GET_VCU_TIME_MS())
			{
				
				Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].BuzzStatus_En = ACTIVATE;
				Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].TurnON_Time_b = true;
				Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].TurnOFF_Time_b = false;
				VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
				if((GET_VCU_TIME_MS() - HighTemptimecapture_u32) >= 40)
				{
					Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].BuzzStatus_En = DEACTIVATE;
					Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].TurnON_Time_b = false;
					Buzzer_Functionality_St[BUZZ_BMS_HIGH_TEMP_E].TurnOFF_Time_b = true;
					VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
					HighTemptimecapture_u32 = GET_VCU_TIME_MS()+2000;
				}
			}
		}
  }
//void temp_bms_safemode()
//{
//if((safetemp == 1) && (bat_temp >= 45))
//  {
//	  safetemp = 1;
//	  SafeMode_SetEvent(VEHICLE_SAFE_MODE_BATT_TEMP_E, VEHICLE_SAFE_MODE_OFF_E);
//	 // safe_mode_func();
//	  if(GET_VCU_TIME_MS() +1000)
//	  {
//		count++;
	  
//	  if(count == 60)
//	  {
//		//safe_mode_func();
//		bat_temp = 35;
//		SafeMode_ClearEvent();
//	  }
//  }}
//}	  

#endif /* DRIVER_MODES_C */

/*---------------------- End of File -----------------------------------------*/