/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : driver_modes.h
|    Project        : VCU
|    Module         : driver modes
|    Description    : This file contains the export variables and functions                     
|                     to initialize and Operate the vehicle driver mode 
|                     functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 15/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef DRIVER_MODES_H
#define DRIVER_MODES_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define SAFE_MODE_MOTOR_TEM             (1 << 0)
#define SAFE_MODE_CONTROLLER_TEM        (1 << 1)
#define SAFE_MODE_MC_COMM               (1 << 2)
//#define SAFE_MODE_HMI_COMM              (1 << 3)
#define SAFE_MODE_BMS_COMM              (1 << 3)
#define SAFE_MODE_MODULE_TEM            (1 << 4)
#define SAFE_MODE_PDU_TEM               (1 << 5)
#define SAFE_MODE_BMS_TEM               (1 << 6)
#define SAFE_MODE_BMS_SOC               (1 << 7)
#define SAFE_MODE_BMS_HIGH_MODULE_TEMP   (1 << 8)

#define DRIVER_MODE_TRANSITION_TIME     (2000U)
#define VEHICLE_IDLE_TIME              (120000U)
#define DRIVER_MODE_TRANSITION_JOYSTICK_TIME     (5000U) 

#define NEUTRAL_ON                      (3 << 6)
#define NEUTRAL_TOGGLE                  (1 << 6)
#define NEUTRAL_OFF                     (0)
#define SPORTS_ON                       (3 << 4)
#define SPORTS_TOGGLE                   (1 << 4)
#define SPORTS_OFF                      (0)
#define ECO_ON                          (3 << 2)
#define ECO_TOGGLE                      (1 << 2)
#define ECO_OFF                         (0)
#define REVERSE_ON                      (3 << 0)
#define REVERSE_TOGGLE                  (1 << 0)
#define REVERSE_OFF                     (0)


#define NEUTRAL_MOVE_SET                      (1 << 3)
#define SPORTS_MOVE_SET                       (1 << 2)
#define ECO_MOVE_SET                          (1 << 1)
#define REVERSE_MOVE_SET                      (1 << 0)



/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
	DRIVER_IDLE_MODE_STATE_E,
	DRIVER_NEUTRAL_MODE_RUN_STATE_E,
	DRIVER_SPORT_MODE_RUN_STATE_E,
	DRIVER_ECO_MODE_RUN_STATE_E,
	DRIVER_REVERSE_MODE_RUN_STATE_E,
	DRIVER_SAFE_MODE_RUN_STATE_E,
} DriverMode_State_En_t;
typedef enum
{
	DRIVER_MODE_IDLE_MODE_EVENT_E,
	//DRIVER_MODE_EVENT_START_E,
	DRIVER_MODE_NEUTRAL_MODE_EVENT_E,// = DRIVER_MODE_EVENT_START_E,
	DRIVER_MODE_SPORT_MODE_EVENT_E,
	DRIVER_MODE_ECO_MODE_EVENT_E,
	DRIVER_MODE_REVERSE_MODE_EVENT_E,
	DRIVER_MODE_SAFE_MODE_EVENT_E,
	//DRIVER_MODE_EVENT_END_E,
} DriverMode_Event_En_t;

typedef struct
{
	bool SafeMode_TurnOn_b;
	bool SafeMode_BMS_Set_b;
	bool SafeMode_MC_Set_b;
	uint32_t Timer_u32;
	uint32_t SafeMode_data_u32;
	bool Vehicle_Idle_Check_b;
	bool Driver_Economy_Move_b;
	bool Driver_Sports_Move_b;
	bool Driver_Reverse_Move_b;
	bool Driver_Neutral_Move_b;
	DriverMode_State_En_t DriverMode_State_En;
	DriverMode_Event_En_t DriverMode_Event_En;
} DriverMode_RunTime_St_t;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Init
*   Description   : This function implements Driver Mode initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void DriverMode_Init(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverMode_Proc
*   Description   : This function implements Driver Mode Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
extern void DriverMode_Proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DriverSafe_Mode_State
*   Description   : This function Gets Driver Safe Mode State.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
extern bool DriverSafe_Mode_State(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_DriverMode
*   Description   : This function Gets Driver Mode State operation.
*   Parameters    : None
*   Return Value  : DriverMode_State_En_t
*******************************************************************************/
extern DriverMode_State_En_t Get_DriverMode(void);

/***********************************new implementation ******************************************************/
#define  SPEED_5kmh    (5U)
#define  SPEED_10kmh   (10)
#define  SPEED_25kmh   (25)
#define  SPEED_40kmh    (40)
#define  NO_SPEED_CHECK  (0)
#define  BRAKE_CHECK_TRUE  (1)
#define  BRAKE_CHECK_FALSE  (0)
#define  SPEED_CHECK_TRUE  (1)
#define  SPEED_CHECK_FALSE  (0)

typedef  void(*Driver_mode_callback_st)(void);

typedef enum
{
	REQ_INIT,
	REQ_BRAKE_PASS,
	REQ_BRAKE_FAIL,
	REQ_SPEED_PASS,
	REQ_SPEED_WAIT,
	REQ_SPEED_FAIL,
	REQ_SPEED_BRAKE_PASS,
	REQ_MCU_START,
	REQ_MCU_WAIT,
	REQ_MCU_PASS,
	REQ_ACCEPTED,
	REQ_REJECTED,
}NewModeRequestStatus_En_t;

typedef enum
{
	DRI_CURRENT_MODE_E,
	DRI_CURRENT_MODE_NEUTRAL_E = DRI_CURRENT_MODE_E,
	DRI_CURRENT_MODE_ECO_E,
	DRI_CURRENT_MODE_SPORTS_E,
	DRI_CURRENT_MODE_REVERSE_E,
	DRI_CURRENT_MODE_SAFE_E,
	DRI_TOTAL_MODE_E,
}DriverCurrentMode_En_t;

typedef enum
{
	DRI_REQ_MODE_E,
	DRI_REQ_MODE_NEUTRAL_E = DRI_REQ_MODE_E,
	DRI_REQ_MODE_ECO_E,
	DRI_REQ_MODE_SPORTS_E,
	DRI_REQ_MODE_REVERSE_E,
	DRI_REQ_MODE_SAFE_E,
	DRI_REQ_MODE_END_E,
}DriverRequestedMode_En_t;


#pragma pack(1)
 typedef struct
 {
 	DriverRequestedMode_En_t DriverRequestedMode_En;
	bool    transAllflag;
 	bool 	BrakeCheck_flag;
	bool    SpeedCheck_flag;
	uint8_t speed_u8;
	uint32_t transition_time; 
 }driving_current_Other_st;

#pragma pack(1)
typedef struct
{
	DriverCurrentMode_En_t DriverCurrentMode_En; 
	driving_current_Other_st *driving_current_Other;
	Driver_mode_callback_st Driver_mode_callback;

}driving_mode_condtion_st;

#pragma pack(1)
typedef struct 
{
	uint32_t CaputuredTime_u32;
	uint8_t CurrentMode;
	uint8_t NewlyReqMode;
	uint8_t OldMode;
	uint8_t SpeedConditionCapture;
	uint8_t ModeChangePinState[5];

	NewModeRequestStatus_En_t NewModeRequestStatus_En;
	bool    NewRideModeProcessing_flag;
	bool    BrakeState_flag;
	bool    Safe_mode_flag;
	bool    first_enteryflag;
	bool    safeMode_exit_b;
}RunTimeDriverMode_st;

void Driver_machine_state(void);
uint8_t GetNewlyRequestedMode(void);
void hmi_netural_other(void);
void hmi_eco_other(void);
void hmi_sports_other(void);
void hmi_reverse_other(void);
void hmi_safe_other(void);
void safe_mode_func(void);

/***********************************new implementation ******************************************************/






#endif /* DRIVER_MODES_H */
/*---------------------- End of File -----------------------------------------*/