/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : vcu.h
|    Project        : VCU
|    Module         : vcu main 
|    Description    : This file contains the export variables and functions 			
|                     to initialize and Operate the vcu functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 13/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef TIMESETTING_H
#define TIMESETTING_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define YEARSTART  (23U)
#define YEAREND  (99U)
#define DATEEND   (31U)
#define MONTHEND   (12U)
#define HOUREND    (23U)
#define MINUTEEND   (59U)



/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
	TIMESETTING_NONE_E,
	DATE_SETTING_E,
	MONTH_SETTING_E,
	YEAR_SETTING_E,
	HOUR_SETTING_E,
	MINUTE_SETTING_E,
	SET_TIMING_E,
}TimeSetting_En_t;

typedef enum
{
	DRIVE_MODE_SEL_E,
	TIME_SET_MODE_E,
}UserButtonSel_En_t;
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Time_Setting_proc
*   Description   : This function implements time setting
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Time_Setting_Mode_Button(void);
void Incement_Button(void);
void Decrement_Button(void);
void Time_setting_proc(void);
void Time_setting_init(void);

#endif /* TIMESETTING_H */
/*---------------------- End of File -----------------------------------------*/