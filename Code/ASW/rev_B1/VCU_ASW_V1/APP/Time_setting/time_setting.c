/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : vcu.c
|    Project        : VCU
|    Module         : vcu main 
|    Description    : This file contains the variables and functions 			
|                     to initialize and Operate the vcu functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 13/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef TIMESETTING_C
#define TIMESETTING_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "DataAquire.h"
#include "vcu.h"
#include "DataBank.h"
#include "driver_modes.h"
#include "switch_signals.h"
#include "motor_control.h"
#include "Communicator.h"
#include "time_setting.h"
#include"rtc_iic.h"
#include "sdcard.h"

/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  LOCAL VARIABLES
 ******************************************************************************/
TimeSetting_En_t TimeSetting_En = DATE_SETTING_E;
UserButtonSel_En_t UserButtonSel_En = DRIVE_MODE_SEL_E;


// uint8_t BCDtoDECIMAL(uint8_t BCD_Num_u8);
// uint8_t DECtoBCD(uint8_t Dec_u8);
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Time_Setting_Mode_Button(void)
{
	RealTimeData_St_t RealTimeData_St_1;
	
switch(TimeSetting_En)
{
	case DATE_SETTING_E:
	{
		TimeSetting_setvalue_St.Month = 1;
		TimeSetting_En = MONTH_SETTING_E;
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Month;
		break;
	}
	case MONTH_SETTING_E:
	{
		TimeSetting_setvalue_St.Year = 23;
		TimeSetting_En = YEAR_SETTING_E;
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Year;
		break;
	}
	case YEAR_SETTING_E:
	{
		TimeSetting_setvalue_St.Hour = 0;
		TimeSetting_En = HOUR_SETTING_E;
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Hour;
		break;
	}
	case HOUR_SETTING_E:
	{
		TimeSetting_setvalue_St.Minutes = 0;
		TimeSetting_En = MINUTE_SETTING_E;
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Minutes;
		break;
	}
	case MINUTE_SETTING_E:
	{
		TimeSetting_En = DATE_SETTING_E;
		TimeSetting_En = SET_TIMING_E;
		RealTimeData_St_1.Date_u8 =   (TimeSetting_setvalue_St.Date);
		RealTimeData_St_1.Month_u8 =      (TimeSetting_setvalue_St.Month);
		RealTimeData_St_1.Year_u8 =      (TimeSetting_setvalue_St.Year);
		RealTimeData_St_1.HoursFormat_u8 = 0x00 ; //_24H_FORMAT;
		RealTimeData_St_1.Hour_u8=         (TimeSetting_setvalue_St.Hour);
		RealTimeData_St_1.Minute_u8=       (TimeSetting_setvalue_St.Minutes);
		TimeSetting_setvalue_St.init_flag = false;
		SetRealTime(&RealTimeData_St_1);
		TimeSetting_ActualValue_St.timeSetFalg = false;
		UserInputSig_St.Neutral_Long_press_u8 = false;
		SET_CLUSTER_DATA(CLUSTER_TIME_SETTING_E, 0);
		ExtRtcInit();
		timeSet = true;
		TimeSetting_En = DATE_SETTING_E;
		TimeSetting_setvalue_St.Value_display = 0;
		break;
	}
	
	case SET_TIMING_E:
	{
		
		break;
	}
	default:
	{
		TimeSetting_En = TIMESETTING_NONE_E;
		break;
	}
	
	
}
return ;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void Incement_Button(void)
{
	switch(TimeSetting_En)
{
	
	case DATE_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Date < DATEEND)
		{
			TimeSetting_setvalue_St.Date++;
			
		}
		else
		{
			TimeSetting_setvalue_St.Date = 1;
		}
		 TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Date;
		break;
	}
	case MONTH_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Month < MONTHEND)
		{
			TimeSetting_setvalue_St.Month++;
		}
		else
		{
			TimeSetting_setvalue_St.Month = 1;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Month;
		break;
	}
	case YEAR_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Year < YEAREND)
		{
			TimeSetting_setvalue_St.Year++;
		}
		else
		{
			TimeSetting_setvalue_St.Year = YEARSTART;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Year;
		break;
	}
	case HOUR_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Hour < HOUREND)
		{
			TimeSetting_setvalue_St.Hour++;
		}
		else
		{
			TimeSetting_setvalue_St.Hour = 0;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Hour;
		break;
	}
	case MINUTE_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Minutes < MINUTEEND)
		{
			TimeSetting_setvalue_St.Minutes++;
		}
		else
		{
			TimeSetting_setvalue_St.Minutes = 0;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Minutes;
		break;
	}
	default:
	{
		
		break;
	}
}
	return;
}

void Decrement_Button(void)
{
	switch(TimeSetting_En)
{
	
	case DATE_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Date > 1)
		{
			TimeSetting_setvalue_St.Date--;
		}
		else
		{
			TimeSetting_setvalue_St.Date = 31;
		}
		 TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Date;
		break;
	}
	case MONTH_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Month > 1)
		{
			TimeSetting_setvalue_St.Month--;
		}
		else
		{
			TimeSetting_setvalue_St.Month = MONTHEND;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Month;
		break;
	}
	case YEAR_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Year >  23)
		{
			TimeSetting_setvalue_St.Year--;
		}
		else
		{
			TimeSetting_setvalue_St.Year = YEAREND;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Year;
		break;
	}
	case HOUR_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Hour >= 1)
		{
			TimeSetting_setvalue_St.Hour--;
		}
		else
		{
			TimeSetting_setvalue_St.Hour = 23;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Hour;
		break;
	}
	case MINUTE_SETTING_E:
	{
		if(TimeSetting_setvalue_St.Minutes > 1)
		{
			TimeSetting_setvalue_St.Minutes--;
		}
		else
		{
			TimeSetting_setvalue_St.Minutes = 59;
		}
		TimeSetting_setvalue_St.Value_display = TimeSetting_setvalue_St.Minutes;
		break;
	}
	default:
	{
		
		break;
	}

}
	return;
}
	

void Time_setting_proc(void)
{
	if(false == TimeSetting_ActualValue_St.timeSetFalg)
	{
		if((NEUTRAL ==  Get_Actual_Ride_Mode()) && (UserInputSig_St.Neutral_Long_press_u8 == true))
		{	
			if(true == UserInputSig_St.BootUpflag_b )
			{
				TimeSetting_ActualValue_St.timeSetFalg = true;
				NOP();
			}
			else
			{
				UserInputSig_St.Neutral_Long_press_u8 = false;
				return;
			}
				
		}
		else
		{
			NOP();
			return;
		}
	}
	
	if(false == TimeSetting_setvalue_St.init_flag)
	{
		TimeSetting_setvalue_St.Date = 1;
		TimeSetting_setvalue_St.Value_display = 1;
		TimeSetting_setvalue_St.init_flag= TRUE;
	}
	
	SET_CLUSTER_DATA(CLUSTER_TIME_SETTING_E, TimeSetting_En);

	if(TURNED_ON_E == UserInputSig_St.TimeSett_inc_u8 )
	{
		UserInputSig_St.TimeSett_inc_u8 = TURN_OFF;
		Incement_Button();
	}
	else if(TURNED_ON_E == UserInputSig_St.TimeSett_dec_u8)
	{
		UserInputSig_St.TimeSett_dec_u8 = TURN_OFF;
		Decrement_Button();
	}
	else if(TURNED_ON_E == UserInputSig_St.TimeSett_Sel_u8)
	{
		UserInputSig_St.TimeSett_Sel_u8 = TURN_OFF;
		Time_Setting_Mode_Button();
	}
	
	else
	{

	}
	
}


void Time_setting_init(void)
{
	TimeSetting_setvalue_St.Date = 0;
	TimeSetting_setvalue_St.Month = 1;
	TimeSetting_setvalue_St.Year = 23;
	TimeSetting_setvalue_St.Hour = 0;
	TimeSetting_setvalue_St.Minutes = 0;
	TimeSetting_En = DATE_SETTING_E;
}


// uint8_t BCDtoDECIMAL(uint8_t BCD_Num_u8)
// {
//     uint8_t Decimal_Num_u8 = 0;
//     Decimal_Num_u8 = ((BCD_Num_u8>>4)) *10;
//     Decimal_Num_u8 = Decimal_Num_u8 + (BCD_Num_u8&0x0f);
//     return Decimal_Num_u8;
// }

// uint8_t DECtoBCD(uint8_t Dec_u8) 
// {
//     uint8_t BcdValue_u8 = 0;
//     BcdValue_u8 = ((Dec_u8/10) << 4);
//     BcdValue_u8 = (BcdValue_u8 | (Dec_u8%10));
//     return BcdValue_u8;
// }

#endif /* VCU_C */
/*---------------------- End of File -----------------------------------------*/