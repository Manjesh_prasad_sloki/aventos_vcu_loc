/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : vcu.c
|    Project        : VCU
|    Module         : vcu main 
|    Description    : This file contains the variables and functions 			
|                     to initialize and Operate the vcu functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 13/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef VCU_C
#define VCU_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "vcu.h"
#include "DataBank.h"
#include "driver_modes.h"
#include "switch_signals.h"
#include "motor_control.h"
#include "Communicator.h"
#include "rtc_iic.h"
#include "Config_RIIC0.h"
#include "sdcard.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static VCU_State_St_t VCU_State_St;
static VCU_State_En_t VCU_SetState_En;
//static VCU_Event_En_t VCU_SetEvent_En;
static VCU_HandShakeEvent_En_t VCU_SetHandShakeEvent_En;

static VCU_Ancillary_Supply_En_t VCU_Set_Ancillary_Supply_En;
uint8_t HandShackRequest_au8[0x8U] = {0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80};
uint32_t call_min=0,temp_t=0;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void VCU_StateMachine_proc(void);
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_HandShake
*   Description   : This function implements VCU handshake event with Master 
*		    ECU.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
//static VCU_HandShake_En_t VCU_HandShake(void);

void VCU_BMS_Charging_Check(void);

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Init
*   Description   : This function implements VCU initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VCU_Init(void)
{

	VCU_State_St.VCU_State_En = VCU_STANDBY_STATE_E;
	VCU_SetState_En = VCU_STANDBY_STATE_E;
	VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_IDLE_E;
	VCU_State_St.VCU_HandShakeEvent_En = VCU_HANDSHAKE_IDLE_EVENT_E;
	VCU_State_St.HandShakeCpmplete_b = false;
	VCU_State_St.Vehicle_KickStand_b = false;
	VCU_State_St.Vehicle_Charging_b  = false;
	//SET_VCU_TIME_MS(0); // todo: to be checked manjesh
	VcuOutputs_St.MotorKill_u8 = TURNED_ON_E;
	VcuOutputs_St.TorqueZero_u8 = TURNED_OFF_E;
	VCU_Set_Ancillary_Supply_En = VCU_OFF_E;
	VCU_State_St.VCU_SafeMode_Check_b = false;
	VCU_SetHandShakeEvent(VCU_HANDSHAKE_IDLE_EVENT_E);
	DriverMode_Init();
	SwitchSignals_Init();
	Cluster_Init();
	BMS_Init();
	MotorControl_Init();
	
	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Proc
*   Description   : This function implements VCU Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void VCU_Proc(void)
{      
	static uint32_t timer_counter =0;
	static uint16_t  dis_counter = 0;
	
	#ifdef TESTING_USER_MODE_SW
	uint8_t testing_var1_ua8[10];
	testing_var1_ua8[0]=UserInputSig_St.Neutral_Mode_Sel_u8;
	testing_var1_ua8[1]=UserInputSig_St.Economy_Mode_Sel_u8;
	testing_var1_ua8[2]=UserInputSig_St.Sports_Mode_Sel_u8;
	testing_var1_ua8[3]=UserInputSig_St.Reverse_Mode_Sel_u8;
	testing_var1_ua8[4]= 0 ;
	testing_var1_ua8[5]= 0;
	testing_var1_ua8[6]= 0;
	testing_var1_ua8[7]=  0;
	CAN0_Transmit(0x110,8,&testing_var1_ua8[0]);
	#endif
	//timer_time();
	Time_setting_proc(); 
	if(false == TimeSetting_ActualValue_St.timeSetFalg)
	 {
	 	DriverMode_Proc();
	 }
	 SDA_release();
	 SwitchSignals_Proc();
	 Cluster_Proc();
	 BMS_Proc();
	 MotorControl_Proc(); 
	 VCU_StateMachine_proc();
	 Acc_gyro_proc();
	 
	if(timer_counter == 0)
	{
		timer_counter = (GET_VCU_TIME_MS() + 500);	
	}
	else if(timer_counter <= GET_VCU_TIME_MS())
	{
		GetRealTime(&RealTimeData_St);
		SET_CLUSTER_DATA(CLUSTER_MINUTES_TIME_E,RealTimeData_St.Minute_u8);
		SET_CLUSTER_DATA(CLUSTER_HOURS_TIME_E,RealTimeData_St.Hour_u8);
		timer_counter = 0;
		dis_counter++;
		if((true == RTC_Disable_b) && (dis_counter > 10))
		{
			R_Config_RIIC0_Start();
			dis_counter = 0;
		}
		
		
	}
	else
	{
		;}
	
	
	/*Add other module Initialisation*/
	if(timeSet == true)                           //time_setting.c
	 { 
	 	timeSet = false;
	 	SD_Card_proc();
	 }
}


/////////////////////////////////////////////////////////////////////////////
/*
void timer_time()
{
if(temp_t ==0)
{
	temp_t = (GET_VCU_TIME_MS() + 1000);
}
else if(temp_t <= GET_VCU_TIME_MS())
{
	RealTimeData_St.Second_u8++;
        temp_t =0;
	///CAN1_Transmit(0x101,0x08,&RealTimeData_St.Second_u8);
	
	if(RealTimeData_St.Second_u8 >= 59)
	{
		
		
		RealTimeData_St.Second_u8=0;
		//time_min++;
		 RealTimeData_St.Minute_u8++;
		
		//CAN1_Transmit(0x102,0x08,&RealTimeData_St.Minute_u8);
	        //call_min++;
		///CAN1_Transmit(0x200,0x02,&call_min);
		//if(call_min == 5)
		
		//	call_min = 0;
		//	GetRealTime(&RealTimeData_St);
		
	if( RealTimeData_St.Minute_u8 == 59)
	{
			//time_min = 0;
			//time_hour++;
			//time_hour=RealTimeData_St.Hour_u8;
			RealTimeData_St.Hour_u8++;
	if(RealTimeData_St.Hour_u8 == 23)
	{
			RealTimeData_St.Hour_u8=0;
	}
	}
	}

}
}
*/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_StateMachine_proc
*   Description   : This function implements VCU State operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/

void VCU_StateMachine_proc(void)
{
	
	switch (VCU_State_St.VCU_State_En)
	{
	case VCU_INIT_STATE_E:
	{
		VCU_SetState_En = VCU_INIT_STATE_E;
		VCU_Init();
		VCU_SetState_En = VCU_INIT_STATE_E;
		VCU_State_St.VCU_State_En = VCU_START_STATE_E;
		break;
	}
	case VCU_START_STATE_E:
	{
		VCU_BMS_Charging_Check();
		vcu_kill_Kick_check();
		
			if ((true == GET_BMS_COMM_STATUS()) &&
				(true == GET_MC_COMM_STATUS()) &&
				(true == GET_HMI_COMM_STATUS()))
			{
				VCU_State_St.VCU_State_En = VCU_RUN_STATE_E;
				VCU_State_St.VCU_Event_En = VCU_KILL_SWITCH_EVENT_E;
				VCU_SetState_En = VCU_RUN_STATE_E;
			}/* code */
			else
			{
				if (false == VCU_State_St.VCU_SafeMode_Check_b)
				{
					VCU_State_St.VCU_SafeMode_Check_b = true;
					VCU_State_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else if (VCU_SAFE_MODE_TIMEOUT <=
						 (GET_VCU_TIME_MS() - VCU_State_St.Timer_u32))
				{
					VCU_State_St.VCU_SafeMode_Check_b = false;
					VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
					VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
					VCU_SetState_En = VCU_SAFE_MODE_STATE_E;
				}
			}
		
		break;
	}
	case VCU_RUN_STATE_E:
	{
		VCU_BMS_Charging_Check();
		vcu_kill_Kick_check();
		// if(true == Get_SafeMode_State())
		// {
		// 	VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
		// 	VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
		// 	VCU_SetState_En = VCU_SAFE_MODE_STATE_E;
		// }
		// else
		{
			/*need to check the communication of the BMS, MOTOR CONTROL and HMI*/
			if ((true == GET_BMS_COMM_STATUS()) &&
				(true == GET_MC_COMM_STATUS()) &&
				(true == GET_HMI_COMM_STATUS()) &&  
				(false == Get_SafeMode_State())) 
			{
				VCU_State_St.VCU_State_En = VCU_RUN_STATE_E;
			}
			else
			{
				if (false == VCU_State_St.VCU_SafeMode_Check_b)
				{
					VCU_State_St.VCU_SafeMode_Check_b = true;
					VCU_State_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else if (VCU_SAFE_MODE_TIMEOUT <=
						 (GET_VCU_TIME_MS() - VCU_State_St.Timer_u32))
				{
					VCU_State_St.VCU_SafeMode_Check_b = false;
					VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
					VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
					VCU_SetState_En = VCU_SAFE_MODE_STATE_E;
				}
			}
		}
		break;
	}
	case VCU_SHUT_DOWN_STATE_E:
	{
		UserInputSig_St.Key_SwitchState_u8 = TURNED_ON_E; // todo Manjesh  it should  be removed 
		VCU_State_St.VCU_State_En = VCU_STANDBY_STATE_E;
		VCU_SetState_En = VCU_SHUT_DOWN_STATE_E;
		VCU_Ancillary_Supply(VCU_OFF_E);
		break;
	}
	case VCU_SAFE_MODE_STATE_E:
	{
		UserInputSig_St.Key_SwitchState_u8 = TURNED_ON_E; // todo Manjesh  it should  be removed 
		if (TURNED_OFF_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_SHUT_DOWN_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else if (false == Get_SafeMode_State())
		{
			VCU_State_St.VCU_State_En = VCU_START_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_SAFE_MODE_EVENT_E;
		}
		else
		{
			VCU_State_St.VCU_State_En = VCU_SAFE_MODE_STATE_E;
		}
		vcu_kill_Kick_check();
		VCU_SetState_En = VCU_SAFE_MODE_STATE_E;
		break;
	}
	case VCU_STANDBY_STATE_E:
	{
		UserInputSig_St.Key_SwitchState_u8 = TURNED_ON_E; // todo Manjesh  it should  be removed 
		if (TURNED_ON_E == UserInputSig_St.Key_SwitchState_u8)
		{
			VCU_State_St.VCU_State_En = VCU_INIT_STATE_E;
			VCU_State_St.VCU_Event_En = VCU_KEY_SWITCH_EVENT_E;
		}
		else
		{
			/* code */
		}
		VCU_SetState_En = VCU_STANDBY_STATE_E;
		/* code */
		break;
	}

	default:
	{
		break;
	}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetState
*   Description   : This function implements VCU State setting.
*   Parameters    : VCU_State_En_t VCU_State_En
*   Return Value  : None
*******************************************************************************/
void VCU_SetState(VCU_State_En_t VCU_State_En)
{
	VCU_SetState_En = VCU_State_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetState
*   Description   : This function implements get VCU State.
*   Parameters    : None
*   Return Value  : VCU_State_En_t
*******************************************************************************/
VCU_State_En_t VCU_GetState(void)
{
	return VCU_SetState_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_HandShake
*   Description   : This function implements VCU handshake event with Master 
*		    ECU.
*   Parameters    : NoneS
*   Return Value  : VCU_HandShake_En_t
*******************************************************************************/
//VCU_HandShake_En_t VCU_HandShake(void)
//{
//	VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En = VCU_HANDSHAKE_IDLE_EVENT_E;
//	VCU_HandShake_En_t Return_HandShake_En = VCU_HANDSHAKE_IDLE_E;
//	VCU_GetHandShakeEvent(&VCU_HandShakeEvent_En);
//	switch (VCU_State_St.VCU_HandShake_En)
//	{
//		case VCU_HANDSHAKE_IDLE_E:
//		{
//			break;
//		}
//		case VCU_HANDSHAKE_START_E:
//		{
//			if (VCU_HANDSHAKE_IDLE_EVENT_E == VCU_HandShakeEvent_En)
//			{
//				//R_Config_UART0_Send(&HandShackRequest_au8[0], 1U);
//				VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_RESPONSE_E;
//				Return_HandShake_En = VCU_HANDSHAKE_RESPONSE_E;
//			}
//			else
//			{
//				/* code */
//			}

//			break;
//		}
//		case VCU_HANDSHAKE_RESPONSE_E:
//		{
//			if (VCU_HANDSHAKE_REQUESTED_EVENT_E == VCU_HandShakeEvent_En)
//			{
//				//R_Config_UART0_Send(&HandShackRequest_au8[0], 1U);
//				VCU_State_St.VCU_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;
//				Return_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;
//				VCU_State_St.HandShakeCpmplete_b = true;
//			}
//			else
//			{
//				/* code */
//			}
//			break;
//		}
//		case VCU_HANDSHAKE_COMPLETE_E:
//		{ /* code */
//			break;
//		}

//		default:
//		{
//			break;
//		}
//	}
//	Return_HandShake_En = VCU_HANDSHAKE_COMPLETE_E;	/*todo-sandeep*/
	
//	return Return_HandShake_En; 
//}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_SetHandShakeEvent
*   Description   : This function implements VCU handshake event Setting.
*   Parameters    : VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
void VCU_SetHandShakeEvent(VCU_HandShakeEvent_En_t VCU_HandShakeEvent_En)
{
	VCU_SetHandShakeEvent_En = VCU_HandShakeEvent_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_GetHandShakeEvent
*   Description   : This function implements get VCU handshake event.
*   Parameters    : VCU_HandShakeEvent_En_t* VCU_HandShakeEvent_En
*   Return Value  : None
*******************************************************************************/
void VCU_GetHandShakeEvent(VCU_HandShakeEvent_En_t *VCU_HandShakeEvent_En)
{
	*VCU_HandShakeEvent_En = VCU_SetHandShakeEvent_En;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : VCU_Ancillary_Supply
*   Description   : This function Provides Ancillary System Supply.
*   Parameters    : VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En
*   Return Value  : None
*******************************************************************************/
void VCU_Ancillary_Supply(VCU_Ancillary_Supply_En_t VCU_Ancillary_Supply_En)
{
	if ((VCU_OFF_E == VCU_Set_Ancillary_Supply_En) && (VCU_ON_E == VCU_Ancillary_Supply_En))
	{
		PORT.P9 &= 0xFFFB;
		if (ZERO == PORT.P9 | 0x04)
		{
			VCU_Set_Ancillary_Supply_En = VCU_Ancillary_Supply_En;
		}
	}
	else if ((VCU_ON_E == VCU_Set_Ancillary_Supply_En) && (VCU_OFF_E == VCU_Ancillary_Supply_En))
	{
		PORT.P9 |= 0x04;
		if (ONE == PORT.P9 | 0x04)
		{
			VCU_Set_Ancillary_Supply_En = VCU_Ancillary_Supply_En;
		}
	}
	else
	{
		/* code */
	}
}

void vcu_kill_Kick_check(void)
{
	
	if (((false == VCU_State_St.Vehicle_KickStand_b) && (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8)) ||
		((false == VCU_State_St.vechile_killSwitch_b) && (TURNED_ON_E == UserInputSig_St.Kill_SwitchState_u8)))
	{
		if (TURNED_ON_E == UserInputSig_St.KickStand_SwitchState_u8)
		{
			VCU_State_St.Vehicle_KickStand_b = true;
			SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ONE);
			VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
		}
		if (TURNED_ON_E == UserInputSig_St.Kill_SwitchState_u8)
		{
			VCU_State_St.vechile_killSwitch_b = true;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, THREE);
			VCU_State_St.VCU_Event_En = VCU_KILL_SWITCH_EVENT_E;
		}
		VcuOutputs_St.TorqueZero_u8 = TURNED_ON_E;
	}
	if (((TRUE == VCU_State_St.Vehicle_KickStand_b) && (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8)) || ((TRUE == VCU_State_St.vechile_killSwitch_b) && (TURNED_OFF_E == UserInputSig_St.Kill_SwitchState_u8)))
	{
		if (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8)
		{
			VCU_State_St.Vehicle_KickStand_b = false;
			SET_CLUSTER_DATA(CLUSTER_SIDE_STAND_E, ZERO);
			VCU_State_St.VCU_Event_En = VCU_KICK_STAND_SWITCH_EVENT_E;
		}
		if (TURNED_OFF_E == UserInputSig_St.Kill_SwitchState_u8)
		{
			VCU_State_St.vechile_killSwitch_b = false;
			SET_CLUSTER_DATA(CLUSTER_KILL_SWITCH_E, ZERO);
			VCU_State_St.VCU_Event_En = VCU_KILL_SWITCH_EVENT_E;
		}

		if ((TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8) && (TURNED_OFF_E == UserInputSig_St.KickStand_SwitchState_u8))
		{
			VcuOutputs_St.TorqueZero_u8 = TURNED_OFF_E;
		}
	}
	return;
}

void VCU_BMS_Charging_Check(void)
{
 if ((true == Get_BatteryCharging_State()) &&
	 (false == VCU_State_St.Vehicle_Charging_b))
 {
	VCU_State_St.Vehicle_Charging_b = true;
	SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ONE);
	VCU_State_St.VCU_Event_En = VCU_VEHICLE_CHARGING_EVENT_E;
	VcuOutputs_St.MotorKill_u8 = TURNED_OFF_E;
	VechileCharging_b = true;
 }
 else if((false == Get_BatteryCharging_State())&& (true == VCU_State_St.Vehicle_Charging_b))
 {
	VCU_State_St.Vehicle_Charging_b = false;
	SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ZERO);
	VcuOutputs_St.MotorKill_u8 = TURNED_ON_E;
	VechileCharging_b = false;
 }

 return;
}

//void timeset()
//{
//	Time_Setting_Mode_Button();
//}
#endif /* VCU_C */
/*---------------------- End of File -----------------------------------------*/