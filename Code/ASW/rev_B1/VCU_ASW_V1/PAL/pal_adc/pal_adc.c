/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : pal_adc.c
|    Project        : VCU_ASW
|    Description    : The file implements the peripheral driver for ADC.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 21/03/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"pal_adc.h"
#include"pal_adc_conf.h"
#include"Config_ADCA0.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/

bool AdcReadCompleted_b = true;

/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : AdcInit
*   Description   : The function initialize the ADC module
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void AdcInit(void)
{
#ifdef RH850_F1KM_S1
    R_Config_ADCA0_Create();
    /* Call HAL driver function*/
#endif     
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : AdcStart
*   Description   : The function enable the ADC input
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void AdcStart(void)
{
#ifdef RH850_F1KM_S1
    /* Call HAL driver function*/
    R_Config_ADCA0_ScanGroup1_Start();
#endif 
    AdcReadCompleted_b = true;
    /* ADC reads the input if previous state is completed */
    return;    
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadAdcInput
*   Description   : The function scan the analog pins.
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void ReadAdcInput(void)
{
#ifdef RH850_F1KM_S1
    /* Call HAL driver function*/
    if(TRUE == AdcReadCompleted_b)
    {
	AdcReadCompleted_b = FALSE;
    R_Config_ADCA0_ScanGroup1_OperationOn();
	while(!AdcReadCompleted_b);
    }
#endif 
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadAdcInput
*   Description   : The function scan the analog pins.
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint16_t GetAdcResult(uint8_t AdcInp_u8)
{
    uint8_t LoopCnt_u8 = 0;
    uint16_t AdcValue_u16 = 0;

    for(LoopCnt_u8 = ANI_START_E;LoopCnt_u8<TOTAL_ANI_E;LoopCnt_u8++)
    {
        if(AdcInp_u8 == AdcInpConf_aSt[LoopCnt_u8].AnalogInput_u8)
        {
            AdcValue_u16 = AdcResultBuff_au8[AdcInpConf_aSt[LoopCnt_u8].BuffNum_u8];
        }
    }
    return AdcValue_u16;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : AdcStop
*   Description   : The function halts the ADC.
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void AdcStop(void)
{
#ifdef RH850_F1KM_S1
    /* Call HAL driver function*/
#endif     
    return;
}
/*---------------------- End of File -----------------------------------------*/