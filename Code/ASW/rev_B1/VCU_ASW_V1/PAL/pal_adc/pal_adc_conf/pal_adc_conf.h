/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : pal_adc_conf.h
|    Project        : VCU ASW
|    Description    : The file contains the configurable data of ADC inputs.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 28/03/2022       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef PAL_ADC_CONF_H
#define PAL_ADC_CONF_H
/*******************************************************************************
 *  HEARDER FILE INCLUDES
 ******************************************************************************/
#include"app_typedef.h"
/*******************************************************************************
 *  MACRO DEFNITION
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
    ADC_BUFF_1_E,
    ADC_BUFF_2_E,
    ADC_BUFF_3_E,
    ADC_BUFF_4_E,
    ADC_BUFF_5_E,
    ADC_BUFF_6_E,
    ADC_BUFF_7_E,
    ADC_BUFF_8_E,
    ADC_BUFF_9_E,
    ADC_BUFF_10_E,
    ADC_BUFF_11_E,
    ADC_BUFF_12_E,
    ADC_BUFF_13_E,
    ADC_BUFF_14_E,
    ADC_BUFF_15_E,
    ADC_BUFF_16_E,
    ADC_BUFF_17_E,
    ADC_BUFF_18_E,
    ADC_BUFF_19_E,
    ADC_BUFF_20_E,
    ADC_BUFF_21_E,
    ADC_BUFF_22_E,
    ADC_BUFF_23_E,
    ADC_BUFF_24_E,
    ADC_BUFF_25_E,
    TOTAL_ADC_BUFF_E,
}AdcResultBuff_En_t;

typedef enum
{
    ANI_START_E,
    ANI_1_E = ANI_START_E,
    ANI_2_E,
    ANI_3_E,
    ANI_4_E,
    ANI_5_E,
    ANI_6_E,
    ANI_7_E,
    ANI_8_E,
    ANI_9_E,
    ANI_10_E,
    ANI_11_E,
    ANI_12_E,
    ANI_13_E,
    ANI_14_E,
    ANI_END_E,
    TOTAL_ANI_E = ANI_END_E,
}AnalogInp_En_t;

typedef struct 
{
    uint8_t AnalogInput_u8;
    uint8_t BuffNum_u8;
}AdcInpConf_St_t;

/*******************************************************************************
 *  EXTERN GLOBAL VARIABLES
 ******************************************************************************/
extern AdcInpConf_St_t AdcInpConf_aSt[TOTAL_ANI_E];
extern uint16_t AdcResultBuff_au8[TOTAL_ADC_BUFF_E];
extern bool AdcReadCompleted_b;
/*******************************************************************************
 *  EXTERN FUNCTION
 ******************************************************************************/

#endif
/*---------------------- End of File -----------------------------------------*/