/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : pal_gpio.c
|    Project        : VCU_ASW
|    Description    : The file implements the peripheral driver for GPIO.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 21/03/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"pal_gpio_conf.h"
#include"pal_gpio.h"
#include"Config_PORT.h"
#include"hal_gpio.h"
#include"Pin.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadDigitalInput
*   Description   : The function reads the digital input 
*   Parameters    : DInput_u8: Digital input number
*   Return Value  : The function returns the pin state 
*                   1 :- High level
*                   0 :- Low level
*  ---------------------------------------------------------------------------*/
void PortInit(void)
{
#ifdef RH850_F1KM_S1
    /* Call HAL driver function*/
    R_Config_PORT_Create();
    R_Pins_Create();
#endif    
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadDigitalInput
*   Description   : The function reads the digital input 
*   Parameters    : DInput_u8: Digital input number
*   Return Value  : The function returns the pin state 
*                   1 :- High level
*                   0 :- Low level
*  ---------------------------------------------------------------------------*/
uint8_t ReadDigitalInput(const uint8_t DInput_u8)
{
    uint8_t LoopCnt_u8 = 0;

    for (LoopCnt_u8 = DI_START_E; LoopCnt_u8 < TOTAL_DI_E; LoopCnt_u8++)
    {
        if (DInput_u8 == DigitalInpConf_aSt[LoopCnt_u8].DigitalInp_u8)
        {
#ifdef RH850_F1KM_S1
            /* Call HAL driver function*/
            return ReadPortPin(DigitalInpConf_aSt[LoopCnt_u8].Port_u8,DigitalInpConf_aSt[LoopCnt_u8].Pin_u8);
#endif
        }
    }

    return TURN_OFF;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : WriteDigitalOutput
*   Description   : The function reads the digital input 
*   Parameters    : DOutput_u8: Digital output number
*                   PinState_u8: 
*                   0:- Reset the pin
*                   1:- Set the pin
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void WriteDigitalOutput(const uint8_t DOutput_u8,const uint8_t PinState_u8)
{
    uint8_t LoopCnt_u8 = 0;

    for (LoopCnt_u8 = DO_START_E; LoopCnt_u8 < TOTAL_DO_E; LoopCnt_u8++)
    {
        if (DOutput_u8 == DigitalOutConf_aSt[LoopCnt_u8].DigitalOut_u8)
        {
#ifdef RH850_F1KM_S1
            /* Call HAL driver function*/
            WritePortPin(DigitalOutConf_aSt[LoopCnt_u8].Port_u8,DigitalOutConf_aSt[LoopCnt_u8].Pin_u8,PinState_u8);
            break;
#endif
        }
    }
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ToggleOutput
*   Description   : The function reads the digital input 
*   Parameters    : DOutput_u8: Digital output number
*                   PinState_u8: 
*                   0:- Reset the pin
*                   1:- Set the pin
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void ToggleOutput(const uint8_t DOutput_u8 )
{
    uint8_t LoopCnt_u8 = 0;

    for (LoopCnt_u8 = DO_START_E; LoopCnt_u8 < TOTAL_DO_E; LoopCnt_u8++)
    {
        if (DOutput_u8 == DigitalOutConf_aSt[LoopCnt_u8].DigitalOut_u8)
        {
#ifdef RH850_F1KM_S1
            /* Call HAL driver function*/
            TogglePortPin(DigitalOutConf_aSt[LoopCnt_u8].Port_u8,DigitalOutConf_aSt[LoopCnt_u8].Pin_u8);
            break;
#endif
        }
    }
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadDigitalOutput
*   Description   : The function reads the pin state of the digital output 
*   Parameters    : DOutput_u8 Digital output number
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint8_t ReadDigitalOutput(const uint8_t DOutput_u8)
{
    uint8_t LoopCnt_u8 = 0;

    for (LoopCnt_u8 = DO_START_E; LoopCnt_u8 < TOTAL_DO_E; LoopCnt_u8++)
    {
        if (DOutput_u8 == DigitalOutConf_aSt[LoopCnt_u8].DigitalOut_u8)
        {
#ifdef RH850_F1KM_S1
            /* Call HAL driver function*/
            return ReadPortPin(DigitalOutConf_aSt[LoopCnt_u8].Port_u8,DigitalOutConf_aSt[LoopCnt_u8].Pin_u8);
#endif
        }
    }

    return TURN_OFF;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : enables the can moblule
*   Description   : The function writes the pin state of the can enble pins 
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/

  void  WriteCanEnablepin(void)
  {
    #ifdef RH850_F1KM_S1
        PORT.P9 &= (~_PORT_Pn1_OUTPUT_HIGH);     //  to enable the can
	    PORT.P11 &= (~_PORT_Pn1_OUTPUT_HIGH);    //  to enable the can
	    PORT.P10 &= (~_PORT_Pn15_OUTPUT_HIGH);   //  to enable the can
  #endif
  return ;
  }



  /* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : disables the can moblule
*   Description   : The function writes the pin state of the can disable pins 
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/

  void  WriteCanDisablepin(void)
  {
    #ifdef RH850_F1KM_S1
        PORT.P9  |= (_PORT_Pn1_OUTPUT_HIGH);     //  to disable the can
	    PORT.P11 |= (_PORT_Pn1_OUTPUT_HIGH);    //  to disable the can
	    PORT.P10 |= (_PORT_Pn15_OUTPUT_HIGH);   //  to disable the can
  #endif
  return ;
  }
/*---------------------- End of File -----------------------------------------*/