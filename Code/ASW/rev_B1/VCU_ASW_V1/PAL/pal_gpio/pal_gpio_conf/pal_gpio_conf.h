/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : pal_gpio_conf.h
|    Project        : VCU_ASW
|    Description    : The file is a configuration files for Port pin..
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 21/03/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef PAL_GPIO_CONF_H
#define PAL_GPIO_CONF_H
/*******************************************************************************
 *  HEARDER FILE INCLUDES
 ******************************************************************************/
#include "app_typedef.h"
/*******************************************************************************
 *  MACRO DEFNITION
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/

typedef enum
{
    DI_START_E,
    DI_1_E = DI_START_E,
    DI_2_E,
    DI_3_E,
    DI_4_E,
    DI_5_E,
    DI_6_E,
    DI_7_E,
    DI_8_E,
    DI_9_E,
    DI_10_E,
    DI_11_E,
    DI_12_E,
    DI_END_E,
    TOTAL_DI_E = DI_END_E,
}DigitalInp_En_t;


typedef enum
{
    DO_START_E,
    DO_1_E = DO_START_E,
    DO_2_E,
    DO_3_E,
    DO_4_E,
    DO_5_E,
    DO_6_E,
    DO_7_E,
    DO_8_E,
    DO_END_E,
    TOTAL_DO_E = DO_END_E,
}DigitalOut_En_t;

#pragma pack(1)
typedef const struct 
{
    uint8_t DigitalInp_u8;
    uint8_t Port_u8;
    uint8_t Pin_u8;
}DigitalInpConf_St_t;
#pragma unpack
#pragma pack(1)
typedef const struct 
{
    uint8_t DigitalOut_u8;
    uint8_t Port_u8;
    uint8_t Pin_u8;   
}DigitalOutConf_St_t;
#pragma unpack

/*******************************************************************************
 *  EXTERN GLOBAL VARIABLES AND STATIC VARIABLES
 ******************************************************************************/
extern DigitalInpConf_St_t DigitalInpConf_aSt[TOTAL_DI_E];
extern DigitalOutConf_St_t DigitalOutConf_aSt[TOTAL_DO_E];
/*******************************************************************************
 *  EXTERN FUNCTION
 ******************************************************************************/

#endif
/*---------------------- End of File -----------------------------------------*/