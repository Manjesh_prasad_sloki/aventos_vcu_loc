/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : pal_iic.c
|    Project        : Aventos Energy
|    Description    : The file implements the pal layer for the IIC.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 28/02/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"pal_iic.h"
#include "Config_RIIC0.h"
#include "Communicator.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

//#define true (1U)
//#define false (0U)
/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/



void I2cInit(void)
{
    #ifdef RH850_F1KM_S1
    /* Call HAL driver function*/
   	R_Config_RIIC0_Create();
#endif
    return;
}

void I2cStart(void)
{
    #ifdef RH850_F1KM_S1
 R_Config_RIIC0_Start();
 #endif
    return;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ConfigureRtc
*   Description   : The function configure the real time.
*   Parameters    : IIcTxBuff_pu8: 
                  : Size_u16:
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Pal_IIC_Send(uint16_t SlaveAddr_u16,uint8_t* IIcTxBuff_pu8,uint16_t Size_u16)
{
    uint16_t i = 0;
    uint32_t timer_var =0;

    // if((RIIC_TxComplete_b != false) || 
	// (RIIC_RxError_b != false) || 
    // 	(RIIC_RxComplete_b != false))
    // {
	//     NOP();
	// if(RIIC_TxComplete_b != false)
	// {
    //         NOP();
	// }
    //     if(RIIC_RxError_b != false)
    //     {
    //         NOP();
    //     }
    //     if(RIIC_RxComplete_b != false)
    //     {
    //         NOP();
    //     }
	//     return;
    // }
	    
         if(IIC_BusBusyCheck())
         {
             return;
         }
    RIIC_TxComplete_b = false;
    RIIC_RxError_b = false;
    R_Config_RIIC0_Master_Send(SlaveAddr_u16,IIcTxBuff_pu8,Size_u16);
    timer_var = GET_VCU_TIME_MS();
    while((!RIIC_TxComplete_b) && (!RIIC_RxError_b))
    {
	    if((GET_VCU_TIME_MS() - timer_var) >= 100)
        {
		NOP();
            break;
        }
    }
    // RIIC_TxComplete_b = false;
    // RIIC_RxError_b = false;ss
    for(i = 0;i<6000;i++)
    {
	    ;
    }

    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadRtcData
*   Description   : The function configure the real time.
*   Parameters    : IIcTxBuff_pu8: 
                  : Size_u16:
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Pal_IIC_Recieve(uint16_t SlaveAddr_u16,uint8_t* IIcRxBuff_pu8,uint16_t Size_u16)
{
    uint16_t i = 0;
    uint32_t timer_var =0;
    
    // if((RIIC_TxComplete_b != false) || 
	// (RIIC_RxError_b != false) || 
    // 	(RIIC_RxComplete_b != false))
    // {
	//     NOP();
	//     return;
    // }
    
     if(IIC_BusBusyCheck())
         {
             return;
         }
    RIIC_RxComplete_b = false;
    RIIC_RxError_b = false;
    R_Config_RIIC0_Master_Receive(SlaveAddr_u16,IIcRxBuff_pu8,Size_u16);
    timer_var = GET_VCU_TIME_MS();
    while((!RIIC_RxComplete_b) && (!RIIC_RxError_b))
    {
	    if((GET_VCU_TIME_MS() - timer_var) >= 100)
        {
	   NOP();
            break;
        }
    }

    // RIIC_RxComplete_b = false;
    // RIIC_RxError_b = false;
    for(i = 0;i<6000;i++)
    {
	    ;
    }

    return;
}

/*---------------------- End of File -----------------------------------------*/