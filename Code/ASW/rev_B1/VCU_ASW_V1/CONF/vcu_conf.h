/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : vcu_conf.h
|    Project	    : VCU
|    Module         : VCU configuration
|    Description    : This file contains the macros which specifies module  			
|                     that VCU supported.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 12/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef	VCU_CONF_H_
#define VCU_CONF_H_

#include "r_cg_macrodriver.h"
//#define TESTING_BMS_trg1
//#define  TESTING_USER_MODE_SW
//#define  TESTING_SOC_LOWERLT
#define RH850_F1KM_S1
#define	VCU_CONF_HMI_SUPPORTED		(TRUE)	//TODO Configurable Parameter
#define	VCU_CONF_BMS_SUPPORTED		(TRUE)	//TODO Configurable Parameter
#define	VCU_CONF_MC_SUPPORTED		(TRUE)	//TODO Configurable Parameter
#define VCU_SD_CARD_LOG_SUPPORTED   (TRUE)
#define VCU_INTERNAL_RTC_SUPPORTED   (TRUE)


#endif /* BOARD_INIT_H */
/*---------------------- End of File -----------------------------------------*/
