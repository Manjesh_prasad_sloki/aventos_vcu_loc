; *********************************************************************************************************************
; File Name     : $Source: dr7f701007_pic.dir $
; Mod. Revision : $Revision: 1.1 $
; Mod. Date     : $Date: 2019/04/26 09:06:16JST $
; Device(s)     : RV40 Flash based RH850 microcontroller
; Description   : Linker directives for FCL sample application
; *********************************************************************************************************************

; *********************************************************************************************************************
; DISCLAIMER
; This software is supplied by Renesas Electronics Corporation and is only  intended for use with
; Renesas products. No other uses are authorized. This software is owned by Renesas Electronics
; Corporation and is protected under all applicable laws, including copyright laws.
; THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE,
; WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
; TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR
; ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
; CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
; BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
; Renesas reserves the right, without notice, to make changes to this software and to discontinue the
; availability of this software. By using this software, you agree to the additional terms and conditions
; found by accessing the  following link:
; http://www.renesas.com/disclaimer
;
; Copyright (C) 2015-2019 Renesas Electronics Corporation. All rights reserved.
; *********************************************************************************************************************

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; IMPORTANT considerations
; ------------------------
; 1. Starting address of FCL_RESERVED section is the define value for FCL_RAM_ADDRESS used inside the code
; 2. The order of sections R_FCL_CODE_RAM_USRINT,R_FCL_CODE_RAM_USR,R_FCL_CODE_RAM,R_FCL_CODE_ROMRAM is critical and
;    other sections should not be interposed.
; 3. Size of section FCL_RESERVED must be equal or larger than the space occupied by the sections enumerated at 2.
; 4. When all of conditions described later are satisfied, user applications must copy the ".pcconst32" section
;    to RAM. Because the R_FCL_CopySections function does not copy the ".pcconst32" section.
;    Caution: Keep the distance between code area and the ".pcconst32" section same as the distance at linkage
;             when copying the ".pcconst32" section to RAM.
;    Conditions:
;    - The compiler options -pic and -pirod are applied to user applications.
;    - Use the R_FCL_CopySections function.
;    - Functions in R_FCL_CODE_USRINT or R_FCL_CODE_USR have data which is allocated at ".pcconst32".
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
-rom=.data=.data.R

-start= RESET,                                  &
        VECT,                                   &
        EIINTTBL,                               &
        .const,                                 &
        .INIT_DSEC.const,                       &
        .INIT_BSEC.const,                       &
        .text,                                  &
        .pctext,                                &
        .data,                                  &
        R_FCL_CODE_ROM.pctext,                  &
        R_FCL_CONST.pcconst32,                  &
        R_FCL_CODE_USRINT.pctext,               &
        R_FCL_CODE_USR.pctext,                  &
        R_FCL_CODE_RAM.pctext,                  &
        R_FCL_CODE_ROMRAM.pctext,               &
        R_FCL_CODE_RAM_EX_PROT.pctext,          &
        .pcconst32                      /00000000

-start= FCL_RESERVED.bss/FEDE0000,              &
        .bss,                                   &
        .data.R,                                &
        R_FCL_DATA.bss,                         &
        .stack.bss                      /FEDE8000
