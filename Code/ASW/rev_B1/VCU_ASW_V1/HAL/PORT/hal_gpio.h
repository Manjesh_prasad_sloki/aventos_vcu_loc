/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : hal_gpio.c
|    Project        : VCU_ASW
|    Description    : The file implements the HAL for the GPIO.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 22/03/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef HAL_GPIO_H
#define HAL_GPIO_H
/*******************************************************************************
 *  HEARDER FILE INCLUDES
 ******************************************************************************/
#include"app_typedef.h"
/*******************************************************************************
 *  MACRO DEFNITION
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef enum
{
    PORT_START_E,
    PORT_0_E = PORT_START_E,
    PORT_8_E,
    PORT_9_E,
    PORT_10_E,
    PORT_11_E,
    APORT_0_E,
    APORT_9_E,
    APORT_8_E,
    APORT_7_E,
    APORT_6_E,
    APORT_5_E,
    APORT_10_E,
    PORT_END_E,
    TOTAL_PORT_E = PORT_END_E,
}PortNumber_En_t;

typedef enum
{
    PIN_0_E,
    PIN_1_E,
    PIN_2_E,
    PIN_3_E,
    PIN_4_E,
    PIN_5_E,
    PIN_6_E,
    PIN_7_E,
    PIN_8_E,
    PIN_9_E,
    PIN_10_E,
    PIN_11_E,
    PIN_12_E,
    PIN_13_E,
    PIN_14_E,
    PIN_15_E,
    PIN_16_E,
}PinNumber_En_t;

#pragma pack(1)
typedef  const struct 
{
    volatile uint16_t *PortPinRead_pu16;
    volatile uint16_t *PortPinWrite_pu16;
    uint8_t Port_u8;
}HalPort_St_t;
#pragma unpack
/*******************************************************************************
 *  EXTERN GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  EXTERN FUNCTION
 ******************************************************************************/
extern void TogglePortPin(uint8_t PortNum_u8, uint8_t Pin_u8);
extern void WritePortPin(uint8_t PortNum_u8, uint8_t Pin_u8, uint8_t State_u8);
extern uint8_t ReadPortPin(uint8_t PortNum_u8,uint8_t Pin_u8);
#endif
/*---------------------- End of File -----------------------------------------*/