/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_RIIC1_user.c
* Version      : 1.2.0
* Device(s)    : R7F701689
* Description  : This file implements device driver for Config_RIIC1.
* Creation Date: 2021-10-21
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_RIIC1.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
extern volatile uint8_t          g_riic1_mode_flag;               /* RIIC1 master transmit receive flag */
extern volatile uint8_t          g_riic1_state;                   /* RIIC1 master state */
extern volatile uint16_t         g_riic1_slave_address;           /* RIIC1 slave address */
extern volatile const uint8_t *  gp_riic1_tx_address;             /* RIIC1 transmit buffer address */
extern volatile uint16_t         g_riic1_tx_count;                /* RIIC1 transmit data number */
extern volatile uint8_t *        gp_riic1_rx_address;             /* RIIC1 receive buffer address */
extern volatile uint16_t         g_riic1_rx_count;                /* RIIC1 receive data number */
extern volatile uint16_t         g_riic1_rx_length;               /* RIIC1 receive data length */
extern volatile uint8_t          g_riic1_stop_generation;         /* RIIC1 stop condition generation flag */
/* Start user code for global. Do not edit comment generated here */
extern bool I2C_Receive_b;
extern bool I2C_Sent_b;
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Create_UserInit
* Description  : This function adds user code after initializing RIIC1 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_RIIC1_Create_UserInit(void)
{
    /* Start user code for user init. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_error_interrupt
* Description  : This function error interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_RIIC1_error_interrupt(enable=false, channel=241, fpu=true, callt=false)
void r_Config_RIIC1_error_interrupt(void)
{
    volatile uint8_t dummy;

    if((RIIC1.IER.UINT32 & _RIIC_ARBITRATION_LOST_INT_ENABLE) && (RIIC1.SR2.UINT32 & _RIIC_ARBITRATION_LOST))
    {
        r_Config_RIIC1_callback_receiveerror(MD_ERROR1);
    }
    else if((RIIC1.IER.UINT32 & _RIIC_TIMEOUT_INT_ENABLE) && (RIIC1.SR2.UINT32 & _RIIC_TIMEOUT_DETECTED))
    {
        r_Config_RIIC1_callback_receiveerror(MD_ERROR2);
    }
    else if((RIIC1.IER.UINT32 & _RIIC_NACK_RECEPTION_INT_ENABLE) && (RIIC1.SR2.UINT32 & _RIIC_NACK_DETECTED)) 
    {
        /* Dummy read to release SCL */
        dummy = RIIC1.DRR.UINT32;
        RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_NACK_DETECTED;
        r_Config_RIIC1_callback_receiveerror(MD_ERROR3);
    }
    else if (g_riic1_mode_flag == _RIIC_MASTER_TRANSMIT)
    {
        if ((g_riic1_state == _RIIC_MASTER_SENDS_ADR_7_W) || (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10A_W))
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_DETECTED;
            RIIC1.IER.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_INT_ENABLE;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_STOP)
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_NACK_DETECTED;
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_STOP_CONDITION_DETECTED;

            r_Config_RIIC1_callback_transmitend();
        }
    }
    else if (g_riic1_mode_flag == _RIIC_MASTER_RECEIVE)
    {
        if ((g_riic1_state == _RIIC_MASTER_SENDS_ADR_7_R) || (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10A_W))
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_DETECTED;
            RIIC1.IER.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_INT_ENABLE;
        }
        else if (g_riic1_state == _RIIC_MASTER_RECEIVES_RESTART)
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_DETECTED;
            RIIC1.IER.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_INT_ENABLE;
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10A_R;
        }
        else if (g_riic1_state == _RIIC_MASTER_RECEIVES_STOP)
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_NACK_DETECTED;
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_STOP_CONDITION_DETECTED;

            r_Config_RIIC1_callback_receiveend();
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_transmit_interrupt
* Description  : This function send interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_RIIC1_transmit_interrupt(enable=false, channel=240, fpu=true, callt=false)
void r_Config_RIIC1_transmit_interrupt(void)
{
    if (g_riic1_mode_flag == _RIIC_MASTER_TRANSMIT)
    {
        if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_7_W)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(g_riic1_slave_address << _RIIC_ADDRESS_7BIT_SHIFT);
            g_riic1_state = _RIIC_MASTER_SENDS_DATA;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10A_W)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(((g_riic1_slave_address & _RIIC_ADDRESS_10BIT_UPPER) >> 
                               _RIIC_ADDRESS_10BIT_SHIFT) | _RIIC_ADDRESS_10BIT_UPPER_FORMAT);
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10B;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10B)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(g_riic1_slave_address & _RIIC_ADDRESS_10BIT_LOWER);
            g_riic1_state = _RIIC_MASTER_SENDS_DATA;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_DATA)
        {
            if (g_riic1_tx_count > 0U)
            {
                RIIC1.DRT.UINT32 = *gp_riic1_tx_address;
                gp_riic1_tx_address++;
                g_riic1_tx_count--;
            } 
            else
            {
                g_riic1_state = _RIIC_MASTER_SENDS_END;
            }
        }
    }
    else if (g_riic1_mode_flag == _RIIC_MASTER_RECEIVE)
    {
        if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_7_R)
        {
            RIIC1.DRT.UINT32 = (uint8_t)((g_riic1_slave_address << _RIIC_ADDRESS_7BIT_SHIFT)  | 
                               _RIIC_ADDRESS_RECEIVE);
            g_riic1_state = _RIIC_MASTER_RECEIVES_START;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10A_W)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(((g_riic1_slave_address & _RIIC_ADDRESS_10BIT_UPPER) >> 
                               _RIIC_ADDRESS_10BIT_SHIFT) | _RIIC_ADDRESS_10BIT_UPPER_FORMAT);
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10B;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10B)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(g_riic1_slave_address &  _RIIC_ADDRESS_10BIT_LOWER);
            g_riic1_state = _RIIC_MASTER_RECEIVES_RESTART;
        }
        else if (g_riic1_state == _RIIC_MASTER_RECEIVES_RESTART)
        {
            RIIC1.IER.UINT32 |= _RIIC_START_CONDITION_INT_ENABLE;
            /* Set restart condition flag */
            RIIC1.CR2.UINT32 |= _RIIC_RESTART_CONDITION_REQUEST;
        }
        else if (g_riic1_state == _RIIC_MASTER_SENDS_ADR_10A_R)
        {
            RIIC1.DRT.UINT32 = (uint8_t)(((g_riic1_slave_address &  _RIIC_ADDRESS_10BIT_UPPER) >> 
                               _RIIC_ADDRESS_10BIT_SHIFT) | _RIIC_ADDRESS_10BIT_UPPER_FORMAT | 
                               _RIIC_ADDRESS_RECEIVE);
            g_riic1_state = _RIIC_MASTER_RECEIVES_START;
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_transmitend_interrupt
* Description  : This function send end interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_RIIC1_transmitend_interrupt(enable=false, channel=243, fpu=true, callt=false)
void r_Config_RIIC1_transmitend_interrupt(void)
{
    if (g_riic1_state == _RIIC_MASTER_SENDS_END)
    {
        if (1U == g_riic1_stop_generation)
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_STOP_CONDITION_DETECTED;
            RIIC1.CR2.UINT32 |= _RIIC_STOP_CONDITION_REQUEST;

            g_riic1_state = _RIIC_MASTER_SENDS_STOP;
        }
        else
        {
            RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_TRANSMIT_END;
            r_Config_RIIC1_callback_transmitend();
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_receive_interrupt
* Description  : This function receive interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_RIIC1_receive_interrupt(enable=false, channel=242, fpu=true, callt=false)
void r_Config_RIIC1_receive_interrupt(void)
{
    volatile uint8_t dummy;
    uint16_t temp;

    if (g_riic1_state == _RIIC_MASTER_RECEIVES_START)
    {
        if (g_riic1_rx_length < 3U)
        {
            RIIC1.MR3.UINT32 |= _RIIC_WAIT;
            if (g_riic1_rx_length == 1U)
            {
                RIIC1.MR3.UINT32 |= _RIIC_ACKBT_BIT_MODIFICATION_ENABLED;
                RIIC1.MR3.UINT32 |= _RIIC_NACK_TRANSMISSION;
            }
        }
        dummy = RIIC1.DRR.UINT32;

        if (g_riic1_rx_length == 1U)
        {
            g_riic1_state = _RIIC_MASTER_RECEIVES_STOPPING;
        }
        else
        {
            g_riic1_state = _RIIC_MASTER_RECEIVES_DATA;
        }
    }
    else if (g_riic1_state == _RIIC_MASTER_RECEIVES_DATA)
    {
        temp = g_riic1_rx_length;
        if (g_riic1_rx_count < temp)
        {
            temp = g_riic1_rx_length;
            if (g_riic1_rx_count == (temp - 3U))
            {
                RIIC1.MR3.UINT32 |= _RIIC_WAIT;
                *gp_riic1_rx_address = RIIC1.DRR.UINT32;
                gp_riic1_rx_address++;
                g_riic1_rx_count++;
            }
            else if (g_riic1_rx_count == (temp - 2U))
            {
                RIIC1.MR3.UINT32 |= _RIIC_ACKBT_BIT_MODIFICATION_ENABLED;
                RIIC1.MR3.UINT32 |= _RIIC_NACK_TRANSMISSION;
                *gp_riic1_rx_address = RIIC1.DRR.UINT32;
                gp_riic1_rx_address++;
                g_riic1_rx_count++;
                g_riic1_state = _RIIC_MASTER_RECEIVES_STOPPING;
            }
            else
            {
                *gp_riic1_rx_address = RIIC1.DRR.UINT32;
                gp_riic1_rx_address++;
                g_riic1_rx_count++;
            }
        }
    }
    else if (g_riic1_state == _RIIC_MASTER_RECEIVES_STOPPING)
    {
        RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_STOP_CONDITION_DETECTED;
        RIIC1.CR2.UINT32 |= _RIIC_STOP_CONDITION_REQUEST;
        *gp_riic1_rx_address = RIIC1.DRR.UINT32;
        gp_riic1_rx_address++;
        g_riic1_rx_count++;
        RIIC1.MR3.UINT32 &= (uint32_t) ~_RIIC_WAIT;
        g_riic1_state = _RIIC_MASTER_RECEIVES_STOP;
    }
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_callback_transmitend
* Description  : This function sendend callback service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_Config_RIIC1_callback_transmitend(void)
{
    /* Start user code for r_Config_RIIC1_callback_transmitend. Do not edit comment generated here */
    NOP();
    I2C_Sent_b = true;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_callback_receiveend
* Description  : This function receiveend callback service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_Config_RIIC1_callback_receiveend(void)
{
    /* Start user code for r_Config_RIIC1_callback_receiveend. Do not edit comment generated here */
    NOP();
    I2C_Receive_b = true;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_Config_RIIC1_callback_receiveerror
* Description  : This function error callback service routine.
* Arguments    : status -
*                    error id
* Return Value : None -
***********************************************************************************************************************/
void r_Config_RIIC1_callback_receiveerror(MD_STATUS status)
{
    /* Start user code for r_Config_RIIC1_callback_receiveerror. Do not edit comment generated here */
    NOP();
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

