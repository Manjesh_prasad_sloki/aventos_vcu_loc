/***********************************************************************************************************************
* File Name    : RangeMileage.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef RANGE_MILEAGE_H
#define RANGE_MILEAGE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "Communicator.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define M_DATA_POINTS					5
#define KM_FACTOR						 (2.0f) 
#define WHKM_LOWER_LIMIT				 (17)
#define ARRAY_SIZE                       (5u)
#define DATASAMPLE_mt                     (100)

#define NOMINAL_VOLTAGE                  (50.4)
#define  NOMINAL_CAP                      (38)
#define  DOD                               (0.85)

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
#pragma pack(1)
typedef struct
{
	real_t Present_AvailableEnergy_Wh;
	real_t Previous_AvailableEnergy_Wh;
	real_t Wh_Consumed;
	real_t Wh_Km_M;
	real_t Wh_Km_M_Values[M_DATA_POINTS];
	real_t Wh_Km_M_Values_ECO[M_DATA_POINTS];
	real_t Historic_Wh_Km;
	real_t Instant_Wh_Km;
	real_t Range_in_Km;
}Mileage_Range_St_t;
#pragma pack(1)
typedef struct
{
	uint8_t Esti_Mileage_Wh_Km_u8;
	uint16_t Esti_Range_Km_u16;	
	bool  Esti_Range_Km_b;

}EstimatedRangeMileage_St_t;


typedef struct 
{
	float   EnergyConsumed_f;
	float    PreAvailEnergy_f;
	float    IGN_Wh_KmE[ARRAY_SIZE]; 
	uint8_t   index_eco;
	float    IGN_Wh_KmS[ARRAY_SIZE]; 
	uint8_t   index_sport;
	uint8_t  ActiveRideMode;
	uint8_t  PreviousRideMode;
	uint8_t  switch_variable;
	uint32_t  present_MRN;
	uint32_t  previous_MRN;
	uint32_t  TraveledMRN;
	float  	DistanceCovered;
	float  	TotalDistance;
	float   Total_WH_km;
	float   AvaEng_init;  
	float   TotalEnergyConsumed;
	float  	DistanceCovered_eco;
	float  	IGN_Km_eco;
	float   IGN_WH_eco;
	float   IGN_WH_km_eco;

	float  	DistanceCovered_sport;
	float  	IGN_Km_sport;
	float   IGN_WH_sport;
	float   IGN_WH_km_sport; 
	float   TotalEnergyConsumed_;
	float   WH_Battery;
	bool    BuffFull_Eco_b;
	bool    BuffFull_sport_b;
	float Range_hmi;
	uint8_t wh_km_hmi;
	bool ModeChange_b;
	 
}Range_estimation_St_t;

extern Range_estimation_St_t Range_estimation_St;
/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern real_t ODOforRM;
extern EstimatedRangeMileage_St_t	EstimatedRangeMileage_St;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Compute_Wh_On_VehicleStart(real_t AvailableCapacity_Ah, real_t BatteryVoltage_V);
extern void Estimate_Mileage_Range(real_t, real_t,uint8_t);
void Store_M_To_CircularBuffer(real_t);
/***********************************************************************************************************************
* Function Name: Restore_Wh_Km_from_FEE
* Description  : This function reads the Wh/Km M0 Data point from the flash memory and stores it to Local circular 
					buffer [On Init].
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
extern void Restore_Wh_Km_from_FEE(void);

/***********************************************************************************************************************
* Function Name: Store_Wh_Km_To_FEE
* Description  : This function stores the Wh/Km M0 Data Point to the Flash memory [On STOP/SHUTDOWN]
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
extern void Store_Wh_Km_To_FEE(void);
extern float rangeEstimation(void);
extern void PowerConsumptionModeChange(void);
float DistanceCovered(void);
bool checkDistanceCovered(void);
float computeAVG(float *davgdata);
void PowerConsumptionActive(void);

#endif /* RANGE_MILEAGE_H */


