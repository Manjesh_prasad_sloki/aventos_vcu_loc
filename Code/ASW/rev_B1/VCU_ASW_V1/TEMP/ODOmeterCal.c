/***********************************************************************************************************************
* File Name    : ODOmeterCalc.c
* Version      : 01
* Description  : This file implements the functions related to calculate the total distance travelled by the
					vehicle.
* Created By   : Dileepa B S
* Creation Date: 07/02/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODOmeterCal.h"
#include "VehicleSpeedCal.h"
#include "Communicator.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/



static uint32_t PresentODOmeter_u32 = 0;
//static uint32_t PreviousMRN_u32 	= 0;
uint32_t IGN_ODO_meter = 0;
 void ResetTheSpeedFilterData(void);
 uint32_t Estimate_ODO(uint32_t);


real_t ODO_m = 0;
ODO_Estimate_St_t       ODO_Estimate_St = {0,0,0,0,true};




/***********************************************************************************************************************
* Function Name: Set_PresentODOmeter
* Description  : This function update the present ODOmeter value in meters.
* Arguments    : uint32_t PresentODO_u32
* Return Value : None
***********************************************************************************************************************/
void Set_PresentODOmeter(uint32_t PresentODO_u32)
{
	//PresentODOmeter_u32 = PresentODO_u32;
	return;
}


/***********************************************************************************************************************
* Function Name: Get_PresentODOmeter
* Description  : This function update the present ODOmeter value in meters.
* Arguments    : None
* Return Value : uint32_t PresentODOmeter_u32
***********************************************************************************************************************/
uint32_t Get_PresentODOmeter(void)
{
	return PresentODOmeter_u32;
}


/***********************************************************************************************************************
* Function Name: Reset_ODOmeter_Data
* Description  : This function resets/clears the data related to the ODOmeter calculation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Reset_ODOmeter_Data(void)
{
	PresentODOmeter_u32 = 0;
	//PreviousMRN_u32 = 0;
	return;
}
/***********************************************************************************************************************
* Function Name: Restore_ODO_From_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_ODO_From_FEE(void)
{
    ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO from EEPROM on Start
}

/***********************************************************************************************************************
* Function Name: ResetTheODOEstimationData
* Description  : This function resets the Data and the Flags related to ODO estimation function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOEstimationData(void)
{
	ODO_Estimate_St.PresentMRN_u32 		= 0;
	ODO_Estimate_St.PreviousMRN_u32 	= 0;
	ODO_Estimate_St.Present_ODO_u32 	= 0;
	ODO_Estimate_St.Previous_ODO_u32 	= 0;
	ODO_Estimate_St.UpdateODOFromFlash_b 	= true;
	return;
}



/***********************************************************************************************************************
* Function Name: Estimate_ODO
* Description  : This function estimates the ODO in meters [Distance covered by the vehicle] corresponding to the MRN 
		 received from the Motor Controller Unit.
* Arguments    : uint32_t MRN_u32
* Return Value : uint32_t ODO_Out_u32
***********************************************************************************************************************/



uint32_t Estimate_ODO(uint32_t MRN_u32) //Motor_Rotation_Number_u32 pass
{
 
    static uint32_t ODO_Meters_u32 = 0;
    uint32_t temp_ODO_Meters_u32 = 0;
    temp_ODO_Meters_u32 = (( MRN_u32/GEAR_RATIO)*PIE * WHEEL_DIAMETER);
    CalculateRangeMileage_b = true; 
    if((temp_ODO_Meters_u32 - IGN_ODO_meter) >=10)
    {
	    
        eeprom_data_Tx.odo_reading_u32 = (temp_ODO_Meters_u32 + Ini_odo_reading_u32);
        FDL_write(0x00000,sizeof(eeprom_data_Tx),(uint8_t*)&eeprom_data_Tx);
        ODO_Meters_u32 = temp_ODO_Meters_u32;
        IGN_ODO_meter = ODO_Meters_u32;
	    //FDL_Read(0x0000,sizeof(eeprom_data_Rx),(uint8_t*)&eeprom_data_Rx_temp);
    }
    return ODO_Meters_u32;
}

/********************************************************EOF***********************************************************/