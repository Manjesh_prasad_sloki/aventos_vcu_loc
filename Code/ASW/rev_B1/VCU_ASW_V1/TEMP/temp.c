/***********************************************************************************************************************
* File Name    : temp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
//#include "temp.h"

#include "RangeMileage.h"
#include "bms.h"
#include "ODOmeterCal.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define _100_METER_ODO    100

#define Master_WH_KM_ECO       (16)
#define Master_WH_KM_Sport     (18)

#define  SAMPLING_DISTANCE_m      (100)
#define  STORAGE_DISTANCE_m       (500)
#define  SAMPLING_DISTANCE_Km     (SAMPLING_DISTANCE_m/1000)
#define  ARRAY_SIZE              (5)

typedef struct
{
	float   IGN_WH ;
	float IGN_KM;
	float IGN_WH_KM ;
	float  IGN_WH_ECO;
	float  IGN_KM_ECO ;
	float  IGN_WH_KM_ECO;
	float  IGN_WH_sport;
	float  IGN_KM_sport ;
	float   IGN_WH_KM_sport;
	float   WH_battery ; // @ end
	float   Range;
	float  WH_KM_HMI;
	float IGN_WH_KM_E_Array[ARRAY_SIZE];
	float IGN_WH_KM_S_Array[ARRAY_SIZE];
	float sampling_flag;
	float storage_flag;
	float previous_Available_energy_WH ;
	float current_Available_energy_WH ;
}Range_Estimation_st_t;

Range_Estimation_st_t Range_Estimation_st;





/***********************************************************************************************************************
* Function Name: ResetTheODOEstimationData
* Description  : This function resets the Data and the Flags related to ODO estimation function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

/*---------------------------------------------------------------------*/



/********************************************************EOF***********************************************************/