/***********************************************************************************************************************
* File Name    : RangeMileage.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "RangeMileage.h"
#include  "Communicator.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
real_t ODOforRM = 0;
Range_estimation_St_t Range_estimation_St;
Mileage_Range_St_t	Mileage_Range_St = {0,0};

bool WhComputeSport_On_Start_b = false;
bool WhComputeEco_On_Start_b = false;

EstimatedRangeMileage_St_t	EstimatedRangeMileage_St;
uint8_t WhKmRideMode_u8 = 0;
/***********************************************************************************************************************
* Function Name: Compute_Wh_On_VehicleStart
* Description  : This function estimates the present Wh in the battery on Ignition-On.
			This function is called when ODO is @ 0Km
* Arguments    : real_t AvailableCapacity_Ah, real_t BatteryVoltage_V
* Return Value : None
***********************************************************************************************************************/
void Compute_Wh_On_VehicleStart(real_t AvailableCapacity_Ah, real_t BatteryVoltage_V)
{
	BatteryVoltage_V = BMS_Get_Voltage();
	Mileage_Range_St.Previous_AvailableEnergy_Wh = (AvailableCapacity_Ah * BatteryVoltage_V);
	WhComputeEco_On_Start_b = true;
	WhComputeSport_On_Start_b = true;
	EstimatedRangeMileage_St.Esti_Range_Km_b = true;
	//EstimatedRangeMileage_St.Esti_Range_Km_u16  = BMS_Get_Battery_Availabe_Energy() /18;
	//EstimatedRangeMileage_St.Esti_Range_Km_u16  = BMS_Get_Battery_Availabe_Energy() /18;
	return;
}



/***********************************************************************************************************************
* Function Name: Estimate_Mileage_Range
* Description  : This function estimates the Mileage [Wh/Km] and Range [Km].
			This function is called every time  when ODO reaches 0.5km.
* Arguments    : real_t AvailableCapacity_Ah, real_t BatteryVoltage_V
* Return Value : EstimatedRangeMileage_St_t 
***********************************************************************************************************************/

void Estimate_Mileage_Range(real_t AvailableEnergy_Wh, real_t BatteryVoltage_V,uint8_t soc)
{
	//real_t SumOf_Wh_Km    = 0;
	//uint16_t LoopCount_u16	= 0;
	//static real_t IGN_wh = 0;
//	static uint32_t ODO_500m_Count_u32 = 0;

	EstimatedRangeMileage_St.Esti_Range_Km_u16  = (38*50.4*0.85*(soc/100));
	NOP();
	
//	Mileage_Range_St.Present_AvailableEnergy_Wh = AvailableEnergy_Wh; 
//	Mileage_Range_St.Wh_Consumed = (Mileage_Range_St.Previous_AvailableEnergy_Wh - Mileage_Range_St.Present_AvailableEnergy_Wh);
//	if(Mileage_Range_St.Wh_Consumed < 0)
//	{
//		Mileage_Range_St.Wh_Consumed *= (-1);
//		Mileage_Range_St.Previous_AvailableEnergy_Wh  = Mileage_Range_St.Present_AvailableEnergy_Wh;
//	}
//	else
//	{
//		;
//	}
//	IGN_wh += Mileage_Range_St.Wh_Consumed;
	

//	Mileage_Range_St.Wh_Km_M = IGN_wh / KM_FACTOR;

//	if(WHKM_LOWER_LIMIT > Mileage_Range_St.Wh_Km_M)
//	{
//		Mileage_Range_St.Wh_Km_M = WHKM_LOWER_LIMIT;
//		NOP();
//	}
//	Store_M_To_CircularBuffer(Mileage_Range_St.Wh_Km_M);
//	Mileage_Range_St.Previous_AvailableEnergy_Wh = Mileage_Range_St.Present_AvailableEnergy_Wh;
	
//	if (SPORT == WhKmRideMode_u8)
//	{
//		for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
//		{
//			SumOf_Wh_Km = SumOf_Wh_Km + Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16];
//		}
//	}
//	else if (ECO == WhKmRideMode_u8 || REVERSE == WhKmRideMode_u8)
//	{
//		for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
//		{
//			SumOf_Wh_Km = SumOf_Wh_Km + Mileage_Range_St.Wh_Km_M_Values_ECO[LoopCount_u16];
//		}
//	}
//	else 
//	{
//		;
//	}

//	Mileage_Range_St.Historic_Wh_Km = (SumOf_Wh_Km / M_DATA_POINTS);	
//					/*This Historic Wh/Km is dispalyed as mileage on HMI*/
	
//	SumOf_Wh_Km = 0;
//	Mileage_Range_St.Range_in_Km = ((Mileage_Range_St.Present_AvailableEnergy_Wh) / Mileage_Range_St.Historic_Wh_Km); 
//						/*0.7 and 0.3 are the weightage values, they may change based on testing*/
//	EstimatedRangeMileage_St.Esti_Mileage_Wh_Km_u8 = (uint8_t)Mileage_Range_St.Historic_Wh_Km;
//	//EstimatedRangeMileage_St.Esti_Range_Km_u16 = (uint16_t)Mileage_Range_St.Range_in_Km;
//	EstimatedRangeMileage_St.Esti_Range_Km_u16 = (uint16_t)Mileage_Range_St.Range_in_Km;//todo manjesh
//	return; 
}


/***********************************************************************************************************************
* Function Name: Store_M_To_CircularBuffer
* Description  : This function stores the estimated Wh/Km into circular buffer.
* Arguments    : real_t Present_WhPerKm_M
* Return Value : None
***********************************************************************************************************************/
void Store_M_To_CircularBuffer(real_t Present_WhPerKm_M)
{
	uint16_t LoopCount_u16 = 0;
	

	WhKmRideMode_u8 = Get_Actual_Ride_Mode();
	
	if(SPORT == WhKmRideMode_u8)
	{
		if(WhComputeSport_On_Start_b == true)
		{
			WhComputeSport_On_Start_b = false;
			
			for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
			{
				Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Present_WhPerKm_M;
			}
		}
		else
		{
			for(LoopCount_u16 = (M_DATA_POINTS - 1); LoopCount_u16 >= 1 ;  LoopCount_u16--)
				{
						Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16 - 1];
				}
			
				Mileage_Range_St.Wh_Km_M_Values[0] = Present_WhPerKm_M;	
		}
	}
	else if (ECO == WhKmRideMode_u8 || REVERSE == WhKmRideMode_u8)
	{
		NOP();
		if(WhComputeEco_On_Start_b == true)
		{
			WhComputeEco_On_Start_b = false;
			
			for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
			{
				Mileage_Range_St.Wh_Km_M_Values_ECO[LoopCount_u16] = Present_WhPerKm_M;
			}
		}
		else
		{
			for(LoopCount_u16 = (M_DATA_POINTS - 1); LoopCount_u16 >= 1 ;  LoopCount_u16--)
				{
						Mileage_Range_St.Wh_Km_M_Values_ECO[LoopCount_u16] = Mileage_Range_St.Wh_Km_M_Values_ECO[LoopCount_u16 - 1];
				}
			
				Mileage_Range_St.Wh_Km_M_Values_ECO[0] = Present_WhPerKm_M;	
		}
	}	
	return;
}


/***********************************************************************************************************************
* Function Name: Restore_Wh_Km_from_FEE
* Description  : This function reads the Wh/Km M0 Data point from the flash memory and stores it to Local circular 
					buffer [On Init].
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_Wh_Km_from_FEE(void)
{
	real_t Wh_Km_M0 = 10.0 ; //Read from FEE and Update
	uint16_t LoopCount_u16 = 0;
	for(LoopCount_u16 = 0; LoopCount_u16 < M_DATA_POINTS; LoopCount_u16++)
	{
		Mileage_Range_St.Wh_Km_M_Values[LoopCount_u16] = Wh_Km_M0;
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Store_Wh_Km_To_FEE
* Description  : This function stores the Wh/Km M0 Data Point to the Flash memory [On STOP/SHUTDOWN]
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Store_Wh_Km_To_FEE(void)
{
	//real_t Wh_Km_M0 = 0 ;
	//Wh_Km_M0 = Mileage_Range_St.Wh_Km_M_Values[0];

	//Todo:Store Wh_Km_M0 value into the EEPROM

	return;

}

/********************************************************EOF***********************************************************/


/*****************************Range Esstimation**************/



float rangeEstimation(void)
{
	if((BMS_Get_Battery_Availabe_Energy() > 0 ) && (Range_estimation_St.AvaEng_init == 0))
	{
		Range_estimation_St.AvaEng_init = BMS_Get_Battery_Availabe_Energy();
		Range_estimation_St.PreAvailEnergy_f = BMS_Get_Battery_Availabe_Energy();
	}
	if(Range_estimation_St.ActiveRideMode != Get_Actual_Ride_Mode())
	{
		Range_estimation_St.ModeChange_b = true;
		PowerConsumptionModeChange();
	}
	else if (Range_estimation_St.ActiveRideMode == Get_Actual_Ride_Mode())
	{
		if(checkDistanceCovered()== true)
		{
			PowerConsumptionActive();
		}
	}
	return	Range_estimation_St.Range_hmi;
}

void PowerConsumptionModeChange(void)
{
	float avg_wh_km ;
	float temp_SOH;
	float temp_SOC;
	Range_estimation_St.TotalEnergyConsumed = (Range_estimation_St.AvaEng_init - BMS_Get_Battery_Availabe_Energy());
	Range_estimation_St.EnergyConsumed_f = Range_estimation_St.PreAvailEnergy_f - BMS_Get_Battery_Availabe_Energy();
	Range_estimation_St.PreAvailEnergy_f = BMS_Get_Battery_Availabe_Energy();
	temp_SOC =((float)Get_BMS_SOC()/100); 
	temp_SOH =  ((float)Get_BMS_SOH()/100);
	Range_estimation_St.WH_Battery = (NOMINAL_VOLTAGE* NOMINAL_CAP * DOD * (temp_SOC) * (temp_SOH));
	if((ECO == Range_estimation_St.ActiveRideMode) && (SPORT == Get_Actual_Ride_Mode()))
	{
		Range_estimation_St.ActiveRideMode = SPORT;
		
		Range_estimation_St.IGN_WH_eco += Range_estimation_St.EnergyConsumed_f ; 
		Range_estimation_St.IGN_Km_eco += DistanceCovered();
		Range_estimation_St.IGN_WH_km_eco = Range_estimation_St.IGN_WH_eco/Range_estimation_St.IGN_Km_eco;
		if(Range_estimation_St.IGN_WH_km_eco < 17)
		{
			Range_estimation_St.IGN_WH_km_eco = 17;
		}
		Range_estimation_St.IGN_Wh_KmE[Range_estimation_St.index_eco] = Range_estimation_St.IGN_WH_km_eco;
		if((Range_estimation_St.index_eco++) >= ARRAY_SIZE )
		{
			Range_estimation_St.index_eco = 0;
			Range_estimation_St.BuffFull_Eco_b = true;
		}
		if(true == Range_estimation_St.BuffFull_sport_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmS);
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery/avg_wh_km;
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_SPORT;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_SPORT;
		}


	}
	else if((SPORT == Range_estimation_St.ActiveRideMode) && ( ECO  == Get_Actual_Ride_Mode()))
	{
		Range_estimation_St.ActiveRideMode = ECO;
		Range_estimation_St.IGN_WH_sport += Range_estimation_St.EnergyConsumed_f ; 
		Range_estimation_St.IGN_Km_sport += DistanceCovered();
		Range_estimation_St.IGN_WH_km_sport = Range_estimation_St.IGN_WH_sport/Range_estimation_St.IGN_Km_sport;
		if(Range_estimation_St.IGN_WH_km_sport < 17)
		{
			Range_estimation_St.IGN_WH_km_sport = 17;
		}
		Range_estimation_St.IGN_Wh_KmS[Range_estimation_St.index_sport] = Range_estimation_St.IGN_WH_km_sport;
		if((Range_estimation_St.index_sport++) >= ARRAY_SIZE )
		{
			Range_estimation_St.index_sport = 0;
			Range_estimation_St.BuffFull_sport_b = true;
		}
		if(true == Range_estimation_St.BuffFull_sport_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmS);
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery/avg_wh_km;;
		}
		if(true == Range_estimation_St.BuffFull_Eco_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmE);
			Range_estimation_St.Range_hmi =Range_estimation_St.WH_Battery/avg_wh_km;
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_ECO;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_ECO;
		}


	}
	else
	{
		Range_estimation_St.ActiveRideMode = Get_Actual_Ride_Mode();
		if(true == Range_estimation_St.BuffFull_Eco_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmE);
			Range_estimation_St.Range_hmi =  Range_estimation_St.WH_Battery/avg_wh_km;
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_ECO;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_ECO;
		}
	}
	return;
}

void PowerConsumptionActive(void)
{
	float avg_wh_km;
	float temp_soh;
	float temp_soc;
	
	Range_estimation_St.TotalEnergyConsumed = (Range_estimation_St.AvaEng_init - BMS_Get_Battery_Availabe_Energy());
	Range_estimation_St.TotalDistance +=DATASAMPLE_mt;
	Range_estimation_St.Total_WH_km = Range_estimation_St.TotalEnergyConsumed/Range_estimation_St.TotalDistance;
	Range_estimation_St.EnergyConsumed_f = Range_estimation_St.PreAvailEnergy_f - BMS_Get_Battery_Availabe_Energy();
	temp_soc =((float)Get_BMS_SOC()/100); 
	temp_soh =  ((float)Get_BMS_SOH()/100);
	Range_estimation_St.PreAvailEnergy_f = BMS_Get_Battery_Availabe_Energy();
	Range_estimation_St.WH_Battery = (NOMINAL_VOLTAGE* NOMINAL_CAP * DOD * (temp_soc) * (temp_soh));
	if(ECO == Get_Actual_Ride_Mode())
	{
		Range_estimation_St.IGN_WH_eco += Range_estimation_St.EnergyConsumed_f ; 
		Range_estimation_St.IGN_Km_eco += DATASAMPLE_mt;
		Range_estimation_St.IGN_WH_km_eco = Range_estimation_St.IGN_WH_eco/Range_estimation_St.IGN_Km_eco;
		if(Range_estimation_St.IGN_WH_km_eco < 17)
		{
			Range_estimation_St.IGN_WH_km_eco = 17;
		}
		Range_estimation_St.IGN_Wh_KmE[Range_estimation_St.index_eco] = Range_estimation_St.IGN_WH_km_eco;
		Range_estimation_St.index_eco++;
		if((Range_estimation_St.index_eco) >= ARRAY_SIZE )
		{
			Range_estimation_St.index_eco = 0;
			Range_estimation_St.BuffFull_Eco_b = true;
		}
		if(true == Range_estimation_St.BuffFull_Eco_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmE);
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery/ avg_wh_km;
			NOP();
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_ECO;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_ECO;
			NOP();
		}
	}
	else if(SPORT == Get_Actual_Ride_Mode())
	{
		Range_estimation_St.IGN_WH_sport += Range_estimation_St.EnergyConsumed_f ; 
		Range_estimation_St.IGN_Km_sport += DATASAMPLE_mt;
		Range_estimation_St.IGN_WH_km_sport = Range_estimation_St.IGN_WH_sport/Range_estimation_St.IGN_Km_sport;
		if(Range_estimation_St.IGN_WH_km_sport < 17)
		{
			Range_estimation_St.IGN_WH_km_sport = 17;
		}
		Range_estimation_St.IGN_Wh_KmS[Range_estimation_St.index_sport] = Range_estimation_St.IGN_WH_km_sport;
		if((Range_estimation_St.index_sport++) >= ARRAY_SIZE )
		{
			Range_estimation_St.index_sport = 0;
			Range_estimation_St.BuffFull_sport_b = true;
		}
		if(true == Range_estimation_St.BuffFull_sport_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmS);
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery/avg_wh_km;
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_SPORT;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_SPORT;
		}

	}
	else if((NEUTRAL == Get_Actual_Ride_Mode()) || (REVERSE == Get_Actual_Ride_Mode()) || (SAFE_MODE == Get_Actual_Ride_Mode()) )
	{
		if(true == Range_estimation_St.BuffFull_Eco_b)
		{
			avg_wh_km = computeAVG(Range_estimation_St.IGN_Wh_KmE);
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery /avg_wh_km;
		}
		else
		{
			Range_estimation_St.Range_hmi = Range_estimation_St.WH_Battery / MASTER_WH_KM_ECO;
			Range_estimation_St.wh_km_hmi = MASTER_WH_KM_ECO;
		}
	}
}


 float DistanceCovered(void)
 {
	Range_estimation_St.present_MRN = Get_Motor_MRN();
	if(Range_estimation_St.previous_MRN != Range_estimation_St.present_MRN)
	{
		Range_estimation_St.TraveledMRN = Range_estimation_St.present_MRN - Range_estimation_St.previous_MRN;
		Range_estimation_St.DistanceCovered = ((Range_estimation_St.TraveledMRN /GEAR_RATIO) *(PIE*WHEEL_DIAMETER));
		Range_estimation_St.previous_MRN	= Range_estimation_St.present_MRN;	
	}
	else 
	{
		Range_estimation_St.TraveledMRN = Range_estimation_St.present_MRN - Range_estimation_St.previous_MRN;
		Range_estimation_St.DistanceCovered = ((Range_estimation_St.TraveledMRN /GEAR_RATIO) *(PIE*WHEEL_DIAMETER));
	}
	return  Range_estimation_St.DistanceCovered; 
 }

bool checkDistanceCovered(void)
{
	bool returnVal = false;
	
	Range_estimation_St.present_MRN = Get_Motor_MRN();
	if(Range_estimation_St.previous_MRN != Range_estimation_St.present_MRN)
	{
		Range_estimation_St.TraveledMRN = Range_estimation_St.present_MRN - Range_estimation_St.previous_MRN;
		Range_estimation_St.DistanceCovered = ((Range_estimation_St.TraveledMRN /GEAR_RATIO) *(PIE*WHEEL_DIAMETER));
		if(Range_estimation_St.DistanceCovered >= 500)
		{
			NOP();	
		}
		if(Range_estimation_St.DistanceCovered >= DATASAMPLE_mt)
		{
			Range_estimation_St.previous_MRN	= Range_estimation_St.present_MRN;
			returnVal = true;
		}
	}

	return returnVal;
}


float computeAVG(float *davgdata)
{
	float sum = 0; 
//	for(index = 0 ;index < ARRAY_SIZE  ; index++)
//	{
//		sum += davgdata[index];
//	}
	sum = (davgdata[0] +davgdata[1]+davgdata[2]+davgdata[3]+davgdata[4])/5;
	//sum = sum/ ARRAY_SIZE;
	return sum ;
}
/*****************************Range Esstimation**************/

/*
Note: To be discuss with Geeth
- What is the RangeKm and Wh/Km on start [first time]
- M values on first time in Buffer 
- What is the Ah and Voltage given from the BMS at 100% SOC
- How Ah and V varies for each 500m
*/







