/******************************************************************************
 *    FILENAME    : uds_serv22.c
 *    DESCRIPTION : Service description for UDS service - READ DATA BY IDENTIFIER(0x22).
 ******************************************************************************
 * Revision history
 *
 * Ver Author          Date                     Description
 * 1   Sloki     10/01/2019		   Initial version
 ******************************************************************************
*/

/* Section: Included Files                                                    */

#include "iso14229_serv22.h"

/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
	banner.
 */
 /*
  @summary : this is for disable timer
  */



  /* ************************************************************************** */
  /* ************************************************************************** */
  /* ************************************************************************** */
  /* ************************************************************************** */
  /* Section: File Scope or Global Data                                         */
  /* ************************************************************************** */
  /* ************************************************************************** */


uint32_t Serv22_Index_u32 = 0u;               // Global Variable for access global data buffer
uint32_t Serv2C_Size_Counter_u32 = 0u;
uint32_t Serv2C_DID_CheckCounter_u32 = 0u;
uint32_t Serv2C_Table_DIDs = 0u;


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */



/**
*  FUNCTION NAME : iso14229_serv22
*  FILENAME      : iso14229_serv22.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributor table.
*  @brief        : This function will process the service_22 requests
*  @return       : Type of response.
*/
UDS_Serv_resptype_En_t iso14229_serv22(UDS_Serv_St_t* UDS_Serv_pSt)
{
	uint32_t Serv2C_Index_u32 = 0u;
	uint32_t i = 0u;                                                   /*Variables i and j for looping operation(LOOP Count)*/
	uint32_t Size = 0u;
	uint32_t DDID_Table_Size_u32 = 0u;
	ISO14229_DidList_En_t j = DID_LIST_START_E;
	uint16_t ReqDID_u16 = 0u;                                         /* Used for 2 Bytes DID NUMBER Storage for DID Comparison*/
	uint16_t Req_DID_List[MAX_DID_REQ] = { 0 };                                   /* Array for DID storage from Request*/
	uint16_t numbytes_u16 = UDS_Serv_pSt->RxLen_u16 - 1;
	uint16_t Total_ReqDIDs = 0u;                                     /* Used for store the available IDDs in request message*/
	UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;       /*Enum variable is used return a corresponding response*/
	bool DID_Found_b = FALSE;                                      /* Variable used as a Flag*/
	bool RTRN_b = FALSE;                                             /* Holds the return value from the Corresponding Function Pointer*/
	bool DID_Session_Status_b = FALSE;
	bool DID_Session_Support_b = FALSE;
	bool Atleast_One_DID_Status_b = FALSE;
	bool Jump_To_DDID_Table_b = FALSE;

	Total_ReqDIDs = numbytes_u16 / 2;                                /* Variable holds the Number of DIDs available in the Request Message*/
	Serv22_Index_u32 = 1u;

	for (i = 0; i < DDID_count_u32; i++)
	{
		if (DDID_ast[i].DDID_Num_u16 == NULL)
		{
			break;
		}
		DDID_Table_Size_u32 += 1;
	}

	if ((SERV22_MIN_LEN > numbytes_u16) || (numbytes_u16 % 2) || (SERV22_MAX_LEN < numbytes_u16))
	{
		UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
		UDS_Serv_pSt->TxBuff_pu8[1] = SID_RDBDID;
		UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;   /*NRC-0x13*/
		UDS_Serv_pSt->TxLen_u16 = 3u;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}

	/*    else if(FALSE == Check_SecurityClearance())
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_RDBDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;   //NRC-0x33
			UDS_Serv_pSt->TxLen_u16 = 3u;
			Serv_resptype_En = SERV_RESP_NEG_E;
		}
	*/

	else
	{
		for (i = 0; i < Total_ReqDIDs; i++)                                 /* executed upto Total number of DIDs available in request Message*/
		{
			Req_DID_List[i] = (UDS_Serv_pSt->RxBuff_pu8[(i * 2) + 1] << 8);   /* In this line of instruction Array Hold the Higher byte of DID*/
			Req_DID_List[i] |= (UDS_Serv_pSt->RxBuff_pu8[(i * 2) + 2]);       /* In this line of instruction Array Hold the Lower byte of DID*/
		}

		for (i = 0; i < Total_ReqDIDs; i++)
		{
			ReqDID_u16 = Req_DID_List[i];                              /*This Variable hold the 2 byte DID Number */
			Jump_To_DDID_Table_b = TRUE;
			for (j = DID_LIST_START_E; j < DID_TOTAL_E; j++)                                /* for loop for DID comparison for below condition*/
			{
				if ((ReqDID_u16 == DID_Config_aSt[j].DID_Num))           /*comparison*/
				{
					Jump_To_DDID_Table_b = FALSE;
					DID_Found_b = TRUE;
					DID_Session_Status_b = DID_Session_Check(j, SERV22_SEESION_CHECK_BIT_POS);
					if ((TRUE == DID_Session_Status_b))
					{
						//Stimulus(DID_Config_aSt[j].DID_Name_En, DID_Config_aSt[j].Real_Time_Val_u8, DID_Config_aSt[j].Var_ptr_pu8);	/* For IO read */ // todo harsh
						Var_Ptr_pu8 = DID_Config_aSt[j].Var_ptr_pu8;
						DID_Session_Support_b = TRUE;
						UDS_Serv_pSt->TxBuff_pu8[Serv22_Index_u32++] = (((DID_Config_aSt[j].DID_Num) >> 8) & 0xFF);    /* Store Higher Byte of DID*/
						UDS_Serv_pSt->TxBuff_pu8[Serv22_Index_u32++] = ((DID_Config_aSt[j].DID_Num) & 0xFF);           /* Store lower Byte of DID */
						RTRN_b = DID_Config_aSt[j].CALLBCK_Fptr(UDS_Serv_pSt->TxBuff_pu8, DID_Config_aSt[j].DID_Val_Size, READ_ONLY_E, Var_Ptr_pu8, DID_Config_aSt[j].Type_En);/* Function Pointer for Corresponding DID as we mentioned*/

						UDS_Serv_pSt->TxBuff_pu8[(DID_MASK_TBL[j].Masking_Byte_pos + 3)] |= DID_MASK_TBL[j].Masking_Data;

						if (RTRN_b)
						{
							DID_Session_Support_b = TRUE;           /* DID EXIST(Valid DID)*/
							Atleast_One_DID_Status_b = TRUE;
						}
						else
							DID_Session_Support_b = FALSE;
					}
					break;
				}
			}

			if (Jump_To_DDID_Table_b == TRUE)
			{
				for (Serv2C_Index_u32 = 0; Serv2C_Index_u32 < DDID_Table_Size_u32; Serv2C_Index_u32++)
				{
					if ((ReqDID_u16 == DDID_ast[Serv2C_Index_u32].DDID_Num_u16))
					{
						DID_Found_b = TRUE;
						DID_Session_Support_b = TRUE;
						Atleast_One_DID_Status_b = TRUE;
						UDS_Serv_pSt->TxBuff_pu8[Serv22_Index_u32++] = (((ReqDID_u16) >> 8) & 0xFF);    /* Store Higher Byte of DDID*/
						UDS_Serv_pSt->TxBuff_pu8[Serv22_Index_u32++] = ((ReqDID_u16) & 0xFF);
						for (Serv2C_Table_DIDs = 0; Serv2C_Table_DIDs < DDID_ast[Serv2C_Index_u32].DDID_DID_Count_u16; Serv2C_Table_DIDs++)
						{
							for (Serv2C_DID_CheckCounter_u32 = 0; Serv2C_DID_CheckCounter_u32 < DID_TOTAL_E; Serv2C_DID_CheckCounter_u32++)
							{
								if (DDID_ast[Serv2C_Index_u32].Req_DID_Table[Serv2C_Table_DIDs] == DID_Config_aSt[Serv2C_DID_CheckCounter_u32].DID_Num)
								{
									for (Size = 0; Size < DDID_ast[Serv2C_Index_u32].Req_DIDSize[Serv2C_Table_DIDs]; Size++)
									{
										UDS_Serv_pSt->TxBuff_pu8[Serv22_Index_u32++] = *(((uint8_t*)DID_Config_aSt[Serv2C_DID_CheckCounter_u32].Var_ptr_pu8) + Size);
									}
									break;
								}
							}
						}
					}
				}
			}
		}
		if (!DID_Found_b)
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_RDBDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;            /* NRC-0x31 */
			UDS_Serv_pSt->TxLen_u16 = 3u;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
		else if ((!DID_Session_Support_b) && (Atleast_One_DID_Status_b == FALSE))
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_RDBDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = CONDITION_NOT_CORRECT;/*NRC- 0x22*/
			UDS_Serv_pSt->TxLen_u16 = 3u;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
		else
		{
			UDS_Serv_pSt->TxLen_u16 = (Serv22_Index_u32 - 1);                            /*Total Number of Response Bytes*/
			Serv22_Index_u32 = 0u;
			Serv2C_Size_Counter_u32 = 0u;
			Serv_resptype_En = UDS_SERV_RESP_POS_E;                         /* Positive Response*/
		}
	}
	return Serv_resptype_En;
}


/**
*  FUNCTION NAME : UDS_Serv22_Timeout
*  FILENAME      : uds_serv22.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.
*/
void UDS_Serv22_Timeout(void) {

	return;
}


/* *****************************************************************************
 End of File
 */
