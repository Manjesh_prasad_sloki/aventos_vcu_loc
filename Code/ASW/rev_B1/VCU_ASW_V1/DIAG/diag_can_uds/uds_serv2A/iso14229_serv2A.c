/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Sloki Auto Technologies

  @File Name
    uds_serv2A.c

  @Summary
    This file contains variables and functions of iso14229 service 2A.

  @Author:
    Sloki
**/
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv2A.h"
#include "uds_DID.h"
 

uint32_t ModeToMs(uint8_t Mode) { 
    if(Mode == SLOW_RATE_E) 
    {
      return 600; 
    }    
else if(Mode == MEDIUM_RATE_E)  
{ 
return 100;  
} 
  else 
  {
     return 10; 
  }
} 

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */
 uint32_t DID_Conf_OffSet = 0u;
uint8_t PDID_Counter_u8 = 0u;
uint32_t Timer_Delay_For_Serv2A_u32;

//uint8_t PDID_F2A1_Comm_au8[];
//uint8_t PDID_F2A2_Comm_au8[];
//uint8_t PDID_F2A3_Comm_au8[];

/* @Summary: the periodic rate for each periodic data response message */
uint32_t	 PeriodicRate_u32;

/* @summary : this variable is for index */
 uint32_t  Index_Val_u32;

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */



/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/**
*  FUNCTION NAME : iso14229_serv2A
*  FILENAME      : iso14229_serv2A.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer
*                  table.
*  @brief        : This function will process the service_2a requests
*  @return       : Type of response.
**/
UDS_Serv_resptype_En_t iso14229_serv2A(UDS_Serv_St_t*  UDS_Serv_pSt)
{
    uint32_t i                                	= 0x00u;
    uint32_t j                                	= 0x00u;
    uint16_t numbytes_req_u16                 	= UDS_Serv_pSt->RxLen_u16;
    uint16_t PDID_Buffer_u8[4];
    uint8_t  transmissionMode_u8              	= UDS_Serv_pSt->RxBuff_pu8[1];
    uint8_t  numberOfPDID_u8                  	= 0x00u;
    bool     pdidSession_b                      = FALSE;
    bool MatchDID_b = FALSE;
    bool Session_Failed_b = FALSE;
    UDS_Serv_resptype_En_t Serv_resptype_En       	= UDS_SERV_RESP_UNKNOWN_E;

    if((SERV_2A_MIN_LEN > numbytes_req_u16) || (SERV_2A_MAX_LEN < numbytes_req_u16) ||
		((SERV_2A_MIN_LEN == numbytes_req_u16) && (STOP_SENDING_E != transmissionMode_u8)))
    {
        /* check whether length should be valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_PDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if((0x00 == transmissionMode_u8) || 
            (STOP_SENDING_E < transmissionMode_u8))
    {
        /* check whether transmissionMode supported or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_PDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        Timer_Delay_For_Serv2A_u32 = GET_VCU_TIME_MS();

        numberOfPDID_u8 = numbytes_req_u16 - 2;
        for(i = 0 ; i < (numbytes_req_u16 - 2) ; i++)
        {
            PDID_Buffer_u8[i] = UDS_Serv_pSt->RxBuff_pu8[i+2];
        }
        
        if(numberOfPDID_u8 != 0)
        {
          for(i = 0 ;  i < (numbytes_req_u16 - 2) ; i++)
          {
              for(j = 0 ; j < DID_TOTAL_E ; j++)
              {
                  if((PDID_Buffer_u8[i] | 0xF200) == DID_Config_aSt[j].DID_Num)
                  {
                      pdidSession_b = DID_Session_Check(j, SERV_2A_BIT_POS);
                      if(FALSE == pdidSession_b)
                      {
                          Session_Failed_b = TRUE;
                          break;
                      }
                      else
                      {
                        MatchDID_b = TRUE;               
                        if(STOP_SENDING_E != transmissionMode_u8)
                        {
                          DID_Config_aSt[j].Periodic_Timer_Ms_u32 = Timer_Delay_For_Serv2A_u32;
                          DID_Config_aSt[j].Transmition_Mode_En = (Transmition_Modes_En_t)transmissionMode_u8;
                          DID_Config_aSt[j].Transmit_Status_b = TRUE;
                          PDID_Counter_u8++;
                          break;
                        }
                        else
                        {
                          DID_Config_aSt[j].Periodic_Timer_Ms_u32 = NULL;
                          DID_Config_aSt[j].Transmition_Mode_En = UN_INIT_E;
                          DID_Config_aSt[j].Transmit_Status_b = FALSE;
                          PDID_Counter_u8--;
                          break;
                        }
                      }
                  }
              }
          }
          
          if((Session_Failed_b == TRUE) && (MatchDID_b == FALSE))
          {
        	/* Session Failed NRC */
        	UDS_Serv_pSt->TxLen_u16 = 3u;
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_PDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = CONDITION_NOT_CORRECT;     
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
          }
          else if((Session_Failed_b == FALSE) && (MatchDID_b == FALSE))
          {
              /* requestOutOfRange-NRC */
        	  UDS_Serv_pSt->TxLen_u16 = 3u;
              UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
              UDS_Serv_pSt->TxBuff_pu8[1] = SID_PDID;
              UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
              Serv_resptype_En = UDS_SERV_RESP_NEG_E;
          }
          else
          {
            /* Positive_Response */
			Serv_resptype_En = UDS_SERV_RESP_POS_E;
          }
        }
        else
        {
          PDID_Counter_u8 = 0u;
          for(i=0; i< DID_TOTAL_E;i++)
          {
          DID_Config_aSt[j].Transmition_Mode_En = UN_INIT_E;
          DID_Config_aSt[j].Transmit_Status_b = FALSE;
          }
          Serv_resptype_En = UDS_SERV_RESP_POS_E;
        }
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : Periodic_Data_Transmit_proc
*  FILENAME      : uds_serv2A.c
*  @param        : None
*  @brief        : This function will process the service_2a requests
*  @return       : Type of response.
**/
void Periodic_Data_Transmit_proc()
{
  uint32_t i =0 ;
  uint32_t j = 0;
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
  if(PDID_Counter_u8 != 0)
  {
    if(DID_Conf_OffSet == DID_TOTAL_E)
    {
      i = 0;
    }
    else
    {
      i = DID_Conf_OffSet;
    }
    
    for(; i<DID_TOTAL_E; i++)
    {
      if((DID_Config_aSt[i].Transmit_Status_b == TRUE) && ((GET_VCU_TIME_MS() > (DID_Config_aSt[i].Periodic_Timer_Ms_u32)+ ModeToMs(DID_Config_aSt[i].Transmition_Mode_En)))
         && (DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->al_state_u8 !=
                 DCAN_TP_ST_TX_INPROG_E) && (DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->al_state_u8 != DCAN_TP_ST_TX_INITIATED_E))
      {
         DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->numbytes_resp_u16 = 0;
		  DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->databuf_pu8[0] = (uint8_t)(DID_Config_aSt[i].DID_Num & 0x00FF);
          for(j=1; j<DID_Config_aSt[i].DID_Val_Size; j++)
          {
            DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->databuf_pu8[j] = (uint8_t)(DID_Config_aSt[i].Var_ptr_pu8[j]);
            DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->numbytes_resp_u16++;
          }
          DID_Config_aSt[i].Periodic_Timer_Ms_u32 = GET_VCU_TIME_MS();
		  DCAN_tpFrameCtrl_paSt[DCAN_TP_INSTANCE_IVN_E][BUFF2_TP_E]->al_state_u8 = DCAN_TP_ST_TX_INITIATED_E;
          break;
      }
    }
    DID_Conf_OffSet = i;
  }
#endif
}


/* *****************************************************************************
 End of File
 */
