/******************************************************************************
 *    FILENAME    : uds_serv3E.h
 *    DESCRIPTION : Header file for UDS service - Tester Present.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/  

#ifndef _UDS_SERV3E_H_
#define	_UDS_SERV3E_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */


    
/* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    /* ************************************************************************** */

#define ZERO_FUNCTION_LENGTH      0x02
#define ZERO_SUBFUN_POSRESP       0x00
#define ZERO_SUBFUN_SUPPPOSRESP   0x80     
    // *****************************************************************************
    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    // *****************************************************************************    

typedef enum{
	ZERO_SUBFUN_FALSE_E,
	ZERO_SUBFUN_TRUE_E,
}ZERO_SUBFUN_En_t;

    
    //  bool tester_present_b ;

 /**
*  FUNCTION NAME : iso14229_serv3E
*  FILENAME      : iso14229_serv3E.h
*  @param        : UDS_Serv_St_t* DataBuff_pu8 - pointer to service distributor table.
*  @brief        : This function will process the service_3E requests
*  @return       : Type of response.                     
*/   
extern UDS_Serv_resptype_En_t iso14229_serv3E (UDS_Serv_St_t*);
 
/**
*  FUNCTION NAME : UDS_Serv3E_Timeout
*  FILENAME      : UDS_Serv3E.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv3E_Timeout(void); 

#ifdef	__cplusplus
}
#endif

#endif	/* ISO14229_SER3E_H */

