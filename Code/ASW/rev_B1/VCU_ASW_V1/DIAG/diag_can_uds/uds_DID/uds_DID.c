/******************************************************************************
 *    FILENAME    : uds_DID.c
 *    DESCRIPTION : Service description for UDS service - READ DATA BY IDENTIFIER(0x22) and for Write Data By Identifier(0x2E).
 ******************************************************************************
 * Revision history
 *  
 * Ver   Author          Date                     Description
 * 1     Sloki     10/01/2019(D/M/Y)         Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                                    */
#include "uds_DID.h"
#include "diag_typedefs.h"
#include "diag_adapt.h"
#include "diag_sys_conf.h"
#if(TRUE == DIAG_CONF_FM_SUPPORTED)
	#include "fmdtc_conf.h"
	#include "fm.h"
#endif
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
/*
 @summary : this is for disable timer 
 */

/*typedef struct 
{
	uint8_t		Data_Byte_pos;
	uint8_t		Data_Bit_pos;
	uint8_t		Data_Bit_length;
}DID_DATA;

typedef struct 
{
	uint8_t		Maskbit_Byte_pos;
	uint8_t		Maskbit_Bit_pos;
}DID_MASK;

typedef struct 
{
	uint32_t	Data_Min;
	uint32_t	Data_Max;
}DID_RANGE;


typedef struct
{
	DID_DATA	st_DID_Data;
	DID_MASK	st_DID_Maskbit;
	DID_RANGE	st_DID_Range;	
}ST_DID_MASK_DATA;*/

#define NVM_NOPE	(0)		/* No operation on NVM, termination/closing of previous process */
#define READ_NVM	(1)
#define WRITE_NVM	(2)

typedef struct
{									/* NUMERIC				*/
	uint8_t		Data_Byte_pos;		/* Byte position		*/
	uint8_t		Data_Byte_length;   /* Byte Length			*/
	uint32_t	Data_Min;           /* Min Data Value		*/
	uint32_t	Data_Max;           /* Maximum Data Value	*/
}ST_DID_MIN_MAX_PARAM;

#define MAX_DID_MASK_PARAM			6

typedef struct
{
	uint8_t ST_DID_Param_Cnt;
	ST_DID_MIN_MAX_PARAM DID_Param_Data[MAX_DID_MASK_PARAM];
}ST_DID_MIN_MAX;

const ST_DID_MIN_MAX DID_MIN_MAX_TBL[DID_TOTAL_E] = {
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{NULL}}},	/* DTC_MASKING_IDENTIFIER_E          	*/
#if (TRUE == DIAG_TEST_DEMO_CODE)
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
		{0, {{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3},{0, 1, 0, 3}}},
#endif
//	{0, NULL},																		               	/* BOOT_SOFT_IDEN_E                		*/
//	{0, NULL},																		               	/* BOOT_FP_DATA_E                    	*/
//	{0, NULL},																		               	/* APP_SOFT_FP_DATA_E                	*/
//	{0, NULL},																						/* APP_SW_DATA_DATE_E					*/
//	{0, NULL},																						/* APP_SW_DATA_TST_SER_NO_E				*/
//	{0, NULL},																						/* APP_SW_DATA_REPAIR_SHOP_E			*/
//	{0, NULL},																						/* APP_SW_DATA_REPROG_SEQ_E				*/
//	{0, NULL},																		               	/* CONFIGURATION_FINGERPRINT_DATA_E  	*/
//	{0, NULL},																						/* CONFIG_DATA_DATE_E					*/
//	{0, NULL},																						/* CONFIG_DATA_TST_SER_NO_E				*/
//	{0, NULL},																						/* CONFIG_DATA_REPAIR_SHOP_E			*/
//	{0, NULL},																						/* CONFIG_DATA_REPROG_SEQ_E				*/
//	{0, NULL},																		               	/* ECU_SERIAL_NUMBER_E                	*/
//	{0, NULL},																		               	/* REPROGM_SVN_E                    	*/
//	{0, NULL},																						/* ACTIVE_DIAGNOSTIC_SESS_E     		*/
//	{0, NULL},																		                /* ECU_MFG_DATE_E                   	*/
//	{0, NULL},																						/* APP_SOFT_STATUS_E            		*/
//	{0, NULL},																						/* CALIBRATION_SOFT_STATUS_E    		*/
//	{0, NULL},																						/* CONFIG_SOFT_STATUS_E        			*/
//	{1, {{0, 1, 0, 3}}},																			/* ACTIVE_SOFTWARE_COMPONENT_E			*/
//	{0, NULL},																						/* ECU_HW_NUM_E                     	*/
//	{0, NULL},																						/* ECU_SW_NUM_E                     	*/
//	{0, NULL},																						/* SYSTEM_NAME_E					    */
//	{0, NULL},																						/* PROGRAM_SECRET_KEY_E             	*/
//	{0, NULL},																						/* EXTENDED_SECRET_KEY_E            	*/
//	{0, NULL},																						/* END_OF_LINE_SECRET_KEY_E				*/
//	{2, {{0, 3, 0, 16777214}, {13, 1, 0, 15}}},														/* SERV_RECORD1_E                    	*/
//	{2, {{0, 3, 0, 16777214}, {13, 1, 0, 15}}},														/* SERV_RECORD2_E                    	*/
//	{2, {{0, 3, 0, 16777214}, {13, 1, 0, 15}}},														/* SERV_RECORD3_E                    	*/
//	{2, {{0, 3, 0, 16777214}, {13, 1, 0, 15}}},														/* SERV_RECORD4_E                    	*/
//	{2, {{0, 3, 0, 16777214}, {13, 1, 0, 15}}},														/* SERV_RECORD5_E                    	*/
//	{0, NULL},			/* To Be Change	PBL_RESERVED_E	*/											/* SAFETY_SECRET_KEY_E               	*/
//	{0, NULL},																					    /* APP_SOFT_IDEN_E                   	*/
//	{0, NULL},																						/* SUPPLIER_PART_NUM_E             		*/
//	{0, NULL},																						/* ECU_PBL_SW_NUM_E                 	*/
//	{0, NULL},																						/* ECU_APP_SW_NUM_E                 	*/
//	{0, NULL},																						/* ECU_CONFIG_SW_NUMBER_E				*/
//	{0, NULL},																						/* ECU_ASSEMBLY_NUMBER_E				*/
//	{0, {{NULL}}},																			            /* DTC_MASKING_IDENTIFIER_E          	*/
//	{0, NULL},																			            /* SBL_VER_NUM_E                     	*/
//	{0, NULL},																			            /* ECU_SPECIFIC_E                    	*/
//	{5, {{0, 1, 0, 2}, {1, 1, 0, 3}, {2, 1, 0, 3}, {3, 1, 0, 2}, {4, 1, 0, 2}}},     				/* ENGINE_WARN_INDICATOR_E           	*/
//	{0, NULL},																						/* SEAT_BELT_WARN_CHIME_E            	*/
//	{4, {{0, 1, 0, 2}, {1, 1, 0, 2}, {2, 1, 0, 2}, {3, 1, 0, 2}}},									/* SEAT_BELT_WARN_INDICATOR_E        	*/
//	{5, {{0, 1, 0, 15}, {1, 1, 0, 15}, {2, 1, 0, 2}, {3, 1, 0, 3}, {4, 1, 0, 2}}},					/* GEAR_MODE_INDICATOR_E             	*/
//	{4, {{0, 1, 0, 254}, {1, 1, 0, 12700}, {2, 1, 1, 100}, {3, 1, 0, 160}}},						/* METER_DISPLAY_E                   	*/
//	{5, {{0, 2, 0, 999}, {2, 2, 0, 999}, {4, 1, 0, 30}, {5, 2, 0, 1999}}},							/* AFE_IFE_DISPLAY_E                 	*/
//	{5, {{0, 1, 0, 1}, {1, 1, 0, 125}, {2, 1, 0, 165}, {3, 3, 0, 100923}, {6, 1, 0, 10}}},			/* REV_GEAR_AUDIO_DISPLAY_E          	*/
//	{5, {{0, 1, 0, 1}, {1, 2, 0, 65534}, {3, 2, 0, 65534}, {5, 2, 0, 65534}, {7, 2, 0, 65534}}},	/* SERV_INTERVAL_DISTANCE_E          	*/
//	{2, {{0, 2, 0, 65534}, {2, 2, 0, 65534}}},														/* DISTANCE_SINCE_LAST_SERV_E        	*/
//	{1, {{0, 1, 0, 254}}},																			/* PARK_BRAKE_WARN_VHCLE_SPEED_E     	*/
//	{4, {{0, 1, 0, 5}, {1, 1, 0, 5}, {2, 1, 0, 5}, {3, 1, 0, 5}}},									/* AMT_WARN_INDICATOR_E              	*/
//	{1, {{0, 1, 0, 5}}},																			/* WIF_WARN_INDICATOR_E              	*/
//	{4, {{0, 1, 0, 5}, {1, 1, 70, 90}, {2, 1, 10, 40}, {3, 1, 0, 5}}},								/* FUEL_LEVEL_E                      	*/
//	{5, {{0, 1, 0, 2}, {1, 1, 1, 23}, {2, 1, 1, 59}, {3, 1, 1, 31}, {4, 1, 1, 12}}},				/* CLK_DISPLAY_SETTING_E             	*/
//	{2, {{0, 1, 0, 3}, {1, 1, 0, 3}}},																/* SET_SWITCHES_E                    	*/
//	{6, {{0, 1, 1, 10}, {1, 1, 0, 5}, {2, 1, 0, 5}, {3, 1, 0, 5}, {4, 1, 0, 5}, {5, 1, 0, 5}}},		/* GLOW_PLUG_WARNING_E               	*/
//	{5, {{0, 1, 0, 3}, {1, 1, 0, 5}, {2, 1, 1, 100}, {3, 1, 1, 20}, {4, 1, 0, 10}}},				/* MIN_DISTANCE_INST_FUEL_E          	*/
//	{5, {{0, 1, 0, 10}, {1, 1, 1, 10}, {2, 2, 0, 1000}, {4, 1, 1, 10}, {5, 1, 0, 50}}},				/* AVG_FUEL_TIME_E                   	*/
//	{3, {{0, 1, 0, 20}, {1, 1, 0, 60}, {2, 1, 0, 60}}},												/* OAT_CONDITION_TIME_E              	*/
//	{6, {{0, 1, 0, 1}, {1, 1, 0, 10}, {2, 1, 0, 2}, {3, 1, 0, 1}, {4, 1, 0, 1}, {5, 1, 0, 1}}},		/* TIRE_SIZE_E                       	*/
//	{1, {{0, 1, 0, 7}}},																			/* FUEL_TANK_SIZE_E                  	*/
//	{0, NULL},																						/* IC_REPLACEMENT_E                  	*/
//	{0, NULL},																						/* SEASON_ODOMETER_E                 	*/
//	{3, {{0, 1, 0, 3}, {1, 1, 0, 220}, {2, 1, 0, 220}}},											/* SPEED_LIMIT_E                     	*/
//	{0, NULL},																						/* ABS_FAC_E                         	*/
//	{0, NULL},	                                                                                  	/* ILLUMINATION_FINE_TUNN_E				*/
//	{1, {{0, 1, 0, 2}}},																			/* CNTRL_MODULE_CONFIG_TYPE_E   		*/
//	{1, {{0, 1, 0, 3}}},																			/* COMMUNICATION_STATUS_E          		*/
//	{1, {{0, 1, 0, 3}}},																			/* VEHICLE_MODE_E                 		*/
//	{1, {{0, 1, 0, 4}}},																			/* ILL_DIMMING_LEV_OUTPUT_E				*/
//	{0, NULL},																						/* GLOBAL_RT_CLEAR_DTC_E        		*/
//	{1, {{0, 1, 0, 254}}},																			/* DTC_SET_DUE_TO_DIAG_ROUTINE_E		*/
//	{1, {{0, 3, 0, 16777214}}},																		/* GLOBAL_REAL_TIME_E           		*/
//	{1, {{0, 3, 0, 16777214}}},				  														/* TOTAL_DISTANCE_TRAVELLED_E   		*/
//	{1, {{0, 1, 8, 16}}},			  																/* ECU_SUPPLY_VOLTAGE_E         		*/
//	{1, {{0, 1, 0, 125}}},																			/* VEHICLE_INTERIOR_TEMP_E      		*/
//	{1, {{0, 1, 0, 125}}},																			/* ABIENT_AIR_TEMP_E            		*/
//	{1, {{0, 1, 0, 15}}},																			/* VEHICLE_POWER_MODE_E            		*/
//	{1, {{0, 2, 0, 8000}}},			  																/* ENGINE_SPEED_E               		*/
//	{1, {{0, 1, 0, 200}}},																			/* VEHICLE_SPEED_E              		*/
//	{1, {{0, 1, 0, 140}}},																			/* ENGINE_COOLANT_TEMP_E        		*/
//	{0, NULL},																						/* THROTTLE_POSITION_E                	*/
//	{0, NULL},																						/* VIN_M_E                          	*/
//	{1, {{0, 1, 0, 20}}},																			/* AFE_BIASING_E					    */
//	{0, NULL},																						/* DIAG_GATEWAY_STATE_E              	*/
//	{0, NULL},																						/* NW_TOPOLOGY_CONFIG_E              	*/
//	{0, NULL},																						/* PARK_LAMP_IN_E                    	*/
//	{0, NULL},																						/* IGN_IN_E                          	*/
//	{0, NULL},																						/* TURN_WARN_INDICATOR_E             	*/
//	{0, NULL},																						/* HAZARD_WARN_INDICATOR_E           	*/
//	{0, NULL},																						/* ABS_WARN_INDICATOR_E              	*/
//	{0, NULL},																						/* EPAS_PSC_WARN_INDICATOR_E         	*/
//	{0, NULL},																						/* HIGH_COOLANT_TEMP_WARN_E          	*/
//	{0, NULL},																						/* BATTERY_CHARGE_WARN_INDICATOR_E   	*/
//	{0, NULL},																						/* BRAKE_FLUIDE_WARN_INDICATOR_E     	*/
//	{0, NULL},																						/* PARK_BRAKE_WARN_INDICATOR_E       	*/
//	{0, NULL},																						/* KEY_IN_OUT_E                      	*/
//	{0, NULL},																						/* SPEED_LIMIT_WARN_INDICATOR_E      	*/
//	{0, NULL},																						/* GLOW_PLUG_WARN_INDICATOR_E        	*/
//	{0, NULL},																						/* TX_FAULT_INDICATOR_E              	*/
//	{0, NULL},																						/* DRL_INDICATOR_E                   	*/
//	{0, NULL},																						/* WELCOME_STRATEGY_E                	*/
//	{0, NULL},																						/* OAT_CONFIG_E                      	*/
//	{0, NULL},																						/* TCU_BUZZER_REQ_E                  	*/
//	{0, NULL},																						/* DEBUG_FRAME_E					    */
//	{0, NULL},																						/* VEHICLE_MFG_ECU_SOFT_NUM_E       	*/
//	{0, NULL},																						/* VIN_1_E                          	*/
//	{0, NULL},																						/* DIAG_VERSION_E                   	*/
//	{0, NULL},																						/* VCN_E                            	*/
//	{0, NULL},																						/* ECU_PARAM_SW_NUM_E            		*/
//	{0, NULL},																						/* CONFIG_VERSION_E					    */
//	{0, NULL},																						/* READ_MASK_DTCs_E                  	*/
//	{1, {{0, 1, 0, 8}}},																			/* POWER_TRAIN_TYPE_E					*/
//	{1, {{0, 1, 0, 100}}},																			/* DRIVER_SBR_TONE_COUNT_E				*/
//	{0, NULL},																		               	/* ECU_INSTALLATION_DATE_E				*/
//	{0, NULL},																		               	/* ECO_SPORT_CITY_WARN_E                */
//	{0, NULL},																		               	/* EOL_SESSION_E					    */
//	{0, NULL},																		               	/* ECU_SBL_SW_NUM_E					    */
//	{0, NULL},																		               	/* SYSTEM_SUPPLIER_E				    */
//	{0, NULL},																		               	/* ELECTRICAL_POWER_E				    */
//	{0, NULL},																		               	/* ECU_STATUS_SIG_E					    */
//	{0, NULL},																		               	/* VEH_MANF_ECU_HW_NUM				    */
//	{0, NULL},																		               	/* FRONTFOG_WARN_INDI				    */

};


#define MAX_INHALE_EXHALE_PARAM		18

//const uint8_t InhaleExhale_Tbl[MAX_INHALE_EXHALE_PARAM] = {
//	TIRE_SIZE_E,
//	POWER_TRAIN_TYPE_E,
//	FUEL_TANK_SIZE_E,
//	SPEED_LIMIT_E,
//	OAT_CONFIG_E,
//	VCN_E,
//	DISTANCE_SINCE_LAST_SERV_E,
//	SERV_RECORD1_E,
//	SERV_RECORD2_E,
//	SERV_RECORD3_E,
//	SERV_RECORD4_E,
//	SERV_RECORD5_E,
//	SEASON_ODOMETER_E,
//	VIN_1_E,
//	ECU_ASSEMBLY_NUMBER_E,
//	VEHICLE_MFG_ECU_SOFT_NUM_E,
//	ECU_PARAM_SW_NUM_E,
//	AFE_BIASING_E
//};



/**
*  FUNCTION NAME : DID_Comm_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests.
*  @return       : Type of response.                     
*/
bool DID_Comm_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
    uint32_t i=0u;
//    uint32_t index_u32=0u;
    bool Response_b = FALSE;
    if((PACKETED_E == DID_categary_En) || (NUMERIC_E == DID_categary_En) || (STATE_ENCODED_E == DID_categary_En)) 
    {
      switch(Access_Type)
      {
          case READ_ONLY_E:
//                        index_u32= size;
                        for(i=0; i<size; i++)
                        {
                            dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
                        }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
                        Response_b = TRUE;
                        break;
                        
          case WRITE_ONLY_E:
                        for(i=0; i<size; i++)
                        {
                            Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
                        }/*Loop store all the data available from the request Message into the global Variable*/  
                        Response_b = TRUE;
						u1_Diag_Config_step = WRITE_NVM;
						NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);	
                        break;
            
          default:      Response_b = FALSE;
                        break;
      }
    }
    
    else if( BIT_MAPPED_E == DID_categary_En)
    {
       switch(Access_Type)
      {
          case READ_ONLY_E:
//                        index_u32= size;
                        for(i=0; i<size; i++)
                        {
                            dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
                        }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
                        Response_b = TRUE;
                        break;
                        
          case WRITE_ONLY_E:

                        for(i=0; i<size; i++)
                        {
                            Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
                        }/*Loop store all the data available from the request Message into the global Variable*/  
                        Response_b = TRUE;
						u1_Diag_Config_step = WRITE_NVM;
						NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
                        break;
            
          default:      Response_b = FALSE;
                        break;           
      }
    }
    
    else if(ASCII_E == DID_categary_En)
    {
       switch(Access_Type)
      {
          case READ_ONLY_E:
//                        index_u32= size;
                        for(i=0; (i<size); i++)
                        {
                            dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
                        }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
                        Response_b = TRUE;
                        break;
                        
          case WRITE_ONLY_E:

                        for(i=0; i<size; i++)
                        {
                            Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
                        }/*Loop store all the data available from the request Message into the global Variable*/  
                        Response_b = TRUE;
						u1_Diag_Config_step = WRITE_NVM;
						NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
                        break;
            
          default:      Response_b = FALSE;
                        break;      
      }
    }
    
    else 
    {
       switch(Access_Type)
      {
          case READ_ONLY_E:
//                        index_u32= size;
                        for(i=0; i<size; i++)
                        {
                            dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
                        }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
                        Response_b = TRUE;
                        break;
                        
          case WRITE_ONLY_E:

                        for(i=0; i<size; i++)
                        {
                            Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
                        }/*Loop store all the data available from the request Message into the global Variable*/  
                        Response_b = TRUE;
						u1_Diag_Config_step = WRITE_NVM;
						NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
                        break;
            
          default:      Response_b = FALSE;
                        break;   
      }
    }
    return Response_b;
}

/**
*  FUNCTION NAME : DID_Odo_4666_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4666 DID.
*  @return       : Type of response.                     
*/
//bool DID_Odo_4666_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
//{
//    uint32_t i=0u;
//    bool Response_b = FALSE;
//
//	switch(Access_Type)
//    {
//        case READ_ONLY_E:
//          //  vd_Load_ASCII_ToDID(Var_Ptr_u8, d4_GetOdometer(), 6);
//			for(i=0; i<size; i++)
//			{
//				dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i];
//			}/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
//			Var_Ptr_u8[6] = 0x01;
//			Response_b = TRUE;
//            break;
//
//        case WRITE_ONLY_E:
//            for(i=0; i<size; i++)
//			{
//				Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
//			}/*Loop store all the data available from the request Message into the global Variable*/
//
//			//if(TRUE == Var_Ptr_u8[6] & BIT0)
//			//{
//				//todo harsh Response_b = (bool)DID_OdoUpdate_4666(Var_Ptr_u8, (size-1));
//				if(TRUE == Response_b)
//				{
//					u1_Diag_Config_step = WRITE_NVM;
//					NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
//				}
//			//}
//			//else
//			//{
//			//	Response_b = TRUE;
//			//}
//
//            break;
//
//        default:
//			Response_b = FALSE;
//            break;
//    }
//	return Response_b;
//}

/**
*  FUNCTION NAME : DID_TyreSize_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4666 DID.
*  @return       : Type of response.                     
*/
//bool DID_TyreSize_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
//{
//    uint32_t i=0u;
//    uint8_t param_index_u8=0u;
//    bool Response_b = FALSE;
//
//	uint8_t 	DataFrame_u8[32];
//	uint32_t	Param_Data_u32;
//	uint8_t		Data_Byte_pos_u8;
//	uint8_t		Data_Byte_length_u8;
//	uint64_t	Data_Min_u32;
//	uint64_t	Data_Max_u32;
//
//	switch(Access_Type)
//    {
//        case READ_ONLY_E:
//            for(i=0; i<size; i++)
//            {
//                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
//
//            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
//            Response_b = TRUE;
//            break;
//
//		case WRITE_ONLY_E:
//			for(i=0; i<size; i++)
//			{
//				DataFrame_u8[i] = dataframe[Serv2E_Index_u32++];
//			}/*Loop store all the data available from the request Message into the global Variable*/
//
//			if(DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt != 0)
//			{
//				while(param_index_u8 < DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt)
//				{
//					Data_Byte_pos_u8      = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_pos;
//					Data_Byte_length_u8   = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_length;
//					Data_Min_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Min;
//					Data_Max_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Max;
//
//					Param_Data_u32 = 0;
//					for(i=0; i<Data_Byte_length_u8; i++)
//					{
//						Param_Data_u32 |= (DataFrame_u8[Data_Byte_pos_u8 + i] << (8 * (Data_Byte_length_u8 - i - 1)));
//					}
//					if(Param_Data_u32 < Data_Min_u32 || Param_Data_u32 > Data_Max_u32)
//					{
//						return FALSE;
//					}
//					param_index_u8++;
//				}
//			}
//
//            for(i=0; i<size; i++)
//			{
//				Var_Ptr_u8[i] = DataFrame_u8[i];
//			}/*Loop store all the data available from the request Message into the global Variable*/
//
//			if(TIRE_SIZE_TYPE == 0x05) {							/* R13Kite	*/
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[0] = 0x62;
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[1] = 0x60;
//			} else if(TIRE_SIZE_TYPE == 0x06) {						/* R14Kite	*/
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[0] = 0x64;
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[1] = 0x62;
//			} else {												/* R15Kite	*/
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[0] = 0x66;
//				DID_Config_aSt[ABS_FAC_E].Var_ptr_pu8[1] = 0x63;
//			}
//			//todo harsh OdoRefreshConfig();
//
//			u1_Diag_Config_step = WRITE_NVM;
//			NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
//            Response_b = TRUE;
//            break;
//
//		default:
//			Response_b = FALSE;
//            break;
//    }
//	return Response_b;
//}


/**
*  FUNCTION NAME : DID_PowerTrain_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4662 DID.
*  @return       : Type of response.                     
*/
//bool DID_PowerTrain_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
//{
//    uint32_t i=0u;
//    uint8_t param_index_u8=0u;
//    bool Response_b = FALSE;
//
//	uint8_t 	DataFrame_u8[32];
//	uint32_t	Param_Data_u32;
//	uint8_t		Data_Byte_pos_u8;
//	uint8_t		Data_Byte_length_u8;
//	uint64_t	Data_Min_u32;
//	uint64_t	Data_Max_u32;
//
//	switch(Access_Type)
//    {
//        case READ_ONLY_E:
//            for(i=0; i<size; i++)
//            {
//                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
//
//            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
//            Response_b = TRUE;
//            break;
//
//		case WRITE_ONLY_E:
//			for(i=0; i<size; i++)
//			{
//				DataFrame_u8[i] = dataframe[Serv2E_Index_u32++];
//			}/*Loop store all the data available from the request Message into the global Variable*/
//
//			if(DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt != 0)
//			{
//				while(param_index_u8 < DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt)
//				{
//					Data_Byte_pos_u8      = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_pos;
//					Data_Byte_length_u8   = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_length;
//					Data_Min_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Min;
//					Data_Max_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Max;
//
//					Param_Data_u32 = 0;
//					for(i=0; i<Data_Byte_length_u8; i++)
//					{
//						Param_Data_u32 |= (DataFrame_u8[Data_Byte_pos_u8 + i] << (8 * (Data_Byte_length_u8 - i - 1)));
//					}
//					if(Param_Data_u32 < Data_Min_u32 || Param_Data_u32 > Data_Max_u32)
//					{
//						return FALSE;
//					}
//					param_index_u8++;
//				}
//			}
//
//            for(i=0; i<size; i++)
//			{
//				Var_Ptr_u8[i] = DataFrame_u8[i];
//			}/*Loop store all the data available from the request Message into the global Variable*/
//
//			if(POWER_TRAIN_TYPE_VAL == PETROL_TYPE) {
//				DID_Config_aSt[AVG_FUEL_TIME_E].Var_ptr_pu8[5] = 0x12;
//			} else {
//				DID_Config_aSt[AVG_FUEL_TIME_E].Var_ptr_pu8[5] = 0x16;
//			}
//
//			u1_Diag_Config_step = WRITE_NVM;
//			NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
//            Response_b = TRUE;
//            break;
//
//		default:
//			Response_b = FALSE;
//            break;
//    }
//	return Response_b;
//}
//

/**
*  FUNCTION NAME : DID_DTCMask_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 722D DID.
*  @return       : Type of response.                     
*/
bool DID_DTCMask_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
    uint32_t i = 0u;
    uint32_t dtc_code = 0u;
    uint32_t dtc_index = 0u;
	
    bool Response_b = FALSE;
    
	switch(Access_Type)
    {
        case READ_ONLY_E:
            for(i=0; i<size; i++)
            {
                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
    
            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
            Response_b = TRUE;
            break;
			
		case WRITE_ONLY_E:
            for(i=0; i<size; i++)
			{
				Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
			}/*Loop store all the data available from the request Message into the global Variable*/  
			
			dtc_code = DTC_MASKING_CODE;
#if(TRUE == DIAG_CONF_FM_SUPPORTED)
			for(dtc_index = 0; dtc_index < NUM_OF_FAULTPATHS_E; dtc_index++)
			{
			if (DTCMappingTable_UDS_aSt[dtc_index][0] == dtc_code)
					break;
			}
			if(dtc_index < NUM_OF_FAULTPATHS_E) 
            {
				FM_SetDTCBitMask(dtc_index, DTC_MASKING_ENABLE);
				Response_b = TRUE;
			} else {
				Response_b = FALSE;
			}
#endif
            break;
        
		default:  
			Response_b = FALSE;
            break;        
    }
	return Response_b;
}

/**
*  FUNCTION NAME : DID_ReadDTCMask_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 722D DID.
*  @return       : Type of response.                     
*/
bool DID_ReadDTCMask_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
//    uint32_t i=0u;
    uint32_t dtc_index=0u;
	
    bool Response_b = FALSE;
    
	switch(Access_Type)
    {
        case READ_ONLY_E:
#if(TRUE == DIAG_CONF_FM_SUPPORTED)
            for(dtc_index = 0; dtc_index < NUM_OF_FAULTPATHS_E; dtc_index++) 
			{
				dataframe[Serv22_Index_u32++] = (DTCMappingTable_UDS_aSt[dtc_index][0] >> 16) & 0xFF;
				dataframe[Serv22_Index_u32++] = (DTCMappingTable_UDS_aSt[dtc_index][0] >> 8) & 0xFF;
				dataframe[Serv22_Index_u32++] = DTCMappingTable_UDS_aSt[dtc_index][0] & 0xFF;
				dataframe[Serv22_Index_u32++] = FM_DTC_Mask_Sts(dtc_index);
			}
#endif
			Response_b = TRUE;
            break;
			
		default:  
			Response_b = FALSE;
            break;        
    }
	return Response_b;
}

/**
*  FUNCTION NAME : DID_ClockSett_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 464E DID.
*  @return       : Type of response.                     
*/
bool DID_ClockSett_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
    uint32_t i = 0u;
    bool Response_b = FALSE;
    
	switch(Access_Type)
    {
        case READ_ONLY_E:
        //todo harsh    DID_ClockRead_464E(Var_Ptr_u8);
            for(i=0; i<size; i++)
            {
                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
    
            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
            Response_b = TRUE;
            break;
			
		case WRITE_ONLY_E:
            for(i=0; i<size; i++)
			{
				Var_Ptr_u8[i] = dataframe[Serv2E_Index_u32++];
			}/*Loop store all the data available from the request Message into the global Variable*/  
			
			//DID_ClockSett_464E(Var_Ptr_u8[0], Var_Ptr_u8[1], Var_Ptr_u8[2]);// todo harsh
            Response_b = TRUE;
            break;
        
		default:  
			Response_b = FALSE;
            break;        
    }
	return Response_b;
}


/**
*  FUNCTION NAME : DID_Numeric_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Numeric DID.
*  @return       : Type of response.                     
*/
bool DID_Numeric_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
    uint32_t i=0u;
    uint8_t param_index_u8=0u;
	bool Response_b = FALSE;
	
	uint8_t 	DataFrame_u8[32];
	uint32_t	Param_Data_u32;
	uint8_t		Data_Byte_pos_u8;
	uint8_t		Data_Byte_length_u8;
	uint64_t	Data_Min_u32;
	uint64_t	Data_Max_u32;
	
	switch(Access_Type)
    {
        case READ_ONLY_E:
            for(i=0; i<size; i++)
            {
                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
            Response_b = TRUE;
            break;
			
        case WRITE_ONLY_E:
			
			for(i=0; i<size; i++)
			{
				DataFrame_u8[i] = dataframe[Serv2E_Index_u32++];
			}/*Loop store all the data available from the request Message into the global Variable*/  
			
			if(DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt != 0)
			{
				while(param_index_u8 < DID_MIN_MAX_TBL[Current_DID_Index_u16].ST_DID_Param_Cnt)
				{
					Data_Byte_pos_u8      = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_pos;
					Data_Byte_length_u8   = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Byte_length;
					Data_Min_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Min;
					Data_Max_u32          = DID_MIN_MAX_TBL[Current_DID_Index_u16].DID_Param_Data[param_index_u8].Data_Max;
					
					Param_Data_u32 = 0;
					for(i=0; i<Data_Byte_length_u8; i++)
					{
						Param_Data_u32 |= (DataFrame_u8[Data_Byte_pos_u8 + i] << (8 * (Data_Byte_length_u8 - i - 1)));
					}
					if(Param_Data_u32 < Data_Min_u32 || Param_Data_u32 > Data_Max_u32)
					{
						return FALSE;
					}
					param_index_u8++;
				}
			}
			for(i=0; i<size; i++)
            {
                Var_Ptr_u8[i] = DataFrame_u8[i];
            }/*Loop store all the data available from the request Message into the global Variable*/  
            
			u1_Diag_Config_step = WRITE_NVM;
			NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);	
            Response_b = TRUE;
            break;
           
        default:      
			Response_b = FALSE;
            break;           
    }
	return Response_b;
}


/**
*  FUNCTION NAME : Inhale_Exhale_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Inhale Exhale DID.
*  @return       : Type of response.                     
*/
//bool Inhale_Exhale_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
//{
//	uint8_t j=0u;
//	uint32_t index_u32=0u;
//    bool Response_b = FALSE;
//
//	switch(Access_Type)
//    {
//        case READ_ONLY_E:
//            for(j = 0; j < MAX_INHALE_EXHALE_PARAM; j++)
//			{
//				index_u32 = InhaleExhale_Tbl[j];
//				Response_b = DID_Config_aSt[index_u32].CALLBCK_Fptr(dataframe, DID_Config_aSt[index_u32].DID_Val_Size, READ_ONLY_E, DID_Config_aSt[index_u32].Var_ptr_pu8,  DID_Config_aSt[index_u32].Type_En);
//				if(FALSE == Response_b)
//					break;
//			}
//			Response_b = TRUE;
//            break;
//		case WRITE_ONLY_E:
//            for(j = 0; j < MAX_INHALE_EXHALE_PARAM; j++)
//			{
//				index_u32 = InhaleExhale_Tbl[j];
//				Response_b = DID_Config_aSt[index_u32].CALLBCK_Fptr(dataframe, DID_Config_aSt[index_u32].DID_Val_Size, WRITE_ONLY_E, DID_Config_aSt[index_u32].Var_ptr_pu8,  DID_Config_aSt[index_u32].Type_En);
//				if(FALSE == Response_b)
//					break;
//			}
//			Response_b = TRUE;
//            u1_Diag_Config_step = WRITE_NVM;
//			NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);
//            break;
//
//        default:
//			Response_b = FALSE;
//            break;
//    }
//	return Response_b;
//}
 
//uint16_t Inhale_Exhale_Size(void)
//{
//	uint8_t j=0u;
//	uint32_t index_u32=0u;
//	uint16_t size_u16=0u;
//
//	for(j = 0; j < MAX_INHALE_EXHALE_PARAM; j++)
//	{
//		index_u32 = InhaleExhale_Tbl[j];
//		size_u16 += DID_Config_aSt[index_u32].DID_Val_Size;
//	}
//	return size_u16;
//}
	
#if 0 
/**
*  FUNCTION NAME : DID_BitMasked_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Bitmasked DID.
*  @return       : Type of response.                     
*/
bool DID_BitMasked_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
    uint32_t i=0u;
    uint32_t j=0u;
    uint8_t bit_pos_u8=0u;
    uint8_t param_index_u8=0u;
	bool Response_b = FALSE;
    ST_DID_MASK *st_DID_Mask_ptr;
	
	uint8_t 	DataFrame_u8[32];
	uint32_t	Param_Data_u32;
	uint8_t		Data_Byte_pos_u8;
	uint8_t		Data_Bit_pos_u8;
	uint8_t		Data_Bit_length_u8;
	uint8_t		Maskbit_Byte_pos_u8;
	uint8_t		Maskbit_Bit_pos_u8;
	uint32_t	Data_Min_u32;
	uint32_t	Data_Max_u32;
	
	switch(Access_Type)
    {
        case READ_ONLY_E:
            for(i=0; i<size; i++)
            {
                dataframe[Serv22_Index_u32++] = Var_Ptr_u8[i] ;
       
            }/*Loop store the Values available from the Global Variable into the  UDS_Serv_St_t* dataframe - pointer to service distributer table with the help of Index Variable*/
            Response_b = TRUE;
            break;
			
        case WRITE_ONLY_E:
			
			for(i=0; i<size; i++)
			{
				DataFrame_u8[i] = dataframe[Serv2E_Index_u32++];
			}/*Loop store all the data available from the request Message into the global Variable*/  
			
			
			*st_DID_Mask_ptr = DID_MASK_TBL[Current_DID_Index_u16];
			while(param_index_u8 < st_DID_Mask_ptr->ST_DID_Mask_Num)
			{
				Data_Byte_pos_u8      = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Data_Byte_pos;
				Data_Bit_pos_u8       = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Data_Bit_pos;
				Data_Bit_length_u8    = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Data_Bit_length;
				Maskbit_Byte_pos_u8   = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Maskbit_Byte_pos;
				Maskbit_Bit_pos_u8    = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Maskbit_Bit_pos;
				Data_Min_u32          = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Data_Min;
				Data_Max_u32          = st_DID_Mask_ptr->DID_Mask_Data[param_index_u8].Data_Max;
				
				//if(DataFrame_u8[Maskbit_Byte_pos_u8] & (0x01 << Maskbit_Bit_pos_u8))
				//{
					Param_Data_u32 = 0;
					for(i=0,j=0,bit_pos_u8=Data_Bit_pos_u8; i<Data_Bit_length_u8; i++,bit_pos_u8++)
					{
						if(bit_pos_u8 >= 8) {
							bit_pos_u8 = 0;
							j++;
						}
						Param_Data_u32 |= (((DataFrame_u8[Data_Byte_pos_u8 + j] >> bit_pos_u8) & 0x01) << i);
					}
					if(Param_Data_u32 < Data_Min_u32 || Param_Data_u32 > Data_Max_u32)
					{
						return FALSE;
					}
					for(i=0,j=0,bit_pos_u8=Data_Bit_pos_u8; i<Data_Bit_length_u8; i++,bit_pos_u8++)
					{
						if(bit_pos_u8 >= 8) {
							bit_pos_u8 = 0;
							j++;
						}
						Var_Ptr_u8[Data_Byte_pos_u8 + j] = ((Var_Ptr_u8[Data_Byte_pos_u8 + j] & ~(0x01 << bit_pos_u8)) | (DataFrame_u8[Data_Byte_pos_u8 + j] & (0x01 << bit_pos_u8)));
					}
				//}
				param_index_u8++;
			}
			u1_Diag_Config_step = WRITE_NVM;
			NvmmgrSetReq(u2_REQ_DIAGCONFIG_PARAM, ON);	
            Response_b = TRUE;
            break;
           
        default:      
			Response_b = FALSE;
            break;           
    }
	return Response_b;
}
#endif


/**
*  FUNCTION NAME : DID_Session_Check
*  FILENAME      : uds_DID.c
*  @param        : Index_u32
*  @brief        : function to Session_Check is valid or not
*  @return       : none.                     
*/

bool DID_Session_Check(uint32_t Index_u32, uint8_t Bit_pos_u8)
{

  uint32_t Session_u32 = 0u;
  uint32_t Sess_u32 = 0u;
  bool Response_b = FALSE;
  /*
  if(ENGINEERING_DIAG_SESSION_SUB_ID_E == current_active_session_u8)    // This Condition For Engineering Session 
  {
    Sess_u32 =  ENGINEERING_SESSION(current_active_session_u8);
  }
  */
  if(SUPPLIER_EOL_SESSION_SUB_ID_E == current_active_session_u8)    /* This Condition For SUPPLIER EOL Session */
  {
    Sess_u32 =  SUPPLIER_EOL_SESS_PERMISSION(current_active_session_u8);
  }
  else
  {
    Sess_u32 = (current_active_session_u8 - 1);
  }
  Session_u32 = DID_Config_aSt[Index_u32].DID_Permission_Un[Sess_u32].Permission_u8;
  if( Session_u32& (1 << Bit_pos_u8))
  {
    Response_b = TRUE;
  }  
  return  Response_b; 
}
