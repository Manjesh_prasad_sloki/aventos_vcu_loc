/*
 ****************************************************************************************
 *    FILENAME    : uds_serv19.h
 *
 *    DESCRIPTION : File contains iso14229_ser14.c related definitions and declarations.
 *
 *    $Id         : $
 *
 ****************************************************************************************
 * Revision history
 *
 * Ver 	Author              Date              Description
 * 1	Sloki       	   25/12/2018	     Initial version
 ****************************************************************************************/

#ifndef _UDS_SERV19_H_
#define	_UDS_SERV19_H_

/*
 ****************************************************************************************
 *    Includes
 ****************************************************************************************
*/


#include "uds_conf.h"
#include "diag_sys_conf.h"
#if(TRUE == DIAG_CONF_FM_SUPPORTED)
	#include "fm.h"
#endif
#include "uds_DTC.h"
/*
 ****************************************************************************************
 *    Defines
 ****************************************************************************************
*/  

#define SERV19_MIN_LEN                                          0x02

#define TEST_FAILED                                             0x00
#define TEST_FAILED_THIS_OPERATION_CYCLE                        0x01
#define PENDING_DTC                                             0x02
#define CONFIRMED_DTC                                           0x03
#define TEST_NOT_COMPLETE_SINCE_LAST_CLEAR                      0x04
#define TEST_FAILED_SINCE_LAST_CLEAR                            0x05
#define TEST_NOT_COMPLETED_THIS_OPERATION_CYCLE                 0x06
#define WARNING_INDICATOR_REQUESTED                             0x07

#define ISO14229_1DTCFORMAT                                     0x01
#define ZERO_SER19                                              0x00

#define SUBFUN_1_RESP_LEN                                       0x05


#define ONE     1u
#define TWO     2u
#define THREE   3u
#define FOUR    4u
#define FIVE    5u
#define SIX     6u
#define SEVEN   7u
#define EIGHT   8u

/*
 ****************************************************************************************
 *    ENUM Definition 
 ****************************************************************************************
*/
//typedef enum
//{
//	SERV19_NUMBER_OF_DTC_BY_STATUS_MASK_E      = 0x01 ,                       
//	SERV19_DTC_BY_STATUS_MASK_E                       ,
//	SERV19_DTC_SNAPSHOT_IDENTIFICATION_E              ,              
//	SERV19_DTC_SNAPSHOT_RECORD_BY_DTC_NUMBER_E        ,           
//	SERV19_DTC_STORED_RECORD_BY_RECORD_NUMBER_E     ,          
//	SERV19_DTC_EXTENDED_DATA_RECORD_BY_DTC_NUMBER_E   ,           
//	SERV19_NUMBER_OF_DTC_BY_SEVERITY_MASK_RECORD_E    ,         
//	SERV19_DTC_BY_SEVERITY_MASK_RECORD_E              ,         
//	SERV19_SEVERITY_INFORMATION_OF_DTC_E              ,       
//	SERV19_SUPPORTED_DTC_E                            ,     
//	SERV19_FIRST_TEST_FAILED_E                        ,      
//	SERV19_FIRST_CONFIRMED_E                          ,      
//	SERV19_MOST_RECENT_TEST_FAILED_E                  ,  
//	SERV19_MOST_RECENT_CONFIRMED_E                    ,
//	SERV19_MIRROR_MEMORY_DTC_BY_STATUS_MASK_E         ,   
//	SERV19_MIRROR_MEMORY_EXTENDED_DATA_RECORD_BY_DTC_NUMBER_E,   
//	SERV19_NUMBER_OF_MIRROR_MEMORY_BY_DTC_E              ,   
//	SERV19_NUMBER_OF_EMISSION_RELATED_OBD_DTC_BY_STATUS_MASK_E,
//	SERV19_EMISSION_RELATED_OBD_DTC_BY_STATUS_MASK_E   ,
//	SERV19_DTC_FAULT_DETECTION_COUNTER_E               ,
//	SERV19_DTC_WITH_PERMANENT_STATUS_E                 ,
//	SERV19_DTC_EXTENDED_DATA_RECORD_BY_RECORD_NUMBER_E ,
//	SERV19_USER_DEFINE_MEMORY_DTC_BY_STATUS_MASK_E     ,
//	SERV19_USER_DEFINE_MEMORY_DTC_SNAPSHOT_RECORD_BY_DTC_NUMBER_E,
//	SERV19_USER_DEFINE_MEMORY_DTC_EXTENDED_DATA_RECORD_BY_DTC_NUMBER_E,  
//	SERV19_WWH_OBD_DTC_BY_MASK_RECORD_E        =  0x42 ,
//	SERV19_WWH_OBD_DTC_WITH_PERMANENT_STATUS_E =  0x55 ,
//}SubFunctionSer19_En_t;
///*
// ****************************************************************************************
// *    Structure Definition 
// ****************************************************************************************
//*/
////todo harsh
//typedef struct
//{
//    SubFunctionSer19_En_t 	SUB_FUNCTION_ID ;
//    uint16_t 				(*Subfun_Fptr_t) (uint8_t*);
//    uint8_t 				DTC_SubFun_Valide_Length_u8;
//}SubFunctionSer19_St_t;

typedef struct
{
  uint16_t 	DataIdentifier_u16;
  uint8_t 	Size_u8;
}DataIdentifierInfo_St_t;

/*
 ****************************************************************************************
 *    global Variables
 ****************************************************************************************
*/ 

/*
 ****************************************************************************************
 *    Function Prototypes
 ****************************************************************************************
*/
 extern UDS_Serv_resptype_En_t iso14229_serv19(UDS_Serv_St_t* /* UDS_Serv_pSt*/);  

#endif
