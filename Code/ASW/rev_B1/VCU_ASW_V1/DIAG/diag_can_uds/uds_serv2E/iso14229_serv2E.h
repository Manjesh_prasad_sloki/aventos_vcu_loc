/******************************************************************************
 *    FILENAME    : uds_serv2E.h
 *    DESCRIPTION : Header file for UDS service - WRITE DATA BY IDENTIFIER9(0x2E).
 ******************************************************************************
 * Revision history
 *  
 * Ver Author          Date                     Description
 * 1   Sloki     10/01/2019		   Initial version
 ******************************************************************************
*/  


#ifndef _UDS_SERV2E_H_
#define	_UDS_SERV2E_H_

#ifdef	__cplusplus
 "C" {
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "uds_conf.h"
#include "iso14229_serv27.h"

  
  
#define SERV2E_SEESION_CHECK_BIT_POS    1u 
  
/*
 **************************************************************************************************
 *    Export Variables
 **************************************************************************************************
 */
extern uint32_t Serv2E_Index_u32;

/**
*  FUNCTION NAME : iso14229_serv2E
*  FILENAME      : iso14229_serv2E.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributor table.
*  @brief        : This function will process the service_2E requests
*  @return       : Type of response.                     
*/   
extern UDS_Serv_resptype_En_t iso14229_serv2E(UDS_Serv_St_t*);

 /**
*  FUNCTION NAME : UDS_Serv2E_Timeout
*  FILENAME      : uds_serv2E.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv2E_Timeout(void);
 

 #ifdef	__cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
