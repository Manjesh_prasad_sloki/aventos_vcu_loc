/******************************************************************************
 *    FILENAME    : uds_serv2E.c
 *    DESCRIPTION : Service description for UDS service - WRITE DATA BY IDENTIFIER(0x2E).
 ******************************************************************************
 * Revision history
 *
 * Ver Author          Date                     Description
 * 1   Sloki        10/01/2019		   Initial version
 ******************************************************************************
*/



/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */


#include "iso14229_serv2E.h"
#include "uds_DID.h"
#include "diag_typedefs.h"
//#include <stdint.h>
//#include "Task_Scheduler.h"


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

uint32_t Serv2E_Index_u32 = 0u;

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */



/**
*  FUNCTION NAME : iso14229_serv2E
*  FILENAME      : iso14229_serv2E.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_2E requests(WRITE DATA BY IDENTIFIER).
*  @return       : Type of response(Positive Or Negative Or Unknown).
*/
/* ************************************************************************** */
/* ************************************************************************** */
#define SERV2E_HEADER_BYTES 3u

UDS_Serv_resptype_En_t iso14229_serv2E(UDS_Serv_St_t* UDS_Serv_pSt)
{
    ISO14229_DidList_En_t Index_u32 = DID_LIST_START_E;
    uint32_t ReqLength_u32 = 0u;
    uint32_t DID_Length_u32 = 0u;
//    uint32_t i = 0u;                                                 /*Variables i and j for looping operation(LOOP Count*/
    uint32_t j = 1u;
    uint16_t DID_Num = 0u;                                           /* Used for 2 Bytes DID NUMBER Storage for DID Comparison */
    bool 	RTRN_b = FALSE;                                           /* Holds the return value from the Corresponding Function Pointer*/
    bool 	DIDFound_b = FALSE;                                    /* Variable used as a Flag*/
    bool 	DID_Session_Status_b = FALSE;
    bool 	Valid_Did_length_b = FALSE;
    bool 	Security_Check_b = FALSE;
    UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
    
    ReqLength_u32 = UDS_Serv_pSt->RxLen_u16;
    DID_Length_u32 = ReqLength_u32 - SERV2E_HEADER_BYTES;
    Serv2E_Index_u32 = 1u;

    DID_Num = UDS_Serv_pSt->RxBuff_pu8[Serv2E_Index_u32++];                         /* Store Higher Byte of DID*/
    DID_Num = (DID_Num << 8) | UDS_Serv_pSt->RxBuff_pu8[Serv2E_Index_u32++];        /* Store Lower Byte of DID*/

//        if(DID_Num == 0xA101)
//         {
//             TS_StopScheduler();
//         }
    for (Index_u32 = DID_LIST_START_E; Index_u32 < DID_TOTAL_E; Index_u32++)                                                    /* for loop for DID comparison for below condition*/
    {
        if (DID_Num == DID_Config_aSt[Index_u32].DID_Num)                                     /*DID Comparison*/
        {
            DIDFound_b = TRUE;    /* DID EXIST*/
//            Index_u32 = i;
            Current_DID_Index_u16 = Index_u32;
            break;
        }
    }

    if ((ReqLength_u32 < SERV2E_MIN_LEN))
    {
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;       /*NRC-0x13*/
        UDS_Serv_pSt->TxLen_u16 = 3u;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        if (DIDFound_b)
        {
            if (DID_Length_u32 == DID_Config_aSt[Index_u32].DID_Val_Size)
            {
                Valid_Did_length_b = TRUE;
                if (1)//TRUE == Check_SecurityClearance()) //todo harsh
                {
                    Security_Check_b = TRUE;
                    if (DID_Session_Check(Index_u32, SERV2E_SEESION_CHECK_BIT_POS))
                    {
                        DID_Session_Status_b = TRUE;
                    }
                }
            }
//            else if (IC_REPLACEMENT_E == Index_u32)
//            {
//                if ((DID_Length_u32) == Inhale_Exhale_Size())
//                {
//                    Valid_Did_length_b = TRUE;
//                    if (1)//TRUE == Check_SecurityClearance()) //todo harsh
//                    {
//                        Security_Check_b = TRUE;
//                        if (DID_Session_Check(Index_u32, SERV2E_SEESION_CHECK_BIT_POS))
//                        {
//                            DID_Session_Status_b = TRUE;
//                        }
//                    }
//                }
//            }
        }

        if ((DIDFound_b == FALSE))
        {
            UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
            UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
            UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;       /* NRC-0x31*/
            UDS_Serv_pSt->TxLen_u16 = 3u;
            Serv_resptype_En = UDS_SERV_RESP_NEG_E;
        }
        else if (Valid_Did_length_b == FALSE)
        {
            UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
            UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
            UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;       /*NRC-0x13*/
            UDS_Serv_pSt->TxLen_u16 = 3u;
            Serv_resptype_En = UDS_SERV_RESP_NEG_E;
        }

        else if (Security_Check_b == FALSE)
        {
            UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
            UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
            UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;       //NRC-0x33
            UDS_Serv_pSt->TxLen_u16 = 3u;
            Serv_resptype_En = UDS_SERV_RESP_NEG_E;
        }

        else if (DID_Session_Status_b == FALSE)
        {

            UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
            UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
            UDS_Serv_pSt->TxBuff_pu8[2] = CONDITION_NOT_CORRECT;  /*NRC- 0x22*/
            UDS_Serv_pSt->TxLen_u16 = 3u;
            Serv_resptype_En = UDS_SERV_RESP_NEG_E;
        }

        else
        {
            if(DID_Config_aSt[Index_u32].Var_ptr_pu8)
            {
                Var_Ptr_pu8 = DID_Config_aSt[Index_u32].Var_ptr_pu8;
            }
            if(DID_Config_aSt[Index_u32].CALLBCK_Fptr)
            {
                RTRN_b = DID_Config_aSt[Index_u32].CALLBCK_Fptr(UDS_Serv_pSt->RxBuff_pu8, DID_Config_aSt[Index_u32].DID_Val_Size, WRITE_ONLY_E, Var_Ptr_pu8, DID_Config_aSt[Index_u32].Type_En);
            }
            if (RTRN_b == TRUE)
            {
                UDS_Serv_pSt->TxLen_u16 = 2u;
                UDS_Serv_pSt->TxBuff_pu8[j] = UDS_Serv_pSt->RxBuff_pu8[j];         /*TxBuff_pu8 contains Higher byte of DID*/
                UDS_Serv_pSt->TxBuff_pu8[j + 1] = UDS_Serv_pSt->RxBuff_pu8[j + 1];     /*TxBuff_pu8 contains Lower byte of DID */
                Serv2E_Index_u32 = 0u;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
            }
            else
            {
                UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
                UDS_Serv_pSt->TxBuff_pu8[1] = SID_WDBDID;
                UDS_Serv_pSt->TxBuff_pu8[2] = CONDITION_NOT_CORRECT;  /*NRC- 0x22*/
                UDS_Serv_pSt->TxLen_u16 = 3u;
                Serv_resptype_En = UDS_SERV_RESP_NEG_E;
            }
        }
    }
    return Serv_resptype_En;
}


/**
*  FUNCTION NAME : UDS_Serv2E_Timeout
*  FILENAME      : uds_serv2E.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.
*/
void UDS_Serv2E_Timeout(void) {

    return;
}


/* *****************************************************************************
 End of File
 */
 /**/
