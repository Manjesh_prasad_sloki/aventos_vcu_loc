/******************************************************************************
 *    FILENAME    : uds_serv23.h
 *    DESCRIPTION : Header file for UDS service - Read Memory By Address.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

#ifndef _UDS_SERV23_H_
#define	_UDS_SERV23_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.*/
#include "uds_conf.h"
#include "iso14229_serv27.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* summary: memory address maximum length */
#define MEM_ADDRESS_MAX_LEN                                 0x04u

/* summary: memory size maximum length */
#define MEM_SIZE_MAX_LEN                                    0x02u

/* summary: length size */
#define SERV_23_MIN_LEN                                     0x04u


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Data Types                                                        */
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : iso14229_serv23
*  FILENAME      : iso14229_serv23.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_23 requests
*  @return       : Type of response.
**/
extern UDS_Serv_resptype_En_t iso14229_serv23(UDS_Serv_St_t* );

/**
*  FUNCTION NAME : UDS_Serv23_Timeout
*  FILENAME      : uds_serv23.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
**/
extern void UDS_Serv23_Timeout(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ISO14229_SER23_H */
