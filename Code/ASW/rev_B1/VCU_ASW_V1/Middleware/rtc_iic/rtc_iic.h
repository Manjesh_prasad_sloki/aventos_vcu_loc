/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : rtc_iic.c
|    Project        : Aventos Energy
|    Description    : Device driver RTC DS1307.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 27/02/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef RTC_IIC_H
#define RTC_IIC_H
/*******************************************************************************
 *  HEARDER FILE INCLUDES
 ******************************************************************************/
#include "app_typedef.h"
/*******************************************************************************
 *  MACRO DEFNITION
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
#pragma pack(1)
typedef struct 
{
    char_t Second_u8;
    char_t Minute_u8;
    char_t Hour_u8;
    char_t Week_u8;
    char_t Date_u8;
    char_t Month_u8;
    char_t Year_u8;
    char_t Meridiem_u8;
    char_t HoursFormat_u8;
}RealTimeData_St_t;
#pragma unpack
/*******************************************************************************
 *  EXTERN GLOBAL VARIABLES
 ******************************************************************************/
extern RealTimeData_St_t RealTimeData_St;
extern RealTimeData_St_t SetTime_St;

extern uint8_t RtcMerdiem_u8;
extern uint8_t RtcHoursFormat_u8;
/*******************************************************************************
 *  EXTERN FUNCTION
 ******************************************************************************/
void GetRealTime(RealTimeData_St_t*const GetRtcTime_pSt);
void ExtRtcInit(void);
extern bool SetRealTime(RealTimeData_St_t*const SetRtcTime_pSt);
void CanCallback(uint16_t CanId_u16,uint8_t * CanRxbuff_pu8);
void SendRtcOverCan(void);
#endif
/*---------------------- End of File -----------------------------------------*/