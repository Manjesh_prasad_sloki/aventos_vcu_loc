/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : rtc_iic.c
|    Project        : Aventos Energy
|    Description    : Device driver RTC DS1307.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 27/02/2023       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"rtc_iic.h"
#include "can_driver.h"
#include "pal_iic.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

/* DS1307 Slave Address*/
#define DS1307_SLAVE_ADDRESS    0x68

#define HOUR_FORMAT_BIT_POS     0x40
#define MERIDIEM_BIT_POS        0x20


/* Time related info */

#define ANTI_MERIDIEM   0x00
#define POST_MERIDIEM   0x01

#define _12H_FORMAT     0x01
#define _24H_FORMAT     0x00 

#define _12H_MAX_TIME   (12U)
#define _24H_MAX_TIME   (23U)


/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/
uint8_t RtcRxBuff_au8[8] = {0};
uint8_t RtcTxBuff_au8[8] = {0};
uint8_t CanTxbuff[8] = {0};
bool SetTime_b = false;

uint8_t RtcMerdiem_u8 = 0;
uint8_t RtcHoursFormat_u8 = 0;
/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/
RealTimeData_St_t RealTimeData_St;
RealTimeData_St_t SetTime_St;

/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/
uint8_t BCDtoDECIMAL(uint8_t BcdNum_u8);
uint8_t DECIMALtoBCD(uint8_t DecimalNum_u8);
void ReadEeprom(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GetRealTime
*   Description   : The function provides time of the RTC
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void GetRealTime(RealTimeData_St_t*const GetRtcTime_pSt)
{
    
    RtcTxBuff_au8[0] = 0;   /* Set the RTC pointer to the 0th address */

    Pal_IIC_Send(DS1307_SLAVE_ADDRESS,RtcTxBuff_au8,1);

    Pal_IIC_Recieve(DS1307_SLAVE_ADDRESS,RtcRxBuff_au8,7); /* Read Actual Time */
    
    GetRtcTime_pSt->Second_u8 = BCDtoDECIMAL(RtcRxBuff_au8[0]);
    GetRtcTime_pSt->Minute_u8 = BCDtoDECIMAL(RtcRxBuff_au8[1]);
    GetRtcTime_pSt->Week_u8   = BCDtoDECIMAL(RtcRxBuff_au8[3]);  
    GetRtcTime_pSt->Date_u8   = BCDtoDECIMAL(RtcRxBuff_au8[4]);
    GetRtcTime_pSt->Month_u8  = BCDtoDECIMAL(RtcRxBuff_au8[5]);
    GetRtcTime_pSt->Year_u8   = BCDtoDECIMAL(RtcRxBuff_au8[6]);

    if(0 == (RtcRxBuff_au8[2] & HOUR_FORMAT_BIT_POS) )
    {
        GetRtcTime_pSt->HoursFormat_u8 = _24H_FORMAT;
        GetRtcTime_pSt->Hour_u8   = BCDtoDECIMAL(RtcRxBuff_au8[2] & 0x3F);
    }
    else
    {
        GetRtcTime_pSt->HoursFormat_u8 = _12H_FORMAT;
        GetRtcTime_pSt->Hour_u8 = BCDtoDECIMAL(RtcRxBuff_au8[2] & 0x1F);

        if(0 == (RtcRxBuff_au8[2] & MERIDIEM_BIT_POS) )
        {
            GetRtcTime_pSt->Meridiem_u8 = ANTI_MERIDIEM;
        }
        else
        {
            GetRtcTime_pSt->Meridiem_u8 = POST_MERIDIEM;
        }

    }

    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SetRealTime
*   Description   : The function set the time of the RTC
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
bool SetRealTime(RealTimeData_St_t*const SetRtcTime_pSt)
{
	

    // if(!SetTime_b)
    // {
    //     /* Time set is not requested  */
    //     return false;
    // }
    
    // SetTime_b = false;

    RtcTxBuff_au8[0] = 0; /* Set the RTC pointer to the 0th address */
    
    RtcTxBuff_au8[1] = DECIMALtoBCD(SetRtcTime_pSt->Second_u8); 
    RtcTxBuff_au8[2] = DECIMALtoBCD(SetRtcTime_pSt->Minute_u8); 
    RtcTxBuff_au8[3] = DECIMALtoBCD(SetRtcTime_pSt->Hour_u8);
    RtcTxBuff_au8[4] = DECIMALtoBCD(SetRtcTime_pSt->Week_u8);
    RtcTxBuff_au8[5] = DECIMALtoBCD(SetRtcTime_pSt->Date_u8);
    RtcTxBuff_au8[6] = DECIMALtoBCD(SetRtcTime_pSt->Month_u8);
    RtcTxBuff_au8[7] = DECIMALtoBCD(SetRtcTime_pSt->Year_u8); 

    if((_12H_FORMAT == SetRtcTime_pSt->HoursFormat_u8) && (SetRtcTime_pSt->Hour_u8 <= _12H_MAX_TIME ))
    {
        if(ANTI_MERIDIEM == SetRtcTime_pSt->Meridiem_u8)
        {
            RtcTxBuff_au8[3] = (HOUR_FORMAT_BIT_POS|(RtcTxBuff_au8[3]))&(~MERIDIEM_BIT_POS);
        }
        else if(POST_MERIDIEM == SetRtcTime_pSt->Meridiem_u8)
        {
            RtcTxBuff_au8[3] = (HOUR_FORMAT_BIT_POS|(RtcTxBuff_au8[3])|MERIDIEM_BIT_POS);
        }
        else
        {
            /* Invalid time meridiem */
            return false;
        }
    }
    else if((_24H_FORMAT == SetRtcTime_pSt->HoursFormat_u8) && (SetRtcTime_pSt->Hour_u8 <= _24H_MAX_TIME ))
    {
        RtcTxBuff_au8[3] = (~HOUR_FORMAT_BIT_POS)&RtcTxBuff_au8[3];
    }
    else
    {
        /* Invalid hour format */
        return false;
    }

    Pal_IIC_Send(DS1307_SLAVE_ADDRESS,RtcTxBuff_au8,8);
    
    return true;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ExtRtcInit
*   Description   : The function Initialize the Write pointer of RTC chip
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void ExtRtcInit(void)
{
    
    RtcTxBuff_au8[0] = 0;
    RtcTxBuff_au8[1] = 0;

    /* Enable the Oscilattor */
    Pal_IIC_Send(DS1307_SLAVE_ADDRESS,RtcTxBuff_au8,2);
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BCDtoDECIMAL
*   Description   : The function converts the BCD valur to the Decimal value
*   Parameters    : BCD_Num_u8 : BCD value
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint8_t BCDtoDECIMAL(uint8_t BcdNum_u8)
{
    uint8_t Decimal_Num_u8 = 0;
    Decimal_Num_u8 = ((BcdNum_u8>>4)) *10;
    Decimal_Num_u8 = Decimal_Num_u8 + (BcdNum_u8&0x0f);
    return Decimal_Num_u8;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DECIMALtoBCD
*   Description   : The function converts the Decimal value to the BCD value
*   Parameters    : DecimalNum_u8 : Decimal value
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint8_t BcdValue_u8 = 0;
uint8_t temp = 0;
uint8_t DECIMALtoBCD(uint8_t DecimalNum_u8) 
{
    temp =   DecimalNum_u8; 
    temp =   (DecimalNum_u8/10);
   temp = temp << 4;
    BcdValue_u8 = ((DecimalNum_u8/10) << 4);
    BcdValue_u8 = (BcdValue_u8 | (DecimalNum_u8%10));
    return BcdValue_u8;
}




void CanCallback(uint16_t CanId_u16,uint8_t * CanRxbuff_pu8)
{

    if(SetTime_b)
    {
        return;
    }
    SetTime_St.Second_u8 = CanRxbuff_pu8[0];
    SetTime_St.Minute_u8 = CanRxbuff_pu8[1];
    SetTime_St.Hour_u8 = CanRxbuff_pu8[2];
    SetTime_St.Date_u8 = CanRxbuff_pu8[3];
    SetTime_St.Month_u8 = CanRxbuff_pu8[4];
    SetTime_St.Year_u8 = CanRxbuff_pu8[5];
    SetTime_St.Week_u8 = CanRxbuff_pu8[6];

    SetTime_St.Meridiem_u8 = CanRxbuff_pu8[7] &0x0F;
    SetTime_St.HoursFormat_u8 = (CanRxbuff_pu8[7] >> 4) & 0x0F;

    SetTime_b = true;
    return;
}

void SendRtcOverCan(void)
{
    CanTxbuff[0] = RealTimeData_St.Second_u8;
    CanTxbuff[1] = RealTimeData_St.Minute_u8;
    CanTxbuff[2] = RealTimeData_St.Hour_u8;
    CanTxbuff[3] = RealTimeData_St.Date_u8;
    CanTxbuff[4] = RealTimeData_St.Month_u8;
    CanTxbuff[5] = RealTimeData_St.Year_u8;

    CanTxbuff[6] = RealTimeData_St.Week_u8;
    CanTxbuff[7] = (RealTimeData_St.Meridiem_u8&0x0f)|((RealTimeData_St.HoursFormat_u8&0x0f) << 4);

    CAN0_Transmit(0x7F1,8,CanTxbuff);


}

void ReadEeprom(void)
{
    RtcTxBuff_au8[0] = 0x00;
    IIC_ReadRtcData(RtcTxBuff_au8,2);
    return;
}
/*---------------------- End of File -----------------------------------------*/