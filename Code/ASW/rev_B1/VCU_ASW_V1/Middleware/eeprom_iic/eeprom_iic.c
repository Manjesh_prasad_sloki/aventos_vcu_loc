/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : eeprom_iic.c
|    Project        : Aventos Energy VCU
|    Description    : Device driver for 24AA256 EEPROM chip.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 01/03/2022     Jeevan Jestin N           Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"eeprom_iic.h"
#include"pal_iic.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/
#define EEPROM_SLAVE_ADDR   0x50

/* 24AA256 chip address range */
#define ADDR_RANGE_MAX      0x7FFF
#define ADDR_RANGE_MIN      0x0000

#define EEPROM_ACC_PASS     0x01
#define EEPROM_ACC_FAIL     0x00

#define EEPROM_OUTOF_ADDR_RANGE 0x02
#define EEPROM_BUFF_OVERFLOW    0x03

#define EEPROM_MAX_SIZE     (50U)

#define MSB_ADDR            0x00
#define LSB_ADDR            0x01
#define ADDR_LENGTH         0x02
/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/
uint8_t Eeprom_Buff_au8[EEPROM_MAX_SIZE+ADDR_LENGTH] = {0};
/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_EEPROM
*   Description   : The function read the data from the EEPROM data 
*   Parameters    : None
*   Return Value  : Return the status of the operation
*                   0 : EEPROM_ACC_FAIL
*                   1 : EEPROM_ACC_PASS
*  ---------------------------------------------------------------------------*/
uint8_t Read_EEPROM(uint16_t ReadAddr_u16,uint16_t ReadSize_u16,uint8_t* ReadBuff_pu8)
{
    if(ReadAddr_u16 > ADDR_RANGE_MAX)
    {
        return EEPROM_OUTOF_ADDR_RANGE;
    }

    if(ReadSize_u16 > EEPROM_MAX_SIZE)
    {
        return EEPROM_BUFF_OVERFLOW;
    }

    Eeprom_Buff_au8[MSB_ADDR] = (ReadAddr_u16 >> 0x08) &0xFF;
    Eeprom_Buff_au8[LSB_ADDR] = (ReadAddr_u16) &0xFF;

    Pal_IIC_Send(EEPROM_SLAVE_ADDR,Eeprom_Buff_au8,ADDR_LENGTH);

    Pal_IIC_Recieve(EEPROM_SLAVE_ADDR,ReadBuff_pu8,ReadSize_u16);

    return EEPROM_ACC_PASS;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_EEPROM
*   Description   : The function read the data from the EEPROM data 
*   Parameters    : None
*   Return Value  : Return the status of the operation
*                   0 : EEPROM_ACC_FAIL
*                   1 : EEPROM_ACC_PASS
*  ---------------------------------------------------------------------------*/
uint8_t Write_EPROM(uint16_t WriteAddr_u16,uint16_t WriteSize_u16,uint8_t* WriteBuff_pu8)
{
    uint16_t i = 0;
    if(WriteAddr_u16 > ADDR_RANGE_MAX)
    {
        return EEPROM_OUTOF_ADDR_RANGE;
    }

    if(WriteSize_u16 > EEPROM_MAX_SIZE)
    {
        return EEPROM_BUFF_OVERFLOW;
    }

    Eeprom_Buff_au8[MSB_ADDR] = (WriteAddr_u16 >> 0x08) &0xFF;
    Eeprom_Buff_au8[LSB_ADDR] = (WriteAddr_u16) &0xFF;

    for(i = 0;i<WriteSize_u16;i++)
    {
        Eeprom_Buff_au8[i+ADDR_LENGTH] = WriteBuff_pu8[i];
    }

    Pal_IIC_Send(EEPROM_SLAVE_ADDR,Eeprom_Buff_au8,ADDR_LENGTH+WriteSize_u16);

    // Pal_IIC_Send(EEPROM_SLAVE_ADDR,WriteBuff_pu8,WriteSize_u16);

    return EEPROM_ACC_PASS;
}
/*---------------------- End of File -----------------------------------------*/