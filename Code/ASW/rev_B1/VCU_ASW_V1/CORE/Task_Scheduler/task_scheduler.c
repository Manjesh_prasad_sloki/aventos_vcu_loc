/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : task_scheduler.c
|    Project	    : VCU
|    Module         : Task Scheduler
|    Description    : This file implements the Task_Scheduler.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 09/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef TASK_SCHEDULER_C
#define TASK_SCHEDULER_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "task_scheduler.h"
#include "r_cg_userdefine.h"
#include "ostm_user.h"
//#include "Config_WDT0.h"
#include "vcu.h"
#include "com_tasksched.h"
#include "Ext_WDT.h"
#include "r_cg_riic.h"
#include "Config_STBC.h"
#include "Config_CSIH0.h"
#include "Config_PORT.h"
#include "iso14229_serv11.h"
#include"rtc_iic.h"

//#include    <assert.h> 

/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 
static   bool TS_ExitSched_b   = false;
volatile bool TS_trigger_b     = false;
uint8_t data[5] = {0};
bool IIC_Tx_flag = true;
uint8_t I2CCHIPID = 0;
bool I2C_Receive_b = false;
bool I2C_Sent_b = false;

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 MD_STATUS Status_Tx = 0;
 MD_STATUS Status_Rx = 0;
uint16_t Data_Tx[10] = {0x4002};
bool Rx_Flag = false;
bool Tx_Flag = false;
uint32_t counter = 0;
uint32_t Counter_5ms_u32 = 0;


bool _5msTaskSchdlr_b;
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TaskScheduler_Start
*   Description   : This function schedules the tasks in multiple of 5ms.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 

void TaskScheduler_Start(void)
{
         		while (!TS_ExitSched_b)
	{

		while(_5msTaskSchdlr_b)
		{
			//GetRealTime(&RealTimeData_St);
			_5msTaskSchdlr_b = false;
			counter++;
			R_Config_WDT0_Restart(); /*Refresh the WDT every-time within 34.13ms*/
			Diag_TS_Proc_5ms();
			ReadInputSignal();
			UpdateDataBankSignals();
			VCU_Proc();
			WriteOutputSignals();
			if(reset_b == true)
			{
				Counter_5ms_u32++;
				if(Counter_5ms_u32 == 3)
				{
					TasksScheduler_Exit();
					break;
				}
			}
		}
	}

	return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TS_PowerOff
*   Description   : This function stops the tasks and try to shutdown the ECU
*					or put it into sleep.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void TS_PowerOff(void)
{
	/*EEPROM*/
	WPROTR.PROTCMD0 = 0x000000A5;

	RESCTL.SWRESA = 0x1;

	RESCTL.SWRESA = ~0x01;

	RESCTL.SWRESA = 0x1;
	//RESCTL.SWRESA = 0x01U;
	return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : TasksScheduler_Exit
*   Description   : This function exit the task scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void TasksScheduler_Exit(void)
{
	TS_ExitSched_b = true;
	return;
}

#endif /* TASK_SCHEDULER_C */
/*---------------------- End of File -----------------------------------------*/