/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : board_init.c
|    Project	    : VCU
|    Module         : Board Initialisation
|    Description    : This file contains the variables and functions to
|                     initialize the Hardware and Software Components.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 09/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BOARD_INIT_C
#define BOARD_INIT_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "board_init.h"
#include"pal_clock.h"
#include "pal_wdt.h"
#include "pal_rtc.h"
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_OSTM0.h"
#include "Config_ADCA0.h"
#include "Config_INTC.h"
//#include "Config_WDT0.h"
#include "Config_PORT.h"
#include "Config_RIIC1.h"
#include "Config_UART1.h"
#include "Config_RTCA0.h"
#include "Config_UART0.h"
#include "r_cg_cgc.h"
//#include "can_driver.h"
#include "Config_STBC.h"
#include "Config_CSIH0.h"
#include "Config_TAUB0.h"
#include "Config_TAUD0.h"
#include "Config_CSIH2.h"
#include "Config_UART2.h"
#include "PFlash.h"
#include "Communicator.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 

 
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
 
 
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Hardware_Init
*   Description   : This function initializes the Hardware peripherals.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void Hardware_Init(void)
{
	DI();
	PortInit();
	ClockInit();
	TimerModuleInit();
	AdcInit();
	WatchDogTimerinit();
	CanInit();
	Rtcinit();
	SpiInit();
	I2cInit();
	DFlash_Init();
	PFlash_Init();
	FDL_Read(0x0000,sizeof(eeprom_data_Tx),(uint8_t*)&eeprom_data_Tx);
	Ini_odo_reading_u32 = eeprom_data_Tx.odo_reading_u32;
	return;
 }

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Software_Init
*   Description   : This function initailizes the Software components and 
*		            starts the hardware peripherals
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
void Software_Init(void)
{
	EI();
	IntervalTimerStart();
	AdcStart();
	SpiStart();
	I2cStart();
	Diag_TS_Init(); 
	VCU_Init(); 
	Sd_Card_Init();
	Gyroscope_init();
	DataAquireInit();
	return;
}



#endif /* BOARD_INIT_C */
/*---------------------- End of File -----------------------------------------*/