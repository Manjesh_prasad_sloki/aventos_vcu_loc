
#include "r_cg_userdefine.h"
#include "Config_ADCA0.h"
#include "adc_user.h"
#include "Test.h"

uint16_t ADC_RPM_u16 = 0;
uint16_t ADC_Result_au16[7] = {0};

uint16_t Read_RPM(void)
{
    float Temp_f = 0;
    R_Config_ADCA0_ScanGroup1_OperationOn();
	while(Group1_Scan_Completed_b == false); /*Wait untill convertion completes*/
	Group1_Scan_Completed_b = false;
//	R_Config_ADCA0_ScanGroup1_GetResult(&ADC_Result_au16[0]);
    ADC_RPM_u16 = ADC_Result_au16[6];
    Temp_f = (((float)ADC_RPM_u16/4096)*10000);
    ADC_RPM_u16 = (uint16_t)Temp_f;
    return ADC_RPM_u16;
}

