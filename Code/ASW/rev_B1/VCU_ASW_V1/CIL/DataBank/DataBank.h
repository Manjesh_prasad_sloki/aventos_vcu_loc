/***********************************************************************************************************************
* File Name    : DataBank.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef DATA_BANK_H
#define DATA_BANK_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
//#include "DataAquire.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	TURNED_OFF_E,
	TURNED_ON_E,
}SignalState_En_t;




  



#pragma pack(1)
typedef struct
{
	uint8_t 	HighBeam_SwitchState_u8;
	uint8_t 	Lights_SwitchState_u8;
	uint8_t 	LeftTurn_SwitchState_u8;
	uint8_t 	RightTurn_SwitchState_u8;
	uint8_t 	Brake_SwitchState_Front_u8;
	uint8_t 	Brake_SwitchState_Back_u8;
	uint8_t 	Key_SwitchState_u8;
	uint8_t 	Kill_SwitchState_u8;
	uint8_t 	KickStand_SwitchState_u8;
	uint8_t 	Throttle_u8;
	uint8_t     ReGen_SwitchState_u8;
	uint8_t     B_Low_SwitchState_u8;
	uint8_t 	Fall_SwitchState_u8;
	uint16_t    LeftTurn_FeedBack_u16;
	uint16_t    RightTurn_FeedBack_u16;
	uint8_t 	Neutral_Mode_Sel_u8;
	uint8_t 	Economy_Mode_Sel_u8;
	uint8_t 	Sports_Mode_Sel_u8;
	uint8_t 	Reverse_Mode_Sel_u8;
	uint8_t 	Safe_Mode_Sel_u8;
	uint8_t     Neutral_Long_press_u8;
	uint8_t     TimeSett_inc_u8;
	uint8_t     TimeSett_dec_u8;
	uint8_t     TimeSett_Sel_u8;
	uint8_t     Hazard_SwitchState_u8;
	bool        BootUpflag_b;
	uint8_t     BmsTempLowerTri_u8;
	uint8_t     Safe_mode_Entery_exit_u8;
}UserInputSig_St_t;


#pragma pack(1)
typedef struct 
{
   uint8_t buzzer_u8; 
   uint8_t RightIndLight_u8;
   uint8_t LeftIndLight_u8;
   uint8_t MotorKill_u8;
   uint8_t TorqueZero_u8;
   bool modechange_b;
   bool TurnInd_b;
}VcuOutputs_St_t; 


#pragma pack(1)
typedef struct 
{
	uint32_t Decounce_u32;
	uint32_t Autdecounter_u32;
	uint8_t  Autoinc_u8;
	uint8_t  Autoinc_b;
}AltFunc_St_t;

extern AltFunc_St_t AltFunc_St[10];



extern VcuOutputs_St_t VcuOutputs_St;


/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern UserInputSig_St_t	UserInputSig_St;

uint8_t AlternateFunction(uint8_t input);

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* DATA_BANK_H */


