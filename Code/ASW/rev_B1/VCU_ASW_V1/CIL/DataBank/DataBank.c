/***********************************************************************************************************************
* File Name    : DataBank.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataBank.h"
#include "DataAquire.h"
#include"data_acquire_conf.h"
#include "pal_gpio.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
UserInputSig_St_t	UserInputSig_St = 
{
	TURNED_OFF_E,	/*High Beam Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Lights Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Left Turn Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Right Turn Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Brake Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Brake Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Key Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Kill Switch - Default Turned-Off*/
	TURNED_OFF_E,   /*SOC Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Kick Stand Switch - Default Turned-Off*/
	0	,	        /*Throttle  - ZERO*/
	0,              /*LeftTurn_FeedBack_u16*/
	0,              /*RightTurn_FeedBack_u16 */
	false,		/*VehicleFallenState - Default false*/
	
	/*Driver Mode Selected*/
	TURNED_ON_E,		/*Neutral Mode*/ 
	TURNED_OFF_E,		/*Economy Mode*/
	TURNED_OFF_E,		/*Sports Mode*/
	TURNED_OFF_E,		/*Reverse Mode*/
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
	TURNED_OFF_E,
};
 
 VcuOutputs_St_t VcuOutputs_St; 
AltFunc_St_t AltFunc_St[10];


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : UpdateDataBankSignals
*   Description   : The function updates the data bank signals
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint32_t Debounce_Counter_u2[10];
void UpdateDataBankSignals(void)
{
	static bool initflag = false;
	if(initflag == false)
	{
		initflag = true;
		DOutputSig_aSt[DO_7_E].PreviousState_u8 = 1;
		DOutputSig_aSt[DO_8_E].PreviousState_u8 = 1;
		DOutputSig_aSt[DO_3_E].PreviousState_u8 = 1;
	}
	    if(TURNED_ON_E ==  DInpSignal_aSt[0].SignalState_u8)
		{
			UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
			UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
			UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		
		}
		else if (TURNED_ON_E == DInpSignal_aSt[1].SignalState_u8)
		{
			UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Sports_Mode_Sel_u8  =  TURNED_OFF_E;
			UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;
		
		}
		else if(TURNED_ON_E == DInpSignal_aSt[2].SignalState_u8)
		{
			UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_ON_E;
			UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		}
		else if(TURNED_ON_E == DInpSignal_aSt[3].SignalState_u8)
		{
			UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
			UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E; 
			UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
			UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		}
			UserInputSig_St.HighBeam_SwitchState_u8 = DInpSignal_aSt[4].SignalState_u8;
			UserInputSig_St.ReGen_SwitchState_u8 = DInpSignal_aSt[5].SignalState_u8;
			UserInputSig_St.Kill_SwitchState_u8 = DInpSignal_aSt[6].SignalState_u8;
			UserInputSig_St.LeftTurn_SwitchState_u8 = DInpSignal_aSt[7].SignalState_u8;
			UserInputSig_St.RightTurn_SwitchState_u8= DInpSignal_aSt[8].SignalState_u8;
			UserInputSig_St.Brake_SwitchState_Front_u8 = DInpSignal_aSt[9].SignalState_u8;
			UserInputSig_St.KickStand_SwitchState_u8 = DInpSignal_aSt[10].SignalState_u8;
			UserInputSig_St.Hazard_SwitchState_u8 = DInpSignal_aSt[11].SignalState_u8;
			//GpioInp_aSt[NEUTRAL_PB_E].AutoFlag_b = 1;
            BuzzerCheck(); 
		
		UserInputSig_St.RightTurn_FeedBack_u16 = GetAdcResult(11); 
		UserInputSig_St.LeftTurn_FeedBack_u16 = GetAdcResult(12);
		DOutputSig_aSt[DO_1_E].PresentState_u8 = VcuOutputs_St.MotorKill_u8;
		DOutputSig_aSt[DO_2_E].PresentState_u8 = VcuOutputs_St.TorqueZero_u8;
		DOutputSig_aSt[DO_3_E].PresentState_u8 = VcuOutputs_St.buzzer_u8;
		DOutputSig_aSt[DO_4_E].PresentState_u8 = 0 ;
		DOutputSig_aSt[DO_5_E].PresentState_u8 = 0 ;
		DOutputSig_aSt[DO_6_E].PresentState_u8 = 0 ;
		DOutputSig_aSt[DO_7_E].PresentState_u8 = VcuOutputs_St.RightIndLight_u8;
		DOutputSig_aSt[DO_8_E].PresentState_u8 = VcuOutputs_St.LeftIndLight_u8;

		 /*******************************************time setting******************************************/
		 UserInputSig_St.TimeSett_inc_u8 = AlternateFunction(0);
		 UserInputSig_St. TimeSett_dec_u8  = AlternateFunction(1);
		 UserInputSig_St.TimeSett_Sel_u8 = AlternateFunction(5);
	return;
}

uint8_t AlternateFunction(uint8_t input)
{
	uint8_t returnValue = TURNED_OFF_E;
	if((input == 0) && (UserInputSig_St.Neutral_Long_press_u8 == false))
	{
		if(DInpSignal_aSt[0].SignalState_u8 == TURNED_ON_E)
	 	{
			Debounce_Counter_u2[0] += 5;
			if (Debounce_Counter_u2[0] > 3000)
			{
				DInpSignal_aSt[0].LongPress_b = true;
				UserInputSig_St.Neutral_Long_press_u8 = true;
				AltFunc_St[0].Autoinc_b = true;
			}
	   }
	   else if(DInpSignal_aSt[0].SignalState_u8 == TURNED_OFF_E)
	   {
			Debounce_Counter_u2[0] = 0;
	   }
	}
	else if(UserInputSig_St.Neutral_Long_press_u8 == true)
	{
	   AltFunc_St[input].Autdecounter_u32 += 5;
	   if ((AltFunc_St[input].Autdecounter_u32 >= 500) && (DInpSignal_aSt[input].SignalState_u8 == TURNED_ON_E))
	   {
			AltFunc_St[input].Autoinc_u8 = true;
			AltFunc_St[input].Autdecounter_u32 = 0;
			returnValue = TURNED_ON_E;
	   }
	}

	return returnValue;
}



/********************************************************EOF***********************************************************/