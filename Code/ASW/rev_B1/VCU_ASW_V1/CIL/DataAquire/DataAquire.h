/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 09/03/2022
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include"pal_gpio_conf.h"
#include"pal_adc_conf.h"
#include "communicator.h"
//#include "DataBank.h"
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/


#define MIN_ADC_VALUE_FOR_TURN_ON					1000
#define MIN_ADC_VALUE_FOR_TURN_OFF					100
#define SW_LONG_PRESS                               3000U

#define GPIO_PIN_SET							1


#define LONG_PRESS_TIME     (3000U)




/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	GPIO_INP_OFF_HOLD_E,
	GPIO_INP_ON_HOLD_E,
	GPIO_INP_OFF_E,
	GPIO_INP_ON_E,
}GpioInputState_En_t;  


typedef enum
{
	INPUT_SIG_START_E,
    /* adc pin*/
	KILL_SWITCH_E = INPUT_SIG_START_E,
	LEFT_TURN_SWITCH_E,
	EXTRA_1_E,
	REGEN_SWITCH_E,
	KICK_STAND_SWITCH_E,
	BRAKE_SWITCH_E,
	RIGHT_TURN_SWITCH_E,
	DIAG_FEEDBACK_1_E,
	DIAG_FEEDBACK_2_E,
	DIAG_FEEDBACK_3_E,
	HED_LIGHT_HIGH_E,
	RIGHT_TURN_FEEDBACK_E ,
	LEFT_TURN_FEEDBACK_E ,
	/*GPIO*/
	
	/*interrupt pins */
    MODE_SELECT_NEUTRAL_E,
    MODE_SELECT_ECO_E,
    MODE_SELECT_SPORTS_E,
    MODE_SELECT_REVERSE_E,
	
	/* PWM pins*/
	EXTRA_2_E,
	INPUT_SIG_END_E,
	TOTAL_USER_INPUT_SIGNALS_E = INPUT_SIG_END_E,
	SOC_SWITCH
}UserInputSig_En_t;




typedef enum
{
	ADC_E,
	GPIO_E,
	EXT_INT_E,
	PWM_E
}InputChannel_En_t;







typedef enum
{
	REVERSE_MODE_E = 0,
	NEUTRAL_MODE_E,
	ECONOMY_MODE_E,
	SPORTS_MODE_E,
	SAFE_MODE_E // todo Manjesh check
}DrivingModes_En_t;

typedef struct 
{
	bool Neutral_TurnOn_b;
	bool Neutral_Pressed_First_b;
	uint32_t Timer_u32;
}Date_update_st_t;

typedef enum
{
	BUTTON_RELEASED_E,
	BUTTON_PRESSED_E,
}UserButtonState_En_t;

typedef enum
{
	UIB_REGEN_SWITCH_E,
	UIB_NEUTRAL_SWITCH_E,
	TOTAL_UIB ,
}UIB_Multi_En_t;


typedef enum
{
	GPIO_PIN_RESET_E,
	GPIO_PIN_SET_E,
}GPIO_PinState_En_t;


typedef struct 
{
	UserButtonState_En_t UserButtonState_En;
	uint16_t adc_data;
	uint32_t TimerCaptureValue_u32;
	bool LongPressed_b;
}UserButton_St_t;


#pragma pack(1)
typedef struct 
{
    uint16_t                DebounceCount_u16;
    uint8_t             	SignalState_u8;
	uint8_t                 AutoInc_u8;
	bool                    AutoFlag_b;
    bool					LongPress_b;
    GpioInputState_En_t 	InpState_En;
    GPIO_PinState_En_t  	InpVal_En;
}GpioInp_St_t;

/**********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
void Set_Drive_Mode_Counters(UserInputSig_En_t);
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En);
uint8_t Kill_switch_state(void);
uint8_t Kick_stand_state(void);
uint8_t Torque_zero_state(void);
uint8_t Motor_kill_state(void);
uint8_t Get_hmi_state(void);

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/

/***********************************************************************************************************************/
 //new architecture
 /***********************************************************************************************************************/
typedef enum 
{
	NEUTRAL_PB_E,
	ECO_PB_E,
	REVERSE_PB_E,
	REGEN_PB_E,
	TOTAL_PB_E,
}PushButton_En_t;



#pragma pack(1)
typedef struct 
{
    uint8_t PresentState_u8;
    uint8_t PreviousState_u8;
}DOutputSignal_St_t;


#pragma pack(1)
typedef struct
{
    uint16_t DebounceCount_u16;
    uint16_t PresentAdc_u16;
    uint16_t PrevAdc_u16;
    uint16_t ActualValue_u16;
    bool StartDebounce_b;
}AdcInpSignal_St_t;

 #pragma pack(1)
typedef struct 
{
    uint16_t            DebounceCount_u16;
    uint8_t             SignalState_u8;
    bool				LongPress_b;
    GpioInputState_En_t InpState_En;
    uint8_t             ReadValue_u8;
}DInpSignal_St_t;


extern void ReadUserInput(void);



extern DInpSignal_St_t DInpSignal_aSt[];
extern DOutputSignal_St_t DOutputSig_aSt[TOTAL_DO_E];
extern GpioInp_St_t GpioInp_aSt[TOTAL_PB_E];
void UpdatePushButtonState( GpioInp_St_t *const VcuPushButtonInp_pst);
extern void WriteOutputSignals(void);
extern void ReadInputSignal(void);
extern uint32_t Differenceof_2_num(uint32_t Digit1,uint32_t Digit2);

extern void DataAquireInit(void);
#endif /* DATA_AQUIRE_H */
