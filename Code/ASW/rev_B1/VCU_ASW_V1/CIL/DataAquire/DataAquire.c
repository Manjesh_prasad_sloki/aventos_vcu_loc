
#include"app_typedef.h"
#include "DataAquire.h"
#include "DataBank.h"
#include"pal_gpio.h"
#include"pal_gpio_conf.h"
#include"data_acquire_conf.h"

#include "Config_PORT.h"
#include "Config_ADCA0.h"
#include "adc_user.h"
#include "intc_user.h"
//#include "can_if.h"
#include "cil_can_conf.h"
#include "Config_RTCA0.h"
#include "Config_STBC.h"






/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


Date_update_st_t Date_update_st;

#define REGEN_ON    (1)
#define REGEN_OFF    (0)

#define PIN_READ_TIME 5


#define DEBOUNCE_COUNT                  (PIN_READ_TIME * 10) // 50 millisec
#define LONG_PRESS_THRESHOLD          (1000) // (PIN_READ_TIME * 300) // 3000 millsec
#define ADC_NOISE_THRESHOLD             (100)
#define ADC_THRESHOLD                   (1000)


#define AUTO_PRESS_THRESHOLD            (400 )


UserButton_St_t UserButton_St[TOTAL_UIB] = 
{
	{BUTTON_PRESSED_E,0,false},
	{BUTTON_RELEASED_E,0,false},
};






 DrivingModes_En_t DrivingModes_En = NEUTRAL_MODE_E;



 

 void BuzzerCheck(void);
 
 /***********************************************************************************************************************/
 //new architecture
 /***********************************************************************************************************************/

DInpSignal_St_t DInpSignal_aSt[TOTAL_DI_E];
uint8_t  DigiInpLong_press[4];
DOutputSignal_St_t DOutputSig_aSt[TOTAL_DO_E];
AdcInpSignal_St_t AdcInpSignal_aSt[TOTAL_ANI_E];
/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/
static void SwitchDebounce(DInpSignal_St_t*const VcuSwitchInp_pSt);

static void AdcDebounce(AdcInpSignal_St_t*const AdcInp_pSt); 
 
 GpioInp_St_t GpioInp_aSt[TOTAL_PB_E] = 
{
    {0,TURN_OFF,false,TURN_OFF,false,GPIO_INP_OFF_E,GPIO_PIN_RESET_E},
    {0,TURN_OFF,false,TURN_OFF,false,GPIO_INP_OFF_E,GPIO_PIN_RESET_E},
    {RESET,TURN_OFF,TURN_OFF,false,false,GPIO_INP_OFF_E,GPIO_PIN_RESET_E},
	{RESET,TURN_OFF,TURN_OFF,false,false,GPIO_INP_OFF_E,GPIO_PIN_RESET_E},
};


/***********************************************************************************************************************
* Function Name: WriteOutputs
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/	

void BuzzerCheck(void)
{
	uint8_t index = 0; 
	for(index = BUZZ_START; index < TOATAL_BUZZ_FUN; index++)
	{
		if(ACTIVATE == Buzzer_Functionality_St[index].BuzzStatus_En )
		{
			NOP();	
			break;
			
		}
		
	}
	
	if(ACTIVATE == Buzzer_Functionality_St[index].BuzzStatus_En )
	{
	     if(true == Buzzer_Functionality_St[index].TurnON_Time_b)
		 {
                        VcuOutputs_St.buzzer_u8 = TURNED_ON_E;
                        Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = true;
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = false;
		 }
		 else if(true == Buzzer_Functionality_St[index].TurnOFF_Time_b)
		 {
                        VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnON_Time_b = false;
			Buzzer_Functionality_St[BUZZ_INDICATOR_E].TurnOFF_Time_b = true;
		 }
	}
	else if(DEACTIVATE == Buzzer_Functionality_St[index].BuzzStatus_En )
	{
		VcuOutputs_St.buzzer_u8 = TURNED_OFF_E;
		Buzzer_Functionality_St[index].TurnON_Time_b = false;
		Buzzer_Functionality_St[index].TurnOFF_Time_b = false;
	}
	return;
}




/***********************************************************************************************************************
* Function Name: Clear_Drive_Mode
* Description  : 
* Arguments    : DrivingModes_En_t DrivingModes_En
* Return Value : None
***********************************************************************************************************************/
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En)
{
	switch (DrivingModes_En)
	{
	case NEUTRAL_MODE_E:
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		break;
	}
	case ECONOMY_MODE_E:
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		break;
	}
	case SPORTS_MODE_E:
	{
		
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
		break;
	}
	case REVERSE_MODE_E:
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;
		break;
	}
	// case SAFE_MODE_E: // todo Manjesh Check
	// {
	// 	UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
	// 	UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
	// 	UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
	// 	UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
	// 	//UserInputSig_St.Safe_Mode_Sel_u8 = TURNED_ON_E;
	// 	break;
	// }
	
	default:
	{
		break;
	}
	}
}





uint8_t Kill_switch_state(void)
{
   return UserInputSig_St.Kill_SwitchState_u8;
}

uint8_t Kick_stand_state(void)
{
	return UserInputSig_St.KickStand_SwitchState_u8;
}

uint8_t Torque_zero_state(void)
{
	return VcuOutputs_St.TorqueZero_u8; 
}

uint8_t Motor_kill_state(void)
{
	if(PORT.P10 & (_PORT_Pn4_OUTPUT_HIGH))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t Get_hmi_state(void)
{
	uint8_t temp =0;
	temp = hmi_switch_state();
	return temp;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : WriteOutputSignals
*   Description   : The function writes the vcu output signals
*   Parameters    : AdcInp_pSt:- ADC input structure
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void WriteOutputSignals(void)
{
    uint8_t OutSig_u8 = 0;
    for(OutSig_u8 = DO_1_E; OutSig_u8 < TOTAL_DO_E; OutSig_u8++)
    {
        if(DOutputSig_aSt[OutSig_u8].PresentState_u8 != DOutputSig_aSt[OutSig_u8].PreviousState_u8)
        {
		if(OutSig_u8 == DO_3_E)
		{
           		 NOP();
		}
            WriteDigitalOutput(OutSig_u8,DOutputSig_aSt[OutSig_u8].PresentState_u8);
            DOutputSig_aSt[OutSig_u8].PreviousState_u8 = DOutputSig_aSt[OutSig_u8].PresentState_u8;
        }
    }
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ReadInputSignal
*   Description   : The function reads the VCU input signal. 
*   Parameters    : None
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void ReadInputSignal(void)
{
    uint8_t InpSig_u8 = 0;

    for(InpSig_u8 = DI_1_E; InpSig_u8 < TOTAL_DI_E; InpSig_u8++)
    {
        /* Read and update the digital input signals */
        DInpSignal_aSt[InpSig_u8].ReadValue_u8 = ReadDigitalInput(InpSig_u8);
        SwitchDebounce(&DInpSignal_aSt[InpSig_u8]); 

    }
    ReadAdcInput();
    for(InpSig_u8 = ANI_START_E; InpSig_u8 < TOTAL_ANI_E; InpSig_u8++)
    {
        /* Read and update the analog signals */
        AdcInpSignal_aSt[InpSig_u8].PresentAdc_u16 = GetAdcResult(InpSig_u8);
        AdcDebounce(&AdcInpSignal_aSt[InpSig_u8]);
    }

    return;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SwitchDebounce
*   Description   : The function update switch type input
*   Parameters    : VcuSwitchInp_pSt:- Gpio input structure
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void SwitchDebounce(DInpSignal_St_t*const VcuSwitchInp_pSt)
{
    if(GPIO_PIN_SET_E == VcuSwitchInp_pSt->ReadValue_u8)
    {
        /* GPIO input is Turned On*/
        if(GPIO_INP_OFF_E == VcuSwitchInp_pSt->InpState_En)
        {
            /* Hold the GPIO state and capture the time */
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_ON_HOLD_E;
            VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else
        {
            ;
        }

        if((GPIO_INP_ON_HOLD_E == VcuSwitchInp_pSt->InpState_En) && 
            (DEBOUNCE_COUNT <= VcuSwitchInp_pSt->DebounceCount_u16) )
        {
            /* If GPIO state is ON more than Debounce count time, Change the state to ON*/
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_ON_E;
            VcuSwitchInp_pSt->SignalState_u8 = TURNED_ON_E;
            VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else if((GPIO_INP_ON_HOLD_E == VcuSwitchInp_pSt->InpState_En) &&
            ( DEBOUNCE_COUNT > VcuSwitchInp_pSt->DebounceCount_u16))
        {
            /* If GPIO state is ON, but it is less than Debounce count time, then update the Hold time*/
           VcuSwitchInp_pSt->DebounceCount_u16 += PIN_READ_TIME;
        }
        else if(GPIO_INP_OFF_HOLD_E == VcuSwitchInp_pSt->InpState_En)
        {
            /* The GPIO state is reseted within the debounce time*/
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_ON_E;
            VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else
        {
            ;
        }
    }
    else if(GPIO_PIN_RESET_E == VcuSwitchInp_pSt->ReadValue_u8)
    {
        /* GPIO input is Turned On*/
        if(GPIO_INP_ON_E == VcuSwitchInp_pSt->InpState_En)
        {
            /* Hold the GPIO state and capture the time */
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_OFF_HOLD_E;
             VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else
        {
            ;
        }

        if((GPIO_INP_OFF_HOLD_E == VcuSwitchInp_pSt->InpState_En) && 
            (DEBOUNCE_COUNT <= VcuSwitchInp_pSt->DebounceCount_u16) )
        {
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_OFF_E;
            VcuSwitchInp_pSt->SignalState_u8 = TURN_OFF;
            VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else if((GPIO_INP_OFF_HOLD_E == VcuSwitchInp_pSt->InpState_En) &&
            ( DEBOUNCE_COUNT > VcuSwitchInp_pSt->DebounceCount_u16))
        {
            VcuSwitchInp_pSt->DebounceCount_u16 += PIN_READ_TIME;
        }
        else if(GPIO_INP_ON_HOLD_E == VcuSwitchInp_pSt->InpState_En)
        {
            /* The GPIO state is reseted within the debounce time*/
            VcuSwitchInp_pSt->InpState_En = GPIO_INP_OFF_E;
            VcuSwitchInp_pSt->DebounceCount_u16 = CLEAR;
        }
        else
        {
            ;
        }
    }
    else
    {

    }
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : AdcDebounce
*   Description   : The function debounce the ADC value
*   Parameters    : AdcInp_pSt:- ADC input structure
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void AdcDebounce(AdcInpSignal_St_t*const AdcInp_pSt)
{
    if((Differenceof_2_num(AdcInp_pSt->PresentAdc_u16,AdcInp_pSt->PrevAdc_u16)) > ADC_NOISE_THRESHOLD)
    {
        /* Change in the ADC value more than the ADC_NOISE_THRESHOLD
        Restart the debounce */
        AdcInp_pSt->StartDebounce_b = true;
        AdcInp_pSt->DebounceCount_u16 = RESET;
        AdcInp_pSt->PrevAdc_u16 = AdcInp_pSt->PresentAdc_u16;
    }

    if(true == AdcInp_pSt->StartDebounce_b)
    {
        if(AdcInp_pSt->DebounceCount_u16 < DEBOUNCE_COUNT)
        {
            /* Increment the debounce */
            AdcInp_pSt->DebounceCount_u16 += PIN_READ_TIME;
        }
        else
        {
            /* ADC value is the actual value(not a noise) */
            AdcInp_pSt->StartDebounce_b = false;
            AdcInp_pSt->ActualValue_u16 =  AdcInp_pSt->PresentAdc_u16;
            AdcInp_pSt->PrevAdc_u16 = AdcInp_pSt->PresentAdc_u16;
        }  
    }

    return;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
* Function Name: Differenceof_2_num
* Description  : The function provides difference of 2 numbers.
* Arguments    : Digit1_u32 :- Digit1
                 Digit2_u32 :- Digit2
* Return Value : Difference_u32 :- difference of two number.
*  ---------------------------------------------------------------------------*/
uint32_t Differenceof_2_num(uint32_t Digit1_u32,uint32_t Digit2_u32)
{
	uint32_t Difference_u32;
	if(Digit1_u32 > Digit2_u32)
	{
		Difference_u32 = Digit1_u32 - Digit2_u32;
	}
	else
	{
		Difference_u32 = Digit2_u32 - Digit1_u32;

	}
	return Difference_u32;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DataAquireInit
*   Description   : The function initialize the Analog and Digital input structure. 
*   Parameters    : None
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void DataAquireInit(void)
{
    uint8_t InpSig_u8 = 0;
    
    for(InpSig_u8 = DI_1_E; InpSig_u8 < TOTAL_DI_E; InpSig_u8++)
    {
        /* Initialize the digital Input structure */
        DInpSignal_aSt[InpSig_u8].ReadValue_u8 = GPIO_PIN_RESET_E;
        DInpSignal_aSt[InpSig_u8].InpState_En = GPIO_INP_OFF_E;
        DInpSignal_aSt[InpSig_u8].SignalState_u8 = TURN_OFF;
        DInpSignal_aSt[InpSig_u8].LongPress_b = false;
        DInpSignal_aSt[InpSig_u8].DebounceCount_u16 = RESET;
    }
    return;
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : UpdatePushButtonState
*   Description   : The function update push button type input state
*   Parameters    : VcuPushButtonInp_pst :-  Gpio input structure
*   Return Value  : None
*  -----------------------------------------------------------------------------*/
void UpdatePushButtonState( GpioInp_St_t *const VcuPushButtonInp_pst)
{
   if(GPIO_PIN_SET_E == VcuPushButtonInp_pst->InpVal_En)
    {
        /* GPIO input is Turned On*/
        if(GPIO_INP_OFF_E == VcuPushButtonInp_pst->InpState_En)
        {
            /* Change state to ON (Push button) */
            VcuPushButtonInp_pst->InpState_En = GPIO_INP_ON_HOLD_E;
            VcuPushButtonInp_pst->DebounceCount_u16 = CLEAR;
        }
        else if(GPIO_INP_ON_HOLD_E == VcuPushButtonInp_pst->InpState_En)
        {
            /* If the GPIO state is ON, update the Hold time */
            VcuPushButtonInp_pst->DebounceCount_u16 += PIN_READ_TIME;
        }
        else
        {
            ;
        }
		if( true == GpioInp_aSt[NEUTRAL_PB_E].AutoFlag_b)
		{
			VcuPushButtonInp_pst->DebounceCount_u16 += PIN_READ_TIME;
			 if(( VcuPushButtonInp_pst->DebounceCount_u16 >= AUTO_PRESS_THRESHOLD))
			{
				if(VcuPushButtonInp_pst->AutoInc_u8 == TURN_ON)
				{
					VcuPushButtonInp_pst->AutoInc_u8 = TURN_OFF;
					VcuPushButtonInp_pst->DebounceCount_u16 = 0;
				}
				else
				{
 					VcuPushButtonInp_pst->AutoInc_u8 = TURN_ON;
					VcuPushButtonInp_pst->DebounceCount_u16 = 0;
				}

			}
		}



        if(( GPIO_INP_ON_HOLD_E == VcuPushButtonInp_pst->InpState_En)&&
            ( VcuPushButtonInp_pst->DebounceCount_u16 >= LONG_PRESS_THRESHOLD))
        {
            VcuPushButtonInp_pst->LongPress_b = true;
            VcuPushButtonInp_pst->InpState_En = GPIO_INP_ON_E;
            VcuPushButtonInp_pst->DebounceCount_u16 = CLEAR;

        }
        else
        {
            ;
        }
		if(true == GpioInp_aSt[NEUTRAL_PB_E].AutoFlag_b)
		{

		}
    }
    else if (GPIO_PIN_RESET_E == VcuPushButtonInp_pst->InpVal_En)
    {
        if(GPIO_INP_ON_E == VcuPushButtonInp_pst->InpState_En)
        {
           VcuPushButtonInp_pst->InpState_En = GPIO_INP_OFF_E;
		   VcuPushButtonInp_pst->LongPress_b = false;
        }
        else if(GPIO_INP_ON_HOLD_E == VcuPushButtonInp_pst->InpState_En)
        {
            if( (false == VcuPushButtonInp_pst->LongPress_b) && 
                (DEBOUNCE_COUNT <= VcuPushButtonInp_pst->DebounceCount_u16))
            {
                /* if the button is not long pressed and holded for more than debounce time.*/
                VcuPushButtonInp_pst->SignalState_u8 = TURNED_ON_E;
                VcuPushButtonInp_pst->InpState_En = GPIO_INP_OFF_HOLD_E;
            }
            else
            {
                VcuPushButtonInp_pst->InpState_En = GPIO_INP_OFF_E;
            }
            VcuPushButtonInp_pst->DebounceCount_u16 = CLEAR;
	    NOP();
        }
        else if(GPIO_INP_OFF_HOLD_E == VcuPushButtonInp_pst->InpState_En)
        {
            /* Wait for VCU to read the Pin state */
            if(DEBOUNCE_COUNT <= VcuPushButtonInp_pst->DebounceCount_u16)
            {
                VcuPushButtonInp_pst->InpState_En =  GPIO_INP_OFF_E;
                VcuPushButtonInp_pst->SignalState_u8 = TURN_OFF;
            }
            else
            {
                VcuPushButtonInp_pst->DebounceCount_u16 += PIN_READ_TIME; 
            }
        }
        else 
        {
            ;
        }
    }
    if( VcuPushButtonInp_pst->DebounceCount_u16 >= 400)
    {
	    NOP();
    }
    if( VcuPushButtonInp_pst->DebounceCount_u16 >= 500)
    {
        NOP();
    }
    return;
}