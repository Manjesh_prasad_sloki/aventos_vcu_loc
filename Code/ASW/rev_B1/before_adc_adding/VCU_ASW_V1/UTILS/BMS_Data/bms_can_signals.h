﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms_can_signals.h
|    Project        : VCU
|    Module         : BMS can signals data
|    Description    : This is the generated file from the DBC2CH Tool for BMS.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 08/07/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_CAN_SIGNALS_H
#define BMS_CAN_SIGNALS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include <stdint.h>
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define OLD_BMS_CM FALSE
#define NEW_BMS_CM TRUE
#define TPD02_FULL_CAP_MASK0  8U
#define TPD03_BATTERY_CURRENT_MASK0  8U
#define TPD03_BATTERY_VOLTAGE_MASK0  8U
#define TPD03_MAX_DCHG_CURRENT_MASK0  8U




#define TPD04_HIGH_MODULE_TEMP_MASK0  8U


#define TPD08__1_CYCLE_CHARGE_CAPACITY_MASK0  8U
#define TPD08__2_CYCLE_DISCHARGE_CAPACITY_MASK0  8U
#define TPD08__3_AVAILABLE_CAPACITY_MASK0  8U
#define TPD08__4_AVAILABLE_ENERGY_MASK0  8U


#define TPD09__1_EQUIVALENT_CYCLE_COUNT_MASK0  8U
#define TPD09__2_LIFETIME_CHARGE_CAPACITY_MASK0  16U
#define TPD09__2_LIFETIME_CHARGE_CAPACITY_MASK1  8U
#define TPD09__3_LIFETIME_DISCHARGE_CAPACITY_MASK0  16U
#define TPD09__3_LIFETIME_DISCHARGE_CAPACITY_MASK1  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @TPD02 CAN Message                                   (682) */
#define TPD02_ID                                            (682U)
#define TPD02_IDE                                           (0U)
#define TPD02_DLC                                           (8U)


#define TPD02_SOCFACTOR                                   (1)
#define TPD02_CANID_SOC_STARTBIT                           (0)
#define TPD02_CANID_SOC_OFFSET                             (0)
#define TPD02_CANID_SOC_MIN                                (0)
#define TPD02_CANID_SOC_MAX                                (100)
#define TPD02_FULL_CAPFACTOR                                   (0.125)
#define TPD02_CANID_FULL_CAP_STARTBIT                           (8)
#define TPD02_CANID_FULL_CAP_OFFSET                             (0)
#define TPD02_CANID_FULL_CAP_MIN                                (0)
#define TPD02_CANID_FULL_CAP_MAX                                (8191.88)
#define TPD02_SOHFACTOR                                   (1)
#define TPD02_CANID_SOH_STARTBIT                           (56)
#define TPD02_CANID_SOH_OFFSET                             (0)
#define TPD02_CANID_SOH_MIN                                (0)
#define TPD02_CANID_SOH_MAX                                (255)

#pragma pack (1)
typedef struct
{
  uint8_t SoC;
  uint16_t Full_CAP;
  uint8_t SoH;
}
BMS_VCU_0x2AA_Rx_St_t;


/* def @TPD03 CAN Message                                   (938) */
#define TPD03_ID                                            (938U)
#define TPD03_IDE                                           (0U)
#define TPD03_DLC                                           (8U)


#define TPD03_BATTERY_CURRENTFACTOR                                   (0.0625)
#define TPD03_CANID_BATTERY_CURRENT_STARTBIT                           (0)
#define TPD03_CANID_BATTERY_CURRENT_OFFSET                             (0)
#define TPD03_CANID_BATTERY_CURRENT_MIN                                (-2048)
#define TPD03_CANID_BATTERY_CURRENT_MAX                                (2047.94)
#define TPD03_BATTERY_VOLTAGEFACTOR                                   (0.000976)
#define TPD03_CANID_BATTERY_VOLTAGE_STARTBIT                           (16)
#define TPD03_CANID_BATTERY_VOLTAGE_OFFSET                             (0)
#define TPD03_CANID_BATTERY_VOLTAGE_MIN                                (0)
#define TPD03_CANID_BATTERY_VOLTAGE_MAX                                (63.9622)
#define TPD03_MAX_DCHG_CURRENTFACTOR                                   (0.0625)
#define TPD03_CANID_MAX_DCHG_CURRENT_STARTBIT                           (32)
#define TPD03_CANID_MAX_DCHG_CURRENT_OFFSET                             (0)
#define TPD03_CANID_MAX_DCHG_CURRENT_MIN                                (-2048)
#define TPD03_CANID_MAX_DCHG_CURRENT_MAX                                (2047.94)
#define TPD03_SOPFACTOR                                   (1)
#define TPD03_CANID_SOP_STARTBIT                           (48)
#define TPD03_CANID_SOP_OFFSET                             (0)
#define TPD03_CANID_SOP_MIN                                (0)
#define TPD03_CANID_SOP_MAX                                (255)
#define TPD03_BATTERY_STATEFACTOR                                   (1)
#define TPD03_CANID_BATTERY_STATE_STARTBIT                           (56)
#define TPD03_CANID_BATTERY_STATE_OFFSET                             (0)
#define TPD03_CANID_BATTERY_STATE_MIN                                (0)
#define TPD03_CANID_BATTERY_STATE_MAX                                (5)

#pragma pack (1)
typedef struct
{
  int16_t Battery_Current;
  uint16_t Battery_Voltage;
  int16_t Max_DCHG_Current;
  uint8_t SOP;
  uint8_t Battery_State;
}
BMS_VCU_0x3AA_Rx_St_t;


/* def @TPDO7 CAN Message                                   (939) */
#define TPDO7_ID                                            (939U)
#define TPDO7_IDE                                           (0U)
#define TPDO7_DLC                                           (8U)


#define TPDO7_BMSTEMPERATUREFACTOR                                   (1)
#define TPDO7_CANID_BMSTEMPERATURE_STARTBIT                           (0)
#define TPDO7_CANID_BMSTEMPERATURE_OFFSET                             (0)
#define TPDO7_CANID_BMSTEMPERATURE_MIN                                (0)
#define TPDO7_CANID_BMSTEMPERATURE_MAX                                (127)
#define TPDO7_PDUTEMPERATUREFACTOR                                   (1)
#define TPDO7_CANID_PDUTEMPERATURE_STARTBIT                           (8)
#define TPDO7_CANID_PDUTEMPERATURE_OFFSET                             (0)
#define TPDO7_CANID_PDUTEMPERATURE_MIN                                (0)
#define TPDO7_CANID_PDUTEMPERATURE_MAX                                (127)

#pragma pack (1)
typedef struct
{
  int8_t BMSTemperature;
  int8_t PDUTemperature;
}
BMS_VCU_0x3AB_Rx_St_t;



#if (OLD_BMS_CM == TRUE)
/* def @TPD04 CAN Message                                   (1194) */
#define TPD04_ID                                            (1194U)
#define TPD04_IDE                                           (0U)
#define TPD04_DLC                                           (8U)


#define TPD04_HIGH_MODULE_TEMPFACTOR                                   (0.125)
#define TPD04_CANID_HIGH_MODULE_TEMP_STARTBIT                           (48)
#define TPD04_CANID_HIGH_MODULE_TEMP_OFFSET                             (0)
#define TPD04_CANID_HIGH_MODULE_TEMP_MIN                                (-4096)
#define TPD04_CANID_HIGH_MODULE_TEMP_MAX                                (4095.88)

#pragma pack (1)
typedef struct
{
  int16_t High_Module_Temp;
} 
BMS_VCU_0x4AA_Rx_St_t;
#endif

#if (NEW_BMS_CM == TRUE)
#define TPDO4_LOW_CELL_VOLT_MASK0  8U
#define TPDO4_HIGH_CELL_VOLT_MASK0  8U
#define TPDO4_HIGH_CV_INDEX_MASK0  3U
#define TPDO4_LOW_CV_INDEX_MASK0  3U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @TPDO4 CAN Message                                   (1194) */
#define TPDO4_ID                                            (1194U)
#define TPDO4_IDE                                           (0U)
#define TPDO4_DLC                                           (8U)


#define TPDO4_LOW_CELL_VOLTFACTOR                                   (0.000977)
#define TPDO4_CANID_LOW_CELL_VOLT_STARTBIT                           (0)
#define TPDO4_CANID_LOW_CELL_VOLT_OFFSET                             (0)
#define TPDO4_CANID_LOW_CELL_VOLT_MIN                                (0)
#define TPDO4_CANID_LOW_CELL_VOLT_MAX                                (64.0277)
#define TPDO4_HIGH_CELL_VOLTFACTOR                                   (0.000977)
#define TPDO4_CANID_HIGH_CELL_VOLT_STARTBIT                           (16)
#define TPDO4_CANID_HIGH_CELL_VOLT_OFFSET                             (0)
#define TPDO4_CANID_HIGH_CELL_VOLT_MIN                                (0)
#define TPDO4_CANID_HIGH_CELL_VOLT_MAX                                (64.0277)
#define TPDO4_LOW_CELL_TEMPFACTOR                                   (1)
#define TPDO4_CANID_LOW_CELL_TEMP_STARTBIT                           (32)
#define TPDO4_CANID_LOW_CELL_TEMP_OFFSET                             (-30)
#define TPDO4_CANID_LOW_CELL_TEMP_MIN                                (-30)
#define TPDO4_CANID_LOW_CELL_TEMP_MAX                                (225)
#define TPDO4_HIGH_CELL_TEMPFACTOR                                   (1)
#define TPDO4_CANID_HIGH_CELL_TEMP_STARTBIT                           (40)
#define TPDO4_CANID_HIGH_CELL_TEMP_OFFSET                             (-30)
#define TPDO4_CANID_HIGH_CELL_TEMP_MIN                                (-30)
#define TPDO4_CANID_HIGH_CELL_TEMP_MAX                                (225)
#define TPDO4_HIGH_CT_INDEXFACTOR                                   (1)
#define TPDO4_CANID_HIGH_CT_INDEX_STARTBIT                           (48)
#define TPDO4_CANID_HIGH_CT_INDEX_OFFSET                             (0)
#define TPDO4_CANID_HIGH_CT_INDEX_MIN                                (0)
#define TPDO4_CANID_HIGH_CT_INDEX_MAX                                (7)
#define TPDO4_HIGH_CV_INDEXFACTOR                                   (1)
#define TPDO4_CANID_HIGH_CV_INDEX_STARTBIT                           (51)
#define TPDO4_CANID_HIGH_CV_INDEX_OFFSET                             (0)
#define TPDO4_CANID_HIGH_CV_INDEX_MIN                                (0)
#define TPDO4_CANID_HIGH_CV_INDEX_MAX                                (31)
#define TPDO4_LOW_CT_INDEXFACTOR                                   (1)
#define TPDO4_CANID_LOW_CT_INDEX_STARTBIT                           (56)
#define TPDO4_CANID_LOW_CT_INDEX_OFFSET                             (0)
#define TPDO4_CANID_LOW_CT_INDEX_MIN                                (0)
#define TPDO4_CANID_LOW_CT_INDEX_MAX                                (7)
#define TPDO4_LOW_CV_INDEXFACTOR                                   (1)
#define TPDO4_CANID_LOW_CV_INDEX_STARTBIT                           (59)
#define TPDO4_CANID_LOW_CV_INDEX_OFFSET                             (0)
#define TPDO4_CANID_LOW_CV_INDEX_MIN                                (0)
#define TPDO4_CANID_LOW_CV_INDEX_MAX                                (31)


typedef struct
{
  uint16_t LOW_CELL_VOLT;
  uint16_t HIGH_CELL_VOLT;
  int16_t LOW_CELL_TEMP;
  int16_t HIGH_CELL_TEMP;
  uint8_t HIGH_CT_INDEX;
  uint8_t HIGH_CV_INDEX;
  uint8_t LOW_CT_INDEX;
  uint8_t LOW_CV_INDEX;
}
TPDO4_t;

#endif





/* def @TPD08 CAN Message                                   (1195) */
#define TPD08_ID                                            (1195U)
#define TPD08_IDE                                           (0U)
#define TPD08_DLC                                           (8U)


#define TPD08__1_CYCLE_CHARGE_CAPACITYFACTOR                                   (0.125)
#define TPD08_CANID__1_CYCLE_CHARGE_CAPACITY_STARTBIT                           (0)
#define TPD08_CANID__1_CYCLE_CHARGE_CAPACITY_OFFSET                             (0)
#define TPD08_CANID__1_CYCLE_CHARGE_CAPACITY_MIN                                (0)
#define TPD08_CANID__1_CYCLE_CHARGE_CAPACITY_MAX                                (8191.88)
#define TPD08__2_CYCLE_DISCHARGE_CAPACITYFACTOR                                   (0.125)
#define TPD08_CANID__2_CYCLE_DISCHARGE_CAPACITY_STARTBIT                           (16)
#define TPD08_CANID__2_CYCLE_DISCHARGE_CAPACITY_OFFSET                             (0)
#define TPD08_CANID__2_CYCLE_DISCHARGE_CAPACITY_MIN                                (0)
#define TPD08_CANID__2_CYCLE_DISCHARGE_CAPACITY_MAX                                (8191.88)
#define TPD08__3_AVAILABLE_CAPACITYFACTOR                                   (0.125)
#define TPD08_CANID__3_AVAILABLE_CAPACITY_STARTBIT                           (32)
#define TPD08_CANID__3_AVAILABLE_CAPACITY_OFFSET                             (0)
#define TPD08_CANID__3_AVAILABLE_CAPACITY_MIN                                (0)
#define TPD08_CANID__3_AVAILABLE_CAPACITY_MAX                                (8191.88)
#define TPD08__4_AVAILABLE_ENERGYFACTOR                                   (0.125)
#define TPD08_CANID__4_AVAILABLE_ENERGY_STARTBIT                           (48)
#define TPD08_CANID__4_AVAILABLE_ENERGY_OFFSET                             (0)
#define TPD08_CANID__4_AVAILABLE_ENERGY_MIN                                (0)
#define TPD08_CANID__4_AVAILABLE_ENERGY_MAX                                (8191.88)

#pragma pack (1)
typedef struct
{
  uint16_t _1_Cycle_Charge_Capacity;
  uint16_t _2_Cycle_discharge_capacity;
  uint16_t _3_Available_Capacity;
  uint16_t _4_Available_Energy;
  bool data_received_b;
}
BMS_VCU_0x4AB_Rx_St_t;


/* def @TPD09 CAN Message                                   (1196) */
#define TPD09_ID                                            (1196U)
#define TPD09_IDE                                           (0U)
#define TPD09_DLC                                           (8U)


#define TPD09__1_EQUIVALENT_CYCLE_COUNTFACTOR                                   (0.1)
#define TPD09_CANID__1_EQUIVALENT_CYCLE_COUNT_STARTBIT                           (0)
#define TPD09_CANID__1_EQUIVALENT_CYCLE_COUNT_OFFSET                             (0)
#define TPD09_CANID__1_EQUIVALENT_CYCLE_COUNT_MIN                                (0)
#define TPD09_CANID__1_EQUIVALENT_CYCLE_COUNT_MAX                                (6553.5)
#define TPD09__2_LIFETIME_CHARGE_CAPACITYFACTOR                                   (1)
#define TPD09_CANID__2_LIFETIME_CHARGE_CAPACITY_STARTBIT                           (16)
#define TPD09_CANID__2_LIFETIME_CHARGE_CAPACITY_OFFSET                             (0)
#define TPD09_CANID__2_LIFETIME_CHARGE_CAPACITY_MIN                                (0)
#define TPD09_CANID__2_LIFETIME_CHARGE_CAPACITY_MAX                                (16777200)
#define TPD09__3_LIFETIME_DISCHARGE_CAPACITYFACTOR                                   (1)
#define TPD09_CANID__3_LIFETIME_DISCHARGE_CAPACITY_STARTBIT                           (40)
#define TPD09_CANID__3_LIFETIME_DISCHARGE_CAPACITY_OFFSET                             (0)
#define TPD09_CANID__3_LIFETIME_DISCHARGE_CAPACITY_MIN                                (0)
#define TPD09_CANID__3_LIFETIME_DISCHARGE_CAPACITY_MAX                                (16777200)

#pragma pack (1)
typedef struct
{
  uint16_t _1_Equivalent_Cycle_Count;
  uint32_t _2_Lifetime_Charge_capacity;
  uint32_t _3_Lifetime_Discharge_capacity;
}
BMS_VCU_0x4AC_Rx_St_t;


 extern uint32_t Deserialize_BMS_Rx_0x2AA(BMS_VCU_0x2AA_Rx_St_t* message, const uint8_t* data);
 extern uint32_t Deserialize_BMS_Rx_0x3AA(BMS_VCU_0x3AA_Rx_St_t* message, const uint8_t* data);
 extern uint32_t Deserialize_BMS_Rx_0x3AB(BMS_VCU_0x3AB_Rx_St_t* message, const uint8_t* data);
 
 extern uint32_t Deserialize_BMS_Rx_0x4AB(BMS_VCU_0x4AB_Rx_St_t* message, const uint8_t* data);
 extern uint32_t Deserialize_BMS_Rx_0x4AC(BMS_VCU_0x4AC_Rx_St_t* message, const uint8_t* data);

#if (OLD_BMS_CM == TRUE)
 extern uint32_t Deserialize_BMS_Rx_0x4AA(BMS_VCU_0x4AA_Rx_St_t* message, const uint8_t* data);
 #endif

#if (NEW_BMS_CM == TRUE)
 extern uint32_t Deserialize_TPDO4(TPDO4_t* message, const uint8_t* data);
 extern uint32_t Serialize_TPDO4(TPDO4_t* message, uint8_t* data);
 #endif
 #endif /* BMS_CAN_SIGNALS_H */
