/***********************************************************************************************************************
* File Name    : temp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
//#include "temp.h"

#include "RangeMileage.h"
#include "bms.h"
#include "ODOmeterCal.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define _100_METER_ODO    100

#define Master_WH_KM_ECO       (16)
#define Master_WH_KM_Sport     (18)

#define  SAMPLING_DISTANCE_m      (100)
#define  STORAGE_DISTANCE_m       (500)
#define  SAMPLING_DISTANCE_Km     (SAMPLING_DISTANCE_m/1000)
#define  ARRAY_SIZE              (5)

typedef struct
{
	float   IGN_WH ;
	float IGN_KM;
	float IGN_WH_KM ;
	float  IGN_WH_ECO;
	float  IGN_KM_ECO ;
	float  IGN_WH_KM_ECO;
	float  IGN_WH_sport;
	float  IGN_KM_sport ;
	float   IGN_WH_KM_sport;
	float   WH_battery ; // @ end
	float   Range;
	float  WH_KM_HMI;
	float IGN_WH_KM_E_Array[ARRAY_SIZE];
	float IGN_WH_KM_S_Array[ARRAY_SIZE];
	float sampling_flag;
	float storage_flag;
	float previous_Available_energy_WH ;
	float current_Available_energy_WH ;
}Range_Estimation_st_t;

Range_Estimation_st_t Range_Estimation_st;



void Range_estimation_ver_4(void)
{
	uint8_t ride_mode;
	float Energy_consumed = 0;
	
	if((IGN_ODO_meter % SAMPLING_DISTANCE_m) == 0)
	{
		Range_Estimation_st.current_Available_energy_WH = (Geth from battery) ;
		Energy_consumed = Range_Estimation_st.previous_Available_energy_WH - Range_Estimation_st.current_Available_energy_WH;
		Range_Estimation_st.IGN_WH += Energy_consumed ;
		Range_Estimation_st.IGN_KM += SAMPLING_DISTANCE_Km ;
		Range_Estimation_st.IGN_WH_KM = Range_Estimation_st.IGN_WH / Range_Estimation_st.IGN_KM;
		Range_Estimation_st.sampling_flag = true;
		ride_mode = Get_Actual_Ride_Mode();
		if(ride_mode == ECO)
		{
  			Range_Estimation_st.IGN_KM_ECO += SAMPLING_DISTANCE_Km; 
			Range_Estimation_st.IGN_WH_KM_ECO = Range_Estimation_st.IGN_WH_ECO / Range_Estimation_st.IGN_KM_ECO;
		}
		else if(ride_mode == SPORT)
		{

		}
	}
	
	
	
}
void init (void)
{
	Range_Estimation_st.previous_Available_energy = BMS_Get_Battery_Availabe_Energy();
}
/***********************************************************************************************************************
* Function Name: Restore_ODO_From_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_ODO_From_FEE(void)
{
    ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO from EEPROM on Start
}

/***********************************************************************************************************************
* Function Name: ResetTheODOEstimationData
* Description  : This function resets the Data and the Flags related to ODO estimation function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOEstimationData(void)
{
	ODO_Estimate_St.PresentMRN_u32 		= 0;
	ODO_Estimate_St.PreviousMRN_u32 	= 0;
	ODO_Estimate_St.Present_ODO_u32 	= 0;
	ODO_Estimate_St.Previous_ODO_u32 	= 0;
	ODO_Estimate_St.UpdateODOFromFlash_b 	= true;
	return;
}

/*---------------------------------------------------------------------*/



/********************************************************EOF***********************************************************/