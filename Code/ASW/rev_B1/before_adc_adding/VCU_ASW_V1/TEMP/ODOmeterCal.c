/***********************************************************************************************************************
* File Name    : ODOmeterCalc.c
* Version      : 01
* Description  : This file implements the functions related to calculate the total distance travelled by the
					vehicle.
* Created By   : Dileepa B S
* Creation Date: 07/02/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODOmeterCal.h"
#include "VehicleSpeedCal.h"
#include "Communicator.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/



static uint32_t PresentODOmeter_u32 = 0;
static uint32_t PreviousMRN_u32 	= 0;
uint32_t IGN_ODO_meter = 0;
 void ResetTheSpeedFilterData(void);
 uint32_t Estimate_ODO(uint32_t);


real_t ODO_m = 0;
ODO_Estimate_St_t       ODO_Estimate_St = {0,0,0,0,true};


/***********************************************************************************************************************
* Function Name: Calculate_DistanceTravelled
* Description  : This function calculates the Distance Travelled vy the Vehicle based on the MRN value
					received from the MCU-CAN.
* Arguments    : uint32_t PresentMRN_u32
* Return Value : None
***********************************************************************************************************************/
void Calculate_DistanceTravelled(uint32_t PresentMRN_u32)
{
	static float 	DistanceTravelled_m_t32 = 0;
	uint32_t		DistanceTravelled_m_u32 = 0;
	static uint16_t MotorRotations_u16 		= 0;
	uint16_t 		WheelRotations_u16 		= 0;
	
	if(PresentMRN_u32 != PreviousMRN_u32)
	{
		MotorRotations_u16 = (MotorRotations_u16 + (uint16_t)(PresentMRN_u32 - PreviousMRN_u32));
		WheelRotations_u16 = (MotorRotations_u16 / GEAR_RATIO);
		//MotorRotations_u16 = (MotorRotations_u16 % 8); // tod do manjesh  CHECKED WITH GEETH 
		PreviousMRN_u32 = PresentMRN_u32;
		if(WheelRotations_u16 != 0)
		{
			DistanceTravelled_m_t32 = (DistanceTravelled_m_t32 + \
										(float)((float)WheelRotations_u16*(PIE*WHEEL_DIAMETER))); 
										/* Distance Travelled = WheelRotations*WheelCircumference */
										/* Distance Travelled = WheelRotations*Pie*Diameter */
			DistanceTravelled_m_u32 = (uint32_t)DistanceTravelled_m_t32;
			DistanceTravelled_m_t32 = (float)(DistanceTravelled_m_t32 - (float)DistanceTravelled_m_u32);
		}
	}
	else
	{
		;
	}
	
	PresentODOmeter_u32 = (PresentODOmeter_u32 + DistanceTravelled_m_u32);
	
	return;
}


/***********************************************************************************************************************
* Function Name: Set_PresentODOmeter
* Description  : This function update the present ODOmeter value in meters.
* Arguments    : uint32_t PresentODO_u32
* Return Value : None
***********************************************************************************************************************/
void Set_PresentODOmeter(uint32_t PresentODO_u32)
{
	//PresentODOmeter_u32 = PresentODO_u32;
	return;
}


/***********************************************************************************************************************
* Function Name: Get_PresentODOmeter
* Description  : This function update the present ODOmeter value in meters.
* Arguments    : None
* Return Value : uint32_t PresentODOmeter_u32
***********************************************************************************************************************/
uint32_t Get_PresentODOmeter(void)
{
	return PresentODOmeter_u32;
}


/***********************************************************************************************************************
* Function Name: Reset_ODOmeter_Data
* Description  : This function resets/clears the data related to the ODOmeter calculation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Reset_ODOmeter_Data(void)
{
	PresentODOmeter_u32 = 0;
	PreviousMRN_u32 = 0;
	return;
}
/***********************************************************************************************************************
* Function Name: Restore_ODO_From_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Restore_ODO_From_FEE(void)
{
    ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO from EEPROM on Start
}

/***********************************************************************************************************************
* Function Name: ResetTheODOEstimationData
* Description  : This function resets the Data and the Flags related to ODO estimation function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOEstimationData(void)
{
	ODO_Estimate_St.PresentMRN_u32 		= 0;
	ODO_Estimate_St.PreviousMRN_u32 	= 0;
	ODO_Estimate_St.Present_ODO_u32 	= 0;
	ODO_Estimate_St.Previous_ODO_u32 	= 0;
	ODO_Estimate_St.UpdateODOFromFlash_b 	= true;
	return;
}

/***********************************************************************************************************************
* Function Name: ResetTheSpeedFilterData
* Description  : This function resets the data and flags related to Speed Filter algorithm.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
// void ResetTheSpeedFilterData(void)
// {
// 	SpeedFilter_St.PresentSpeed_u16 	= 0;
// 	SpeedFilter_St.PrevSpeed_u16 		= 0;
// 	SpeedFilter_St.FromSpeed_u16 		= 0;
// 	SpeedFilter_St.ToSpeed_u16 		    = 0;
// 	SpeedFilter_St.IncreaseSpeed_b 		= false;
// 	SpeedFilter_St.DecreaseSpeed_b 		= false;
	
// 	return;
// }


/***********************************************************************************************************************
* Function Name: Estimate_ODO
* Description  : This function estimates the ODO in meters [Distance covered by the vehicle] corresponding to the MRN 
		 received from the Motor Controller Unit.
* Arguments    : uint32_t MRN_u32
* Return Value : uint32_t ODO_Out_u32
***********************************************************************************************************************/
uint32_t Estimate_ODO(uint32_t MRN_u32) //Motor_Rotation_Number_u32 pass
{
    static real_t Prev_ODO_Meter = 0;
    real_t Distence_Covered_Meter = 0;
    uint32_t ODO_Meters_u32 = 0;
    uint32_t MRN_Over_u32 = 0;
    static uint8_t RM_Tigger_Counter_u8 = 0;

   
    ODO_Meters_u32 = (( MRN_u32/GEAR_RATIO)*PIE * WHEEL_DIAMETER);

    IGN_ODO_meter = ODO_Meters_u32;
    return ODO_Meters_u32;

    // if (ODO_Estimate_St.PresentMRN_u32 != ODO_Estimate_St.PreviousMRN_u32)
    // {


    //     MRN_Over_u32 = (ODO_Estimate_St.PresentMRN_u32 - ODO_Estimate_St.PreviousMRN_u32);

    //     if (MRN_Over_u32 >= 12)
    //     {

    //         ODO_Estimate_St.PreviousMRN_u32 = ODO_Estimate_St.PresentMRN_u32;
	    
    //         Distence_Covered_Meter = (((PIE * WHEEL_DIAMETER) / GEAR_RATIO) * (MRN_Over_u32));

    //         ODO_m = Prev_ODO_Meter + Distence_Covered_Meter;

    //         if (ODO_m > _100_METER_ODO)
    //         {
    //             ODO_Meters_u32 = (uint32_t)ODO_m;
    //             ODO_m = ODO_m - _100_METER_ODO;
    //             ODO_Estimate_St.Present_ODO_u32 += ODO_Meters_u32;
    //             RM_Tigger_Counter_u8++;
    //         }
    //         else
    //         {
    //             /* code */
    //         }
	    
	//      Prev_ODO_Meter = ODO_m;
    //     }
    //     else
    //     {
    //         ;
    //     }
    // }
    // else
    // {
    //     ;
    // }

    // if(RM_Tigger_Counter_u8 == (RANGE_MILEAGE_CAL_DISTANCE/_100_METER_ODO))
    // {
    //     RM_Tigger_Counter_u8 = 0;
    //     CalculateRangeMileage_b = true; 
    //     /*Set to true for each distance travelled of 500m to estimate mileage and range km*/
    // }
    // else
    // {
    //     /* code */
    // }
    

    //return ODO_Estimate_St.Present_ODO_u32; /*ODO in meters*/
}

/********************************************************EOF***********************************************************/