#include "r_cg_macrodriver.h"

/*Motor Power Limits in Watts*/
#define MOTOR_POWER_LIMIT_ECO_MODE				2000
#define MOTOR_POWER_LIMIT_SPORTS_MODE			3000
#define MOTOR_POWER_LIMIT_SAFE_MODE				1000
#define MOTOR_POWER_LIMIT_REVERSE_MODE			250


#define MAX_PWR_CONSUM_BARS						10


typedef enum
{
	IDLE_MODE_STATE_E,
	NEUTRAL_MODE_RUN_STATE_E,
	SPORT_MODE_RUN_STATE_E,
	ECO_MODE_RUN_STATE_E,
	REVERSE_MODE_RUN_STATE_E,
	SAFE_MODE_RUN_STATE_E,
} DrivingMode_En_t;

extern uint16_t CalculatePowerConsum(float, float, DrivingMode_En_t);