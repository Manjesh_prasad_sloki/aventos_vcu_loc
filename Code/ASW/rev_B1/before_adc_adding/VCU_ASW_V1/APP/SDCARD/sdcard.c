
/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : sdcard.c
|    Project        : VCU
|    Module         : sdcard
|    Description    : 
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 29/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SD_CARD_C
#define SD_CARD_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "sdcard.h"
#include "r_cg_macrodriver.h"
 /******************************************************************************/
static void string_reverse(uint8_t *);
static void Int_to_string(uint16_t data_au8 , uint8_t flag);
static void string_adjust_csv(uint8_t flag);
static void clearbuffer(uint8_t*,uint8_t );
Sd_opertion_St_t Sd_opertion_St;
SdCard_Data_temp_st_t SdCard_Data_temp_st;
FRESULT fresult2;
FIL fil5;
FATFS fs1;
uint16_t br1 =0 ;
void Sd_Card_Init(void)
{
	MX_FATFS_Init();
	fresult2 = f_mount(&fs1, "", 0);
}



void File_open(void)
{
	fresult2 = f_open(&fil5, "man2.csv",  FA_OPEN_APPEND | FA_WRITE);
	br1 =0 ;
}

void File_write(uint8_t *data ,uint16_t length )
{
	Sd_opertion_St.br  =0;
	br1 =0 ; 
	Sd_opertion_St.Fresult =   f_write(&fil5, data, length, &br1);	
}

void File_close(FIL *pt)
{
	f_close(&fil5);	
}

/***********************************************************************************************************************
* Function Name: string_reverse
* Description  : This function reverse the string.
* Arguments    : uint8_t *
* Return Value : None
***********************************************************************************************************************/
uint8_t trest_length  =0;
static void  string_reverse(uint8_t *string_reverse)
{
 uint8_t length =0;
 uint8_t index_start_u8 = 0;
 uint8_t index_end_u8;
 uint8_t buffer;
 length = strlen(string_reverse);
 length--;
 trest_length = length ;
 for(index_start_u8 =0 ,index_end_u8 = length ; index_end_u8 > index_start_u8  ; index_start_u8++,index_end_u8--  )
 {
	buffer = string_reverse[index_start_u8] ;
	string_reverse[index_start_u8] = string_reverse[index_end_u8];
	string_reverse[index_end_u8] = buffer; 
 }

}



void Int_to_string(uint16_t data_au8 , uint8_t flag)
{
	uint8_t index_u8 = 0;
	uint8_t remainder = 0;
	uint32_t int_data_u32 ;//= int_data;
	uint16_t temp_data =0;
	if(data_au8 == 0)
	{
		Sd_opertion_St.buffer_temp_ua8[index_u8++] = '0';
		Sd_opertion_St.buffer_temp_ua8[index_u8++] = '0';
		Sd_opertion_St.buffer_temp_ua8[index_u8++] = '\0';
		string_adjust_csv(flag);
	}
	else
	{
		while(data_au8)
		{
			temp_data++;
			remainder = (data_au8 % 16);
			if(remainder <= 9 )
			{
				Sd_opertion_St.buffer_temp_ua8[index_u8++] = remainder + '0';
			}
			else
			{
				switch(remainder)
				{
					case 10:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'A';
						break;
					}
					case 11:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'B';
						break;
					}
					case 12:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'C';
						break;
					}
					case 13:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'D';
						break;
					}
					case 14:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'E';
						break;
					}
					case 15:
					{
						Sd_opertion_St.buffer_temp_ua8[index_u8++] = 'F';
						break;
					}
				}
				
			}
			data_au8 /= 16;	
		}
		if(temp_data == 1)
		{
			Sd_opertion_St.buffer_temp_ua8[index_u8++] = '0';
		}
		Sd_opertion_St.buffer_temp_ua8[index_u8] = '\0';
		string_reverse(&Sd_opertion_St.buffer_temp_ua8[0]);
		string_adjust_csv(flag);
	}
	
}
void string_adjust_csv(uint8_t flag)
{
	Sd_opertion_St.length_temp_u16 = strlen(Sd_opertion_St.buffer_temp_ua8);
 	strncpy(&Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16],Sd_opertion_St.buffer_temp_ua8,Sd_opertion_St.length_temp_u16);
 	Sd_opertion_St.length_u16 += Sd_opertion_St.length_temp_u16;
 	if(flag == 1)
	{
		Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = ',';
	}
	else
	{
		Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = ' ';
	}
 	
	Sd_opertion_St.length_u16++;
}

uint8_t temp_data[10];

void user_SdCard_write(uint16_t canid_u16 ,uint8_t data_ua8[10] )
{
	uint8_t data_index;
	
	uint8_t buf[200],a;
	Sd_opertion_St.length_u16 = 0;
	Sd_opertion_St.length_temp_u16 = 0;
	clearbuffer(Sd_opertion_St.buffer_temp_ua8,50);
	
	for(data_index = 0;  data_index < 8 ;data_index++ )
	{
		temp_data[data_index] = data_ua8[data_index];
	}
	Int_to_string(canid_u16 , true);
	for(data_index = 2;  data_index < 10 ;data_index++ )
	{
		Int_to_string(data_ua8[data_index],false);
	}

	Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = '\r';
	Sd_opertion_St.length_u16++;
	Sd_opertion_St.buffer_ua8[Sd_opertion_St.length_u16] = '\n';
	File_write(Sd_opertion_St.buffer_ua8,Sd_opertion_St.length_u16 );
}
void clearbuffer(uint8_t *buff,uint8_t size)
{
	uint8_t index_u8 =0;
	for(index_u8 = 0; index_u8 < size; index_u8++)
	{
		buff[index_u8] = '\n';
	}
}



void sd_card_data_write(uint16_t canid ,uint8_t *Can_data)
{
	uint16_t   Canid_u16 = canid;
	uint8_t    index_u8;
	uint8_t    Can_data_ua8[10];
	Can_data_ua8[0] = canid;
	Can_data_ua8[1] = canid>>8;
	for(index_u8 = 0; index_u8 <8 ; index_u8++)
	{
		Can_data_ua8[2 + index_u8] = Can_data[index_u8];	
	}
	//user_SdCard_write(Canid_u16 ,&Can_data_ua8[0]);
}
#endif /*SD_CARD_C */
/*---------------------- End of File -----------------------------------------*/