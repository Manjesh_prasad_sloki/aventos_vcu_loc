/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : sdcard.h
|    Project        : VCU
|    Module         : sdcard
|    Description    :       
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 29/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef SD_CARD_H
#define SD_CARD_H
/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h" 
#include "fatfs.h"
#include "fatfs_sd.h"
#include "string.h"
#include "Config_CSIH0.h"


#define NUM_INT_DATA    3
#define NUM_FLOAT_DATA  2

typedef struct 
{
    FIL        file_pointer;
    FRESULT    Fresult;
    FATFS fs;
    uint16_t br;
    uint8_t buffer_ua8[200];
    uint8_t buffer_temp_ua8[50];
    uint16_t length_u16;
    uint16_t length_temp_u16;
    
}Sd_opertion_St_t;

typedef struct 
{
    uint32_t data_ua8[10];
    //float    float_data_fa32[10];
}
SdCard_Data_temp_st_t ;

/************************************************************
GLOBAL VARIABLE
*************************************************************/
extern SdCard_Data_temp_st_t SdCard_Data_temp_st;
extern Sd_opertion_St_t Sd_opertion_St;
void Sd_Card_Init(void);
void File_open(void);
void File_write(uint8_t *data ,uint16_t length );
void File_close(void);
void SD_card_write(void);
void sd_card_data_write(uint16_t ,uint8_t*);

#endif /* SD_CARD_H */
/*---------------------- End of File -----------------------------------------*/