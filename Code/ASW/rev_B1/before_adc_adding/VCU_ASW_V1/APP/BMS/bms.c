/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : bms.c
|    Project        : VCU
|    Module         : bms module 
|    Description    : This file contains the variables and functions                    
|                     to initialize and Operate the bms functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 20/04/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef BMS_C
#define BMS_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "r_cg_macrodriver.h"
#include "bms.h"
#include "bms_can_signals.h"
#include "Communicator.h"
#include "cil_can_conf.h"
#include "RangeMileage.h"
/*******************************************************************************
 *  macros
 ******************************************************************************/
#define Get_SOC()  		(BMS_VCU_0x2AA_Rx_St.SoC)
#define Get_Full_CAP() 	(BMS_VCU_0x2AA_Rx_St.Full_CAP)
#define Get_SoH()		(BMS_VCU_0x2AA_Rx_St.SoH)

#define GET_BATTERY_VOLTAGE()	(BMS_voltage)
#define GET_BATTERY_CURRENT()	(BMS_Current)
#define GET_AVAILABLE_CAPACITY()	      (AvailableCapacity)
#define Get_AVAILABLE_ENERGY()    (Available_Energy)

#define  GET_PDU_TEMP()  (25)  // BMS_VCU_0x3AB_Rx_St.BMSTemperature
#define  GET_BMS_TEMP()   (25) // BMS_VCU_0x3AB_Rx_St.PDUTemperature


#define Get_Max_DCHG_Current	(BMS_VCU_0x3AA_Rx_St.Max_DCHG_Current)
#define Get_SOP()				(BMS_VCU_0x3AA_Rx_St.SOP)
#define Get_Battery_state()		(BMS_VCU_0x3AA_Rx_St.Battery_State)

#define PDU_temp()      (25)
#define BMS_temp()		(25) 

#if (OLD_BMS_CM == TRUE)
#define Get_high_module_temp()	(BMS_VCU_0x4AA_Rx_St.High_Module_Temp )
 #endif
 #if (NEW_BMS_CM == TRUE)
 #define Get_high_cell_temp() (High_Cell_temp)
 #endif
#define Get_Cycle_charge_capacity()      (BMS_VCU_0x4AB_Rx_St._1_Cycle_Charge_Capacity/8)
#define Get_Cycle_discharge_capacity()      (BMS_VCU_0x4AB_Rx_St._2_Cycle_discharge_capacity/8)
	


#define Get_Equivalent_cycle_count()	(BMS_VCU_0x4AC_Rx_St._1_Equivalent_Cycle_Count/10)
#define Get_Lifetime_charge_capacity()	(BMS_VCU_0x4AC_Rx_St._2_Lifetime_Charge_capacity)
#define Get_Lifetime_discharge_capacity() (BMS_VCU_0x4AC_Rx_St._3_Lifetime_Discharge_capacity)

 #define BMS_4AA_BIT          (0) 
 #define BMS_4AB_BIT          (1)
 #define BMS_3AA_BIT          (2)
 #define BMS_3AB_BIT          (3)
 #define BMS_2AA_BIT          (4)
 #define BMS_4AC_BIT          (5)
 #define BMS_COMM_BITS        (0x04)

 #define BATTERY_MAX_TEMP     (50) // in celicius
 #define PDU_MAX_TEMP         (65) // in celicius
 #define BMS_MAX_TEMP         (50)// in celicius

 #define BATTERY_MAX_TEMP_EXT  (45)
 #define PDU_MAX_TEMP_EXT      (60)
#define  BMS_MAX_TEMP_EXT      (50)


 #define SET_BMS_COMM_BIT(X)   (BMS_Comm_u8 |= (1<<X) )
 #define CLEAR_BMS_COMM_BIT(X)  (BMS_Comm_u8 &= (~(1<<X)) )
 #define GET_BMS_COMM_BVAL()	(BMS_Comm_u8)

uint32_t SOC_u32 = 0;
uint32_t SOH_u32 = 0;
uint32_t Power_u32 = 0;
uint32_t Voltage_u32 = 0;
uint32_t Temperature_u32 = 0;
uint8_t BMS_Comm_u8 =  0; 


/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/
static Bms_RunTime_St_t Bms_RunTime_St;
//static BMS_Rx_0x2AA_t BMS_Rx_0x2AA;
//static BMS_Rx_0x4AA_t BMS_Rx_0x4AA;
//static BMS_Rx_0x4AB_t BMS_Rx_0x4AB;
//static BMS_Rx_0x3AA_t BMS_Rx_0x3AA;  /* TODO: Jeevan*/

static BMS_VCU_0x2AA_Rx_St_t BMS_VCU_0x2AA_Rx_St;
static BMS_VCU_0x3AA_Rx_St_t BMS_VCU_0x3AA_Rx_St;
static BMS_VCU_0x3AB_Rx_St_t BMS_VCU_0x3AB_Rx_St;
#if (OLD_BMS_CM == TRUE)
static BMS_VCU_0x4AA_Rx_St_t BMS_VCU_0x4AA_Rx_St;
#endif

#if (NEW_BMS_CM == TRUE)
static TPDO4_t BMS_VCU_0x4AA_Rx_St;
 #endif
static BMS_VCU_0x4AB_Rx_St_t BMS_VCU_0x4AB_Rx_St;
static BMS_VCU_0x4AC_Rx_St_t BMS_VCU_0x4AC_Rx_St;

static float BMS_BatteryVoltage = 0;
//static float BMS_BatteryCurrent = 0;
static float AvailableCapacity = 0;
static float Available_Energy = 0;
static float BMS_voltage =0;
static float BMS_Current = 0;
static float High_Cell_temp = 0;

float AvailableEnergy=0;

// static EstimatedRangeMileage_St_t EstimatedRangeMileage_St;
/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
static void BMS_StateMachine_Proc(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Init
*   Description   : This function implements BMS initialisation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Init(void)
{
	Bms_RunTime_St.Bms_State_En = BMS_STANDBY_STATE_E;
	Bms_RunTime_St.Bms_Event_En = BMS_STOP_EVENT_E;
	Bms_RunTime_St.BMS_InitCommCheck_b = false;
	Bms_RunTime_St.BMS_LateCommCheck_b = false;
	Bms_RunTime_St.BMS_Charging_b = false;
	Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = false;
	Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = false;
	Bms_RunTime_St.Timer_u32 = 0;
	Bms_RunTime_St.Bms_Start_b = false;
	SOC_u32 = ZERO;
	SOH_u32 = ZERO;
	Power_u32 = ZERO;
	Voltage_u32 = ZERO;
	Temperature_u32 = ZERO;
	CLEAR_BMS_COMM_STATUS();

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Proc
*   Description   : This function implements BMS Scheduler.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_Proc(void)
{
	BMS_StateMachine_Proc();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_StateMachine_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_StateMachine_Proc(void)
{
	static uint8_t Status_Flag = false;
	static uint8_t Bms_Flag = false;
	Bms_State_En_t Bms_State_En = (Bms_State_En_t)GET_VCU_STATE();
	Bms_RunTime_St.Bms_State_En = Bms_State_En;
	

	 if( BMS_COMM_BITS == GET_BMS_COMM_BVAL())
	 {
		SET_BMS_COMM_STATUS();
	 }
	 else
	 {
	 	CLEAR_BMS_COMM_STATUS();
	 }

	switch (Bms_RunTime_St.Bms_State_En)
	{
	case BMS_INIT_STATE_E:
	{
		Bms_RunTime_St.BMS_InitCommCheck_b = true;
		Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		Bms_RunTime_St.Bms_State_En = BMS_START_STATE_E;
		Bms_RunTime_St.Bms_Start_b = true;
		Restore_Wh_Km_from_FEE();
		Compute_Wh_On_VehicleStart(AvailableCapacity, BMS_BatteryVoltage);
		//Compute_Wh_On_VehicleStart(500, 60);
		break;
	}
	case BMS_START_STATE_E:
	{

		if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
				//SET_BMS_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			}
			else if (false == GET_BMS_COMM_STATUS() &&
					 (BMS_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}
		else if (true == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
			}
		}

		if((false == Bms_RunTime_St.BMS_Charging_b) && (FOUR == BMS_VCU_0x3AA_Rx_St.Battery_State))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ONE);
			Bms_RunTime_St.BMS_Charging_b = true;
		}
		else if((true == Bms_RunTime_St.BMS_Charging_b) && (ZERO == BMS_VCU_0x3AA_Rx_St.Battery_State))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ZERO);
			Bms_RunTime_St.BMS_Charging_b = false;
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
 			if (Get_high_cell_temp() >= BATTERY_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E); // AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true; // AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if((false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b) ||(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b))
		{
			if (BMS_temp()	 >= BMS_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			if (PDU_temp()>= PDU_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(Bms_Flag == true)
		{
			Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			Bms_Flag = false;
		}
		if(2000 <=GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32 )
		{
			Status_Flag = true;
		}
		if (BMS_VCU_0x2AA_Rx_St.SoC <= 20 )
		{
			if(true == Status_Flag)
			{
				
				//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_ON_E);//todo manjesh removed
				//SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE); //todo manjesh removed
			}
		}
		else if (BMS_VCU_0x2AA_Rx_St.SoC > 20)
		{
			//SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);//todo manjesh removed
			//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_OFF_E);//todo manjesh removed
			
		}
		break;
	}
	case BMS_RUN_STATE_E:
	{
		if (false == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			if ((false == Bms_RunTime_St.BMS_InitCommCheck_b) &&
				(false == GET_BMS_COMM_STATUS()))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}

			if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
			{
				if (true == GET_BMS_COMM_STATUS())
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
				}
				else if (false == GET_BMS_COMM_STATUS() &&
						 (BMS_COMM_LOST_TIMEOUT <=
						  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
					Bms_RunTime_St.BMS_LateCommCheck_b = true;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
					// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
					// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
					Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				}
				else if(false == GET_BMS_COMM_STATUS())
				{
					
				}
			}
			else
			{
				/* code */
			}
		}
		else if (false == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (Get_high_cell_temp() >= BMS_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_temp()	 >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (PDU_temp() >= 70)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else
		{
			
			/* code */
		}	

		if(true == CalculateRangeMileage_b)
		{
			CalculateRangeMileage_b = false;
			BMS_BatteryVoltage = GET_BATTERY_VOLTAGE();	
			AvailableEnergy =  Get_AVAILABLE_ENERGY();
			Estimate_Mileage_Range(AvailableEnergy, BMS_BatteryVoltage);
			
		}
		else
		{
			
			if(true == EstimatedRangeMileage_St.Esti_Range_Km_b)
			{
				if(true == BMS_VCU_0x4AB_Rx_St.data_received_b)
				{
					EstimatedRangeMileage_St.Esti_Range_Km_b = false;
					BMS_VCU_0x4AB_Rx_St.data_received_b = false;
					EstimatedRangeMileage_St.Esti_Range_Km_u16  = BMS_Get_Battery_Availabe_Energy() /18;
				}
			}
			/* code */
		}
		if (BMS_VCU_0x2AA_Rx_St.SoC <= 20)
		{
			//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_ON_E);todo : manjesh commented
			//SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE); // todo : manjesh commented
			
			
		}
		else if (BMS_VCU_0x2AA_Rx_St.SoC > 20)
		{
			//SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO); //todo : manjesh commented
			//SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_OFF_E); 
		}
		break;
	}
	case BMS_SHUT_DOWN_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		Store_Wh_Km_To_FEE();
		break;
	}
	case BMS_SAFE_MODE_STATE_E:
	{
		if (true == Bms_RunTime_St.BMS_InitCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = false;
				// SET_BMS_COMM_STATUS();
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
			}
			else if (false == GET_BMS_COMM_STATUS() &&
					 (BMS_COMM_LOST_TIMEOUT <=
					  (GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32)))
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_ON_E);
				// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ONE);
				// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
			}
			else
			{
				/* code */
			}
		}
		else if (true == Bms_RunTime_St.BMS_LateCommCheck_b)
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				if (BMS_COMM_RESTORE_TIME <=
					(GET_VCU_TIME_MS() - Bms_RunTime_St.Timer_u32))
				{
					Bms_RunTime_St.BMS_InitCommCheck_b = false;
					Bms_RunTime_St.BMS_LateCommCheck_b = false;
					SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_COMM_E, VEHICLE_SAFE_MODE_OFF_E);
					// SET_CLUSTER_DATA(CLUSTER_SERVICE_INDICATOR_E, ZERO);
					// SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO);
					//SET_BMS_COMM_STATUS();
				}
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				//Bms_RunTime_St.BMS_LateCommCheck_b = true;
				//Bms_RunTime_St.BMS_InitCommCheck_b = true;
			}
		}
		else
		{
			if (true == GET_BMS_COMM_STATUS())
			{
				Bms_RunTime_St.BMS_InitCommCheck_b = false;
			}
			else
			{
				Bms_RunTime_St.Timer_u32 = GET_VCU_TIME_MS();
				Bms_RunTime_St.BMS_InitCommCheck_b = true;
				Bms_RunTime_St.BMS_LateCommCheck_b = true;
			}
		}

		if(false == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (Get_high_cell_temp() > BATTERY_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_ON_E); // AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = true;// AE_TEST removed
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_Module_temp_b)
		{
			if (Get_high_cell_temp() < BATTERY_MAX_TEMP_EXT)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_MODULE_TEM_E, VEHICLE_SAFE_MODE_OFF_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_Module_temp_b = false;// AE_TEST removed
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_temp()	 > BMS_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = true;// AE_TEST removed
				
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_BMS_temp_b)
		{
			if (BMS_temp()	 < BMS_MAX_TEMP_EXT)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
				Bms_RunTime_St.BMS_SafeMode_BMS_temp_b = false;
			
			}
			else
			{
				/* code */
			}
		} 
		else
		{
			/* code */
		}
		if(false == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (PDU_temp() > PDU_MAX_TEMP)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_ON_E);// AE_TEST removed
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = true;// AE_TEST removed
				
			}
			else
			{
				/* code */
			}
			
		}
		else if (true == Bms_RunTime_St.BMS_SafeMode_PDU_temp_b)
		{
			if (PDU_temp() < PDU_MAX_TEMP_EXT)
			{
				SafeMode_SetEvent(VEHICLE_SAFE_MODE_PDU_TEM_E, VEHICLE_SAFE_MODE_OFF_E);
				Bms_RunTime_St.BMS_SafeMode_PDU_temp_b = false;
				
			}
			else
			{
				/* code */
			}
		}
		else
		{
			/* code */
		}
		if ((false == Bms_RunTime_St.BMS_Charging_b) && (FOUR == BMS_VCU_0x3AA_Rx_St.Battery_State))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ONE);
			Bms_RunTime_St.BMS_Charging_b = true;
		}
		else if ((true == Bms_RunTime_St.BMS_Charging_b) && (BMS_VCU_0x3AA_Rx_St.Battery_State))
		{
			SET_CLUSTER_DATA(CLUSTER_BATTERY_STATUS_E, ZERO);
			Bms_RunTime_St.BMS_Charging_b = false;
		}
		else
		{
			/* code */
		}
		if (BMS_VCU_0x2AA_Rx_St.SoC <= 20)
		{
			
			SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_ON_E);
			SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ONE);// todo manjesh commented
			
			
		}
		else if (BMS_VCU_0x2AA_Rx_St.SoC > 20)
		{
			
			SET_CLUSTER_DATA(CLUSTER_BATTERY_FAULT_E, ZERO); // todo manjesh commented
			SafeMode_SetEvent(VEHICLE_SAFE_MODE_BMS_SOC_E, VEHICLE_SAFE_MODE_OFF_E);
			
		}
		break;
	}
	case BMS_STANDBY_STATE_E:
	{
		Bms_RunTime_St.Bms_Start_b = false;
		CLEAR_BMS_COMM_STATUS();
		break;
	}

	default:
	{
		break;
	}
	}

	/*Add other module Initialisation*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x2AA_RX_E == CIL_SigName_En)
		{
 			//SET_BMS_COMM_BIT(BMS_2AA_BIT);
			Deserialize_BMS_Rx_0x2AA(&BMS_VCU_0x2AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AA_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_4AA_BIT);
			#if (OLD_BMS_CM == TRUE)
			Deserialize_BMS_Rx_0x4AA(&BMS_VCU_0x4AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);

			 #endif
			#if (NEW_BMS_CM == TRUE)
			Deserialize_TPDO4(&BMS_VCU_0x4AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			High_Cell_temp = BMS_VCU_0x4AA_Rx_St.HIGH_CELL_TEMP ;
			
			#endif
		}
	}
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AC_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AC_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AC_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_4AC_BIT);
			Deserialize_BMS_Rx_0x4AC(&BMS_VCU_0x4AC_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
		}
	}
	
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x4AB_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_4AB_BIT);
			Deserialize_BMS_Rx_0x4AB(&BMS_VCU_0x4AB_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			BMS_VCU_0x4AB_Rx_St.data_received_b = true;
			Available_Energy  = (BMS_VCU_0x4AB_Rx_St._4_Available_Energy/8 );
			AvailableCapacity = (BMS_VCU_0x4AB_Rx_St._3_Available_Capacity /8);  /* TODO: Jeevan*/
			
		}
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_RxMsgCallback
*   Description    : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x3AA_RX_E == CIL_SigName_En)
		{
			SET_BMS_COMM_BIT(BMS_3AA_BIT);
			Deserialize_BMS_Rx_0x3AA(&BMS_VCU_0x3AA_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
 			BMS_Current = (float)BMS_VCU_0x3AA_Rx_St.Battery_Current/16;
			BMS_voltage =(float) BMS_VCU_0x3AA_Rx_St.Battery_Voltage/1024;
			
		}
	}

}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AB_RxMsgCallback
*   Description   : This function Receive BMS data over CAN.
*   Parameters    : uint16_t CIL_SigName_En, CAN_MessageFrame_St_t* Can_Applidata_St
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AB_RxMsgCallback(uint16_t CIL_SigName_En, CAN_MessageFrame_St_t *Can_Applidata_St)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		if (CIL_BMS_0x3AB_RX_E == CIL_SigName_En)
		{
			//SET_BMS_COMM_BIT(BMS_3AB_BIT);
			Deserialize_BMS_Rx_0x3AB(&BMS_VCU_0x3AB_Rx_St, &Can_Applidata_St->DataBytes_au8[ZERO]);
			
		}
	}

}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x2AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x2AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_BIT(BMS_2AA_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AA_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_BIT(BMS_4AA_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AB_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_BIT(BMS_4AB_BIT);
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x4AC_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x4AC_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
	CLEAR_BMS_COMM_BIT(BMS_4AC_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AA_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AA_TimeOut_RxMsgCallback(void)
{
	uint8_t static time_out_counter = 0;
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		
		CLEAR_BMS_COMM_BIT(BMS_3AA_BIT);
		
	}
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_0x3AB_TimeOut_RxMsgCallback
*   Description   : This function will call when Rx Timeout happens.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
void BMS_0x3AB_TimeOut_RxMsgCallback(void)
{
	if (true == Bms_RunTime_St.Bms_Start_b)
	{
		CLEAR_BMS_COMM_BIT(BMS_3AB_BIT);
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BMS_SOC
*   Description   : This function will Returns the Present Battery SOC.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/
uint8_t Get_BMS_SOC(void)
{
	return BMS_VCU_0x2AA_Rx_St.SoC;
	
}
float Get_BMS_AvaE (void)
{
	return Available_Energy;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Charging_state
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : bool
*******************************************************************************/
bool BMS_Get_Charging_state(void)
{
	return Bms_RunTime_St.BMS_Charging_b;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Voltage
*   Description   : This function provides the voltage.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

float BMS_Get_Voltage(void)
{
	return GET_BATTERY_VOLTAGE();
}


/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Current
*   Description   : This function provides the Current.
*   Parameters    : None
*   Return Value  : int16_t
*******************************************************************************/

float BMS_Get_Current(void)
{
	
	return GET_BATTERY_CURRENT();  //testing
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Availability_Capacity
*   Description   : This function provides the Battery Availability Capacity.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

int16_t BMS_Get_Availability_Capacity(void)
{
 return GET_AVAILABLE_CAPACITY();
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Availabe_Energy
*   Description   : This function provides the Battery Availabe Energy.
*   Parameters    : None
*   Return Value  : float
*******************************************************************************/

 float BMS_Get_Availabe_Energy(void)
{
	static float nan_tammana_esaru_jeevan = 1;
	return nan_tammana_esaru_jeevan;
}


float get_test_Available_Energy(void )
{
	return 987; //Available_Energy;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Mileage_Wh_Km
*   Description   : This function returns the Estimated Mileage(Wh/Km)
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t BMS_Get_Mileage_Wh_Km(void)
{
	return EstimatedRangeMileage_St.Esti_Mileage_Wh_Km_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Range_Km
*   Description   : This function returns the Estimated Range(Km)
*   Parameters    : None
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t BMS_Get_Range_Km(void)
{
	return EstimatedRangeMileage_St.Esti_Range_Km_u16;
}

float Get_PDU_Temperature(void)
{
	return GET_PDU_TEMP();
}
float Get_BMS_Temperature(void)
{
	return GET_BMS_TEMP();
}

float Get_BMS_high_cell_Temp(void)
{
	return Get_high_cell_temp();
}

#endif /* BMS_C */
/*---------------------- End of File -----------------------------------------*/
