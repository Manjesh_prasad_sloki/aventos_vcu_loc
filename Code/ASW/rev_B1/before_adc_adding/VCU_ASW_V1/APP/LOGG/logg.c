/***********************************************************************************************************************
* File Name    : logg.c
* Version      : 01
* Description  : 
* Created By   : 
* Creation Date: 
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "logg.h"
#include "Communicator.h"
#include "can_if.h"
#include "cil_can_conf.h"
#include  "logging_signals.h"
#include "sdcard.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/



/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t  counter_ = 0 ;

void logging_Proc()
{
	static uint8_t st_machine = 0;
	switch(st_machine)
	{
		case 0:
		{
			Logg_0x709_TxMsgCallback();
			st_machine = 1;
			break;
		}
		case 1:
		{
			Logg_0x710_TxMsgCallback();
			st_machine = 2;
			break;
		}
		case 2:
		{
			Logg_0x711_TxMsgCallback();
			st_machine = 3;
			break;
		}
		case 3:
		{
			Logg_0x712_TxMsgCallback();
			st_machine = 4;
			break;
		}
		case 4:
		{
			Logg_0x713_TxMsgCallback();
			st_machine = 5;
			break;
		}
		case 5:
		{
			Logg_0x715_TxMsgCallback();
			st_machine = 6;
			break;
		}
		case 6:
		{
			Logg_0x716_TxMsgCallback();
			st_machine = 7;
			break;
		}
		case 7:
		{
			Logg_0x717_TxMsgCallback();
			st_machine = 8;
			break;
		}
		case 8:
		{
			Logg_0x718_TxMsgCallback();
			st_machine = 9;
			break;
		}
		case 9:
		{
			Logg_0x719_TxMsgCallback();
			st_machine = 10;
			break;
		}
		case 10:
		{
			Logg_0x720_TxMsgCallback();
			st_machine = 11;
			break;
		}
		case 11:
		{
			Logg_0x721_TxMsgCallback();
			st_machine = 0;
			break;
		}
		default :
		{
			st_machine = 0;
			break ;
		}
	}

}
void Logg_0x709_TxMsgCallback()
{
	 
	Data_709_t Data_709_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
    Data_709_struture = Update_0x709_struture();
	Serialize_Data_709(&Data_709_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x709 ,&data[0]);
	counter_++;
	if(counter_>0x5)
	 {
	 File_close();
	 File_open();
	 counter_ = 0;
	 }
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x709_TX_E, Can_Applidata_St);
}

/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Logg_0x710_TxMsgCallback(void)
{
	Data_710_t Data_710_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_710_struture = Update_0x710_struture();
	Serialize_Data_710(&Data_710_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x710 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x710_TX_E, Can_Applidata_St);

}

/***********************************************************************************************************************
* Function Name: 
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Logg_0x711_TxMsgCallback(void)
{
	Data_711_MCU_150_t  Data_711_struture;
	MCU_VCU_0x150_Rx_t  MCU_VCU_0x150_Tx_;
	uint8_t  *data,i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	data = MotorControl_0x711_TxMsgCallback();
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
 	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x711 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x711_TX_E, Can_Applidata_St);
}


void Logg_0x712_TxMsgCallback(void)
{
	Data_712_MCU_200_t Data_712_struture;
	uint8_t  *data,i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	data = MotorControl_0x712_TxMsgCallback();
	
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x712,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x712_TX_E, Can_Applidata_St);
}

void Logg_0x713_TxMsgCallback(void)
{
 	Data_713_t Data_713_struture;
 	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_713_struture = Update_0x713_struture();
	Serialize_Data_713(&Data_713_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x713 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x713_TX_E, Can_Applidata_St);
}



void Logg_0x715_TxMsgCallback(void)
{
	Data_715_t Data_715_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_715_struture = Update_0x715_struture();
	Serialize_Data_715(&Data_715_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x715 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x715_TX_E, Can_Applidata_St);
}

void Logg_0x716_TxMsgCallback(void)
{
	Data_716_t Data_716_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_716_struture = Update_0x716_struture();
	Serialize_Data_716(&Data_716_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x716 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x716_TX_E, Can_Applidata_St);
}

void Logg_0x717_TxMsgCallback(void)
{
	Data_717_t Data_717_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_717_struture = Update_0x717_struture();
	Serialize_Data_717(&Data_717_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x717 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x717_TX_E, Can_Applidata_St);
}

void Logg_0x718_TxMsgCallback(void)
{
	Data_718_t Data_718_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_718_struture = Update_0x718_struture();
	Serialize_Data_718(&Data_718_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x718 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x718_TX_E, Can_Applidata_St);
}
void Logg_0x719_TxMsgCallback(void)
{
	Data_719_MCU_250_t Data_719_struture;
	uint8_t  *data,i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	data = MotorControl_0x719_TxMsgCallback();
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x719 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x719_TX_E, Can_Applidata_St);
}


void Logg_0x720_TxMsgCallback(void)
{
	Data_720_MCU_100_t Data_720_struture;
	uint8_t  *data,i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	data = MotorControl_0x720_TxMsgCallback();
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x720 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x720_TX_E, Can_Applidata_St);
}


void Logg_0x721_TxMsgCallback(void)
{
	Data_721_t Data_721_struture;
	uint8_t  data[8],i;
	CAN_MessageFrame_St_t Can_Applidata_St;
	Data_721_struture = Update_0x721_struture();
	Serialize_Data_721(&Data_721_struture, data );
	for (i = ZERO; i < EIGHT; i++)
		{
			Can_Applidata_St.DataBytes_au8[i] = data[i];
		}
	Can_Applidata_St.DataLength_u8 = EIGHT;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//user_SdCard_write(0x721 ,&data[0]);
	CIL_CAN_Tx_AckMsg(CIL_LOG_0x721_TX_E, Can_Applidata_St);

}

/********************************************************EOF***********************************************************/