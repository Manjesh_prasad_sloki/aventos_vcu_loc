﻿

#include "logging_signals.h"



 uint32_t Deserialize_Data_709(Data_709_t* message, const uint8_t* data)
{
  message->_1_Battery_Voltage = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__1_BATTERY_VOLTAGE_OFFSET;
  message->_2_Current_direction = ((data[1] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__2_CURRENT_DIRECTION_OFFSET;
  message->_3_Battery_current = (((data[3] & (SIGNLE_READ_Mask8)) << DATA_709__3_BATTERY_CURRENT_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__3_BATTERY_CURRENT_OFFSET;
  message->_4_Vehicle_Speed = ((data[4] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__4_VEHICLE_SPEED_OFFSET;
  message->_5_Battery_SoC = ((data[5] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__5_BATTERY_SOC_OFFSET;
  message->_6_Available_Energy = (((data[7] & (SIGNLE_READ_Mask8)) << DATA_709__6_AVAILABLE_ENERGY_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + DATA_709_CANID__6_AVAILABLE_ENERGY_OFFSET;
   return DATA_709_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_709(Data_709_t* message, uint8_t* data)
{
  uint16_t temp = 0;
  message->_1_Battery_Voltage = (message->_1_Battery_Voltage  - DATA_709_CANID__1_BATTERY_VOLTAGE_OFFSET);
  message->_2_Current_direction = (message->_2_Current_direction  - DATA_709_CANID__2_CURRENT_DIRECTION_OFFSET);
  message->_3_Battery_current = (message->_3_Battery_current  - DATA_709_CANID__3_BATTERY_CURRENT_OFFSET);
  message->_4_Vehicle_Speed = (message->_4_Vehicle_Speed  - DATA_709_CANID__4_VEHICLE_SPEED_OFFSET);
  message->_5_Battery_SoC = (message->_5_Battery_SoC  - DATA_709_CANID__5_BATTERY_SOC_OFFSET);
  message->_6_Available_Energy = (message->_6_Available_Energy  - DATA_709_CANID__6_AVAILABLE_ENERGY_OFFSET);
  data[0] = (message->_1_Battery_Voltage & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->_2_Current_direction & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->_3_Battery_current & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->_3_Battery_current >> DATA_709__3_BATTERY_CURRENT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->_4_Vehicle_Speed & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->_5_Battery_SoC & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->_6_Available_Energy & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->_6_Available_Energy >> DATA_709__6_AVAILABLE_ENERGY_MASK0) & (SIGNLE_READ_Mask8)) ;
   return DATA_709_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_710(Data_710_t* message, const uint8_t* data)
{
  message->Kill_Switch = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_KILL_SWITCH_OFFSET;
  message->Kick_stand = ((data[1] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_KICK_STAND_OFFSET;
  message->Torque_zero = ((data[2] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_TORQUE_ZERO_OFFSET;
  message->Motor_kill = ((data[3] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_MOTOR_KILL_OFFSET;
  message->Motor_temperature = ((data[4] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_MOTOR_TEMPERATURE_OFFSET;
  message->Controller_temperature = ((data[5] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_CONTROLLER_TEMPERATURE_OFFSET;
  message->Cell_High_temperature = ((data[6] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_CELL_HIGH_TEMPERATURE_OFFSET;
  message->PDU_Temperature = ((data[7] & (SIGNLE_READ_Mask8))) + DATA_710_CANID_PDU_TEMPERATURE_OFFSET;
   return DATA_710_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_710(Data_710_t* message, uint8_t* data)
{
  message->Kill_Switch = (message->Kill_Switch  - DATA_710_CANID_KILL_SWITCH_OFFSET);
  message->Kick_stand = (message->Kick_stand  - DATA_710_CANID_KICK_STAND_OFFSET);
  message->Torque_zero = (message->Torque_zero  - DATA_710_CANID_TORQUE_ZERO_OFFSET);
  message->Motor_kill = (message->Motor_kill  - DATA_710_CANID_MOTOR_KILL_OFFSET);
  message->Motor_temperature = (message->Motor_temperature  - DATA_710_CANID_MOTOR_TEMPERATURE_OFFSET);
  message->Controller_temperature = (message->Controller_temperature  - DATA_710_CANID_CONTROLLER_TEMPERATURE_OFFSET);
  message->Cell_High_temperature = (message->Cell_High_temperature  - DATA_710_CANID_CELL_HIGH_TEMPERATURE_OFFSET);
  message->PDU_Temperature = (message->PDU_Temperature  - DATA_710_CANID_PDU_TEMPERATURE_OFFSET);
  data[0] = (message->Kill_Switch & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Kick_stand & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->Torque_zero & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->Motor_kill & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->Motor_temperature & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->Controller_temperature & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->Cell_High_temperature & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->PDU_Temperature & (SIGNLE_READ_Mask8)) ;
   return DATA_710_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_711_MCU_150(Data_711_MCU_150_t* message, const uint8_t* data)
{
  message->Motor_speed = (((data[1] & (SIGNLE_READ_Mask8)) << DATA_711_MCU_150_MOTOR_SPEED_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + DATA_711_MCU_150_CANID_MOTOR_SPEED_OFFSET;
  message->Battery_current = (((data[3] & (SIGNLE_READ_Mask8)) << DATA_711_MCU_150_BATTERY_CURRENT_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + DATA_711_MCU_150_CANID_BATTERY_CURRENT_OFFSET;
  message->Position_sensor_error = (((data[5] >> DATA_711_MCU_150_POSITION_SENSOR_ERROR_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_OFFSET;
  message->Throttle_disable_status = (((data[5] >> DATA_711_MCU_150_THROTTLE_DISABLE_STATUS_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_OFFSET;
  message->Motor_enable_status = ((data[6] & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_OFFSET;
  message->Pheriphery_supply_failure = (((data[6] >> DATA_711_MCU_150_PHERIPHERY_SUPPLY_FAILURE_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_OFFSET;
  message->Motor_temperature_warning = (((data[6] >> DATA_711_MCU_150_MOTOR_TEMPERATURE_WARNING_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_OFFSET;
  message->Controller_Temp_warning = (((data[6] >> DATA_711_MCU_150_CONTROLLER_TEMP_WARNING_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_OFFSET;
  message->Over_voltage_error = (((data[6] >> DATA_711_MCU_150_OVER_VOLTAGE_ERROR_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_OFFSET;
  message->under_voltage_error = (((data[6] >> DATA_711_MCU_150_UNDER_VOLTAGE_ERROR_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_OFFSET;
  message->over_current_error = (((data[6] >> DATA_711_MCU_150_OVER_CURRENT_ERROR_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_OFFSET;
  message->Timeout_error = (((data[6] >> DATA_711_MCU_150_TIMEOUT_ERROR_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_TIMEOUT_ERROR_OFFSET;
  message->Analog_throttle_error = ((data[7] & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_OFFSET;
  message->Driving_direction_actual = (((data[7] >> DATA_711_MCU_150_DRIVING_DIRECTION_ACTUAL_MASK0) & (SIGNLE_READ_Mask1))) + DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_OFFSET;
  message->Ride_mode_actual = (((data[7] >> DATA_711_MCU_150_RIDE_MODE_ACTUAL_MASK0) & (SIGNLE_READ_Mask3))) + DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_OFFSET;
  message->Throttle_map_actual = (((data[7] >> DATA_711_MCU_150_THROTTLE_MAP_ACTUAL_MASK0) & (SIGNLE_READ_Mask3))) + DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_OFFSET;
   return DATA_711_MCU_150_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_711_MCU_150(Data_711_MCU_150_t* message, uint8_t* data)
{
  message->Motor_speed = (message->Motor_speed  - DATA_711_MCU_150_CANID_MOTOR_SPEED_OFFSET);
  message->Battery_current = (message->Battery_current  - DATA_711_MCU_150_CANID_BATTERY_CURRENT_OFFSET);
  message->Position_sensor_error = (message->Position_sensor_error  - DATA_711_MCU_150_CANID_POSITION_SENSOR_ERROR_OFFSET);
  message->Throttle_disable_status = (message->Throttle_disable_status  - DATA_711_MCU_150_CANID_THROTTLE_DISABLE_STATUS_OFFSET);
  message->Motor_enable_status = (message->Motor_enable_status  - DATA_711_MCU_150_CANID_MOTOR_ENABLE_STATUS_OFFSET);
  message->Pheriphery_supply_failure = (message->Pheriphery_supply_failure  - DATA_711_MCU_150_CANID_PHERIPHERY_SUPPLY_FAILURE_OFFSET);
  message->Motor_temperature_warning = (message->Motor_temperature_warning  - DATA_711_MCU_150_CANID_MOTOR_TEMPERATURE_WARNING_OFFSET);
  message->Controller_Temp_warning = (message->Controller_Temp_warning  - DATA_711_MCU_150_CANID_CONTROLLER_TEMP_WARNING_OFFSET);
  message->Over_voltage_error = (message->Over_voltage_error  - DATA_711_MCU_150_CANID_OVER_VOLTAGE_ERROR_OFFSET);
  message->under_voltage_error = (message->under_voltage_error  - DATA_711_MCU_150_CANID_UNDER_VOLTAGE_ERROR_OFFSET);
  message->over_current_error = (message->over_current_error  - DATA_711_MCU_150_CANID_OVER_CURRENT_ERROR_OFFSET);
  message->Timeout_error = (message->Timeout_error  - DATA_711_MCU_150_CANID_TIMEOUT_ERROR_OFFSET);
  message->Analog_throttle_error = (message->Analog_throttle_error  - DATA_711_MCU_150_CANID_ANALOG_THROTTLE_ERROR_OFFSET);
  message->Driving_direction_actual = (message->Driving_direction_actual  - DATA_711_MCU_150_CANID_DRIVING_DIRECTION_ACTUAL_OFFSET);
  message->Ride_mode_actual = (message->Ride_mode_actual  - DATA_711_MCU_150_CANID_RIDE_MODE_ACTUAL_OFFSET);
  message->Throttle_map_actual = (message->Throttle_map_actual  - DATA_711_MCU_150_CANID_THROTTLE_MAP_ACTUAL_OFFSET);
  data[0] = (message->Motor_speed & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->Motor_speed >> DATA_711_MCU_150_MOTOR_SPEED_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->Battery_current & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->Battery_current >> DATA_711_MCU_150_BATTERY_CURRENT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->Position_sensor_error & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_POSITION_SENSOR_ERROR_MASK0) | ((message->Throttle_disable_status & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_THROTTLE_DISABLE_STATUS_MASK0) ;
  data[6] = (message->Motor_enable_status & (SIGNLE_READ_Mask1)) | ((message->Pheriphery_supply_failure & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_PHERIPHERY_SUPPLY_FAILURE_MASK0) | ((message->Motor_temperature_warning & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_MOTOR_TEMPERATURE_WARNING_MASK0) | ((message->Controller_Temp_warning & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_CONTROLLER_TEMP_WARNING_MASK0) | ((message->Over_voltage_error & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_OVER_VOLTAGE_ERROR_MASK0) | ((message->under_voltage_error & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_UNDER_VOLTAGE_ERROR_MASK0) | ((message->over_current_error & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_OVER_CURRENT_ERROR_MASK0) | ((message->Timeout_error & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_TIMEOUT_ERROR_MASK0) ;
  data[7] = (message->Analog_throttle_error & (SIGNLE_READ_Mask1)) | ((message->Driving_direction_actual & (SIGNLE_READ_Mask1)) << DATA_711_MCU_150_DRIVING_DIRECTION_ACTUAL_MASK0) | ((message->Ride_mode_actual & (SIGNLE_READ_Mask3)) << DATA_711_MCU_150_RIDE_MODE_ACTUAL_MASK0) | ((message->Throttle_map_actual & (SIGNLE_READ_Mask3)) << DATA_711_MCU_150_THROTTLE_MAP_ACTUAL_MASK0) ;
   return DATA_711_MCU_150_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_712_MCU_200(Data_712_MCU_200_t* message, const uint8_t* data)
{
  message->Throttle_out_p = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_712_MCU_200_CANID_THROTTLE_OUT_P_OFFSET;
  message->Throttle_reference = ((data[1] & (SIGNLE_READ_Mask8))) + DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_OFFSET;
  message->Battery_voltage = ((data[7] & (SIGNLE_READ_Mask8))) + DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_OFFSET;
   return DATA_712_MCU_200_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_712_MCU_200(Data_712_MCU_200_t* message, uint8_t* data)
{
  message->Throttle_out_p = (message->Throttle_out_p  - DATA_712_MCU_200_CANID_THROTTLE_OUT_P_OFFSET);
  message->Throttle_reference = (message->Throttle_reference  - DATA_712_MCU_200_CANID_THROTTLE_REFERENCE_OFFSET);
  message->Battery_voltage = (message->Battery_voltage  - DATA_712_MCU_200_CANID_BATTERY_VOLTAGE_OFFSET);
  data[0] = (message->Throttle_out_p & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Throttle_reference & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->Battery_voltage & (SIGNLE_READ_Mask8)) ;
   return DATA_712_MCU_200_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_713(Data_713_t* message, const uint8_t* data)
{
  message->_1_Battery_Voltage = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__1_BATTERY_VOLTAGE_OFFSET;
  message->_2_Battery_Capacity = (((data[2] & (SIGNLE_READ_Mask8)) << DATA_713__2_BATTERY_CAPACITY_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__2_BATTERY_CAPACITY_OFFSET;
  message->_3_Distance_from_start = ((data[3] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__3_DISTANCE_FROM_START_OFFSET;
  message->_4_M_Wh_Km = (((data[5] & (SIGNLE_READ_Mask8)) << DATA_713__4_M_WH_KM_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__4_M_WH_KM_OFFSET;
  message->_5_Instant_Mileage = ((data[6] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__5_INSTANT_MILEAGE_OFFSET;
  message->_6_Historic_Mileage = ((data[7] & (SIGNLE_READ_Mask8))) + DATA_713_CANID__6_HISTORIC_MILEAGE_OFFSET;
   return DATA_713_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_713(Data_713_t* message, uint8_t* data)
{
  message->_1_Battery_Voltage = (message->_1_Battery_Voltage  - DATA_713_CANID__1_BATTERY_VOLTAGE_OFFSET);
  message->_2_Battery_Capacity = (message->_2_Battery_Capacity  - DATA_713_CANID__2_BATTERY_CAPACITY_OFFSET);
  message->_3_Distance_from_start = (message->_3_Distance_from_start  - DATA_713_CANID__3_DISTANCE_FROM_START_OFFSET);
  message->_4_M_Wh_Km = (message->_4_M_Wh_Km  - DATA_713_CANID__4_M_WH_KM_OFFSET);
  message->_5_Instant_Mileage = (message->_5_Instant_Mileage  - DATA_713_CANID__5_INSTANT_MILEAGE_OFFSET);
  message->_6_Historic_Mileage = (message->_6_Historic_Mileage  - DATA_713_CANID__6_HISTORIC_MILEAGE_OFFSET);
  data[0] = (message->_1_Battery_Voltage & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->_2_Battery_Capacity & (SIGNLE_READ_Mask8)) ;
  data[2] = ((message->_2_Battery_Capacity >> DATA_713__2_BATTERY_CAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->_3_Distance_from_start & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->_4_M_Wh_Km & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->_4_M_Wh_Km >> DATA_713__4_M_WH_KM_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->_5_Instant_Mileage & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->_6_Historic_Mileage & (SIGNLE_READ_Mask8)) ;
   return DATA_713_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_715(Data_715_t* message, const uint8_t* data)
{
  message->_1_Battery_voltage = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_715_CANID__1_BATTERY_VOLTAGE_OFFSET;
  message->_2_Available_Capacity = (((data[2] & (SIGNLE_READ_Mask8)) << DATA_715__2_AVAILABLE_CAPACITY_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + DATA_715_CANID__2_AVAILABLE_CAPACITY_OFFSET;
  message->_3_Distance_km = ((data[3] & (SIGNLE_READ_Mask8))) + DATA_715_CANID__3_DISTANCE_KM_OFFSET;
  message->_4_IGN_WH = (((data[5] & (SIGNLE_READ_Mask8)) << DATA_715__4_IGN_WH_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + DATA_715_CANID__4_IGN_WH_OFFSET;
  message->_5_IGN_WH_KM = (((data[7] & (SIGNLE_READ_Mask8)) << DATA_715__5_IGN_WH_KM_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + DATA_715_CANID__5_IGN_WH_KM_OFFSET;
   return DATA_715_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_715(Data_715_t* message, uint8_t* data)
{
  message->_1_Battery_voltage = (message->_1_Battery_voltage  - DATA_715_CANID__1_BATTERY_VOLTAGE_OFFSET);
  message->_2_Available_Capacity = (message->_2_Available_Capacity  - DATA_715_CANID__2_AVAILABLE_CAPACITY_OFFSET);
  message->_3_Distance_km = (message->_3_Distance_km  - DATA_715_CANID__3_DISTANCE_KM_OFFSET);
  message->_4_IGN_WH = (message->_4_IGN_WH  - DATA_715_CANID__4_IGN_WH_OFFSET);
  message->_5_IGN_WH_KM = (message->_5_IGN_WH_KM  - DATA_715_CANID__5_IGN_WH_KM_OFFSET);
  data[0] = (message->_1_Battery_voltage & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->_2_Available_Capacity & (SIGNLE_READ_Mask8)) ;
  data[2] = ((message->_2_Available_Capacity >> DATA_715__2_AVAILABLE_CAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->_3_Distance_km & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->_4_IGN_WH & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->_4_IGN_WH >> DATA_715__4_IGN_WH_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->_5_IGN_WH_KM & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->_5_IGN_WH_KM >> DATA_715__5_IGN_WH_KM_MASK0) & (SIGNLE_READ_Mask8)) ;
   return DATA_715_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_716(Data_716_t* message, const uint8_t* data)
{
  message->_1_WH_KM_ECO = (((data[1] & (SIGNLE_READ_Mask8)) << DATA_716__1_WH_KM_ECO_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + DATA_716_CANID__1_WH_KM_ECO_OFFSET;
  message->_2_WH_KM_SPORT = (((data[3] & (SIGNLE_READ_Mask8)) << DATA_716__2_WH_KM_SPORT_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + DATA_716_CANID__2_WH_KM_SPORT_OFFSET;
  message->_3_Range = ((data[4] & (SIGNLE_READ_Mask8))) + DATA_716_CANID__3_RANGE_OFFSET;
  message->Battery_available_Energy = (((data[7] & (SIGNLE_READ_Mask8)) << DATA_716_BATTERY_AVAILABLE_ENERGY_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_OFFSET;
   return DATA_716_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_716(Data_716_t* message, uint8_t* data)
{
  message->_1_WH_KM_ECO = (message->_1_WH_KM_ECO  - DATA_716_CANID__1_WH_KM_ECO_OFFSET);
  message->_2_WH_KM_SPORT = (message->_2_WH_KM_SPORT  - DATA_716_CANID__2_WH_KM_SPORT_OFFSET);
  message->_3_Range = (message->_3_Range  - DATA_716_CANID__3_RANGE_OFFSET);
  message->Battery_available_Energy = (message->Battery_available_Energy  - DATA_716_CANID_BATTERY_AVAILABLE_ENERGY_OFFSET);
  data[0] = (message->_1_WH_KM_ECO & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->_1_WH_KM_ECO >> DATA_716__1_WH_KM_ECO_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->_2_WH_KM_SPORT & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->_2_WH_KM_SPORT >> DATA_716__2_WH_KM_SPORT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->_3_Range & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->Battery_available_Energy & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->Battery_available_Energy >> DATA_716_BATTERY_AVAILABLE_ENERGY_MASK0) & (SIGNLE_READ_Mask8)) ;
   return DATA_716_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_717(Data_717_t* message, const uint8_t* data)
{
  message->_1_AccelerationAngle_X = ((data[0] & (SIGNLE_READ_Mask8))) + DATA_717_CANID__1_ACCELERATIONANGLE_X_OFFSET;
  message->_2_AccelerationAngle_Y = ((data[1] & (SIGNLE_READ_Mask8))) + DATA_717_CANID__2_ACCELERATIONANGLE_Y_OFFSET;
   return DATA_717_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_717(Data_717_t* message, uint8_t* data)
{
  message->_1_AccelerationAngle_X = (message->_1_AccelerationAngle_X  - DATA_717_CANID__1_ACCELERATIONANGLE_X_OFFSET);
  message->_2_AccelerationAngle_Y = (message->_2_AccelerationAngle_Y  - DATA_717_CANID__2_ACCELERATIONANGLE_Y_OFFSET);
  data[0] = (message->_1_AccelerationAngle_X & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->_2_AccelerationAngle_Y & (SIGNLE_READ_Mask8)) ;
   return DATA_717_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_718(Data_718_t* message, const uint8_t* data)
{
  message->MCUTransmissionStatus = ((data[7] & (SIGNLE_READ_Mask8))) + DATA_718_CANID_MCUTRANSMISSIONSTATUS_OFFSET;
   return DATA_718_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_718(Data_718_t* message, uint8_t* data)
{
  message->MCUTransmissionStatus = (message->MCUTransmissionStatus  - DATA_718_CANID_MCUTRANSMISSIONSTATUS_OFFSET);
  data[7] = (message->MCUTransmissionStatus & (SIGNLE_READ_Mask8)) ;
   return DATA_718_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_719_MCU_250(Data_719_MCU_250_t* message, const uint8_t* data)
{
  message->Motor_temperature = (((data[1] & (SIGNLE_READ_Mask2)) << DATA_719_MCU_250_MOTOR_TEMPERATURE_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_OFFSET;
  message->Controller_temperature = (((data[3] & (SIGNLE_READ_Mask2)) << DATA_719_MCU_250_CONTROLLER_TEMPERATURE_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_OFFSET;
  message->Motor_Rotation_Number = (((data[7] & (SIGNLE_READ_Mask8)) << DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK0) | ((data[6] & (SIGNLE_READ_Mask8)) << DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK1) | ((data[5] & (SIGNLE_READ_Mask8)) << DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK2) | (data[4] & (SIGNLE_READ_Mask8))) + DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_OFFSET;
   return DATA_719_MCU_250_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_719_MCU_250(Data_719_MCU_250_t* message, uint8_t* data)
{
  message->Motor_temperature = (message->Motor_temperature  - DATA_719_MCU_250_CANID_MOTOR_TEMPERATURE_OFFSET);
  message->Controller_temperature = (message->Controller_temperature  - DATA_719_MCU_250_CANID_CONTROLLER_TEMPERATURE_OFFSET);
  message->Motor_Rotation_Number = (message->Motor_Rotation_Number  - DATA_719_MCU_250_CANID_MOTOR_ROTATION_NUMBER_OFFSET);
  data[0] = (message->Motor_temperature & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->Motor_temperature >> DATA_719_MCU_250_MOTOR_TEMPERATURE_MASK0) & (SIGNLE_READ_Mask2)) ;
  data[2] = (message->Controller_temperature & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->Controller_temperature >> DATA_719_MCU_250_CONTROLLER_TEMPERATURE_MASK0) & (SIGNLE_READ_Mask2)) ;
  data[4] = (message->Motor_Rotation_Number & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->Motor_Rotation_Number >> DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK2) & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->Motor_Rotation_Number >> DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK1) & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->Motor_Rotation_Number >> DATA_719_MCU_250_MOTOR_ROTATION_NUMBER_MASK0) & (SIGNLE_READ_Mask8)) ;
   return DATA_719_MCU_250_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_720_MCU_100(Data_720_MCU_100_t* message, const uint8_t* data)
{
  message->_1_Motor_Speed_Limit = (((data[1] & (SIGNLE_READ_Mask8)) << DATA_720_MCU_100__1_MOTOR_SPEED_LIMIT_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_OFFSET;
  message->_2_LTVS = ((data[2] & (SIGNLE_READ_Mask8))) + DATA_720_MCU_100_CANID__2_LTVS_OFFSET;
  message->_3_Max_Regeneration = ((data[3] & (SIGNLE_READ_Mask8))) + DATA_720_MCU_100_CANID__3_MAX_REGENERATION_OFFSET;
  message->_4_LTVS = (((data[6] & (SIGNLE_READ_Mask8)) << DATA_720_MCU_100__4_LTVS_MASK0) | ((data[5] & (SIGNLE_READ_Mask8)) << DATA_720_MCU_100__4_LTVS_MASK1) | (data[4] & (SIGNLE_READ_Mask8))) + DATA_720_MCU_100_CANID__4_LTVS_OFFSET;
  message->_5_Ride_Mode_Request = ((data[7] & (SIGNLE_READ_Mask3))) + DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_OFFSET;
  message->_6_Throttle_Map_select = (((data[7] >> DATA_720_MCU_100__6_THROTTLE_MAP_SELECT_MASK0) & (SIGNLE_READ_Mask3))) + DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_OFFSET;
  message->_7_Motor_Stop = (((data[7] >> DATA_720_MCU_100__7_MOTOR_STOP_MASK0) & (SIGNLE_READ_Mask1))) + DATA_720_MCU_100_CANID__7_MOTOR_STOP_OFFSET;
  message->_8_Driving_Direction = (((data[7] >> DATA_720_MCU_100__8_DRIVING_DIRECTION_MASK0) & (SIGNLE_READ_Mask1))) + DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_OFFSET;
   return DATA_720_MCU_100_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_720_MCU_100(Data_720_MCU_100_t* message, uint8_t* data)
{
  message->_1_Motor_Speed_Limit = (message->_1_Motor_Speed_Limit  - DATA_720_MCU_100_CANID__1_MOTOR_SPEED_LIMIT_OFFSET);
  message->_2_LTVS = (message->_2_LTVS  - DATA_720_MCU_100_CANID__2_LTVS_OFFSET);
  message->_3_Max_Regeneration = (message->_3_Max_Regeneration  - DATA_720_MCU_100_CANID__3_MAX_REGENERATION_OFFSET);
  message->_4_LTVS = (message->_4_LTVS  - DATA_720_MCU_100_CANID__4_LTVS_OFFSET);
  message->_5_Ride_Mode_Request = (message->_5_Ride_Mode_Request  - DATA_720_MCU_100_CANID__5_RIDE_MODE_REQUEST_OFFSET);
  message->_6_Throttle_Map_select = (message->_6_Throttle_Map_select  - DATA_720_MCU_100_CANID__6_THROTTLE_MAP_SELECT_OFFSET);
  message->_7_Motor_Stop = (message->_7_Motor_Stop  - DATA_720_MCU_100_CANID__7_MOTOR_STOP_OFFSET);
  message->_8_Driving_Direction = (message->_8_Driving_Direction  - DATA_720_MCU_100_CANID__8_DRIVING_DIRECTION_OFFSET);
  data[0] = (message->_1_Motor_Speed_Limit & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->_1_Motor_Speed_Limit >> DATA_720_MCU_100__1_MOTOR_SPEED_LIMIT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->_2_LTVS & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->_3_Max_Regeneration & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->_4_LTVS & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->_4_LTVS >> DATA_720_MCU_100__4_LTVS_MASK1) & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->_4_LTVS >> DATA_720_MCU_100__4_LTVS_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->_5_Ride_Mode_Request & (SIGNLE_READ_Mask3)) | ((message->_6_Throttle_Map_select & (SIGNLE_READ_Mask3)) << DATA_720_MCU_100__6_THROTTLE_MAP_SELECT_MASK0) | ((message->_7_Motor_Stop & (SIGNLE_READ_Mask1)) << DATA_720_MCU_100__7_MOTOR_STOP_MASK0) | ((message->_8_Driving_Direction & (SIGNLE_READ_Mask1)) << DATA_720_MCU_100__8_DRIVING_DIRECTION_MASK0) ;
   return DATA_720_MCU_100_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_Data_721(Data_721_t* message, const uint8_t* data)
{
  message->DistanceCovered = (((data[1] & (SIGNLE_READ_Mask8)) << DATA_721_DISTANCECOVERED_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + DATA_721_CANID_DISTANCECOVERED_OFFSET;
  message->No_of_100m = (((data[3] & (SIGNLE_READ_Mask8)) << DATA_721_NO_OF_100M_MASK0) | (data[2] & (SIGNLE_READ_Mask8))) + DATA_721_CANID_NO_OF_100M_OFFSET;
  message->Available_Energy_BMS = (((data[7] & (SIGNLE_READ_Mask8)) << DATA_721_AVAILABLE_ENERGY_BMS_MASK0) | (data[6] & (SIGNLE_READ_Mask8))) + DATA_721_CANID_AVAILABLE_ENERGY_BMS_OFFSET;
   return DATA_721_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_Data_721(Data_721_t* message, uint8_t* data)
{
  message->DistanceCovered = (message->DistanceCovered  - DATA_721_CANID_DISTANCECOVERED_OFFSET);
  message->No_of_100m = (message->No_of_100m  - DATA_721_CANID_NO_OF_100M_OFFSET);
  message->Available_Energy_BMS = (message->Available_Energy_BMS  - DATA_721_CANID_AVAILABLE_ENERGY_BMS_OFFSET);
  data[0] = (message->DistanceCovered & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->DistanceCovered >> DATA_721_DISTANCECOVERED_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->No_of_100m & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->No_of_100m >> DATA_721_NO_OF_100M_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->Available_Energy_BMS & (SIGNLE_READ_Mask8)) ;
  data[7] = ((message->Available_Energy_BMS >> DATA_721_AVAILABLE_ENERGY_BMS_MASK0) & (SIGNLE_READ_Mask8)) ;
   return DATA_721_ID; 
}
