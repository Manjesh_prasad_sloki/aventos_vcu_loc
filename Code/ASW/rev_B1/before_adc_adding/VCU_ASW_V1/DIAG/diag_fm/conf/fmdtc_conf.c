/***************************************************************************************************
 *    FILENAME    :  fmdtc_conf.c
 *
 *    DESCRIPTION : File contains the configuration table for DTC values
 *
 *    $Id         : $    
 *
 ***************************************************************************************************
 * Revision history
 * 
 * Ver Author       Date       Description
 * 1   Sloki          25/09/2018
 ***************************************************************************************************
*/


#include "fmdtc_conf.h"


#if (TRUE == FM_UDS_SUPPORTED)

/*
 **************************************************************************************************
 *    "CONFIGURATION DATA TO BE FILLED BY APPLICATION USER" 
 *     Mapping of the DTC to Fault Paths for each Error types
 **************************************************************************************************
*/
const uint32_t  DTCMappingTable_UDS_aSt[NUM_OF_FAULTPATHS_E][MAX_NUM_ERROR_TYPES_E-1]=
{
/*    Fault         Error           DTC Value
      Labels,       type,           (24bit)
*/
//   MAX_ERR,     MIN_ERR,   SIG_ERR,   NPL_ERR,   FER_ERR     			/* Index - DTC Name							*/
    {0xD60455    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	00 - FM_FP_TEST1_E			*/
    {0xF00616    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	01 - FM_FP_TEST2_E  	*/
    {0xF00617    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	02 - FM_FP_TEST3_E   	*/
    {0xC02813    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	03 - FM_FP_TEST4_E  	*/
    {0xC02888    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	04 - FM_FP_TEST5_E   	*/
    {0xD60981    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	05 - FM_FP_TEST6_E          	*/
    {0xC40181    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	06 - FM_FP_TEST7_E          	*/
    {0xC42481    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	07 - FM_FP_TEST8_E             	*/
    {0xC40281    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	08 - FM_FP_TEST9_E              	*/
    {0xD30908    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	09 - FM_FP_TEST10_E         	*/

/* Note addition of DTC will require removing Default DTC masking */

};

#endif

#if (TRUE == FM_OBD_SUPPORTED)

 const uint16_t  DTCMappingTable_OBD_aSt[NUM_OF_FAULTPATHS_E][MAX_NUM_ERROR_TYPES_E-1] =
{
/*    Fault         Error           DTC Value
      Labels,       type,           (24bit)
*/
//   MAX_ERR,     MIN_ERR,   SIG_ERR,   NPL_ERR,   FER_ERR     			/* Index - DTC Name							*/
    {0xD604    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	00 - FM_FP_TEST1_E			*/
    {0xF006    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	01 - FM_FP_TEST2_E  	*/
    {0xF006    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	02 - FM_FP_TEST3_E   	*/
    {0xC028    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	03 - FM_FP_TEST4_E  	*/
    {0xC028    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	04 - FM_FP_TEST5_E   	*/
    {0xD609    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	05 - FM_FP_TEST6_E          	*/
    {0xC401    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	06 - FM_FP_TEST7_E          	*/
    {0xC424    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	07 - FM_FP_TEST8_E             	*/
    {0xC402    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	08 - FM_FP_TEST9_E              	*/
    {0xD309    ,0x0000    ,0x0000    ,0x0000    ,0x0000    },			/*	09 - FM_FP_TEST10_E         	*/

/* Note addition of DTC will require removing Default DTC masking */

};

#endif

#if (TRUE == FM_J1939_SUPPORTED)


const J1939_DTC_Uni_t J1939_DTC_Conf_aUni[NUM_OF_FAULTPATHS_E] = 
{
	{.DTC_St = {0x7FF01,	 ABOVE_NORMAL_SEVERE,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF02,	 VOLTAGE_ABOVE_NORMAL,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF03,	 VOLTAGE_ABOVE_NORMAL,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF04,	 ABOVE_NORMAL_SEVERE,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF05,	 ABOVE_NORMAL_SEVERE,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF06,	 VOLTAGE_ABOVE_NORMAL,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF07,	 ABOVE_NORMAL_SEVERE,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF08,	 VOLTAGE_ABOVE_NORMAL,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF09,	 ABOVE_NORMAL_SEVERE,	  0 ,	 0x7F}},
	{.DTC_St = {0x7FF0A,	 VOLTAGE_ABOVE_NORMAL,	  0 ,	 0x7F}}

};
#endif

/* Note addition of DTC will require removing Default DTC masking */
