/***************************************************************************************************
 *    FILENAME    :  fmff_conf.h
 *
 *    DESCRIPTION : File contains the configuration table for freeze frames.
 *
 *    $Id         : $    
 *
 ***************************************************************************************************
 * Revision history
 * 
 * Ver Author       Date       Description
 * 1   Sloki/Harsh          24/07/2020
 ***************************************************************************************************
*/

#ifndef    _FMFF_CONF_H_
#define    _FMFF_CONF_H_

#include <stdint.h>
//#include "uds_conf.h"
#include "fm_conf.h"
#include "diag_typedefs.h"

/*
***************************************************************************************************
*    Includes
***************************************************************************************************
*/

/*
***************************************************************************************************
*    Type Defines
***************************************************************************************************
*/
typedef uint16_t (*FrzFrm_Fptr_t) (void);

/*
***************************************************************************************************
*    Defines
***************************************************************************************************
*/

/* Freeze Frame Configuration for UDS Protocol */
/* # of entries in Global snapshot freeze frame config table */
#define FMFF_CONF_UDS_GLBSS_ENTRIES				11

/* # of entries in specific snapshots (UDS Protocol) */
#define FMFF_CONF_UDS_SPECIFIC_01_ENTRIES		1
#define FMFF_CONF_UDS_SPECIFIC_02_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_03_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_04_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_05_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_06_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_07_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_08_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_09_ENTRIES		0
#define FMFF_CONF_UDS_SPECIFIC_10_ENTRIES		0

/* Freeze Frame Configuration for OBD Protocol */
/* # of entries in Global snapshot freeze frame config table */
#define FMFF_CONF_OBD_GLBSS_ENTRIES				11

/* # of entries in specific snapshots */
#define FMFF_CONF_OBD_SPECIFIC_01_ENTRIES		1
#define FMFF_CONF_OBD_SPECIFIC_02_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_03_ENTRIES		3
#define FMFF_CONF_OBD_SPECIFIC_04_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_05_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_06_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_07_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_08_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_09_ENTRIES		0
#define FMFF_CONF_OBD_SPECIFIC_10_ENTRIES		0


/* Freeze Frame Configuration for J1939 Protocol */
/* # of entries in Global snapshot freeze frame config table */
#define FMFF_CONF_J1939_GLBSS_ENTRIES			11

/* # of entries in specific snapshots */
#define FMFF_CONF_J1939_SPECIFIC_01_ENTRIES		1
#define FMFF_CONF_J1939_SPECIFIC_02_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_03_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_04_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_05_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_06_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_07_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_08_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_09_ENTRIES		0
#define FMFF_CONF_J1939_SPECIFIC_10_ENTRIES		0



/*
The function FM_ReadFrzFrm_ByDTC() returns freezeframe for faults in L2, in addition if
the freezeframe from L1 memory is also required, then the below macro can be set to TRUE. 
*/
#define FRZFRM_FROM_FML1    TRUE
/*
**************************************************************************************************
*    Enum definitions
**************************************************************************************************
*/


/*
**************************************************************************************************
*    Structure definitions
**************************************************************************************************
*/



/* 
The Freeze frame structure definition for EOBD faults.
@code tag - @[CODE_FM_FRZFRM_CONFTBL]@{SDD_FM_FRZFRM_CONFTBL}
*/
#if (TRUE == FM_UDS_SUPPORTED)

#define FMFF_CONF_UDS_FRZFRM_GLBSS_BYTES    (20)
#define FMFF_CONF_UDS_FRZFRM_TOTALBYTES     (FMFF_CONF_UDS_FRZFRM_GLBSS_BYTES + 4)  /* Total number of bytes of Global and Specific FF together */

#pragma pack (1)
typedef struct
{
  uint16_t		    DID_u16;   				// DID number.
  uint8_t*		    ParamBuffer_pu8;   		// freeze frame parameter buffer.
  uint16_t          ParamSize_u16;          // parameter buffer length in bytes.
  FrzFrm_Fptr_t     FrzFrm_Fptr;            // Function for freezeframe capture
}FM_FrzFrm_UDS_Data_St_t;



#pragma pack (1)
typedef struct
{
  FM_FaultPath_En_t    	            FM_FaultPath_En;
  const FM_FrzFrm_UDS_Data_St_t* 	FrzFrm_Data_pSt;
  uint16_t			        	    NumOfEntries_u16;
}FM_FrzFrmMasterConfig_UDS_St_t;

extern const FM_FrzFrm_UDS_Data_St_t FM_Global_Snapshot_UDS_aSt[FMFF_CONF_UDS_GLBSS_ENTRIES];
extern const FM_FrzFrmMasterConfig_UDS_St_t	FM_FrzFrmMasterConfig_UDS_aSt[NUM_OF_FAULTPATHS_E];

#endif



#if (TRUE == FM_OBD_SUPPORTED)

#define FMFF_CONF_OBD_FRZFRM_GLBSS_BYTES    (20)
#define FMFF_CONF_OBD_FRZFRM_TOTALBYTES     (FMFF_CONF_OBD_FRZFRM_GLBSS_BYTES + 4)  /* Total number of bytes of Global and Specific FF together */

#pragma pack (1)
typedef struct
{
  uint8_t		    PID_u8;   				// PID number.
  uint8_t*		    ParamBuffer_pu8;   		// freeze frame parameter buffer.
  uint16_t          ParamSize_u16;          // parameter buffer length in bytes.
  FrzFrm_Fptr_t     FrzFrm_Fptr;            // Function for freezeframe capture
}FM_FrzFrm_OBD_Data_St_t;

#pragma pack (1)
typedef struct
{
  FM_FaultPath_En_t    	            FM_FaultPath_En;
  const FM_FrzFrm_OBD_Data_St_t* 	FrzFrm_Data_pSt;
  uint16_t			        	    NumOfEntries_u16;
}FM_FrzFrmMasterConfig_OBD_St_t;

extern const FM_FrzFrm_OBD_Data_St_t        FM_Global_Snapshot_OBD_aSt[FMFF_CONF_OBD_GLBSS_ENTRIES];
extern const FM_FrzFrmMasterConfig_OBD_St_t	FM_FrzFrmMasterConfig_OBD_aSt[NUM_OF_FAULTPATHS_E];

#endif




#if(TRUE == FM_J1939_SUPPORTED)

#define FMFF_CONF_J1939_FRZFRM_GLBSS_BYTES  (20)      /* Total number of bytes of Global FF  */
#define FMFF_CONF_J1939_FRZFRM_TOTALBYTES   (FMFF_CONF_J1939_FRZFRM_GLBSS_BYTES + 3)  /* Total number of bytes of Global and Specific FF together */

#pragma pack (1)
typedef struct
{
  uint32_t		    SPN_u32;   			    // SPN number.
  uint8_t*		    ParamBuffer_pu8;   		// freeze frame parameter buffer.
  uint16_t          ParamSize_u16;          // parameter buffer length in bytes.
  FrzFrm_Fptr_t     FrzFrm_Fptr;            // Function for freezeframe capture
}FM_FrzFrm_J1939_Data_St_t;


#pragma pack (1)
typedef struct
{
  FM_FaultPath_En_t    	            FM_FaultPath_En;
  const FM_FrzFrm_J1939_Data_St_t* 	FrzFrm_Data_pSt;
  uint16_t			        	    NumOfEntries_u16;
}FM_FrzFrmMasterConfig_J1939_St_t;

extern const FM_FrzFrm_J1939_Data_St_t        FM_Global_Snapshot_J1939_aSt[FMFF_CONF_J1939_GLBSS_ENTRIES];
extern const FM_FrzFrmMasterConfig_J1939_St_t FM_FrzFrmMasterConfig_J1939_aSt[NUM_OF_FAULTPATHS_E];


#endif
   
#endif



