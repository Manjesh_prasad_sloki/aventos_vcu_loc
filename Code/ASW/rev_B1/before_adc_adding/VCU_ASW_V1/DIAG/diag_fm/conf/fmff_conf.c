/***************************************************************************************************
 *    FILENAME    :  fmff_conf.c
 *
 *    DESCRIPTION : File contains the configuration table for freeze frames.
 *
 *    $Id         : $    
 *
 ***************************************************************************************************
 * Revision history
 * 
 * Ver Author       Date       Description
 * 1   Sloki          25/09/2018
 ***************************************************************************************************
*/

#include "fmff_conf.h"
#include "diag_adapt.h"
#include <stdlib.h>
#include "fm_conf.h"
/*
 **************************************************************************************************
 *    "CONFIGURATION DATA TO BE FILLED BY APPLICATION USER" 
 *     FREEZE FRAME CONFIGURATION
 **************************************************************************************************
*/

/*
 **************************************************************************************************
    The Freeze frame table for OEM specific faults.
 **************************************************************************************************
*/


#if (TRUE == FM_UDS_SUPPORTED)

/**
* Freeze Frame Parameter Configuration for UDS Specific Protocol
*/
const FM_FrzFrm_UDS_Data_St_t FM_Global_Snapshot_UDS_aSt[FMFF_CONF_UDS_GLBSS_ENTRIES] = 
{
  {0xA000,		(uint8_t*) (GLOBAL_Real_Time_au8	        ), 		3,      NULL},     
  {0xA001,		(uint8_t*) (TOTAL_Distance_Travelled_au8	), 		3,      NULL},
  {0xA002,		(uint8_t*) (ECU_Supply_Voltage_au8		    ), 		1,      NULL},
  {0xA004,		(uint8_t*) (VEHICLE_Interior_Temp_au8		), 		1,      NULL},
  {0xA005,		(uint8_t*) (ABIENT_AIR_Temp_au8			    ), 		1,      NULL},
  {0xA006,		(uint8_t*) (VEHICLE_Power_Mode_au8		    ), 		1,      NULL},
  {0xA007,		(uint8_t*) (ENGINE_Speed_au8				), 		2,      NULL},
  {0xA008,		(uint8_t*) (VEHICLE_Speed_au8				), 		1,      NULL},
  {0xA009,		(uint8_t*) (ENGINE_Coolant_Temp_au8		    ), 		1,      NULL},
  {0xA00A,		(uint8_t*) (Throttle_Position_au8			), 		1,      NULL},
  {0xA00B,		(uint8_t*) (VIN_M_au8						), 		5,      NULL},
};


const FM_FrzFrm_UDS_Data_St_t  FM_FrzFrm_Specific01_UDS_aSt[FMFF_CONF_UDS_SPECIFIC_01_ENTRIES] = 
{
	{0x7203,	(uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		1,      NULL},
};



const FM_FrzFrmMasterConfig_UDS_St_t	FM_FrzFrmMasterConfig_UDS_aSt[NUM_OF_FAULTPATHS_E] = 
{
	{FM_FP_TEST1_E,				FM_FrzFrm_Specific01_UDS_aSt,	   FMFF_CONF_UDS_SPECIFIC_01_ENTRIES  },
	{FM_FP_TEST2_E,		        NULL,    			               FMFF_CONF_UDS_SPECIFIC_02_ENTRIES  },
	{FM_FP_TEST3_E,             NULL,     			               FMFF_CONF_UDS_SPECIFIC_03_ENTRIES  },
	{FM_FP_TEST4_E,		        NULL,    			               FMFF_CONF_UDS_SPECIFIC_04_ENTRIES  },
	{FM_FP_TEST5_E,             NULL,     			               FMFF_CONF_UDS_SPECIFIC_05_ENTRIES  },
	{FM_FP_TEST6_E,	        	NULL,            			       FMFF_CONF_UDS_SPECIFIC_06_ENTRIES  },
	{FM_FP_TEST7_E,	        	NULL,     			               FMFF_CONF_UDS_SPECIFIC_07_ENTRIES  },
	{FM_FP_TEST8_E,             NULL,               	           FMFF_CONF_UDS_SPECIFIC_08_ENTRIES  },
	{FM_FP_TEST9_E,	            NULL,                		       FMFF_CONF_UDS_SPECIFIC_09_ENTRIES  },
	{FM_FP_TEST10_E,            NULL,           			       FMFF_CONF_UDS_SPECIFIC_10_ENTRIES },
	
};


#endif

#if (TRUE == FM_OBD_SUPPORTED)

const FM_FrzFrm_OBD_Data_St_t FM_Global_Snapshot_OBD_aSt[FMFF_CONF_OBD_GLBSS_ENTRIES] = 
{
  {0x00,		(uint8_t*) (GLOBAL_Real_Time_au8	        ), 		3,      NULL},     
  {0x01,		(uint8_t*) (TOTAL_Distance_Travelled_au8	), 		3,      NULL},
  {0x02,		(uint8_t*) (ECU_Supply_Voltage_au8		    ), 		1,      NULL},
  {0x04,		(uint8_t*) (VEHICLE_Interior_Temp_au8		), 		1,      NULL},
  {0x05,		(uint8_t*) (ABIENT_AIR_Temp_au8			    ), 		1,      NULL},
  {0x06,		(uint8_t*) (VEHICLE_Power_Mode_au8		    ), 		1,      NULL},
  {0x07,		(uint8_t*) (ENGINE_Speed_au8				), 		2,      NULL},
  {0x08,		(uint8_t*) (VEHICLE_Speed_au8				), 		1,      NULL},
  {0x09,		(uint8_t*) (ENGINE_Coolant_Temp_au8		    ), 		1,      NULL},
  {0x0A,		(uint8_t*) (Throttle_Position_au8			), 		1,      NULL},
  {0x0B,		(uint8_t*) (VIN_M_au8						), 		5,      NULL},
};

const FM_FrzFrm_OBD_Data_St_t  FM_FrzFrm_Specific01_OBD_aSt[FMFF_CONF_OBD_SPECIFIC_01_ENTRIES] =
{
	{0x03,	    (uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		1,      NULL},
};

const FM_FrzFrm_OBD_Data_St_t  FM_FrzFrm_Specific03_OBD_aSt[FMFF_CONF_OBD_SPECIFIC_03_ENTRIES] =
{
	{0x0C,	    (uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		1,      NULL},
	{0x0D,	    (uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		2,      NULL},
	{0x0E,	    (uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		1,      NULL},
};

const FM_FrzFrmMasterConfig_OBD_St_t	FM_FrzFrmMasterConfig_OBD_aSt[NUM_OF_FAULTPATHS_E] = 
{
	{FM_FP_TEST1_E,				FM_FrzFrm_Specific01_OBD_aSt,	           	    FMFF_CONF_OBD_SPECIFIC_01_ENTRIES  },
	{FM_FP_TEST2_E,		        NULL,    			                            FMFF_CONF_OBD_SPECIFIC_02_ENTRIES  },
	{FM_FP_TEST3_E,             FM_FrzFrm_Specific03_OBD_aSt,     			    FMFF_CONF_OBD_SPECIFIC_03_ENTRIES  },
	{FM_FP_TEST4_E,		        NULL,    			                            FMFF_CONF_OBD_SPECIFIC_04_ENTRIES  },
	{FM_FP_TEST5_E,             NULL,     			                            FMFF_CONF_OBD_SPECIFIC_05_ENTRIES  },
	{FM_FP_TEST6_E,	        	NULL,            			                    FMFF_CONF_OBD_SPECIFIC_06_ENTRIES  },
	{FM_FP_TEST7_E,	        	NULL,     			                            FMFF_CONF_OBD_SPECIFIC_07_ENTRIES  },
	{FM_FP_TEST8_E,             NULL,               	                        FMFF_CONF_OBD_SPECIFIC_08_ENTRIES  },
	{FM_FP_TEST9_E,	            NULL,                		                    FMFF_CONF_OBD_SPECIFIC_09_ENTRIES  },
	{FM_FP_TEST10_E,            NULL,           			                    FMFF_CONF_OBD_SPECIFIC_10_ENTRIES },
	
};


#endif

#if(TRUE == FM_J1939_SUPPORTED)
 
const FM_FrzFrm_J1939_Data_St_t FM_Global_Snapshot_J1939_aSt[FMFF_CONF_J1939_GLBSS_ENTRIES] = 
{
  {0x0000000,		(uint8_t*) (GLOBAL_Real_Time_au8	        ), 		3,      NULL},     
  {0x0010001,		(uint8_t*) (TOTAL_Distance_Travelled_au8	), 		3,      NULL},
  {0x0020002,		(uint8_t*) (ECU_Supply_Voltage_au8		    ), 		1,      NULL},
  {0x0040003,		(uint8_t*) (VEHICLE_Interior_Temp_au8		), 		1,      NULL},
  {0x0050004,		(uint8_t*) (ABIENT_AIR_Temp_au8			    ), 		1,      NULL},
  {0x0060005,		(uint8_t*) (VEHICLE_Power_Mode_au8		    ), 		1,      NULL},
  {0x0070006,		(uint8_t*) (ENGINE_Speed_au8				), 		2,      NULL},
  {0x0080007,		(uint8_t*) (VEHICLE_Speed_au8				), 		1,      NULL},
  {0x0090008,		(uint8_t*) (ENGINE_Coolant_Temp_au8		    ), 		1,      NULL},
  {0x00A0009,		(uint8_t*) (Throttle_Position_au8			), 		1,      NULL},
  {0x00B000A,		(uint8_t*) (VIN_M_au8						), 		5,      NULL},
};

const FM_FrzFrm_J1939_Data_St_t  FM_FrzFrm_Specific01_J1939_aSt[FMFF_CONF_J1939_SPECIFIC_01_ENTRIES] = 
{
	{0x00B000C,	(uint8_t*) (CNTRL_MODULE_Config_Type_au8),  		1,      NULL},
};



const FM_FrzFrmMasterConfig_J1939_St_t	FM_FrzFrmMasterConfig_J1939_aSt[NUM_OF_FAULTPATHS_E] = 
{
	{FM_FP_TEST1_E,				FM_FrzFrm_Specific01_J1939_aSt,	                FMFF_CONF_J1939_SPECIFIC_01_ENTRIES  },
	{FM_FP_TEST2_E,		        NULL,    			                            FMFF_CONF_J1939_SPECIFIC_02_ENTRIES  },
	{FM_FP_TEST3_E,             NULL,     			                            FMFF_CONF_J1939_SPECIFIC_03_ENTRIES  },
	{FM_FP_TEST4_E,		        NULL,    			                            FMFF_CONF_J1939_SPECIFIC_04_ENTRIES  },
	{FM_FP_TEST5_E,             NULL,     			                            FMFF_CONF_J1939_SPECIFIC_05_ENTRIES  },
	{FM_FP_TEST6_E,	        	NULL,            			                    FMFF_CONF_J1939_SPECIFIC_06_ENTRIES  },
	{FM_FP_TEST7_E,	        	NULL,     			                            FMFF_CONF_J1939_SPECIFIC_07_ENTRIES  },
	{FM_FP_TEST8_E,             NULL,               	                        FMFF_CONF_J1939_SPECIFIC_08_ENTRIES  },
	{FM_FP_TEST9_E,	            NULL,                		                    FMFF_CONF_J1939_SPECIFIC_09_ENTRIES  },
	{FM_FP_TEST10_E,            NULL,           			                    FMFF_CONF_J1939_SPECIFIC_10_ENTRIES },
	
};
#endif


