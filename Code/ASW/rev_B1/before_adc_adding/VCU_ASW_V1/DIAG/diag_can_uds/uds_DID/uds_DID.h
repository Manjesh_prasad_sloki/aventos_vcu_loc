/******************************************************************************
 *    FILENAME    : uds_DID.h
 *    DESCRIPTION : Header file for UDS service - READ DATA BY IDENTIFIER(0x22) and for Service Write Data By Identifier(0x2E).
 ******************************************************************************
 * Revision history
 *  
 * Ver Author          Date                     Description
 * 1   Sloki     10/01/2019		   Initial version
 ******************************************************************************
*/  

#ifndef UDS_DID_H_
#define UDS_DID_H_


#ifdef	__cplusplus
 "C" {
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "uds_conf.h"
#include "iso14229_serv2E.h"
#include "iso14229_serv22.h"
#include "iso14229_serv2C.h"


  
#define ENGINEERING_SESSION(current_active_session_u8)          		(current_active_session_u8 - 0x5C) 
#define SUPPLIER_EOL_SESS_PERMISSION(current_active_session_u8)         (current_active_session_u8 - 0x61) 
  
  

  
/**
*  FUNCTION NAME : DID_Comm_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests.
*  @return       : Type of response.                     
*/
extern bool DID_Comm_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_Odo_4666_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4666 DID.
*  @return       : Type of response.                     
*/
//extern bool DID_Odo_4666_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_TyreSize_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4666 DID.
*  @return       : Type of response.                     
*/
//extern bool DID_TyreSize_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_PowerTrain_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 4662 DID.
*  @return       : Type of response.                     
*/
//extern bool DID_PowerTrain_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);


/**
*  FUNCTION NAME : DID_DTCMask_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Bitmasked DID.
*  @return       : Type of response.                     
*/
extern bool DID_DTCMask_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_ReadDTCMask_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 722D DID.
*  @return       : Type of response.                     
*/
extern bool DID_ReadDTCMask_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_ClockSett_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of 464E DID.
*  @return       : Type of response.                     
*/
extern bool DID_ClockSett_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : DID_Numeric_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Numeric DID.
*  @return       : Type of response.                     
*/
extern bool DID_Numeric_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);

/**
*  FUNCTION NAME : Inhale_Exhale_Fn
*  FILENAME      : uds_DID.c
*  @param        : uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En.
*  @brief        : This function will process the service_22 and Service 2E requests of Inhale Exhale DID.
*  @return       : Type of response.                     
*/
//extern bool Inhale_Exhale_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En);
//extern uint16_t Inhale_Exhale_Size(void);

/**
*  FUNCTION NAME : DID_Session_Check
*  FILENAME      : uds_DID.c
*  @param        : Index_u32
*  @brief        : function to Session_Check is valid or not
*  @return       : none.                     
*/
extern bool DID_Session_Check(uint32_t Index_u32, uint8_t Bit_pos_u8);




#endif
