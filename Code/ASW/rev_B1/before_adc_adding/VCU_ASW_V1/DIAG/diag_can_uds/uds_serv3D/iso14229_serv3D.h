/******************************************************************************
 *    FILENAME    : uds_serv3D.h
 *    DESCRIPTION : Header file for UDS service - Write Memory By Address.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

#ifndef _UDS_SERV3D_H_
#define	_UDS_SERV3D_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.*/
#include "uds_conf.h"
#include "iso14229_serv27.h"
#include "fee_adapt.h"
  
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* @summary: memory address maximum length */
#define MEM_ADDRESS_MAX_LEN                                 0x04u

/* @summary: memory size maximum length */
#define MEM_SIZE_MAX_LEN                                    0x02u

/* @summary: length size */
#define SERV_3D_MIN_LEN                                     0x05u

    
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Data Types                                                        */
/* ************************************************************************** */
/* ************************************************************************** */

/* @summary: this variable is for which block is selected */
extern  uint8_t AddrBlockNum_u8;


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : MemoryAddr_Check
*  FILENAME      : uds_serv3D.c
*  @param        : const uint32_t memoryAddress_u32 - 32 bit memory address.
*  @param        : uint16_t memorySize_u16 - 16 bit memory size.
*  @brief        : This function will check the start and end address is valid or not.
*  @return       : which block is selected.
**/
extern  uint8_t MemoryAddr_Check(const uint32_t , uint16_t );

/** 
*  FUNCTION NAME : iso14229_serv3D
*  FILENAME      : iso14229_serv3D.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_3D requests
*  @return       : Type of response.
**/
extern  UDS_Serv_resptype_En_t iso14229_serv3D(UDS_Serv_St_t* );

/**
*  FUNCTION NAME : UDS_Serv3D_Timeout
*  FILENAME      : uds_serv3D.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
**/
extern  void UDS_Serv3D_Timeout(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ISO14229_SER3D_H */
