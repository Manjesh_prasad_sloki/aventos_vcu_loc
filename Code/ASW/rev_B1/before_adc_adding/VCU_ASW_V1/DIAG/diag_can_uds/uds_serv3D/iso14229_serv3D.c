/******************************************************************************
 *    FILENAME    : uds_serv3D.c
 *    DESCRIPTION : Service description for UDS service - Write Memory By Address.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv3D.h"
#include "diag_typedefs.h"
#include "diag_adapt.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* @summary: this global variable is for delay */
uint32_t DelayValue_u32 = 0u;

/* @summary: this global variable is for which block is selected */
uint8_t AddrBlockNum_u8 = 0u;
        
/* @summary: this is a global variable is for checking general programming failure or not */
bool IsGeneralProgFailed_b = TRUE;


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : MemoryAddr_Check
*  FILENAME      : uds_serv3D.c
*  @param        : const uint32_t memoryAddress_u32 - 32 bit memory address.
*  @param        : uint16_t memorySize_u16 - 16 bit memory size.
*  @brief        : This function will check the start and end address is valid or not.
*  @return       : which block is selected.
**/
uint8_t MemoryAddr_Check(const uint32_t memoryAddress_u32, uint16_t memorySize_u16)
{
    uint32_t i = 0u;
    uint32_t end_Addr_u32 = memoryAddress_u32 + memorySize_u16/2 - 1;
    uint8_t block_value_u8 = 0u;
    for(i = 0 ; i < MEM_ARRAY_ROW_SIZE ; i++)
    {
        if((MemoryArray_config[i][0] <= memoryAddress_u32) && (MemoryArray_config[i][1] >= memoryAddress_u32))
        {
            if((MemoryArray_config[i][0] <= end_Addr_u32) && (MemoryArray_config[i][1] >= end_Addr_u32))
            {
                block_value_u8 = i+1;
                break;
            }
        }
    }
	if((block_value_u8 == 1) && (UDS_GetCurrentSession() != SUPPLIER_EOL_SESSION_SUB_ID_E))
	{
		block_value_u8 = 0u;
	}
    return block_value_u8;
}

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : iso14229_serv3D
*  FILENAME      : iso14229_serv3D.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_3D requests
*  @return       : Type of response.
**/
UDS_Serv_resptype_En_t iso14229_serv3D(UDS_Serv_St_t*  UDS_Serv_pSt)
{
    uint32_t i = 0u;
    uint32_t memoryAddress_u32 = 0x00u;
    uint32_t memoryAddress_copy_u32 = 0u;
//    uint16_t memoryData_u16[0xFF];
    uint16_t numbytes_req_u16 = UDS_Serv_pSt->RxLen_u16;
    uint16_t memorySize_u16 = 0x00u;
//    uint16_t data_count_u16 = 0u;
    uint8_t format_identifier_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    uint8_t memoryAddress_len_u8 = format_identifier_u8 & 0x0F;
    uint8_t memorySize_len_u8 = ((format_identifier_u8 & 0xF0) >> 4);
    UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
	uint8_t nvm_busy_flag;
    
    for(i = 0 ; i < memoryAddress_len_u8 ; i++) /* Memory Address */
    {
        memoryAddress_u32 = ((memoryAddress_u32 << 8) | UDS_Serv_pSt->RxBuff_pu8[i+2]);
    }
    memoryAddress_copy_u32 = memoryAddress_u32;
    for(i = memoryAddress_len_u8 ; i < (memoryAddress_len_u8 + memorySize_len_u8) ; i++) /* Memory Size */
    {
        memorySize_u16 = ((memorySize_u16 << 8) | UDS_Serv_pSt->RxBuff_pu8[i+2]);
    }
   
    AddrBlockNum_u8 = MemoryAddr_Check(memoryAddress_u32, memorySize_u16);
    
    if(SERV_3D_MIN_LEN > numbytes_req_u16)
    {
        /* Check whether length should be valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_WMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH; /* 0x13 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if((MEM_ADDRESS_MAX_LEN < memoryAddress_len_u8) || (MEM_SIZE_MAX_LEN < memorySize_len_u8))
    {
        /* Check whether the memory address length and memory size length is valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_WMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if(!AddrBlockNum_u8)
    {
        /* Check whether the memory start and end address is valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_WMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if(FALSE == Check_SecurityClearance())
    {
        /* check whether security access should be locked or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_WMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED; /* 0x33 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {      
//        for(i = (memoryAddress_len_u8 + memorySize_len_u8 + 2) , data_count_u16 = 0 ; i < (numbytes_req_u16 - 1) ; data_count_u16++)
//        {
//            memoryData_u16[data_count_u16] = UDS_Serv_pSt->RxBuff_pu8[i];
//            memoryData_u16[data_count_u16] = memoryData_u16[data_count_u16] | (UDS_Serv_pSt->RxBuff_pu8[i + 1] << 8);
//            //memoryData_u16[data_count_u16] = memoryData_u16[data_count_u16] | (UDS_Serv_pSt->RxBuff_pu8[i + 2] << 16);
//            //memoryData_u16[data_count_u16] = memoryData_u16[data_count_u16] | (UDS_Serv_pSt->RxBuff_pu8[i + 3] << 24);
//            i = i + 2;
//        }
//        for(i = 0 ; i < data_count_u16 ; i++)
        for(i = (memoryAddress_len_u8 + memorySize_len_u8 + 2) ; i < (numbytes_req_u16 - 1) ; i += 2)	/* Write step size increased to one word	*/
        {
            //WriteFlash_Adapt(memoryAddress_u32, UDS_Serv_pSt->RxBuff_pu8[i]); /* writing in to flash memory*/
            //for(DelayValue_u32 = 0; DelayValue_u32 < 1440; DelayValue_u32++); /* Delay for write operation to complete*/
            nvm_busy_flag = WriteNVM_3DReq(memoryAddress_u32,((UDS_Serv_pSt->RxBuff_pu8[i] << 8)|UDS_Serv_pSt->RxBuff_pu8[i+1]));	/* Writing in EEPROM	*/
			//if(FALSE == WRITE_STATUS_Adapt())
            if(FALSE == nvm_busy_flag)
			{
                memoryAddress_u32 = memoryAddress_u32 + 1;
            }
            else
            {
                IsGeneralProgFailed_b = FALSE;
				break;	/* Added for NVM Busy	*/
            }
        }
	//	NvmmgrSetReq(u2_REQ_3D_PARAM, ON); // todo harsh
        if(TRUE == IsGeneralProgFailed_b)
        {
            UDS_Serv_pSt->TxLen_u16 = 6u;
            UDS_Serv_pSt->TxBuff_pu8[1] = format_identifier_u8;
            UDS_Serv_pSt->TxBuff_pu8[2] = ((memoryAddress_copy_u32 & 0xFF000000) >> 24);
            UDS_Serv_pSt->TxBuff_pu8[3] = ((memoryAddress_copy_u32 & 0x00FF0000) >> 16);
            UDS_Serv_pSt->TxBuff_pu8[4] = ((memoryAddress_copy_u32 & 0x0000FF00) >> 8);
            UDS_Serv_pSt->TxBuff_pu8[5] =  (memoryAddress_copy_u32 & 0x000000FF);
            UDS_Serv_pSt->TxBuff_pu8[6] = memorySize_u16;
            Serv_resptype_En = UDS_SERV_RESP_POS_E;
        }
        else
        {
            UDS_Serv_pSt->TxLen_u16 = 3u;
            UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
            UDS_Serv_pSt->TxBuff_pu8[1] = SID_WMBA;
            UDS_Serv_pSt->TxBuff_pu8[2] = GEN_PROG_FAILURE; /* 0x72 */
            Serv_resptype_En = UDS_SERV_RESP_NEG_E;
        }
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : UDS_Serv3D_Timeout
*  FILENAME      : uds_serv3D.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv3D_Timeout(void)
{
    return;
}

/* *****************************************************************************
 End of File
*/
