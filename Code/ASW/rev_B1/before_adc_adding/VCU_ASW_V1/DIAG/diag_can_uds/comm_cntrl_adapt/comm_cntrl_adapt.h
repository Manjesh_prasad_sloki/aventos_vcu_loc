/******************************************************************************
 *    FILENAME    : comm_cntrl_adapt.h
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

#ifndef _COMM_CONTROL_ADAPT_H_
#define _COMM_CONTROL_ADAPT_H_



#include "uds_conf.h"

 void NormalCommMsg_Adapt(bool ,  bool );

 void NWMngmntCommMsg_Adapt(bool ,  bool );

 extern void NWMngmnt_NormalCommMsg_Adapt(bool ,  bool );

 bool Get_IVN_Tx_Sts(void);

 bool Get_IVN_Rx_Sts(void);
 
 
 #endif
 
 