/*
 *******************************************************************************************
 *                                                                                         *
 *    FILENAME    : uds_serv2F.c                                                           *
 *    DESCRIPTION : Service description for UDS service - Input Output Control Parameter.  *
 *******************************************************************************************     
 * Revision history                                                                        *            
 *                                                                                         *
 * Ver Author       Date               Description                                         *
 * 1   Sloki     1/10/2019		   Initial version                                     *         
 *******************************************************************************************
*/ 


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */


#include "iso14229_serv2F.h"
#include "diag_typedefs.h"
#include "diag_adapt.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */
#define SET_BUTTON_PRESS_STS()			u1_GetSetButtonPressSts()
#define MODE_BUTTON_PRESS_STS()			u1_GetModeButtonPressSts()


uint32_t        IO_Index_u32;
uint8_t         FORCE_value_u8[SHORT_TERM_ADJUSTMENT_LEN];
UDS_Serv_St_t*  EOL_UDS_serv;
/*
 @summary :- This IO_Index_u32 used to store the index value.*/



   
 /**
*  FUNCTION NAME : IS_Request_outof_range
*  FILENAME      : uds_serv2F.c
*  @param        : DID_id_u16 
*  @brief        : This function will check Data identifier is valid or not  
*  @return       : Type of response.                     
*/
 
bool IS_Request_outof_range(uint16_t DID_id_u16 );


/**
*  FUNCTION NAME : UDS_Serv2F_Timeout
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/

void UDS_Serv2F_Timeout(void);



/**
*  FUNCTION NAME : UDS_Serv2F_Init
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function to reset mode to false
*  @return       : none.                     
*/
void UDS_Serv2F_Init();




/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */


 /**
*  FUNCTION NAME : IS_Request_outof_range
*  FILENAME      : uds_serv2F.c
*  @param        : DID_id_u16 variable to store DIDs
*  @brief        : This function will check Data identifier is valid or not 
*  @return       : Type of response.                     
*/
 
bool IS_Request_outof_range(uint16_t DID_id_u16 )
{
    uint32_t i;
    bool Response_b =false;
    for(i=0 ;i< DID_TOTAL_E ;i++)
    {
        if(DID_Config_aSt[i].DID_Num== DID_id_u16)
        {
         IO_Index_u32 =i;
         Response_b =true; 
        }
            
    }
    return Response_b;
}

/***********stimulus******************/

/**
*  FUNCTION NAME : UDS_Serv2F_Init
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function to reset all DIDs mode to false
*  @return       : none.                     
*/

void UDS_Serv2F_Init()
{
     uint32_t i;
  
    for(i=0 ;i< DID_TOTAL_E ;i++)
    {
        
       //   ResetStimuls(DID_Config_aSt[i].DID_Name_En);
            
    }
}




/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */


 /**
*  FUNCTION NAME : iso14229_serv2F
*  FILENAME      : iso14229_serv2F.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_2f requests
*  @return       : Type of response.                     
*/
 
UDS_Serv_resptype_En_t iso14229_serv2F (UDS_Serv_St_t* UDS_Serv_pSt)

{
	uint16_t numbytes_req_u16 = UDS_Serv_pSt->RxLen_u16;
	uint16_t DID_id_u16 = (UDS_Serv_pSt->RxBuff_pu8[2] | (UDS_Serv_pSt->RxBuff_pu8[1]<<8));
	/*IO_Control Parameter*/
	uint8_t  Control_u8 =UDS_Serv_pSt->RxBuff_pu8[3];
	
	/*FORCE_value_u32 to store data from the short term adjustment */
	uint32_t i=0;
	bool Request_outof_range_u8=IS_Request_outof_range(DID_id_u16);
	
	
	/*To store default size of DIDs*/
	uint8_t DID_val_Size_u8 = DID_Config_aSt[IO_Index_u32].DID_Val_Size;
	
	/*IO Bit Position*/
	uint8_t IO_Bit_pos_u8=IO_BIT_POSITION;
	
	/*function to Session_Check is valid or not*/
	bool Did_session_b = DID_Session_Check(IO_Index_u32, IO_Bit_pos_u8);
	UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
	
	
	EOL_UDS_serv = UDS_Serv_pSt;
	/* numbytes_req_u16 to hold number of bytes received */
	
	for(i=0;i<SHORT_TERM_ADJUSTMENT_LEN;i++)
	{
		FORCE_value_u8[i] = UDS_Serv_pSt->RxBuff_pu8[4+i];
	}
	
	/* To check request out of range */
	
	/*Invalid message length */
	if(numbytes_req_u16 < MESSAGE_LEN)
		{
			
		UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
		UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
		UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;       /* NRC : 0x13 *//*Invalid message length */
		UDS_Serv_pSt->TxLen_u16 = 3;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}
	else if((FALSE == Check_SecurityClearance()) && (SUPPLIER_EOL_SESSION_SUB_ID_E != UDS_GetCurrentSession()))
   	{
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
		UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
		UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;
		UDS_Serv_pSt->TxLen_u16 = 3;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
	/*request out of range of DIDs */ 
	else if(!Request_outof_range_u8 )
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE ;
			UDS_Serv_pSt->TxLen_u16 = 3;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		} 
	
	/* DIDs session*/ 
	else if(!Did_session_b  )
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
			UDS_Serv_pSt->TxBuff_pu8[2] = CONDITION_NOT_CORRECT;
			UDS_Serv_pSt->TxLen_u16 = 3;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		} 
	
	else
		{
	//  ResetStimuls(DID_Config_aSt[IO_Index_u32].DID_name);
		/* Control option record */
		switch(Control_u8)
		{
      case RETURN_CONTROL_TO_ECU:
      {
        UDS_Serv_pSt->TxBuff_pu8[1]= UDS_Serv_pSt->RxBuff_pu8[1];
        UDS_Serv_pSt->TxBuff_pu8[2]= UDS_Serv_pSt->RxBuff_pu8[2];
        UDS_Serv_pSt->TxBuff_pu8[3]= UDS_Serv_pSt->RxBuff_pu8[3];
        /*ResetStimuls*/
     //   ResetStimuls(DID_Config_aSt[IO_Index_u32].DID_Name_En);
        /*Call_back_Fn to store data to the buffer */
        DID_Config_aSt[IO_Index_u32].CALLBCK_Fptr(&UDS_Serv_pSt->TxBuff_pu8[4], DID_val_Size_u8, READ_ONLY_E,DID_Config_aSt[IO_Index_u32].Var_ptr_pu8,DID_Config_aSt[IO_Index_u32].Type_En);     
        UDS_Serv_pSt->TxLen_u16 = 3+DID_val_Size_u8;
        Serv_resptype_En = UDS_SERV_RESP_POS_E;
      break;
      }
    
      case RESET_TO_DEFAULT :
      {
      UDS_Serv_pSt->TxBuff_pu8[1]= UDS_Serv_pSt->RxBuff_pu8[1];
      UDS_Serv_pSt->TxBuff_pu8[2]= UDS_Serv_pSt->RxBuff_pu8[2];
      UDS_Serv_pSt->TxBuff_pu8[3]= UDS_Serv_pSt->RxBuff_pu8[3];
      /*SetStimuls*/
     // SetStimuls(DID_Config_aSt[IO_Index_u32].DID_Name_En,DID_Config_aSt[IO_Index_u32].STM_st->Default_Val_pu8); //todo harsh
      /*Call_back_Fn to store data to the buffer */
      DID_Config_aSt[IO_Index_u32].CALLBCK_Fptr(&UDS_Serv_pSt->TxBuff_pu8[4], DID_val_Size_u8, READ_ONLY_E,DID_Config_aSt[IO_Index_u32].Var_ptr_pu8,DID_Config_aSt[IO_Index_u32].Type_En);  
      UDS_Serv_pSt->TxLen_u16 = 3+DID_val_Size_u8;
      Serv_resptype_En = UDS_SERV_RESP_POS_E;
      break;
      }
    
      case FREEZE_CURRNT_STATE:
      {
        UDS_Serv_pSt->TxBuff_pu8[1]= UDS_Serv_pSt->RxBuff_pu8[1];
        UDS_Serv_pSt->TxBuff_pu8[2]= UDS_Serv_pSt->RxBuff_pu8[2];
        UDS_Serv_pSt->TxBuff_pu8[3]= UDS_Serv_pSt->RxBuff_pu8[3];   
        /*SetStimuls*/
        //SetStimuls(DID_Config_aSt[IO_Index_u32].DID_Name_En, &UDS_Serv_pSt->TxBuff_pu8[4]);
        /*******Call_back_Fn to store data to the buffer ********/
        DID_Config_aSt[IO_Index_u32].CALLBCK_Fptr(&UDS_Serv_pSt->TxBuff_pu8[4], DID_val_Size_u8, READ_ONLY_E,DID_Config_aSt[IO_Index_u32].Var_ptr_pu8,DID_Config_aSt[IO_Index_u32].Type_En);  
        UDS_Serv_pSt->TxLen_u16 = 3+DID_val_Size_u8;
        Serv_resptype_En = UDS_SERV_RESP_POS_E;
      break;
      }
    
      case SHORT_TERM_ADJUSTMENT:
      {
        UDS_Serv_pSt->TxBuff_pu8[1]= UDS_Serv_pSt->RxBuff_pu8[1];
        UDS_Serv_pSt->TxBuff_pu8[2]= UDS_Serv_pSt->RxBuff_pu8[2];
        UDS_Serv_pSt->TxBuff_pu8[3]= UDS_Serv_pSt->RxBuff_pu8[3];
        /*SetStimuls*/
        //SetStimuls(DID_Config_aSt[IO_Index_u32].DID_Name_En, FORCE_value_u8);
        /*******Call_back_Fn to store data to the buffer ********/
        DID_Config_aSt[IO_Index_u32].CALLBCK_Fptr(&UDS_Serv_pSt->TxBuff_pu8[4], DID_val_Size_u8, READ_ONLY_E,DID_Config_aSt[IO_Index_u32].Var_ptr_pu8,DID_Config_aSt[IO_Index_u32].Type_En);  
        UDS_Serv_pSt->TxLen_u16 = 3+DID_val_Size_u8;
        Serv_resptype_En = UDS_SERV_RESP_POS_E;
      break;
      }
      default :
      {
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
        UDS_Serv_pSt->TxLen_u16 = 3;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
      }    
		}		
	}
//todo harsh
 // if(u1_Get_EOL_state() == EOL_SET_CHK)
  {
   // if(SET_BUTTON_PRESS_STS() == ON)
    {
      UDS_Serv_pSt->TxBuff_pu8[0] = UDS_Serv_pSt->RxBuff_pu8[1];
      UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
      UDS_Serv_pSt->TxBuff_pu8[2] = 10;
      UDS_Serv_pSt->TxLen_u16 = 3;
      Serv_resptype_En = UDS_SERV_RESP_POS_E;
    }
  }

 // if(u1_Get_EOL_state() == EOL_MODE_CHK)
  {
   // if(MODE_BUTTON_PRESS_STS() == ON)
    {
      UDS_Serv_pSt->TxBuff_pu8[0] = UDS_Serv_pSt->RxBuff_pu8[1];
      UDS_Serv_pSt->TxBuff_pu8[1] = SID_IOCBDID;
      UDS_Serv_pSt->TxBuff_pu8[2] = 11;
      UDS_Serv_pSt->TxLen_u16 = 3;
      Serv_resptype_En = UDS_SERV_RESP_POS_E;
    }
  }
  
  return Serv_resptype_En; 
}


/**
*  FUNCTION NAME : Temp_Proc_10ms
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function to assign forcevalue or realvalue to global variable 
*  @return       : none.                     
*/
void Temp_Proc_10ms()	/* Not used. Stimulus set in 22 service */
{
  uint8_t *RTV_Ptr_pu8 = DID_Config_aSt[IO_Index_u32].Real_Time_Val_u8;
  UDS_Stimulus(DID_Config_aSt[IO_Index_u32].DID_Name_En, RTV_Ptr_pu8, DID_Config_aSt[IO_Index_u32].Var_ptr_pu8);
}

/**
*  FUNCTION NAME : UDS_Serv2F_Timeout
*  FILENAME      : uds_serv2F.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv2F_Timeout(void){

	return;
}


/*For EOL functionality*/
bool EOL_Cmd_Fn(uint8_t *dataframe, uint32_t size, uint8_t Access_Type, uint8_t *Var_Ptr_u8, DID_Value_Status_En_t DID_categary_En)
{
	vd_EOL_Cmd((U2)((EOL_UDS_serv->RxBuff_pu8[4]<<8) | EOL_UDS_serv->RxBuff_pu8[5]));
	if(TRUE == EOL_Key_NoTimeout()) {
	//	vd_EOL_NVM_Key((U4)(EOL_UDS_serv->RxBuff_pu8[4]<<24) |(EOL_UDS_serv->RxBuff_pu8[5]<<16) | (EOL_UDS_serv->RxBuff_pu8[6]<<8) | (EOL_UDS_serv->RxBuff_pu8[7]));
		vd_EOL_NVM_Key((U2)(EOL_UDS_serv->RxBuff_pu8[4]<<8) |(EOL_UDS_serv->RxBuff_pu8[5]) );
		} else {
		;
	}
	return TRUE;
}

/*******************************************************************************
 End of File
 */
