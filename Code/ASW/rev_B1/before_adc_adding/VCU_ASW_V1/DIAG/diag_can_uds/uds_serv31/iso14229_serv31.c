/******************************************************************************
 *    FILENAME    : uds_serv31.c
 *    DESCRIPTION : Header file for UDS service - Diagnostic Session Control.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Arun	       20/06/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv31.h"
#include "uds_conf.h"
#include "diag_typedefs.h"
#include "diag_adapt.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

uint8_t RoutineResult_au32[TOTAL_ROUTINES_E];
uint8_t Serv31_Routine_flag = FALSE;

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

UDS_Serv_resptype_En_t iso14229_serv31(UDS_Serv_St_t*  UDS_Serv_pSt)  
{
//	int16_t response_length_s16 	= 0x00u ;
	uint16_t numbytes_req_u16 		= UDS_Serv_pSt->RxLen_u16;
	uint16_t Routine_id_u16    		= 0x0000u ;
	uint8_t SubFn_ID = 0u;
	Serv31_RoutineList_En_t index_u8 = SERV31_SERVICE_INFO_RESET_E;
	uint8_t Serv31_Index_u8 = 0u;
	uint8_t NegResp_u8 = UDS_SERV_RESP_UNKNOWN_E;
	bool Routine_found_u8 = FALSE;
	UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
	
	
	SubFn_ID = UDS_Serv_pSt->RxBuff_pu8[Serv31_Index_u8+1];
	Routine_id_u16 = (UDS_Serv_pSt->RxBuff_pu8[Serv31_Index_u8+2]<<8);
	Routine_id_u16 = (Routine_id_u16 | UDS_Serv_pSt->RxBuff_pu8[Serv31_Index_u8+3]);
	
	for(index_u8 = SERV31_SERVICE_INFO_RESET_E; index_u8 < TOTAL_ROUTINES_E ; index_u8++ ) {
		if(Service31_ServIDaSt[index_u8].SERVICE_ID == Routine_id_u16) {
			Routine_found_u8 = TRUE;
			break;
		}
	}
	
	
	if(numbytes_req_u16 < SERV31_MIN_LEN)
	{
		NegResp_u8 = INVALID_MESSAGE_LENGTH;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}
	else if(UDS_GetCurrentSession() != EXTENDED_DIAG_SESSION_SUB_ID_E) 
	{
		NegResp_u8 = SERVNOSUPP_IN_ACTIVE_SESS;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}
	else if(FALSE == Check_SecurityClearance())
	{
        NegResp_u8 = SECURITY_ACCESS_DENIED;
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
	else if(Serv31_PreconditionCheck() == (uint8_t)FAIL)
	{
		NegResp_u8 = CONDITION_NOT_CORRECT;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}
	else if(FALSE == Routine_found_u8)
	{	
		NegResp_u8 = REQUEST_OUT_OF_RANGE;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}
	else if(SubFn_ID == 0x01)
	{
		if(TRUE == Serv31_Routine_flag) 
		{
			NegResp_u8 = REQUEST_SEQUENCE_ERR;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
		else 
		{
			Serv31_Routine_flag = TRUE;
			RoutineResult_au32[index_u8] = SERV31_ROUTINE_INPROGRESS;
			Service31_ServIDaSt[index_u8].ServID_Fptr_t();
			UDS_Serv_pSt->TxBuff_pu8[0] = UDS_Serv_pSt->RxBuff_pu8[0];
			UDS_Serv_pSt->TxBuff_pu8[1] = UDS_Serv_pSt->RxBuff_pu8[1];
			UDS_Serv_pSt->TxBuff_pu8[2] = UDS_Serv_pSt->RxBuff_pu8[2];
			UDS_Serv_pSt->TxBuff_pu8[3] = UDS_Serv_pSt->RxBuff_pu8[3];
			UDS_Serv_pSt->TxBuff_pu8[4] = 0x08;
			UDS_Serv_pSt->TxLen_u16 = 4;
			Serv_resptype_En = UDS_SERV_RESP_POS_E;
		}
	}
	else if(0x02 == SubFn_ID)
	{
		if(SERV31_ROUTINE_INPROGRESS != RoutineResult_au32[index_u8])
		{
			NegResp_u8 = REQUEST_SEQUENCE_ERR;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
		else
		{
			Serv31_Routine_Complete(index_u8);
			UDS_Serv_pSt->TxBuff_pu8[0] = UDS_Serv_pSt->RxBuff_pu8[0];
			UDS_Serv_pSt->TxBuff_pu8[1] = UDS_Serv_pSt->RxBuff_pu8[1];
			UDS_Serv_pSt->TxBuff_pu8[2] = UDS_Serv_pSt->RxBuff_pu8[2];
			UDS_Serv_pSt->TxBuff_pu8[3] = UDS_Serv_pSt->RxBuff_pu8[3];
			UDS_Serv_pSt->TxLen_u16 = 4;
			Serv_resptype_En = UDS_SERV_RESP_POS_E;
		}
	}
	else if(0x03 == SubFn_ID)
	{
		if(SERV31_ROUTINE_NOTSTARTED == RoutineResult_au32[index_u8])
		{
			NegResp_u8 = REQUEST_SEQUENCE_ERR;
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
		else
		{
			UDS_Serv_pSt->TxBuff_pu8[0] = UDS_Serv_pSt->RxBuff_pu8[0];
			UDS_Serv_pSt->TxBuff_pu8[1] = UDS_Serv_pSt->RxBuff_pu8[1];
			UDS_Serv_pSt->TxBuff_pu8[2] = UDS_Serv_pSt->RxBuff_pu8[2];
			UDS_Serv_pSt->TxBuff_pu8[3] = UDS_Serv_pSt->RxBuff_pu8[3];
			UDS_Serv_pSt->TxBuff_pu8[4] = RoutineResult_au32[index_u8];
			UDS_Serv_pSt->TxLen_u16 = 4;
			Serv_resptype_En = UDS_SERV_RESP_POS_E;
		}
	}
	else
	{
		NegResp_u8 = SUB_FUNC_NOT_SUPPORTED;
		Serv_resptype_En = UDS_SERV_RESP_NEG_E;
	}  
	
	if(UDS_SERV_RESP_NEG_E == Serv_resptype_En)
	{
		UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
		UDS_Serv_pSt->TxBuff_pu8[1] = 0x31;
		UDS_Serv_pSt->TxBuff_pu8[2] = NegResp_u8;
		UDS_Serv_pSt->TxLen_u16 = 3;
	}
	
	return Serv_resptype_En ; 
}


void ServiceInfoReset_Routine(void)
{
	Serv31_Routine_Complete(SERV31_SERVICE_INFO_RESET_E);
}

void DispAllWarning_TXT_Routine(void)
{
	//todo harsh vd_SetTXTDsp_DiagRoutine();
}


void SetGaugeMAX_Routine(void)
{
	// todo harsh Start_GaugeMaximum_3F02();
}

void SetGaugeMIN_Routine(void)
{
	//todo harsh Start_GaugeMinimum_3F03();
}


void SetWarningLampOFF_Routine(void)
{
	Serv31_Routine_Complete(SERV31_SET_WARN_LAMP_OFF_E);
}


void SetWarningLampON_Routine(void)
{
	// todo harsh Start_WarningLampOn_3F05();
}


void AutomaticLearning_Routine(void)
{
	// todo harsh Start_AutoConfig_3F06();
}


void OdometerReset_Routine(void)
{
	//TODO Harsh ResetOdometer_3F07();
} 

uint8_t Serv31_PreconditionCheck(void)
{
	uint8_t ret_prechech;
	
	ret_prechech = (uint8_t)FAIL;
	
	//if(System_vd_IgStatus() == (uint8_t)TRUE) 
    {
		//if(u4_GetSpeed_Disp() == 0)
      {
		//	if(stcGetEngSpd() == 0) 
          {
			//	if(u1_GetBattVoltage() >= 90)
              {
					ret_prechech = (uint8_t)PASS;
				}
			}
		}
	}
	
	return (uint8_t)ret_prechech;
}

void Serv31_Routine_Complete(Serv31_RoutineList_En_t Routine_Index_t)
{
	//TT_DiagRoutine_Exit();
	//TXT_DiagRoutine_Exit();
	RoutineResult_au32[Routine_Index_t] = SERV31_ROUTINE_COMPLETED;
	Serv31_Routine_flag = FALSE;
}

void ClearRoutineResult(void)
{
	uint8_t index_u8;
	for(index_u8 = 0; index_u8 < TOTAL_ROUTINES_E; index_u8++) {
		RoutineResult_au32[index_u8] = SERV31_ROUTINE_NOTSTARTED;
	}
}

/******************************************************************************
 End of File
 ******************************************************************************/
