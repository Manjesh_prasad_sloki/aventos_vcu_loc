/******************************************************************************
 *    FILENAME    : diag_adapt.c
 *    DESCRIPTION : Application adapter interface file for DIAG Stacks.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "diag_adapt.h" 
#include "diag_sys_conf.h"
#if(TRUE == DIAG_CONF_UDS_SUPPORTED)
	#include "uds_conf.h"
#endif
#if(DIAG_CONF_INTERRUPTS == TRUE)
//	#include "s32_core_cm4.h"
#endif




/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

//static int16_t ECT_Deg_s16 = 0;    //variable which holds the Engine starting coolent temperature    
int16_t Diff_ECT_s16 = 0;
uint16_t Dist_Drvn = 0;

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

void vd_Load_ASCII_ToDID(U1* u1_valptr_DID, U4 u4_data, U1 u1_size)
{
	while(u1_size) {
		*u1_valptr_DID = (U1)((0x0F & (u4_data>>(4*(u1_size-1)))) + 0x30);
		u1_valptr_DID++;
		u1_size--;
	}
}
void vd_EOL_Cmd(U2 u2_t_cmd)
{
	//STS_EOL_CMD_REC = TRUE;
	//u2_eol_cmd_data = u2_t_cmd;
}

void	NvmmgrSetReq(U4 u4_ch, U1 u1_mode)
{
//	if(u1_mode == ON){					/* if request exists 			*/
//		st_fnvm.lword |= u4_ch;				/* set request flag 			*/
//	}
//	else{
//		st_fnvm.lword &= ~u4_ch;				/* clear request flag 			*/
//	}
}

void vd_EOL_NVM_Key(U4 u4_NVM_Key)
{
//	if(NVM_EOL_KEY == u4_NVM_Key)
//	{
//		STS_EOL_KEY_ACCEPTED = TRUE;
//		STS_EOL_CMD_REC = FALSE;
//	}
}

U1 EOL_Key_NoTimeout(void)
{
//	if(u2_EOL_KeyAcceptCnt < NVM_KEY_MAX_TIME) {
//		return TRUE;
//	} else {
//		return FALSE;
//	}
  return 0;
}

U1 Check_EOL_NVM_Security(void)
{
	//return STS_EOL_KEY_ACCEPTED;
  return 0;
}

void u1_Start_EOL(void)
{
//	EOL_Initial();
//	LCD_Wkup();
//	/*STS_EOL_BEGIN = TRUE;*/
//	STS_EOL_INPROGRESS = TRUE;
//	vd_SetTachHi();
//	vd_SetTempHi();
//	FuelctlSetHi();
//	vd_SetOatHi();
//	STS_EOL_KEY_ACCEPTED = FALSE;
//	u2_EOL_KeyAcceptCnt = 0;
}

U1 Read23_Ser_NVM(U2 memoryAddress, U1 memorySize, U1* u1_ptr_data)
{
//	U1 u1_index;
//	NVM_RET	st_ret;
//
//	IRQ_DISABLE();
//	for(u1_index = 0; u1_index < (memorySize/2); u1_index++)
//	{
//		st_ret 	= st_NvmRead(memoryAddress);
//		if(st_ret.u1_ret == OK)
//		{
//			*(u1_ptr_data++) = (U1)((st_ret.u2_data & 0xFF00) >> 8);
//			*(u1_ptr_data++) = (U1)(st_ret.u2_data & 0x00FF);
//			memoryAddress++;
//		}
//	}
//	IRQ_ENABLE();
//	return TRUE;
  return 0;
}

U1 WriteNVM_3DReq(U4 u4_addr, U2 u2_data)
{
//	if(u1_NVM_3D_loop_index != 0) {			/* Check Busy	*/
//		return TRUE;
//	}
//	if(u1_NVM_3D_size >= NVM_3D_MAX_DATA) {		/* Max Limit of u2_NVM_3D_Data[]	*/
//		return TRUE;
//	}
//	if(!u1_NVM_3D_size) {
//		u2_NVM_3D_Addr = u4_addr;
//	} else if(u4_addr != u2_NVM_3D_Addr + u1_NVM_3D_size) {		/* Checking for consecutive data	*/
//		return TRUE;
//	}
//	u2_NVM_3D_Data[u1_NVM_3D_size] = u2_data;
//	u1_3D_Serv_step = WRITE_NVM;
//	u1_NVM_3D_size++;
//	return FALSE;
  return 0;
}

uint8_t DTC_Mask_Iden_au8              	     	[4 ];
uint8_t CNTRL_MODULE_Config_Type_au8		 	[1 ];
uint8_t COMMUNICATION_Status_au8			 	[1 ];


uint8_t GLOBAL_Real_Time_au8				 	[3 ];
uint8_t TOTAL_Distance_Travelled_au8		 	[3 ];
uint8_t ECU_Supply_Voltage_au8			     	[1 ];
uint8_t VEHICLE_Interior_Temp_au8			 	[1 ];
uint8_t ABIENT_AIR_Temp_au8				     	[1 ];
uint8_t VEHICLE_Power_Mode_au8			     	[1 ];
uint8_t ENGINE_Speed_au8		        	 	[2 ];
uint8_t VEHICLE_Speed_au8					 	[1 ];
uint8_t ENGINE_Coolant_Temp_au8			     	[1 ];
uint8_t Throttle_Position_au8                  	[1 ];
uint8_t VIN_M_au8                      	     	[5 ];


uint8_t Sample_DID_1_Data_au8    			    [1 ];
uint8_t Sample_DID_2_Data_au8    			    [1 ];
uint8_t Sample_DID_3_Data_au8    			    [1 ];
uint8_t Sample_DID_4_Data_au8    			    [1 ];
uint8_t Sample_DID_5_Data_au8    			    [1 ];
uint8_t Sample_DID_6_Data_au8    			    [1 ];
uint8_t Sample_DID_7_Data_au8    			    [1 ];
uint8_t Sample_DID_8_Data_au8    			    [1 ];
uint8_t Sample_DID_9_Data_au8    			    [1 ];
uint8_t Sample_DID_10_Data_au8    			    [1 ];



U1 u1_config_param_tbl[U1_CONFIG_SIZE] ;

U1 u1_Diag_param_tbl[U2_DIAG_SIZE] ;
U1 u1_NVM_Config_step;
U1 u1_DTC_NVM_step;
U1 u1_Diag_Config_step;
U1 u1_3D_Serv_step;


void Diag_Disable_Interrupts()
{
#if(DIAG_CONF_INTERRUPTS == TRUE)
	DI();
#endif
}

void Diag_Restore_Interrupts()
{
#if(DIAG_CONF_INTERRUPTS == TRUE)
	EI();
#endif
}
