/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_RIIC1.c
* Version      : 1.2.0
* Device(s)    : R7F701689
* Description  : This file implements device driver for Config_RIIC1.
* Creation Date: 2021-10-21
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_RIIC1.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
volatile uint8_t          g_riic1_mode_flag;               /* RIIC1 master transmit receive flag */
volatile uint8_t          g_riic1_state;                   /* RIIC1 state */
volatile uint16_t         g_riic1_slave_address;           /* RIIC1 slave address */
volatile const uint8_t *  gp_riic1_tx_address;             /* RIIC1 transmit buffer address */
volatile uint16_t         g_riic1_tx_count;                /* RIIC1 transmit data number */
volatile uint8_t *        gp_riic1_rx_address;             /* RIIC1 receive buffer address */
volatile uint16_t         g_riic1_rx_count;                /* RIIC1 receive data number */
volatile uint16_t         g_riic1_rx_length;               /* RIIC1 receive data length */
volatile uint8_t          g_riic1_dummy_read_count;        /* RIIC1 count for dummy read */
volatile uint8_t          g_riic1_stop_generation;         /* RIIC1 stop condition generation flag */
extern volatile uint32_t  g_cg_sync_read;
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Create
* Description  : This function initializes the RIIC1 Bus Interface.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_RIIC1_Create(void)
{
    uint32_t tmp_port;

    /* Disable RIIC1 interrupt (INTRIIC1TI) operation and clear request */
    INTC2.ICRIIC1TI.BIT.MKRIIC1TI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1TI.BIT.RFRIIC1TI = _INT_REQUEST_NOT_OCCUR;
    /* Disable RIIC1 interrupt (INTRIIC1TEI) operation and clear request */
    INTC2.ICRIIC1TEI.BIT.MKRIIC1TEI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1TEI.BIT.RFRIIC1TEI = _INT_REQUEST_NOT_OCCUR;
    /* Disable RIIC1 interrupt (INTRIIC1RI) operation and clear request */
    INTC2.ICRIIC1RI.BIT.MKRIIC1RI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1RI.BIT.RFRIIC1RI = _INT_REQUEST_NOT_OCCUR;
    /* Disable RIIC1 interrupt (INTRIIC1EE) operation and clear request */
    INTC2.ICRIIC1EE.BIT.MKRIIC1EE = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1EE.BIT.RFRIIC1EE = _INT_REQUEST_NOT_OCCUR;
    /* Set RIIC1 interrupt (INTRIIC1TI) setting */
    INTC2.ICRIIC1TI.BIT.TBRIIC1TI = _INT_TABLE_VECTOR;
    INTC2.ICRIIC1TI.UINT16 &= _INT_PRIORITY_LEVEL5;
    /* Set RIIC1 interrupt (INTRIIC1TEI) setting */
    INTC2.ICRIIC1TEI.BIT.TBRIIC1TEI = _INT_TABLE_VECTOR;
    INTC2.ICRIIC1TEI.UINT16 &= _INT_PRIORITY_LEVEL5;
    /* Set RIIC1 interrupt (INTRIIC1RI) setting */
    INTC2.ICRIIC1RI.BIT.TBRIIC1RI = _INT_TABLE_VECTOR;
    INTC2.ICRIIC1RI.UINT16 &= _INT_PRIORITY_LEVEL5;
    /* Set RIIC1 interrupt (INTRIIC1EE) setting */
    INTC2.ICRIIC1EE.BIT.TBRIIC1EE = _INT_TABLE_VECTOR;
    INTC2.ICRIIC1EE.UINT16 &= _INT_PRIORITY_LEVEL5;
    /* Reset RIIC1 */
    RIIC1.CR1.UINT32 &= _RIIC_DISABLE ;
    RIIC1.CR1.UINT32 |= _RIIC_RESET;
    RIIC1.CR1.UINT32 |= _RIIC_INTERNAL_RESET;
    /* Set RIIC1 setting */
    RIIC1.MR1.UINT32 = _RIIC_CLOCK_SELECTION_1;
    RIIC1.BRL.UINT32 = _RIIC_RIIC0BRL_DEFAULT_VALUE | _RIIC1_BITRATE_LOW_LEVEL_PERIOD;
    RIIC1.BRH.UINT32 = _RIIC_RIIC0BRH_DEFAULT_VALUE | _RIIC1_BITRATE_HIGH_LEVEL_PERIOD;
    RIIC1.MR3.UINT32 = _RIIC_DIGITAL_NF_STAGE_SINGLE;
    RIIC1.FER.UINT32 = _RIIC_SCL_SYNC_CIRCUIT_USED | _RIIC_NOISE_FILTER_USED | _RIIC_TRANSFER_SUSPENSION_DISABLED | 
                       _RIIC_NACK_ARBITRATION_DISABLE | _RIIC_MASTER_ARBITRATION_DISABLE | 
                       _RIIC_TIMEOUT_FUNCTION_DISABLED;
    RIIC1.IER.UINT32 = _RIIC_TRANSMIT_DATA_EMPTY_INT_ENABLE | _RIIC_TRANSMIT_END_INT_ENABLE | 
                       _RIIC_RECEIVE_COMPLETE_INT_ENABLE | _RIIC_TIMEOUT_INT_DISABLE | 
                       _RIIC_ARBITRATION_LOST_INT_DISABLE | _RIIC_START_CONDITION_INT_ENABLE | 
                       _RIIC_STOP_CONDITION_INT_ENABLE | _RIIC_NACK_RECEPTION_INT_ENABLE;
    /* Cancel internal reset */
    RIIC1.CR1.UINT32 &= _RIIC_CLEAR_INTERNAL_RESET;
    /* Synchronization processing */
    g_cg_sync_read = RIIC1.MR1.UINT32;
    __syncp();
    /* Set RIIC1SCL pin */
    PORT.PBDC8 &= _PORT_CLEAR_BIT1;
    PORT.PM8 |= _PORT_SET_BIT1;
    PORT.PMC8 &= _PORT_CLEAR_BIT1;
    tmp_port = PORT.PODC8;
    PORT.PPCMD8 = _WRITE_PROTECT_COMMAND;
    PORT.PODC8 = (tmp_port | _PORT_SET_BIT1);
    PORT.PODC8 = (uint32_t) ~(tmp_port | _PORT_SET_BIT1);
    PORT.PODC8 = (tmp_port | _PORT_SET_BIT1);
    PORT.PBDC8 |= _PORT_SET_BIT1;
    PORT.PFC8 &= _PORT_CLEAR_BIT1;
    PORT.PFCE8 &= _PORT_CLEAR_BIT1;
    PORT.PFCAE8 |= _PORT_SET_BIT1;
    PORT.PMC8 |= _PORT_SET_BIT1;
    PORT.PM8 &= _PORT_CLEAR_BIT1;
    /* Set RIIC1SDA pin */
    PORT.PBDC8 &= _PORT_CLEAR_BIT0;
    PORT.PM8 |= _PORT_SET_BIT0;
    PORT.PMC8 &= _PORT_CLEAR_BIT0;
    tmp_port = PORT.PODC8;
    PORT.PPCMD8 = _WRITE_PROTECT_COMMAND;
    PORT.PODC8 = (tmp_port | _PORT_SET_BIT0);
    PORT.PODC8 = (uint32_t) ~(tmp_port | _PORT_SET_BIT0);
    PORT.PODC8 = (tmp_port | _PORT_SET_BIT0);
    PORT.PBDC8 |= _PORT_SET_BIT0;
    PORT.PFC8 &= _PORT_CLEAR_BIT0;
    PORT.PFCE8 &= _PORT_CLEAR_BIT0;
    PORT.PFCAE8 |= _PORT_SET_BIT0;
    PORT.PMC8 |= _PORT_SET_BIT0;
    PORT.PM8 &= _PORT_CLEAR_BIT0;

    R_Config_RIIC1_Create_UserInit();
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Start
* Description  : This function starts the IIC Bus Interface.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_RIIC1_Start(void)
{
    /* Clear RIIC1 interrupt request and enable operation */
    INTC2.ICRIIC1TI.BIT.RFRIIC1TI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1TEI.BIT.RFRIIC1TEI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1RI.BIT.RFRIIC1RI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1EE.BIT.RFRIIC1EE = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1TI.BIT.MKRIIC1TI = _INT_PROCESSING_ENABLED;
    INTC2.ICRIIC1TEI.BIT.MKRIIC1TEI = _INT_PROCESSING_ENABLED;
    INTC2.ICRIIC1RI.BIT.MKRIIC1RI = _INT_PROCESSING_ENABLED;
    INTC2.ICRIIC1EE.BIT.MKRIIC1EE = _INT_PROCESSING_ENABLED;
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Stop
* Description  : This function stops the IIC Bus Interface.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_RIIC1_Stop(void)
{
    /* Disable RIIC1 interrupt operation and clear request */
    INTC2.ICRIIC1TI.BIT.RFRIIC1TI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1TEI.BIT.RFRIIC1TEI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1RI.BIT.RFRIIC1RI = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1EE.BIT.RFRIIC1EE = _INT_REQUEST_NOT_OCCUR;
    INTC2.ICRIIC1TI.BIT.MKRIIC1TI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1TEI.BIT.MKRIIC1TEI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1RI.BIT.MKRIIC1RI = _INT_PROCESSING_DISABLED;
    INTC2.ICRIIC1EE.BIT.MKRIIC1EE = _INT_PROCESSING_DISABLED;
    /* Synchronization processing */
    g_cg_sync_read =  INTC2.ICRIIC1RI.UINT16;
    __syncp();
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Master_Send
* Description  : This function write data to a slave device.
* Arguments    : adr -
*                    address of slave device
*                txbuf -
*                    transmit buffer pointer
*                txnum -
*                    transmit data length
* Return Value : status -
***********************************************************************************************************************/
MD_STATUS R_Config_RIIC1_Master_Send(uint16_t adr, uint8_t * const tx_buf, uint16_t tx_num)
{
    MD_STATUS status = MD_OK;

    if (RIIC1.CR2.UINT32 & _RIIC_BUS_BUSY)
    {
        status = MD_ERROR1;
    }
    else if (adr > _RIIC_ADDRESS_10BIT)
    {
        status = MD_ERROR2;
    }
    else
    {
        /* Set parameter */
        g_riic1_tx_count = tx_num;
        gp_riic1_tx_address = tx_buf;
        g_riic1_slave_address = adr;
        g_riic1_mode_flag = _RIIC_MASTER_TRANSMIT;

        if (adr <= _RIIC_ADDRESS_7BIT)
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_7_W;
        }
        else
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10A_W;
        }

        R_Config_RIIC1_StartCondition();  /* Issue a start condition */

        /* Set flag for generating stop condition when transmission finishes */
        g_riic1_stop_generation = 1;
    }

    return (status);
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Master_Send_Without_Stop
* Description  : This function write data to a slave device.
* Arguments    : adr -
*                    address of slave device
*                txbuf -
*                    transmit buffer pointer
*                txnum -
*                    transmit data length
* Return Value : status -
***********************************************************************************************************************/
MD_STATUS R_Config_RIIC1_Master_Send_Without_Stop(uint16_t adr, uint8_t * const tx_buf, uint16_t tx_num)
{
    MD_STATUS status = MD_OK;

    if (RIIC1.CR2.UINT32 & _RIIC_BUS_BUSY)
    {
        status = MD_ERROR1;
    }
    else if (adr > _RIIC_ADDRESS_10BIT)
    {
        status = MD_ERROR2;
    }
    else
    {
        /* Set parameter */
        g_riic1_tx_count = tx_num;
        gp_riic1_tx_address = tx_buf;
        g_riic1_slave_address = adr;
        g_riic1_mode_flag = _RIIC_MASTER_TRANSMIT;
        
        if (adr <= _RIIC_ADDRESS_7BIT)
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_7_W;
        }
        else
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10A_W;
        }

        R_Config_RIIC1_StartCondition();  /* Issue a start condition */

        /* Set flag for not generating stop condition when transmission finishes */
        g_riic1_stop_generation = 0;
    }

    return (status);
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_Master_Receive
* Description  : This function read data from a slave device.
* Arguments    : adr -
*                    address of slave device
*                rx_buf -
*                    receive buffer pointer
*                rx_num -
*                    receive data length
* Return Value : status -
***********************************************************************************************************************/
MD_STATUS R_Config_RIIC1_Master_Receive(uint16_t adr, uint8_t * const rx_buf, uint16_t rx_num)
{
    MD_STATUS status = MD_OK;

    if (adr > _RIIC_ADDRESS_10BIT)
    {
        status = MD_ERROR2;
    }
    else
    {
        /* Set parameter */
        g_riic1_rx_length = rx_num;
        g_riic1_rx_count = 0U;
        gp_riic1_rx_address = rx_buf;
        g_riic1_slave_address = adr;
        g_riic1_dummy_read_count = 0U;
        g_riic1_mode_flag = _RIIC_MASTER_RECEIVE;

        if (adr <= _RIIC_ADDRESS_7BIT)
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_7_R;
        }
        else
        {
            g_riic1_state = _RIIC_MASTER_SENDS_ADR_10A_W;
        }

        if (RIIC1.CR2.UINT32 & _RIIC_BUS_BUSY)
        {
            /* Has a stop been issued or detected? */
            if ((_RIIC_STOP_CONDITION_REQUEST & RIIC1.CR2.UINT32) || (_RIIC_STOP_CONDITION_DETECTED & RIIC1.SR2.UINT32))
            {
                /* Wait for the bus to become idle */
                do
                {
                    /* Arbitration lost or timeout? */
                    if ((_RIIC_TIMEOUT_DETECTED & RIIC1.SR2.UINT32) || (_RIIC_ARBITRATION_LOST & RIIC1.SR2.UINT32))
                    {
                        return (MD_ERROR4);
                    }
                } while (_RIIC_TRANSMIT_DATA_EMPTY & RIIC1.CR2.UINT32);

                /* Issue a start condition */
                R_Config_RIIC1_StartCondition();
            }
            /* Bus is busy and it is master mode (MST = 1) */
            else if (0x40U & RIIC1.CR2.UINT32)
            {
                /* Issue a restart condition */
                RIIC1.SR2.UINT32 &= (uint32_t) ~_RIIC_START_CONDITION_DETECTED;
                RIIC1.IER.UINT32 |= _RIIC_START_CONDITION_INT_ENABLE;
                RIIC1.CR2.UINT32 |= _RIIC_RESTART_CONDITION_REQUEST;
            }
            else
            {
                /* Another master must have the bus */
                status = MD_ERROR5;
            }
        }
        else
        {
            /* Issue a start condition */
            R_Config_RIIC1_StartCondition();
        }
    }

    return (status);
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_StartCondition
* Description  : This function generate IIC start condition.
* Arguments    : adr -
*                    address of slave device
*                rx_buf -
*                    receive buffer pointer
*                rx_num -
*                    receive data length
* Return Value : None -
***********************************************************************************************************************/
void R_Config_RIIC1_StartCondition(void)
{
    /* Set start condition flag */
    RIIC1.CR2.UINT32 |= _RIIC_START_CONDITION_REQUEST;
}

/***********************************************************************************************************************
* Function Name: R_Config_RIIC1_StopCondition
* Description  : This function generate IIC stop condition.
* Arguments    : adr -
*                    address of slave device
*                rx_buf -
*                    receive buffer pointer
*                rx_num -
*                    receive data length
* Return Value : None -
***********************************************************************************************************************/
void R_Config_RIIC1_StopCondition(void)
{
    /* Set stop condition flag */
    RIIC1.CR2.UINT32 |= _RIIC_STOP_CONDITION_REQUEST;
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
