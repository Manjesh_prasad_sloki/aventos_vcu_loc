
#include "diag_typedefs.h"
#include "r_typedefs.h"
#include "fcl_cfg.h"
#include "r_fcl_types.h"
#include "r_fcl.h"
#include "fcl_descriptor.h"
#include "fcl_user.h"
#include "target.h"

//void RDP_FCL_CTRL(void);
// void RDP_FCL_ERASE(uint32_t , uint16_t );
// void RDP_FCL_WRITE(uint32_t , uint16_t );
//const uint8_t *Lib_Ver;

//extern uint32_t            readBuffer_u32[8];
//extern uint16_t            abc;


//extern uint32_t Padding;

bool PFlash_Erase(uint32_t address,uint32_t size);
bool PFlash_Write(uint32_t address,uint32_t size,uint8_t* data_buff);
bool PFlash_Init(void);
bool PFlash_Pattern_Write(uint32_t address, uint32_t data);

//void RDP_FCL_ERASE (uint32_t Erase_BLK_CNT, uint16_t Num_of_Blocks);

//void RDP_FCL_WRITE (uint32_t Address_u32, uint16_t Num_256byte_block );