/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_main.c
* Version      : 1.0.100
* Device(s)    : R7F701688
* Description  : This function implements main function.
* Creation Date: 2021-09-27
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_WDT0.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "Config_TAUB0_0.h"
#include "r_cg_cgc.h"
/* Start user code for include. Do not edit comment generated here */
#include "can_driver.h"

#include "r_typedefs.h"
#include "fcl_cfg.h"
#include "r_fcl_types.h"
#include "r_fcl.h"
#include "fcl_descriptor.h"
#include "fcl_user.h"
#include "target.h"

#define FLMD0_PROTECTION_OFF    (0x01u)
#define FLMD0_PROTECTION_ON     (0x00u)

/* This array reserves the copy area in the device RAM */
#define FCL_RAM_EXECUTION_AREA_SIZE 0x8000

#if R_FCL_COMPILER == R_FCL_COMP_GHS
    #pragma ghs startdata
    #pragma ghs section bss = ".FCL_RESERVED"
    #define R_FCL_NOINIT
#elif R_FCL_COMPILER == R_FCL_COMP_IAR
    #pragma segment = "FCL_RESERVED"
    #pragma location = "FCL_RESERVED"
    #define R_FCL_NOINIT __no_init
#elif R_FCL_COMPILER == R_FCL_COMP_REC
    #pragma section r0_disp32 "FCL_RESERVED"
    #define R_FCL_NOINIT
#endif

R_FCL_NOINIT uint8_t FCL_Copy_area[FCL_RAM_EXECUTION_AREA_SIZE];

#if R_FCL_COMPILER == R_FCL_COMP_GHS
    #pragma ghs section bss = default
    #pragma ghs enddata
#elif R_FCL_COMPILER == R_FCL_COMP_IAR
    /* location only for one function, so no default required */
#elif R_FCL_COMPILER == R_FCL_COMP_REC
    #pragma section default
#endif
//void RDP_FCL_CTRL(void);
void RDP_FCL_ERASE(uint32_t , uint16_t );
void RDP_FCL_WRITE(uint32_t , uint16_t );
//const uint8_t *Lib_Ver;
uint8_t writeBuffer_u08[512];
//extern uint32_t            readBuffer_u32[8];
//extern uint16_t            abc;
r_fcl_status_t ret;
r_fcl_request_t myRequest;    
uint32_t flag = 1;
uint32_t k = 0;
///* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void main(void)
{
    R_MAIN_UserInit();
    /* Start user code for main. Do not edit comment generated here */
    
    CAN_Init();
    R_Pins_Create();
    R_Config_OSTM0_Start();
    while(1)
    {
	    if(flag == 1)
	    {
		    flag = 2;
		    
		    
		    DI();    
		    R_CGC_Create();
		    EI();    
		    for(k = 0; k< 512; k++)
		    {
		        writeBuffer_u08[k] = 0xAA;
		    }
		    ret = R_FCL_Init (&sampleApp_fclConfig_enu);
		    if (R_FCL_OK == ret)
		    {
		        ret = R_FCL_CopySections ();      
		    }
		    //r_fcl_request_t     myRequest;   

		    FCLUser_Open ();    
		    /* prepare environment */
		    myRequest.command_enu = R_FCL_CMD_PREPARE_ENV;
		    R_FCL_Execute (&myRequest);
		    //RDP_FCL_CTRL();		//RASHIK Testing purpose.   
    			//FCL_Ctrl();
        
    		    RDP_FCL_ERASE (0x06,0x01);   
    		    RDP_FCL_WRITE (0xC000, 0x01);
		    
	    }
	    R_Config_WDT0_Restart();
    }
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    DI();
    /* Start user code for R_MAIN_UserInit. Do not edit comment generated here */
    
    /* End user code. Do not edit comment generated here */
    R_Systeminit();
    EI();
}

/* Start user code for adding. Do not edit comment generated here */
void RDP_FCL_ERASE (uint32_t Erase_BLK_CNT, uint16_t Num_of_Blocks)
{
	 r_fcl_request_t     myRequest; 
	DI();		
  /* Erase Specified blocks*/
    myRequest.command_enu = R_FCL_CMD_ERASE;
    myRequest.idx_u32     = Erase_BLK_CNT;                   /* Block count */
    myRequest.cnt_u16     = Num_of_Blocks;			/* Num of Blocks */	
    R_FCL_Execute (&myRequest);    
    EI();
}

void RDP_FCL_WRITE (uint32_t Address_u32, uint16_t Num_256byte_block )
{
    r_fcl_request_t     myRequest;    
	DI();
 //   FCLUser_Open ();    
    // REINITIALIZE_BUFFER;		// Uncomment if you use writeBuffer_u08   
    /**** RASHIK testing****/
    myRequest.command_enu = R_FCL_CMD_WRITE;
    myRequest.bufferAdd_u32 = (uint32_t)&writeBuffer_u08[0];
    myRequest.idx_u32       = Address_u32;
    myRequest.cnt_u16       = Num_256byte_block;                    /* written bytes = 256 x Num_256byte_block */
    R_FCL_Execute (&myRequest);   
    
    FCLUser_Close ();
    EI();
}
/* End user code. Do not edit comment generated here */
