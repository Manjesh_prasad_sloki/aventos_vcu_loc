/***********************************************************************************************************************
* File Name    : board_init.c
* Version      : 01
* Description  : This file contains the functions to initialize the Hardware and Software components required by the 
                 HMI Cluster Software.
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "board_init.h"
#include "PFlash.h"
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_WDT0.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "r_cg_cgc.h"
#include "com_tasksched.h"
#include "Config_TAUB0_0.h"
#include "Config_UART0.h"
#include "uart_user.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/ 


/***********************************************************************************************************************
* Function Name: Hardware_Init
* Description  : This function initializes the Hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Hardware_Init(void)
{

	DI();
	R_Config_PORT_Create();
        R_CGC_Create();
        R_Config_WDT0_Create();
        R_Config_OSTM0_Create();
	R_Config_TAUB0_0_Create();
	R_Config_UART0_Create();
	R_Pins_Create();
	CAN_Init();	
	PFlash_Init();
	PORT.P9 |= _PORT_Pn1_OUTPUT_LOW; // to enable CAN
	PORT.P11 |= _PORT_Pn1_OUTPUT_LOW; // to enable CAN
	PORT.P10 |= _PORT_Pn15_OUTPUT_LOW; // to enable CAN
	//PORT.P11 = ((PORT.P11) | (_PORT_Pn1_OUTPUT_HIGH | _PORT_Pn2_OUTPUT_HIGH | _PORT_Pn3_OUTPUT_HIGH));
	return;
}


/***********************************************************************************************************************
* Function Name: Software_Init
* Description  : This function initailizes the Software components and starts the hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Software_Init(void)
{
	EI();
	//R_Config_OSTM0_Start();
	R_Config_TAUB0_0_Start();
	R_Config_UART0_Start();
	R_Config_UART0_Receive(&UART_DataByte_u8, 1U);
	Diag_TS_Init();            /*Initialize the CIL-CAN Layer*/
	return;
}

/********************************************************EOF***********************************************************/
