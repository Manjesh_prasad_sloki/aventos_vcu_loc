/***********************************************************************************************************************
* File Name    : task_scheduler.c
* Version      : 01
* Description  : This file implements the Task_Scheduler
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "task_scheduler.h"
#include "r_cg_macrodriver.h"
#include "Config_WDT0.h"
#include "com_tasksched.h"
#include "PFlash.h"
#include "main.h"
#include "can_driver.h"
#include "Config_UART0.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool   TS_ExitSched_b = false;
uint32_t		Time_Tick_u32 = 0;
bool 			_5ms_TS_Flag_b = false;
uint8_t data[512] = {0x80};
uint8_t data_buffer[0x08] = {1,2,3,4,5,6,7,8};
extern uint8_t FCL_Erase_status;
extern uint8_t FCL_Erase_status1;
//uint32_t Padding = 0x11111111;
//uint8_t addres_u32 = 0x18000;
//uint32_t Value_u32 = 0;
/***********************************************************************************************************************
* Function Name: Taskscheduler_start
* Description  : This function schedules the tasks for every 5-ms
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Taskscheduler_start(void)
{
	uint32_t Counter_5ms_u32 = 0;
	
	uint8_t CommandByte_u8 = 0x00U;
	CommandByte_u8 = 0x02U;
	R_Config_UART0_Send(&CommandByte_u8, 1U);
	
 	while(!TS_ExitSched_b)
	{
		while(_5ms_TS_Flag_b)
		{
			_5ms_TS_Flag_b = false;
			R_Config_WDT0_Restart();   /*Refresh the WDT*/
			Diag_TS_Proc_5ms();

			if(reset_b == true)
			{
				Counter_5ms_u32++;
				if(Counter_5ms_u32 == 100)
				{
					TS_StopScheduler();
					break;
					//TS_PowerOff();
				}
			}
		}
	}
	return;
}
/*
*  @brief         : The function stops the task scheduler operation
*                   by Setting the Exit flag to true.
*  @param         : None
*  @return        : None
*/
void TS_StopScheduler (void)
{
    TS_ExitSched_b = true;
}


typedef unsigned char (*pt2Function)(unsigned long, unsigned short * );
void TS_PowerOff(void)
{
	WPROTR.PROTCMD0 = 0x000000A5;

	RESCTL.SWRESA = 0x1;

	RESCTL.SWRESA = ~0x01;

	RESCTL.SWRESA = 0x1;
}


void Dummy_exception1(void)
{
	CAN_Transmit(CAN0,0x501,0x08U,&data_buffer[0]);	
}
void Dummy_exception2(void)
{
	CAN_Transmit(CAN0,0x502,0x08U,&data_buffer[0]);	
}
void Dummy_exception3(void)
{
	CAN_Transmit(CAN0,0x503,0x08U,&data_buffer[0]);	
}
void Dummy_exception4(void)
{
	CAN_Transmit(CAN0,0x504,0x08U,&data_buffer[0]);	
}
void Dummy_exception5(void)
{
	CAN_Transmit(CAN0,0x505,0x08U,&data_buffer[0]);	
}
void Dummy_exception6(void)
{
	CAN_Transmit(CAN0,0x506,0x08U,&data_buffer[0]);	
}
void Dummy_exception7(void)
{
	CAN_Transmit(CAN0,0x507,0x08U,&data_buffer[0]);	
}
void Dummy_exception8(void)
{
	CAN_Transmit(CAN0,0x508,0x08U,&data_buffer[0]);	
}
void Dummy_exception9(void)
{
	CAN_Transmit(CAN0,0x509,0x08U,&data_buffer[0]);	
}
void Dummy_exception10(void)
{
	CAN_Transmit(CAN0,0x510,0x08U,&data_buffer[0]);	
}
void Dummy_exception11(void)
{
	CAN_Transmit(CAN0,0x511,0x08U,&data_buffer[0]);	
}


void Dummy_exception_1(void)
{
	CAN_Transmit(CAN0,0x601,0x08U,&data_buffer[0]);	
}
void Dummy_exception_2(void)
{
	CAN_Transmit(CAN0,0x602,0x08U,&data_buffer[0]);	
}
void Dummy_exception_3(void)
{
	CAN_Transmit(CAN0,0x603,0x08U,&data_buffer[0]);	
}
void Dummy_exception_4(void)
{
	CAN_Transmit(CAN0,0x604,0x08U,&data_buffer[0]);	
}
void Dummy_exception_5(void)
{
	CAN_Transmit(CAN0,0x605,0x08U,&data_buffer[0]);	
}
void Dummy_exception_6(void)
{
	CAN_Transmit(CAN0,0x606,0x08U,&data_buffer[0]);	
}
void Dummy_exception_7(void)
{
	CAN_Transmit(CAN0,0x607,0x08U,&data_buffer[0]);	
}
void Dummy_exception_8(void)
{
	CAN_Transmit(CAN0,0x608,0x08U,&data_buffer[0]);	
}
void Dummy_exception_9(void)
{
	CAN_Transmit(CAN0,0x609,0x08U,&data_buffer[0]);	
}
void Dummy_exception_10(void)
{
	CAN_Transmit(CAN0,0x610,0x08U,&data_buffer[0]);	
}
void Dummy_exception_11(void)
{
	CAN_Transmit(CAN0,0x611,0x08U,&data_buffer[0]);	
}
void Dummy_exception_12(void)
{
	CAN_Transmit(CAN0,0x612,0x08U,&data_buffer[0]);	
}
void Dummy_exception_13(void)
{
	CAN_Transmit(CAN0,0x613,0x08U,&data_buffer[0]);	
}
void Dummy_exception_14(void)
{
	CAN_Transmit(CAN0,0x614,0x08U,&data_buffer[0]);	
}
void Dummy_exception_15(void)
{
	CAN_Transmit(CAN0,0x615,0x08U,&data_buffer[0]);	
}
void Dummy_exception_16(void)
{
	CAN_Transmit(CAN0,0x616,0x08U,&data_buffer[0]);	
}
void Dummy_exception_17(void)
{
	CAN_Transmit(CAN0,0x617,0x08U,&data_buffer[0]);	
}
void Dummy_exception_18(void)
{
	CAN_Transmit(CAN0,0x618,0x08U,&data_buffer[0]);	
}
void Dummy_exception_19(void)
{
	CAN_Transmit(CAN0,0x619,0x08U,&data_buffer[0]);	
}


/********************************************************EOF***********************************************************/