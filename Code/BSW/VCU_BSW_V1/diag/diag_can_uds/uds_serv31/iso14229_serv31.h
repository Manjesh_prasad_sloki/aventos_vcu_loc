/*
 * iso14229_serv31.h
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */

/* ************************************************************************** */
/** Descriptive File Name

   @Company
 	 Sloki Technologies Pvt Ltd.

  	@File Name
    	iso14229_serv31.h

   @Summary
 	 This File contains macros, typedefs, extern definitions and
 	 user defined datatypes required by iso14229_serv34.c
 */


/* ************************************************************************** */

#ifndef ISO14229_SERV31_H_
#define ISO14229_SERV31_H_



/* Section: Included Files                                                    */
/* ************************************************************************** */
#include "uds_conf.h"
#include <stdbool.h>
#include <stddef.h>
#include "iso14229_serv27.h"
/* ************************************************************************** */




/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif



    /* Section: Constants                                                         */
    /* ************************************************************************** */
		#define SERV31_MIN_LEN              0x04
		#define  RID_ERASE_MIN_LENGTH       0x05
		#define  RID_CHECKSUM_MIN_LENGTH    0x08
	/* Sub functions IDS */

		#define RC_START                   0x01
		#define RC_STOP                    0x02
		#define RC_REQROUTRESULTS          0x03

	/* RIDs       */

		//#define BOOT_STATUS                0x0200
		#define RID_ERASE                  0xFF00
		#define RID_CHECKSUM               0xFF01

		//#define ASW_SYSTEM                        0x01
		#define MINUS_FOUR                        -4
    /* ************************************************************************** */

	// enums
	/* ************************************************************************** */

		typedef enum{
			IDLE_STATE_E = 0U,
			WAIT_PENDING_E,
			ERASE_COMPLETE_E
		}ST_MACHINE_ERASE_En_t;


	/* ************************************************************************** */

    // structures
    // *****************************************************************************


    // *****************************************************************************


    // Section: Data Types
    // *****************************************************************************
		extern uint8_t CallBCK;
		extern uint32_t BootLoader_ser34_Data_Size_u32 ;
		extern uint32_t BootLoader_ser34_ReqAddr_u32 ;
		extern bool EraseFlash_b ;
		extern uint32_t Addr31CheckSum_u32 ;
		extern uint32_t Size31CheckSum_u32 ;
		extern int8_t EraseMemory(uint8_t *);
		extern int8_t CalculateCheckSum(uint8_t *);
    // *****************************************************************************







/**
*  FUNCTION NAME : iso14229_serv31
*  FILENAME      : iso14229_serv31.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_31 requests
*  @return       : Type of response.
*/
UDS_Serv_resptype_En_t iso14229_serv31(UDS_Serv_St_t*  UDS_Serv_pSt);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* ISO14229_SERV31_H H */

/* *****************************************************************************
 End of File
 */

