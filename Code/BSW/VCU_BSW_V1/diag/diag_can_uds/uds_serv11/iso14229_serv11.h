/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _ISO14229_SERV11_H    /* Guard against multiple inclusion */
#define _ISO14229_SERV11_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include"uds_conf.h"
/* ************************************************************************** */



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
	#define SERVICE_10_MAX_LEN        0x02
	#define HARD_RESET                0x01
	#define SOFT_RESET                0x03

    /* ************************************************************************** */

    // Section: Enumaration
    // *****************************************************************************
		typedef enum{
			HARD_RESET_SUB_ID = 1,
			SOFT_REST_SUB_ID = 3
		} RESET_SUBID_En_t;
    // *****************************************************************************


/**
		*  FUNCTION NAME : iso14229_serv11
		*  FILENAME      : iso14229_serv11.h
		*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
		*  @brief        : This function will process the service_11 requests
		*  @return       : Type of response.
*/

		UDS_Serv_resptype_En_t iso14229_serv11(UDS_Serv_St_t* UDS_Serv_pSt);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
