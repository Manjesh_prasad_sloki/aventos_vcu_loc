/*
 * iso14229_serv37.h
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */



/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Sloki Technologies Pvt Ltd.

  @File Name
    iso14229_serv37.h

  @Summary
    This File contains macros, typedefs, extern definitions and
 * user defined datatypes required by iso14229_serv37.c

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef ISO14229_SERV37_H_
#define ISO14229_SERV37_H_


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
//#include "Cpu.h"
#include "uds_conf.h"
#include "iso14229_serv36.h"
/* ************************************************************************** */



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
		#define SERV_37_MAX_LEN                         0x01
    /* ************************************************************************** */



/**
*  FUNCTION NAME : iso14229_serv37
*  FILENAME      : iso14229_serv37.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_37 requests
*  @return       : Type of response.
*/


UDS_Serv_resptype_En_t iso14229_serv37(UDS_Serv_St_t*  UDS_Serv_pSt);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* ISO14229_SERV37_H_ */

/* *****************************************************************************
 End of File
 */




