/* ************************************************************************** */
/** Descriptive File Name

  @Company
 Sloki Software Technologies

  @File Name
    iso14229_serv10.h

  @Summary
 This File contains macros, typedefs, extern definitions and 
 * user defined datatypes required by iso14229_serv10.c

 */
/* ************************************************************************** */

#ifndef _ISO14229_SERV10_H    /* Guard against multiple inclusion */
#define _ISO14229_SERV10_H
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "uds_conf.h"
/*
 **************************************************************************************************
 *    Defines
 **************************************************************************************************
 */
#define NOT_SESSION                             0x80    
#define SERV_10_MAX_LEN                         0x02

/* Session enum*/
//typedef enum{
//        DEFAULT_SESSION_SUB_ID_E = 1,
//        PROGRAMMING_SESSION_SUB_ID_E,
//        EXTENDED_DIAG_SESSION_SUB_ID_E,
//        SAFETY_SYSTEM_DIAG_SESSION_SUB_ID_E,
//    }SESSION_SUBID_En_t ;
//

/*iso14229_serv10 function declaration*/

/**
*  FUNCTION NAME : iso14229_serv10
*  FILENAME      : iso14229_serv10.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_10 requests
*  @return       : Type of response.
*/

UDS_Serv_resptype_En_t iso14229_serv10(UDS_Serv_St_t*  UDS_Serv_pSt);


#endif /* _EXAMPLE_FILE_NAME_H */

/* ******************************************************************************/
