/*
 * iso14229_serv36.h
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */


#ifndef ISO14229_SERV36_H_
#define ISO14229_SERV36_H_

/* ************************************************************************** */
/** Descriptive File Name

  @Company
 Sloki Technologies Pvt Ltd.

  @File Name
    iso14229_serv36.h

  @Summary
 This File contains macros, typedefs, extern definitions and
 * user defined datatypes required by iso14229_serv36.c

 */
/* ************************************************************************** */




/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include "uds_conf.h"
//#include "Cpu.h"
#include "iso14229_serv34.h"
/* ************************************************************************** */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    #define SERV_36_MIN_LEN                         0x02
    #define SERV_36_MAX_LEN                         0xFFF
    #define MAX_NUM_OF_BLOCKLENGTH                  0xFF
    /* ************************************************************************** */


/**
*  FUNCTION NAME : iso14229_serv36
*  FILENAME      : iso14229_serv36.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_36 requests
*  @return       : Type of response.
*/

UDS_Serv_resptype_En_t iso14229_serv36(UDS_Serv_St_t*  UDS_Serv_pSt);
extern uint8_t rx_block_seq_number_u8;
    // *****************************************************************************
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* ISO14229_SERV36_H_ */

/* *****************************************************************************
 End of File
 */




