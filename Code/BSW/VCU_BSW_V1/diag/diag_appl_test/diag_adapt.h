
/*********************************************************************************
 *    FILENAME    : diag_adapt.h                                                 *          
 *    DESCRIPTION : Application adapter interface file for DIAG Stacks.          *
 *********************************************************************************
 * Revision history                                                              *               
 *                                                                               *
 * Ver Author       Date               Description                               *
 * 1   Sloki     1/10/2019		   Initial version                           *
 *********************************************************************************
*/  

#ifndef _DIAG_ADAPT_H_
#define _DIAG_ADAPT_H_

#include "diag_typedefs.h"
#include <stdint.h>
#include <stdbool.h>

#endif
