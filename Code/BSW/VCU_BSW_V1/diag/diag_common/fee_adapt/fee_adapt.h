/******************************************************************************
 *    FILENAME    : fee_adapt.h
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 
#ifndef    _FEE_ADAPT_H_
#define    _FEE_ADAPT_H_



#include  <stdbool.h>
#include "diag_typedefs.h"

extern bool Flash_Erase(uint32_t address,uint32_t size);
extern bool Flash_Write(uint32_t address,uint32_t size,uint8_t* data_buff);
extern bool Flash_Pattern_Write(uint32_t address,uint32_t data);

#endif
