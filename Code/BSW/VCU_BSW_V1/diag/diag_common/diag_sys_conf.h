/******************************************************************************
 *    FILENAME    : diag_sys_conf.h
 *    DESCRIPTION : This file contains version # of ISO 14229 stack software.
 *                  This is an internal version # to track the stack development.
 *                  This version # does not refer to ver # of UDS Standard. 
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/    

#ifndef	_DIAG_SYS_CONF_H_
#define _DIAG_SYS_CONF_H_
#include "diag_typedefs.h"

#define	DIAG_FM_SUPPORTED		(FALSE)
#define	DIAG_CANTP_SUPPORTED	(TRUE)

#define	DIAG_UDS_SUPPORTED		(TRUE)
#define	DIAG_OBD2_SUPPORTED		(FALSE)
#define	DIAG_J1939_SUPPORTED	(FALSE)


#define	DIAG_J1587_SUPPORTED	(FALSE)
#define	DIAG_DOIP_SUPPORTED		(FALSE)


#define DIAG_INTERRUPTS         (TRUE)
#define DIAG_CAN_IF         	(TRUE)
#define DIAG_EEPROM         	(FALSE)
#define DIAG_FLASH         	(TRUE)

#define DIAG_CORE_LITTLE_ENDIAN (TRUE)

#if(FALSE ==  DIAG_CORE_LITTLE_ENDIAN)
	#define DIAG_CORE_BIG_ENDIAN   (TRUE)
#endif


#endif
