/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_main.c
* Version      : 1.0.100
* Device(s)    : R7F701688
* Description  : This function implements main function.
* Creation Date: 2021-07-18
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_WDT0.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "r_cg_cgc.h"
/* Start user code for include. Do not edit comment generated here */
#include "can_driver.h"

//#include "r_typedefs.h"
//#include "fcl_cfg.h"
//#include "r_fcl_types.h"
//#include "r_fcl.h"
//#include "fcl_descriptor.h"
//#include "fcl_user.h"
//#include "target.h"

//#define FLMD0_PROTECTION_OFF    (0x01u)
//#define FLMD0_PROTECTION_ON     (0x00u)

///* This array reserves the copy area in the device RAM */
//#define FCL_RAM_EXECUTION_AREA_SIZE 0x8000

//#if R_FCL_COMPILER == R_FCL_COMP_GHS
//    #pragma ghs startdata
//    #pragma ghs section bss = ".FCL_RESERVED"
//    #define R_FCL_NOINIT
//#elif R_FCL_COMPILER == R_FCL_COMP_IAR
//    #pragma segment = "FCL_RESERVED"
//    #pragma location = "FCL_RESERVED"
//    #define R_FCL_NOINIT __no_init
//#elif R_FCL_COMPILER == R_FCL_COMP_REC
//    #pragma section r0_disp32 "FCL_RESERVED"
//    #define R_FCL_NOINIT
//#endif

//R_FCL_NOINIT uint8_t FCL_Copy_area[FCL_RAM_EXECUTION_AREA_SIZE];

//#if R_FCL_COMPILER == R_FCL_COMP_GHS
//    #pragma ghs section bss = default
//    #pragma ghs enddata
//#elif R_FCL_COMPILER == R_FCL_COMP_IAR
//    /* location only for one function, so no default required */
//#elif R_FCL_COMPILER == R_FCL_COMP_REC
//    #pragma section default
//#endif
////void RDP_FCL_CTRL(void);
//void RDP_FCL_ERASE(uint32_t , uint16_t );
//void RDP_FCL_WRITE(uint32_t , uint16_t );
////const uint8_t *Lib_Ver;
//uint8_t writeBuffer_u08[512];
////extern uint32_t            readBuffer_u32[8];
////extern uint16_t            abc;
//r_fcl_status_t ret;
//r_fcl_request_t myRequest;    
//uint32_t flag = 1;
//uint32_t k = 0;
///* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

/* Start user code for adding. Do not edit comment generated here */

/* End user code. Do not edit comment generated here */
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Board_Init.h"
#include "Task_Scheduler.h"
#include "main.h"
#include "fee_adapt.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
typedef unsigned char (*pt2Function)(unsigned long, unsigned short * );
unsigned int *ptr;
pt2Function fp;
/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
void JumpToUserApplication( unsigned int App_Start_Address)
{
	ptr = ((unsigned int *)( APP_START_ADDRESS ));

	if ( *ptr != 0xffff )
	{
		// call user program and never return
		fp = (pt2Function) ptr;
		fp( 0, 0);
	} 
}
/***********************************************************************************************************************
* Function Name: main
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
  void main(void)
{	
  
	if((APP_PATTERN == *(uint32_t *)DRIVE_VALIDATION_START_ADDRESS) )
	{
		//DI();
  		JumpToUserApplication(APP_START_ADDRESS);

	}
	Hardware_Init();
	Software_Init();
	while(1)
	{
		Taskscheduler_start();
		TS_PowerOff();
	}

}
