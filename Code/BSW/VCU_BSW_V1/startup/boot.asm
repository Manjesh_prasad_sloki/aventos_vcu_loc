;/**********************************************************************************************************************
; * DISCLAIMER
; * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
; * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
; * applicable laws, including copyright laws.
; * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
; * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
; * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
; * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
; * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
; * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
; * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
; * this software. By using this software, you agree to the additional terms and conditions found by accessing the
; * following link:
; * http://www.renesas.com/disclaimer
; *
; * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
; *********************************************************************************************************************/
;   NOTE       : THIS IS A TYPICAL EXAMPLE.
;   DATE       : Mon, Oct 24, 2016

	; if using eiint as table reference method,
	; enable next line's macro.

	USE_TABLE_REFERENCE_METHOD .set 1

;-----------------------------------------------------------------------------
;	exception vector table
;-----------------------------------------------------------------------------
	.section "RESET", text
	.align	512
	jr32	__start ; RESET

	.align	16
	syncp
	jr32	_Dummy1 ; SYSERR

	.align	16
	jr32	_Dummy2

	.align	16
	jr32	_Dummy3 ; FETRAP

	.align	16
	jr32	_Dummy_EI1 ; TRAP0

	.align	16
	jr32	_Dummy_EI2 ; TRAP1

	.align	16
	jr32	_Dummy4 ; RIE

	.align	16
	syncp
	jr32	_Dummy_EI3 ; FPP/FPI

	.align	16
	jr32	_Dummy5 ; UCPOP

	.align	16
	jr32	_Dummy6 ; MIP/MDP

	.align	16
	jr32	_Dummy7 ; PIE

	.align	16
	jr32	_Dummy8

	.align	16
	jr32	_Dummy9 ; MAE

	.align	16
	jr32	_Dummy10

	.align	16
	syncp
	jr32	FENMI ; FENMI

	.align	16
	syncp
	jr32	_Dummy11 ; FEINT

	.align	16
	syncp
	jr32	_Dummy_EI4 ; INTn(priority0)

	.align	16
	syncp
	jr32	_Dummy_EI5 ; INTn(priority1)

	.align	16
	syncp
	jr32	_Dummy_EI6 ; INTn(priority2)

	.align	16
	syncp
	jr32	_Dummy_EI7 ; INTn(priority3)

	.align	16
	syncp
	jr32	_Dummy_EI8 ; INTn(priority4)

	.align	16
	syncp
	jr32	_Dummy_EI9 ; INTn(priority5)

	.align	16
	syncp
	jr32	_Dummy_EI10 ; INTn(priority6)

	.align	16
	syncp
	jr32	_Dummy_EI11 ; INTn(priority7)

	.align	16
	syncp
	jr32	_Dummy_EI12 ; INTn(priority8)

	.align	16
	syncp
	jr32	_Dummy_EI13 ; INTn(priority9)

	.align	16
	syncp
	jr32	_Dummy_EI14 ; INTn(priority10)

	.align	16
	syncp
	jr32	_Dummy_EI15 ; INTn(priority11)

	.align	16
	syncp
	jr32	_Dummy_EI16 ; INTn(priority12)

	.align	16
	syncp
	jr32	_Dummy_EI17 ; INTn(priority13)

	.align	16
	syncp
	jr32	_Dummy_EI18 ; INTn(priority14)

	.align	16
	syncp
	jr32	_Dummy_EI19 ; INTn(priority15)

	;.section "EIINTTBL", const
	;.align	512
	;.dw	#_Dummy_EI ; INT0
	;.dw	#_Dummy_EI ; INT1
	;.dw	#_Dummy_EI ; INT2
	;.rept	512 - 3
	;.dw	#_Dummy_EI ; INTn
	;.endm

	.section ".text", text
	.align	2
_Dummy1:
	br	_Dummy_exception1
_Dummy2:
	br	_Dummy_exception2
_Dummy3:
	br	_Dummy_exception3
_Dummy4:
	br	_Dummy_exception4
_Dummy5:
	br	_Dummy_exception5
_Dummy6:
	br	_Dummy_exception6
_Dummy7:
	br	_Dummy_exception7
_Dummy8:
	br	_Dummy_exception8
_Dummy9:
	br	_Dummy_exception9
_Dummy10:
	br	_Dummy_exception10
_Dummy11:
	br	_Dummy_exception11


_Dummy_EI1:
	br	_Dummy_exception_1
_Dummy_EI2:
	br	_Dummy_exception_2
_Dummy_EI3:
	br	_Dummy_exception_3
_Dummy_EI4:
	br	_Dummy_exception_4
_Dummy_EI5:
	br	_Dummy_exception_5
_Dummy_EI6:
	br	_Dummy_exception_6
_Dummy_EI7:
	br	_Dummy_exception_7
_Dummy_EI8:
	br	_Dummy_exception_8
_Dummy_EI9:
	br	_Dummy_exception_9
_Dummy_EI10:
	br	_Dummy_exception_10
_Dummy_EI11:
	br	_Dummy_exception_11
_Dummy_EI12:
	br	_Dummy_exception_12
_Dummy_EI13:
	br	_Dummy_exception_13
_Dummy_EI14:
	br	_Dummy_exception_14
_Dummy_EI15:
	br	_Dummy_exception_15
_Dummy_EI16:
	br	_Dummy_exception_16
_Dummy_EI17:
	br	_Dummy_exception_17
_Dummy_EI18:
	br	_Dummy_exception_18
_Dummy_EI19:
	br	_Dummy_exception_19						
FENMI:
	br  FENMI	

;-----------------------------------------------------------------------------
;	startup
;-----------------------------------------------------------------------------
	.section	".text", text
	.align	2
__start:
$if 1	; initialize register
	$nowarning
	mov	r0, r1
	$warning
	mov	r0, r2
	mov	r0, r3
	mov	r0, r4
	mov	r0, r5
	mov	r0, r6
	mov	r0, r7
	mov	r0, r8
	mov	r0, r9
	mov	r0, r10
	mov	r0, r11
	mov	r0, r12
	mov	r0, r13
	mov	r0, r14
	mov	r0, r15
	mov	r0, r16
	mov	r0, r17
	mov	r0, r18
	mov	r0, r19
	mov	r0, r20
	mov	r0, r21
	mov	r0, r22
	mov	r0, r23
	mov	r0, r24
	mov	r0, r25
	mov	r0, r26
	mov	r0, r27
	mov	r0, r28
	mov	r0, r29
	mov	r0, r30
	mov	r0, r31
	ldsr	r0, 0, 0		;  EIPC
	ldsr	r0, 16, 0		;  CTPC
$endif

	jarl	_hdwinit, lp	; initialize hardware
$ifdef USE_TABLE_REFERENCE_METHOD
	mov	#__sEIINTTBL.const, r6
	jarl	_set_table_reference_method, lp ; set table reference method
$endif
	jr32	__cstart

;-----------------------------------------------------------------------------
;	hdwinit
; Specify RAM addresses suitable to your system if needed.
;-----------------------------------------------------------------------------
	GLOBAL_RAM_ADDR	.set	0xFEDD8000
	GLOBAL_RAM_END	.set	0xFEDE0000
	LOCAL_RAM_ADDR	.set	0xFEDE0000
	LOCAL_RAM_END	.set	0xFEE00000

	.align	2
_hdwinit:
	mov	lp, r14			; save return address

	; clear Global RAM
	mov	GLOBAL_RAM_ADDR, r6
	mov	GLOBAL_RAM_END, r7
	jarl	_zeroclr4, lp

	; clear Local RAM
	mov	LOCAL_RAM_ADDR, r6
	mov	LOCAL_RAM_END, r7
	jarl	_zeroclr4, lp

	mov	r14, lp
	jmp	[lp]

;-----------------------------------------------------------------------------
;	zeroclr4
;-----------------------------------------------------------------------------
	.align	2
_zeroclr4:
	br	.L.zeroclr4.2
.L.zeroclr4.1:
	st.w	r0, [r6]
	add	4, r6
.L.zeroclr4.2:
	cmp	r6, r7
	bh	.L.zeroclr4.1
	jmp	[lp]

$ifdef USE_TABLE_REFERENCE_METHOD
;-----------------------------------------------------------------------------
;	set table reference method
;-----------------------------------------------------------------------------
	; interrupt control register address
	ICBASE	.set	0xfffeea00

	.align	2
_set_table_reference_method:
	ldsr	r6, 4, 1		; set INTBP

	; Some interrupt channels use the table reference method.
	mov	ICBASE, r10		; get interrupt control register address
	set1	6, 0[r10]		; set INT0 as table reference
	set1	6, 2[r10]		; set INT1 as table reference
	set1	6, 4[r10]		; set INT2 as table reference

	jmp	[lp]
$endif
;-------------------- end of start up module -------------------;
