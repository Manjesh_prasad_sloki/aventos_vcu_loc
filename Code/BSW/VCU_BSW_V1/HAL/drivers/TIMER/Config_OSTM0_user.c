/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_OSTM0_user.c
* Version      : 1.1.0
* Device(s)    : R7F701688
* Description  : This file implements device driver for Config_OSTM0.
* Creation Date: 2021-07-18
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_OSTM0.h"
/* Start user code for include. Do not edit comment generated here */
#include "task_scheduler.h"
#include "diag_typedefs.h"
#include "com_tasksched.h"
#include "can_driver.h"
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_OSTM0_Create_UserInit
* Description  : This function adds user code after initializing OSTM module
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_OSTM0_Create_UserInit(void)
{
    /* Start user code for user init. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}
//uint8_t array_au8[8] = {1,2,3,4,5,6,7,8};
/***********************************************************************************************************************
* Function Name: r_Config_OSTM0_interrupt
* Description  : This function handles the INTOSTM0 interrupt
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_OSTM0_interrupt(enable=false, channel=84, fpu=true, callt=false)
void r_Config_OSTM0_interrupt(void)
{
    /* Start user code for r_Config_OSTM0_interrupt. Do not edit comment generated here */
    Time_Tick_u32++;
    //INC_TIME_MS();
    //CAN_Transmit(0,0x7F3,0x08U, &array_au8[0]);
    //Diag_TS_Proc_5ms();
    //if((Time_Tick_u32 % 5) == 0)
    //{
	    //R_WDT_Restart();   /*Refresh the WDT*/
    _5ms_TS_Flag_b = true;
    //}
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
