 /*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : can_driver.h
|    Project	    : VCU
|    Module         : CAN Driver
|    Description    : 
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 09/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef CAN_DRIVER_H
#define CAN_DRIVER_H


/*******************************************************************************
 *  Includes
 ******************************************************************************/
 #include "r_cg_macrodriver.h"


/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define TX_BUFFER 						1
#define TX_RX_FIFO						2

#define TX_MODE							TX_BUFFER

#define CAN0 							0U
#define CAN1 							1U
#define CAN2 							2U
#define Set_Rules  						6

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
typedef struct can
{
   uint8_t 		CAN_Module;
   const uint32_t 	CAN_ID;
   const uint32_t 	CAN_ID_MASK;
   const uint8_t 	CAN_DLC;
   const uint8_t 	BUFFER_NUMBER;
   const uint32_t 	Recive_Fifo;
}Rx_SetRules_St_t;


typedef struct 
{
unsigned char 	rx_msg_DLC;
unsigned int 	rx_msg_ID;
unsigned long 	rx_msg_data0;   
unsigned long 	rx_msg_data1;  
}RX_fifo_data;

/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
void CAN_Init(void);
//void can_tx(void);
//extern void CAN_Rx_FIFO_ISR(void);
extern void CAN0_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data);
extern void CAN1_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data);
extern void CAN2_Transmit( uint32_t CanId, uint8_t data_len, uint8_t *data);
extern void CAN_Transmit( uint8_t Can_modude, uint32_t CanId, uint8_t data_len, uint8_t *data);

#endif /* CAN_DRIVER_H */
/*---------------------- End of File -----------------------------------------*/