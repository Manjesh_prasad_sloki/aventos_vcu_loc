/*
 * uart.h
 *
 *  Created on: 03-Feb-2020
 *      Author: dell
 */

#ifndef UART_USER_H_
#define UART_USER_H_

#include "r_cg_macrodriver.h"


//export variables
extern uint8_t UART_DataByte_u8;
extern bool UART_Data_Rec_b;


#endif /* UART_USER_H_ */
