#include "PFlash.h"

#define FLMD0_PROTECTION_OFF    (0x01u)
#define FLMD0_PROTECTION_ON     (0x00u)

/* This array reserves the copy area in the device RAM */
#define FCL_RAM_EXECUTION_AREA_SIZE 0x8000

#if R_FCL_COMPILER == R_FCL_COMP_GHS
    #pragma ghs startdata
    #pragma ghs section bss = ".FCL_RESERVED"
    #define R_FCL_NOINIT
#elif R_FCL_COMPILER == R_FCL_COMP_IAR
    #pragma segment = "FCL_RESERVED"
    #pragma location = "FCL_RESERVED"
    #define R_FCL_NOINIT __no_init
#elif R_FCL_COMPILER == R_FCL_COMP_REC
    #pragma section r0_disp32 "FCL_RESERVED"
    #define R_FCL_NOINIT
#endif

R_FCL_NOINIT uint8_t FCL_Copy_area[FCL_RAM_EXECUTION_AREA_SIZE];

#if R_FCL_COMPILER == R_FCL_COMP_GHS
    #pragma ghs section bss = default
    #pragma ghs enddata
#elif R_FCL_COMPILER == R_FCL_COMP_IAR
    /* location only for one function, so no default required */
#elif R_FCL_COMPILER == R_FCL_COMP_REC
    #pragma section default
#endif

#if(0)

uint32_t ADDRESS,SIZE;
#endif


r_fcl_status_t ret;
r_fcl_request_t myRequest;    
uint32_t flag = 1;
uint8_t FCL_Erase_status = 0;
uint8_t FCL_Erase_status1 = 0;
//uint32_t k = 0;

uint8_t writeBuffer_u08[512];

bool PFlash_Erase(uint32_t address,uint32_t size)
{
    bool flag = false;
    r_fcl_request_t     myRequest; 
    uint32_t Block_Number = 0;
    uint32_t Address_u32 = 0;
    uint32_t Size_u32 = 0;
    uint32_t Total_Block_Number = 0;
    DI();		
    FCLUser_Open ();	
    if(address < 0x10000)
    {
        Block_Number = address/0x2000;
        if(0x10000 >= (address + size))
        {
	    if(size > 0x2000)
	    {
            	Total_Block_Number = size / 0x2000;
	    }
	    else
	    {
		   Total_Block_Number = 1; 
	    }
        }
        else
        {
            Address_u32 = address;
            Size_u32 = size;
            if(0 != Address_u32 % 0x2000)
            {
                Total_Block_Number++;
                Address_u32 = (address - (address % 0x2000));
                Size_u32 = size - (address % 0x2000);
            }
            while(0x10000 > Address_u32)
            {
                Address_u32 = (Address_u32-0x2000);
                Total_Block_Number++;
                Size_u32 = (Size_u32 - 0x2000);
            }

            Total_Block_Number = Size_u32 / 0x2000;

        }
        
    }
    else
    {
	
       	Block_Number = ((address - 0x10000)/0x8000 + 8);
       	Total_Block_Number = size / 0x8000;

    }
    
    /* Erase Specified blocks*/
    myRequest.command_enu = R_FCL_CMD_ERASE;
    myRequest.idx_u32     = Block_Number;                   /* Block count */
    myRequest.cnt_u16     = Total_Block_Number;			/* Num of Blocks */	
    R_FCL_Execute (&myRequest);  
    if (R_FCL_OK == myRequest.status_enu)
	{
		flag = true;    
	}   
    FCLUser_Close ();
    EI();
    flag = true;
    return flag;
}
bool PFlash_Write(uint32_t address,uint32_t size, uint8_t* data_buff)
{
    bool flag = false;
    r_fcl_request_t     myRequest;
    uint32_t   Number_of_block;
    if(0 != size%256) 
    {
        Number_of_block = (size/256) + 1;
    }   
    else
    {
        Number_of_block = size/256;
    }
    
    DI();
    FCLUser_Open ();    
    myRequest.command_enu = R_FCL_CMD_WRITE;
    myRequest.bufferAdd_u32 = (uint32_t)&data_buff[0];
    myRequest.idx_u32       = address;
    myRequest.cnt_u16       = Number_of_block;                    /* written bytes = 256 x Num_256byte_block */
    R_FCL_Execute (&myRequest);   
    if (R_FCL_OK == myRequest.status_enu)
    {
	flag = true;    
    }   
    FCLUser_Close ();
    EI();
    flag = true;
    return flag;
}
bool PFlash_Pattern_Write(uint32_t address, uint32_t data)
{
	bool flag = false;
	#if(1)
	uint8_t data_buff_au8[8] = {0};
	uint8_t i = 0;
	flag = PFlash_Erase(address,0x400);
	if(false == flag)
	{
		return false;
	}
	for(i=0; i<4U; i++)
	{
		data_buff_au8[i] = (uint8_t)((data >> (i*8)) & 0xFF);
	}

	flag = PFlash_Write(address, 4U, data_buff_au8);
	if(false == flag)
	{
		return false;
	}
	#endif
	return flag;
	
}
bool PFlash_Init(void)
{
	DI();    

	ret = R_FCL_Init (&sampleApp_fclConfig_enu);
	if (R_FCL_OK == ret)
	{
		ret = R_FCL_CopySections ();      
	}   
	FCL_Erase_status1 = (uint8_t)ret;
	FCLUser_Open ();    
	/* prepare environment */
	myRequest.command_enu = R_FCL_CMD_PREPARE_ENV;
	R_FCL_Execute (&myRequest);
	FCL_Erase_status = (uint8_t)myRequest.status_enu;
    return true;

}
// void RDP_FCL_ERASE (uint32_t Erase_BLK_CNT, uint16_t Num_of_Blocks)
// {
//     r_fcl_request_t     myRequest; 
//     DI();		
//     FCLUser_Open ();	
//     /* Erase Specified blocks*/
//     myRequest.command_enu = R_FCL_CMD_ERASE;
//     myRequest.idx_u32     = Erase_BLK_CNT;                   /* Block count */
//     myRequest.cnt_u16     = Num_of_Blocks;			/* Num of Blocks */	
//     R_FCL_Execute (&myRequest);  
//     FCLUser_Close ();
//     EI();
// }

// void RDP_FCL_WRITE (uint32_t Address_u32, uint16_t Num_256byte_block )
// {
//     r_fcl_request_t     myRequest;    
// 	DI();
	
//     for(k = 0; k< 512; k++)
//     {
// 	writeBuffer_u08[k] = 0x55;
//     }
//     FCLUser_Open ();    
//     // REINITIALIZE_BUFFER;		// Uncomment if you use writeBuffer_u08   
//     /**** RASHIK testing****/
//     myRequest.command_enu = R_FCL_CMD_WRITE;
//     myRequest.bufferAdd_u32 = (uint32_t)&writeBuffer_u08[0];
//     myRequest.idx_u32       = Address_u32;
//     myRequest.cnt_u16       = Num_256byte_block;                    /* written bytes = 256 x Num_256byte_block */
//     R_FCL_Execute (&myRequest);   
    
//     FCLUser_Close ();
//     EI();
// }