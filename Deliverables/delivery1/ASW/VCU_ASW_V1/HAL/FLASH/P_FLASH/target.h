/*********************************************************************************************************************
 * File Name     : $Source: target.h $
 * Mod. Revision : $Revision: 1.5 $
 * Mod. Date     : $Date: 2019/05/07 11:24:03JST $
 * Device(s)     : RV40 Flash based RH850 microcontroller
 * Description   : Target specific defines
 *********************************************************************************************************************/

/*********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only  intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY
 * REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * www.renesas.com/disclaimer
 *
 * Copyright (C) 2015-2019 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

#ifndef TARGET_H
#define TARGET_H

//#if R_FCL_COMPILER == R_FCL_COMP_GHS
//    #include "dr7f701587.dvf.h"
//#elif R_FCL_COMPILER == R_FCL_COMP_IAR
//    #include "ior7f701587.h"
//#elif R_FCL_COMPILER == R_FCL_COMP_REC
//    #include "iodefine.h"
//#endif


/************************************************************************************************************
Macro definitions
************************************************************************************************************/
#define protected_write(preg,pstatus,reg,value)             \
do                                                          \
{                                                           \
    (preg) = 0xa5u;                                         \
    (reg) = (value);                                        \
    (reg) = ~(value);                                       \
    (reg) = (value);                                        \
} while ((pstatus) == 1u)


#define FLMD0_PROTECTION_OFF    (0x01u)
#define FLMD0_PROTECTION_ON     (0x00u)

#define FCL_INIT_FLASHACCESS                                        \
            volatile uint32_t i;                                    \
                                                                    \
            /* enable FLMD0 */                                      \
            FLMD.PCMD = 0xa5;                                        \
            FLMD.CNT  = FLMD0_PROTECTION_OFF;                        \
            FLMD.CNT  = ~FLMD0_PROTECTION_OFF;                       \
            FLMD.CNT  = FLMD0_PROTECTION_OFF;                        \
            for (i = 0; i < 10000; i++)                             \
            {                                                       \
                /* do nothing ... delay time may depend on */       \
                /* external FLMD0 pin connection */                 \
            }

#define FCL_DISABLE_FLASHACCESS                                     \
            volatile uint32_t i;                                    \
                                                                    \
            /* enable FLMD0 */                                      \
            FLMD.PCMD = 0xa5;                                        \
            FLMD.CNT  = FLMD0_PROTECTION_ON;                         \
            FLMD.CNT  = ~FLMD0_PROTECTION_ON;                        \
            FLMD.CNT  = FLMD0_PROTECTION_ON;                         \
            for (i = 0; i < 10000; i++)                             \
            {                                                       \
                /* do nothing ... delay time may depend on */       \
                /* external FLMD0 pin connection */                 \
            }

/************************************************************************************************************
Typedef definitions
************************************************************************************************************/


/************************************************************************************************************
Exported global variables
************************************************************************************************************/


/************************************************************************************************************
Exported global functions (to be accessed by other files) 
************************************************************************************************************/



#endif /* end of TARGET_H */
