
#include "r_cg_macrodriver.h"

typedef enum
{

    EEPROM_SIGNAL_START_E,
    EEPROM_SIGNAL_DUMMY1_E = EEPROM_SIGNAL_START_E,
    EEPROM_SIGNAL_DUMMY2_E,
    EEPROM_SIGNAL_DUMMY3_E,
    EEPROM_SIGNAL_DUMMY4_E,
    EEPROM_SIGNAL_END_E, 
}EEPROM_EntryName_En_t;

typedef const struct
{
    EEPROM_EntryName_En_t     EEPROM_EntryName_En;    /* Message Name */
    uint32_t                  StartAddress_u32;   /* Starting address of the message */
    uint16_t                  Size_u16;           /* Length of the data to be stored in memory */
}EEPROM_AddrConfig_St_t;
/**
* Function name    : DFlash_Init()
* @brief           : Initialize EEPROM
* @param           : none
* @return          : bool
*/
extern bool DFlash_Init(void);
/**
* Function name    : DFlash_Write()
* @brief           : Write data to eeprom.
* @param           : EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8.
* @return          : bool
*/
extern bool DFlash_Write(EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8);
/**
* Function name    : DFlash_Read()
* @brief           : Read data From eeprom.
* @param           : EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8.
* @return          : bool
*/
extern bool DFlash_Read(EEPROM_EntryName_En_t EEPROM_EntryName_En, uint8_t *Data_u8);
/**
* Function name    : Write()
* @brief           : Write data to eeprom.
* @param           : uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8.
* @return          : bool
*/
extern bool Write(uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8);
/**
* Function name    : Read()
* @brief           : Read data From eeprom.
* @param           : uint32_t Address_u32, uint16_t Size, uint8_t *Data_u8.
* @return          : bool
*/
extern bool Read(uint32_t Address_u32, uint16_t Size_u16, uint8_t *Data_u8);