/***********************************************************************************************************************
* File Name    : Ext_WDT.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 07/07/2021
***********************************************************************************************************************/

#ifndef EXTERNAL_WDT_H
#define EXTERNAL_WDT_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define HAND_SHAKE_DATA				0xD1U
#define UART_TX_BYTE_LEN			1

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern uint8_t UART_Tx_Byte;
extern uint8_t UART_Rx_Byte;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void HandShakeWithWDT(void);

#endif /* EXTERNAL_WDT_H */


