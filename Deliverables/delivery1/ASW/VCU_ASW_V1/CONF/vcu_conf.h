/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : vcu_conf.h
|    Project	    : VCU
|    Module         : VCU configuration
|    Description    : This file contains the macros which specifies module  			
|                     that VCU supported.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	     Name                      Company
| --------     ---------------------     ---------------------------------------
| 12/04/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef	VCU_CONF_H_
#define VCU_CONF_H_

#include "r_cg_macrodriver.h"


#define	VCU_CONF_HMI_SUPPORTED		(TRUE)	//TODO Configurable Parameter
#define	VCU_CONF_BMS_SUPPORTED		(TRUE)	//TODO Configurable Parameter
#define	VCU_CONF_MC_SUPPORTED		(TRUE)	//TODO Configurable Parameter

#endif /* BOARD_INIT_H */
/*---------------------- End of File -----------------------------------------*/
