/***********************************************************************************************************************
* File Name    : VehicleSpeedCal.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 08/02/2022
***********************************************************************************************************************/
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "VehicleSpeedCal.h"
#include "Communicator.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
static uint32_t TimeCounter_5ms_u32 	= 0;
static int16_t 	PrevMotorSpeedRpm_s16 	= 0;
static uint16_t CalVehicleSpeed_u16 	= 0;	
static uint16_t	SpeedToDisp_u16			= 0;
static uint16_t LPF_Output_u16			= 0;

/***********************************************************************************************************************
* Function Name: CalculateVehicleSpeed
* Description  : This function calculates the Vehicle Speed in Kmph based on the Motor Speed in RPM value received
					from the MCU.
* Arguments    : int16_t PresentMotorSpeedRpm_s16
* Return Value : None
***********************************************************************************************************************/
uint16_t CalculateVehicleSpeed(int16_t PresentMotorSpeedRpm_s16)
{
	//Calculate the Present Vehicle Speed based on the MotorSpeed in RPM received from the MCU
	float	 WheelSpeedRpm_t32			= 0;
	float    CalVehicleSpeedKmph_t32 	= 0;
	uint16_t MotorSpeedRpm_u16 			= 0; /*To store absolute value of the MotorSpeed RPM*/ 
	
	TimeCounter_5ms_u32++; /*Increment by 1 for every 5m-sec*/

	if(PresentMotorSpeedRpm_s16 != PrevMotorSpeedRpm_s16)
	{
		if(PresentMotorSpeedRpm_s16 >= FORWARD_MODE_RPM_START_RANGE)
		{
			/*Vehicle is in Forward Mode*/
			MotorSpeedRpm_u16 = (uint16_t)PresentMotorSpeedRpm_s16;
		}
		else
		{
			/*Vehicle is in Reverse Mode*/
			MotorSpeedRpm_u16 = (uint16_t)(PresentMotorSpeedRpm_s16 & 0x7FFFU);
											/*Suppress the sign bit to take the absolute value of the RPM*/
		}
		
		if(GET_VEHICLE_STATE() != VEHICLE_STANDSTILL_MODE)
		{
			/*Calculate Present Vehicle Speed in Kmph*/
			WheelSpeedRpm_t32 = (float)(((float)MotorSpeedRpm_u16 * MCU_RPM_FACTOR) / GEAR_RATIO); 
			CalVehicleSpeedKmph_t32 = (float)((WheelSpeedRpm_t32 * PIE * WHEEL_DIAMETER * 60) / 1000);
			CalVehicleSpeed_u16 = (uint16_t)CalVehicleSpeedKmph_t32;
			if(((CalVehicleSpeedKmph_t32 - (uint32_t)CalVehicleSpeedKmph_t32)*1000) >= 500)
			{
				CalVehicleSpeed_u16 = (CalVehicleSpeed_u16 + 1);
			}
		}
		else
		{
			CalVehicleSpeed_u16 = 0;
				/*Reset Speed to zero, when the vehicle is in stand-still mode*/
		}
		
		PrevMotorSpeedRpm_s16 = PresentMotorSpeedRpm_s16;
	}
	else
	{
		if(GET_VEHICLE_STATE() == VEHICLE_STANDSTILL_MODE)
		{
			CalVehicleSpeed_u16 = 0;
				/*Reset Speed to zero, when the vehicle is in stand-still mode*/
		}
	}

	if(0 == (TimeCounter_5ms_u32 % SPEED_LPF_PERIODICITY))
	{
		/*Schedule Vehicle Speed filter periodically*/
		//Set_VehicleSpeed(Filter_Vehicle_Speed(Get_CalVehicleSpeed())); // todo Manjesh
	}

	return CalVehicleSpeed_u16;
}


/***********************************************************************************************************************
* Function Name: Get_CalVehicleSpeed
* Description  : This function returns the latest calculated speed in Kmph.
* Arguments    : None
* Return Value : uint16_t CalVehicleSpeed_u16
***********************************************************************************************************************/
uint16_t Get_CalVehicleSpeed(void)
{
	return CalVehicleSpeed_u16;
}


/***********************************************************************************************************************
* Function Name: Get_VehicleSpeed
* Description  : This function returns the latest speed in Kmph [LPF Output].
* Arguments    : None
* Return Value : uint16_t SpeedToDisp_u16
***********************************************************************************************************************/
uint16_t Get_VehicleSpeed(void)
{
	return SpeedToDisp_u16;
}


/***********************************************************************************************************************
* Function Name: Set_VehicleSpeed
* Description  : This function returns the latest speed in Kmph [LPF Output].
* Arguments    : uint16_t LPF_SpeedToDisp_u16
* Return Value : None
***********************************************************************************************************************/
void Set_VehicleSpeed(uint16_t LPF_SpeedToDisp_u16)
{
	SpeedToDisp_u16 = LPF_SpeedToDisp_u16;
	return;
}


/***********************************************************************************************************************
* Function Name: Filter_Vehicle_Speed
* Description  : This function implements filter to supress the Vehicle Speed Oscillations.
* Arguments    : uint16_t Present_Calculated_Speed_u16
* Return Value : None
***********************************************************************************************************************/
uint16_t Filter_Vehicle_Speed(uint16_t Present_Calculated_Speed_u16)
{
	 return Vehicle_Speed_LPF(Present_Calculated_Speed_u16, &LPF_Output_u16, SPEED_LPF_POS_GAIN,
								SPEED_LPF_NEG_GAIN);
}


/***********************************************************************************************************************
* Function Name: Low_Pass_Filter
* Description  : This function Implements the Low Pass Filter.
* Arguments    : uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16
* Return Value : None
***********************************************************************************************************************/
uint16_t Vehicle_Speed_LPF(uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16)
{
	uint16_t DiffVar_u16 	= 0;
	uint16_t OffsetVal_u16  = 0;
	
	// If the current measeured value is not equal to the input
	if(Input_u16 != *Output_pu16)
	{
		// Compute the filter output by using the formula for the first order low pass filter
        	// y[i] = y[i-1] + alpha(x[i] - y[i-1])
		if(Input_u16 > *Output_pu16)
		{
			DiffVar_u16 = (uint16_t)(Input_u16 - *Output_pu16);
			
			if(PosConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)PosConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		else
		{
			DiffVar_u16 = (uint16_t)(*Output_pu16 - Input_u16);
			
			if(NegConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)NegConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		
		// If the step value is 0 then set it to 1
		if(OffsetVal_u16 == 0)
		{
			OffsetVal_u16 = 1;
		}
		else
		{
			;
		}
		
		if(Input_u16 > *Output_pu16)
		{
			// Input curve is moving up, hence step up the result.
			*Output_pu16 += OffsetVal_u16;
		}
		else
		{
			// Input curve is moving down, hence step down the result.
			*Output_pu16 -= OffsetVal_u16;
		}
	}
	else
	{
		;
	}
	
	return (*Output_pu16);
}


/***********************************************************************************************************************
* Function Name: Reset_SpeedCal_Data
* Description  : This function resets/clears the data/states used for vehicle speed calculation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Reset_SpeedCal_Data(void)
{
	PrevMotorSpeedRpm_s16 	= 0;
	CalVehicleSpeed_u16 	= 0;
	SpeedToDisp_u16			= 0;
	LPF_Output_u16			= 0;
	TimeCounter_5ms_u32		= 0;
	return;
}

/********************************************************EOF***********************************************************/