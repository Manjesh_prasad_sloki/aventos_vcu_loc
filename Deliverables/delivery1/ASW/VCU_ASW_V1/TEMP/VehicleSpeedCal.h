/***********************************************************************************************************************
* File Name    : VehicleSpeedCalc.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 07/02/2022
***********************************************************************************************************************/

#ifndef VEHICLE_SPEED_CALC_H
#define VEHICLE_SPEED_CALC_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	

#include "r_cg_macrodriver.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define MCU_RPM_FACTOR								0.305176
#define WHEEL_DIAMETER								0.590 /*in meters*/
#define GEAR_RATIO									12
#define PIE											3.142

#define FORWARD_MODE_RPM_START_RANGE				0 /*RPM*/

#define VEHICLE_STANDSTILL_MODE						0x01U
#define VEHICLE_MOVEMENT_MODE						0x00U

/*todo:configurable*/ //Gain Range : 0[0%] to 10,000[100%] 
#define SPEED_LPF_POS_GAIN				            1000 /*Presently @ 10%*/
#define SPEED_LPF_NEG_GAIN				            SPEED_LPF_POS_GAIN

#define SPEED_LPF_PERIODICITY                       10 /*50ms*/ /*todo:configurable*/   



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern uint16_t CalculateVehicleSpeed(int16_t);
extern uint16_t Get_CalVehicleSpeed(void);
extern void 	Reset_SpeedCal_Data(void);
extern uint16_t	Get_VehicleSpeed(void);
extern uint16_t Filter_Vehicle_Speed(uint16_t ); 
uint16_t Vehicle_Speed_LPF(uint16_t, uint16_t *, int16_t, int16_t);


#endif /* VEHICLE_SPEED_CALC_H */


