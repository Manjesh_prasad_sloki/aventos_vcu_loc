/***********************************************************************************************************************
* File Name    : uart_user.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 06/07/2021
***********************************************************************************************************************/

#ifndef UART_USER_H
#define UART_USER_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* UART_USER_H */


