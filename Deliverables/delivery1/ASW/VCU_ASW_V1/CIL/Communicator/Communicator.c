/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File	    : Communicator.c
|    Project	    : VCU
|    Module         : Communicator module 
|    Description    : This file contains the variables and functions 			
|                     to Communicator with BMS, VCU, HMI, and other module.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date     	      Name                        Company
| ----------     ---------------     -----------------------------------
| 06/06/2021       Sandeep K Y         Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef COMMUNICATOR_C
#define COMMUNICATOR_C

/*******************************************************************************
 *  Includes
 ******************************************************************************/

#include "Communicator.h"
#include "r_cg_macrodriver.h"
#include "vcu.h"
#include "cluster.h"
#include "driver_modes.h"
#include "vcu_conf.h"
#if(TRUE == VCU_CONF_BMS_SUPPORTED)
#include "bms.h"
#endif

/*******************************************************************************
 *  macros
 ******************************************************************************/



//  #define Set_Analog_throttle_error(x)   	      (Motor_0x150_SetRx.Analog_throttle_error_u8 = x)    
//  #define Set_Battery_current(x)                   (Motor_0x150_SetRx.Battery_current_s16 = x)    
//  #define Set_Controller_temp_warn(x)   	          (Motor_0x150_SetRx.Controller_temp_warning_u8 = x)    
//  #define Set_Driving_direction_actual(x)   	      (Motor_0x150_SetRx.Driving_direction_actual_u8 = x)   
//  #define Set_Motor_Enable_Status(x)   	          (Motor_0x150_SetRx.Motor_Enable_Status_u8 = x)    
//  #define Set_Motor_Speed(x)                       (Motor_0x150_SetRx.Motor_Speed_s16 = x)    
//  #define Set_Motor_temperature_warn(x)   	      (Motor_0x150_SetRx.Motor_temperature_warning_u8 = x)    
//  #define Set_Over_current_error(x)   	          (Motor_0x150_SetRx.Over_current_error_u8 = x)   
//  #define Set_Over_Voltage_error(x)   	          (Motor_0x150_SetRx.Over_Voltage_error_u8 = x)    
//  #define Set_Peripheral_supply_failure(x)         (Motor_0x150_SetRx.Peripheral_supply_failure_u8 = x)    
//  #define Set_Position_Sensor_Error(x)   	      (Motor_0x150_SetRx.Position_Sensor_Error_u8 = x)    
//  #define Set_Reserved_for_0x150_LTVS_s16(x)   	  (Motor_0x150_SetRx.Reserved_for_LTVS_s16 = x)   
//  #define Set_Ride_mode_Actual(x)   	              (Motor_0x150_SetRx.Ride_mode_Actual_u8 = x)    
//  #define Set_Throttle_Disable_Status(x)           (Motor_0x150_SetRx.Throttle_Disable_Status_u8 = x)    
//  #define Set_Throttle_Map_Actual(x)   	          (Motor_0x150_SetRx.Throttle_Map_Actual_u8 = x)    
//  #define Set_Timeout_error(x)   	              (Motor_0x150_SetRx.Timeout_error_u8 = x)   
//  #define Set_Under_Voltage_Error(x)   	          (Motor_0x150_SetRx.Under_Voltage_Error_u8 = x)
/*******************************************************************************
 *  GLOBAL VARIABLES
 ******************************************************************************/ 
static Cluster_Tx_Eve_St_t Cluster_Tx_Eve_St[HMI_EVENT_END_E];
// uint8_t HMI_Comm_u8 = false;
// uint8_t BMS_Comm_u8 = false;
// uint8_t MC_Comm_u8  = false;

bool HMI_Comm_b = false;
bool BMS_Comm_b = false;
bool MCU_Comm_b = false;
bool VehicleStandStill_b		= false;
uint32_t Vehicle_Time_out_u32 = 0;

// bool Vehicle_TurnOn_b = false;
// bool Vehicle_ignition_b = false;

HMI_Tx_Per_1_t Cluster_Tx_Per_1;
HMI_Tx_Per_2_t Cluster_Tx_Per_2;

static VCU_MCU_0x100_Tx_t	VCU_MCU_0x100_SetTx;

/*static Vehicle_0x100_Tx_t        Vehicle_0x100_SetTx; TODO: Jeevan*/
/*Motor_0x150_Rx_t	      Motor_0x150_SetRx; TODO: Jeevan*/

int16_t Motor_Speed_s16 = 0;

bool Breaks_Applied_b = false;
bool CalculateRangeMileage_b = false;
bool Regen_Applied_b = false; 
static uint16_t SafeMode_Data_u16 = 0;
SpeedFilter_St_t		SpeedFilter_St =
{
	0, 	/*Present Calculated Speed*/
	0, 	/*Previous Calculated Speed*/
	0, 	/*From_Speed*/
	0, 	/*To_Speed*/
	false,	/*Increase Speed Flag*/
	false   /*Decrease Speed Flag*/
};

/*******************************************************************************
 *  FUNCTION PROTOTYPES
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_State_Proc
*   Description   : This function implements State Machine operation.
*   Parameters    : None
*   Return Value  : None
*******************************************************************************/ 
// static void BMS_State_Proc(void);
 uint16_t VehicleSpeedFilter(uint16_t);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SET_CLUSTER_DATA
*   Description   : This function Set the cluster data.
*   Parameters    : Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En, uint8_t data
*   Return Value  : None
*******************************************************************************/ 
void SET_CLUSTER_DATA(Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En, uint8_t data)
{
	if ((CLUSTER_EVENT_START_E <= Cluster_Tx_Eve_En) && (CLUSTER_EVENT_END_E > Cluster_Tx_Eve_En))
	{
		Cluster_Tx_Eve_St[Cluster_Tx_Eve_En].data = data;
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_PER_1_DATA
*   Description   : This function get the periodic 1 cluster data.
*   Parameters    : None
*   Return Value  : HMI_Rx_Per_1_t
*******************************************************************************/ 
HMI_Tx_Per_1_t GET_CLUSTER_PER_1_DATA(void)
{
	Cluster_Tx_Per_1.Vehicle_Speed_u8 =  (uint8_t)Get_Speed_Display();
	Cluster_Tx_Per_1.ODO_u32 = Get_ODO_meters();
	Cluster_Tx_Per_1.Mileage_u8 = BMS_Get_Mileage_Wh_Km();	
	return Cluster_Tx_Per_1;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_PER_2_DATA
*   Description   : This function get the periodic 2 cluster data.
*   Parameters    : None
*   Return Value  : HMI_Rx_Per_2_t
*******************************************************************************/
HMI_Tx_Per_2_t GET_CLUSTER_PER_2_DATA(void)
{
	Cluster_Tx_Per_2.Battery_SOC_u8 = Get_BMS_SOC();
	Cluster_Tx_Per_2.Power_Consumption_u16 = Get_Power_consum_bar();
	//Cluster_Tx_Per_2.Power_Consumption_u16 = 3;
	if (true == Get_Regen_State())
	{
		Cluster_Tx_Per_2.Regen_Braking_u8 = THREE;
	}
	else
	{
		Cluster_Tx_Per_2.Regen_Braking_u8 = ZERO;
	}
	Cluster_Tx_Per_2.Range_Km_u16 = BMS_Get_Range_Km();
	//Cluster_Tx_Per_2.Range_Km_u16 = 50; //todo remove Manjesh
	return Cluster_Tx_Per_2;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_CLUSTER_DATA
*   Description   : This function get the cluster data.
*   Parameters    : Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En
*   Return Value  : uint8_t
*******************************************************************************/ 
uint8_t GET_CLUSTER_DATA(Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En)
{
	if ((CLUSTER_EVENT_START_E <= Cluster_Tx_Eve_En) && (CLUSTER_EVENT_END_E > Cluster_Tx_Eve_En))
	{
		return Cluster_Tx_Eve_St[Cluster_Tx_Eve_En].data;
	}
	else
	{
		return 0;
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : CLEAR_CLUSTER_DATA
*   Description   : This function clears the cluster data.
*   Parameters    : None.
*   Return Value  : None.
*******************************************************************************/ 
void CLEAR_CLUSTER_DATA(void)
{
	Cluster_Tx_Eve_En_t Cluster_Tx_Eve_En = CLUSTER_EVENT_START_E;
	for (Cluster_Tx_Eve_En = CLUSTER_EVENT_START_E; 
		Cluster_Tx_Eve_En < CLUSTER_EVENT_END_E; 
		Cluster_Tx_Eve_En++)
	{
		Cluster_Tx_Eve_St[Cluster_Tx_Eve_En].data = ZERO;
	}
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : GET_VCU_STATE
*   Description   : This function get the vcu state.
*   Parameters    : None
*   Return Value  : Vehicle_State_En_t
*******************************************************************************/ 
Vehicle_State_En_t GET_VCU_STATE(void)
{
    //#if(false)
    return (Vehicle_State_En_t)VCU_GetState();
    //#endif
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SafeMode_SetEvent
*   Description   : This function implements Safe Mode Event setting.
*   Parameters    : Vehicle_SafeMode_Event_En_t Vehicle_SafeMode_Event_En, 
*					Vehicle_SafeMode_Set_En_t SafeMode_Set_En
*   Return Value  : None
*******************************************************************************/
void SafeMode_SetEvent(Vehicle_SafeMode_Event_En_t Vehicle_SafeMode_Event_En, Vehicle_SafeMode_Set_En_t SafeMode_Set_En)
{
	Vehicle_SafeMode_Event_En_t SafeMode_En;
	Vehicle_State_En_t Vehicle_State_En = GET_VCU_STATE();

	if ((VEHICLE_START_STATE_E == Vehicle_State_En) ||
		(VEHICLE_RUN_STATE_E == Vehicle_State_En) ||
		(VEHICLE_SAFE_MODE_STATE_E == Vehicle_State_En))
	{
		for (SafeMode_En = VEHICLE_SAFE_MODE_START_E; SafeMode_En < VEHICLE_SAFE_MODE_END_E; SafeMode_En++)
		{
			if (SafeMode_En == Vehicle_SafeMode_Event_En)
			{
				if (VEHICLE_SAFE_MODE_ON_E == SafeMode_Set_En)
				{
					if (ZERO == ((SafeMode_Data_u16 >> SafeMode_En) & 0x01U))
					{
 						SafeMode_Data_u16 |= (1 << SafeMode_En);
					}
					else
					{
						/* code */
					}
				}
				else if (VEHICLE_SAFE_MODE_OFF_E == SafeMode_Set_En)
				{
					if (ONE == ((SafeMode_Data_u16 >> SafeMode_En) & 0x01U))
					{
						SafeMode_Data_u16 &= (~(1 << SafeMode_En));
					}
					else
					{
						/* code */
					}
				}
				else
				{
					/* code */
				}
			}
		}
	}
	else
	{
		/* code */
	}
	
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : SafeMode_ClearEvent
*   Description   : This function clears Safe Mode Event setting.
*   Parameters    : None.
*   Return Value  : None.
*******************************************************************************/
void SafeMode_ClearEvent(void)
{
	SafeMode_Data_u16 = ZERO;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_SafeMode_Data
*   Description   : This function Gets Safe Mode data.
*   Parameters    : None, 
*   Return Value  : uint32_t
*******************************************************************************/
uint32_t Get_SafeMode_Data(void)
{
	return SafeMode_Data_u16;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_SafeMode_State
*   Description   : This function Gets Safe Mode State.
*   Parameters    : None, 
*   Return Value  : bool
*******************************************************************************/
bool Get_SafeMode_State(void)
{
	//#if(false)
	return DriverSafe_Mode_State();
	//#endif
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Vehicle_Speed
*   Description   : This function Gets Current Vehicle Speed.
*   Parameters    : None, 
*   Return Value  : uint16_t
*******************************************************************************/
uint16_t Get_Vehicle_Speed(void)
{
	return Get_Vehicle_Speed_kmph();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Actual_Ride_Mode
*   Description   : This function Gets Actaul Ride mode from Motor.
*   Parameters    : None, 
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t Get_Actual_Ride_Mode(void)
{
	return Get_Actual_MC_Ride_Mode();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Drive_Dir
*   Description   : This function Sets Vehicle direction.
*   Parameters    : Vehicle_Direction_En_t Vehicle_Direction_En 
*   Return Value  : None
*******************************************************************************/
void Set_Drive_Dir(Vehicle_Direction_En_t Vehicle_Direction_En)
{
//	Vehicle_0x100_SetTx.DrivingDirection_u8 = Vehicle_Direction_En;  /*TODO: Jeevan*/
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Drive_Dir
*   Description   : This function Gets Vehicle direction.
*   Parameters    : None
*   Return Value  : Vehicle_Direction_En_t
*******************************************************************************/
//Vehicle_Direction_En_t Get_Drive_Dir(void)
//{
////	return (Vehicle_Direction_En_t)Vehicle_0x100_SetTx.DrivingDirection_u8;
//}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Ride_mode_Request
*   Description   : This function Sets Ride Mode.
*   Parameters    : uint8_t Ride_mode_u8
*   Return Value  : None
*******************************************************************************/
void Set_Ride_mode_Request(uint8_t Ride_mode_u8)
{
	VCU_MCU_0x100_SetTx.Ride_mode_Request = Ride_mode_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Ride_mode_Request
*   Description   : This function Gets Ride Mode.
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
uint8_t Get_Ride_mode_Request(void)
{
	return VCU_MCU_0x100_SetTx.Ride_mode_Request;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Motor_Stop_Request
*   Description   : This function Gets Motor Stop request.
*   Parameters    : uint8_t Motor_Request_u8
*   Return Value  : None
*******************************************************************************/
void Set_Motor_Stop_Request(uint8_t Motor_Request_u8)
{
//	Vehicle_0x100_SetTx.MotorStop_u8 = Motor_Request_u8;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Motor_Stop_Request
*   Description   : This function Gets Motor Stop request.
*   Parameters    : None
*   Return Value  : uint8_t
*******************************************************************************/
//uint8_t Get_Motor_Stop_Request(void)
//{
////	return Vehicle_0x100_SetTx.MotorStop_u8;
//}
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Set_Motor_Speed_Limit
*   Description   : This function Sets Motor Speed limit.
*   Parameters    : int16_t Motor_Speed_s16
*   Return Value  : None
*******************************************************************************/
void Set_Motor_Speed_Limit(int16_t Motor_Speed_s16)
{
	VCU_MCU_0x100_SetTx.Motor_Speed_Limit = Motor_Speed_s16;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DECLERATION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_Motor_Speed_Limit
*   Description   : This function Gets Motor Speed limit.
*   Parameters    : None
*   Return Value  : int16_t
*******************************************************************************/
int16_t Get_Motor_Speed_Limit(void)
{
	return VCU_MCU_0x100_SetTx.Motor_Speed_Limit;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Get_BatteryCharging_State
*   Description   : This function Gets battery charging State.
*   Parameters    : None, 
*   Return Value  : bool
*******************************************************************************/
bool Get_BatteryCharging_State(void)
{
	//#if(false)
	return BMS_Get_Charging_state();
	//#endif
}
#endif /* COMMUNICATOR_C */

/***********************************************************************************************************************
* Function Name: Store_ODO_To_FEE
* Description  :
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Store_ODO_To_FEE(void)
{   
	// todo Manjesh	
   // ODO_Estimate_St.Present_ODO_u32 = 0;//Update present ODO to EEPROM on SHUT DOWN
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Battery_Voltage
*   Description   : This function Gets battery voltage.SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
*   Parameters    : None, 
*   Return Value  : float
*******************************************************************************/
 float BMS_Get_Battery_Voltage(void)
{
	return BMS_Get_Voltage();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Battery_Current
*   Description   : This function Gets battery Current.
*   Parameters    : None, 
*   Return Value  : float
*******************************************************************************/
 real_t BMS_Get_Battery_Availability_Capacity(void)
{
	return BMS_Get_Availability_Capacity();
}


 uint16_t BMS_Get_Battery_Availabe_Energy(void)
{
	return BMS_Get_Availabe_Energy();
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : BMS_Get_Battery_Current
*   Description   : This function Gets battery Current.
*   Parameters    : None, 
*   Return Value  : float
*******************************************************************************/

float BMS_Get_Battery_Current(void)
{
	return BMS_Get_Current();
}
/***********************************************************************************************************************
* Function Name: VehicleSpeedLPF
* Description  : Filter function to prepare for the Speed oscillations. Call this function @x ms rate periodically
		 [Ex: call @50-ms rate].
* Arguments    : uint16_t PresentSpeedKmph_u16
* Return Value : uint16_t SpeedToDisp_u16
***********************************************************************************************************************/
uint16_t VehicleSpeedFilter(uint16_t PresentSpeedKmph_u16)//VehicleSpeedKmph_u16//for hmi only
{	
	uint16_t SpeedToDisp_u16 = 0;
	
	SpeedFilter_St.PresentSpeed_u16 = PresentSpeedKmph_u16;
	
	if(SpeedFilter_St.PresentSpeed_u16 != SpeedFilter_St.PrevSpeed_u16)
	{
		int16_t SpeedRange_s16 = 0;
		
		SpeedFilter_St.ToSpeed_u16 = SpeedFilter_St.PresentSpeed_u16;
		
		SpeedRange_s16 = SpeedFilter_St.FromSpeed_u16 - SpeedFilter_St.ToSpeed_u16;
		
		if(SpeedRange_s16 < 0)
		{
			SpeedRange_s16 = ((SpeedRange_s16)*(-1));
			SpeedFilter_St.IncreaseSpeed_b = true;
			SpeedFilter_St.DecreaseSpeed_b = false;
		}
		else
		{
			SpeedFilter_St.DecreaseSpeed_b = true;
			SpeedFilter_St.IncreaseSpeed_b = false;
		}
		
		SpeedFilter_St.PrevSpeed_u16 = SpeedFilter_St.PresentSpeed_u16;
	}
	
	
	if(SpeedFilter_St.FromSpeed_u16 != SpeedFilter_St.ToSpeed_u16)
	{
		if(true == SpeedFilter_St.IncreaseSpeed_b)
		{
			SpeedFilter_St.FromSpeed_u16 += 1;
		}
		else if(true == SpeedFilter_St.DecreaseSpeed_b)
		{
			SpeedFilter_St.FromSpeed_u16 -= 1;
		}
		else
		{
			;
		} 
	}
	
	SpeedToDisp_u16 = SpeedFilter_St.FromSpeed_u16;
	
	return SpeedToDisp_u16;
}



/***********************************************************************************************************************
* Function Name: ResetTheSpeedFilterData
* Description  : This function resets the data and flags related to Speed Filter algorithm.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheSpeedFilterData(void)
{
	SpeedFilter_St.PresentSpeed_u16 	= 0;
	SpeedFilter_St.PrevSpeed_u16 		= 0;
	SpeedFilter_St.FromSpeed_u16 		= 0;
	SpeedFilter_St.ToSpeed_u16 		    = 0;
	SpeedFilter_St.IncreaseSpeed_b 		= false;
	SpeedFilter_St.DecreaseSpeed_b 		= false;
	
	return;
}

/*---------------------- End of File -----------------------------------------*/