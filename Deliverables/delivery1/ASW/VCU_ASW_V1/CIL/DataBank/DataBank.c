/***********************************************************************************************************************
* File Name    : DataBank.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataBank.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
UserInputSig_St_t	UserInputSig_St = 
{
	TURNED_OFF_E,	/*High Beam Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Lights Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Left Turn Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Right Turn Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Brake Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Brake Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Key Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Kill Switch - Default Turned-Off*/
	TURNED_OFF_E,   /*SOC Switch - Default Turned-Off*/
	TURNED_OFF_E,	/*Kick Stand Switch - Default Turned-Off*/
	0	,	/*Throttle  - ZERO*/
	
	false,		/*VehicleFallenState - Default false*/
	
	/*Driver Mode Selected*/
	TURNED_ON_E,		/*Neutral Mode*/ 
	TURNED_OFF_E,		/*Economy Mode*/
	TURNED_OFF_E,		/*Sports Mode*/
	TURNED_OFF_E,		/*Reverse Mode*/
	/*Driver Mode Selected but not set*/
	// TURNED_OFF_E,
	// TURNED_OFF_E,
	// TURNED_OFF_E,
	// TURNED_OFF_E
};

UserOutputSig_St_t UserOutputSig_St =
{
	TURNED_OFF_E,	/*Relay1*/// todo check Manjesh wat is default value
	TURNED_OFF_E,	/*Relay2*/// todo check Manjesh wat is default value
	TURNED_OFF_E,	/*Relay3*/// todo check Manjesh wat is default value
	TURNED_OFF_E,	/*Torque - Default Turned-Off*/
	TURNED_OFF_E,    /*Motor_Kill - Default Turned-Off*/
};

/***********************************************************************************************************************
* Function Name: function_name
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/





/********************************************************EOF***********************************************************/