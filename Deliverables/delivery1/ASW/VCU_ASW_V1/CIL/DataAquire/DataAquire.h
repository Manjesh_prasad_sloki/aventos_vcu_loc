/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 09/03/2022
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define HIGH_BEAM_DEBOUNCE_COUNT					1
#define LIGHT_SWITCH_DEBOUNCE_COUNT					1
#define LEFT_TURN_DEBOUNCE_COUNT					1
#define RIGHT_TURN_DEBOUNCE_COUNT					1
#define BRAKE_DEBOUNCE_COUNT						1
#define EXTRA_1_DEBOUNCE_COUNT						1
#define EXTRA_2_DEBOUNCE_COUNT						1
#define REGEN_DEBOUNCE_COUNT                        1
#define DIAG_FEEDBACK_1_DEBOUNCE_COUNT              1
#define DIAG_FEEDBACK_2_DEBOUNCE_COUNT              1
#define DIAG_FEEDBACK_3_DEBOUNCE_COUNT              1


#define KILL_SWITCH_DEBOUNCE_COUNT					1
#define KICK_STAND_DEBOUNCE_COUNT					1

#define MODE_SELECT_NEUTRAL_DEBOUNCE_COUNT          1
#define MODE_SELECT_ECO_DEBOUNCE_COUNT		        1
#define MODE_SELECT_SPORTS_DEBOUNCE_COUNT			1 
#define MODE_SELECT_REVERSE_DEBOUNCE_COUNT			1 

#define MIN_ADC_VALUE_FOR_TURN_ON					2200
#define MIN_ADC_VALUE_FOR_TURN_OFF					100
#define SW_LONG_PRESS                               3000U

#define GPIO_PIN_SET							1
#define GPIO_PIN_CLR							0

#define NEUTEAL_MODE_VALUE						0
#define ECONOMY_MODE_VALUE						1
#define SPORTS_MODE_VALUE						2
#define REVERSE_MODE_VALUE					    (-1)

#define LONG_PRESS_TIME     (3000U)




/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/

typedef enum
{
	INPUT_SIG_START_E,
    /* adc pin*/
	KILL_SWITCH_E = INPUT_SIG_START_E,
	LEFT_TURN_SWITCH_E,
	EXTRA_1_E,
	HED_LIGHT_HIGH ,
	REGEN_SWITCH_E,
	KICK_STAND_SWITCH_E,
	BRAKE_SWITCH_E,
	RIGHT_TURN_SWITCH_E,
	DIAG_FEEDBACK_1_E,
	DIAG_FEEDBACK_2_E,
	DIAG_FEEDBACK_3_E,
	/*interrupt pins */
    MODE_SELECT_NEUTRAL_E,
    MODE_SELECT_ECO_E,
    MODE_SELECT_SPORTS_E,
    MODE_SELECT_REVERSE_E,
	/* PWM pins*/
	EXTRA_2_E,
	INPUT_SIG_END_E,
	TOTAL_USER_INPUT_SIGNALS_E = INPUT_SIG_END_E,
	SOC_SWITCH
}UserInputSig_En_t;

typedef enum
{
	OUTPUT_SIG_START_E,
	OUTPUT_RELAY1_GND_E = OUTPUT_SIG_START_E,
	OUTPUT_RELAY2_GND_E,
	OUTPUT_RELAY3_GND_E,
	OUTPUT_TORQUE_ZERO_E,
	OUTPUT_MOTOR_KILL_E,
	OUTPUT_SIG_END_E,
	TOTAL_USER_OUTPUT_SIGNALS_E = OUTPUT_SIG_END_E

}UserOutputSig_En_t;

typedef enum
{
	KILL_SWITCH_ADC_E,
	LEFT_TURN_ADC_E,
	EXTRA_1_ADC_E,
	HEDLIGHT_ADC_E,

	REGEN_SWITCH_ADC_E,
	KICK_SWITCH_ADC_E,
	BRAKE_ADC_E,
	RIGHT_TURN_ADC_E,
	DIAG_FEEDBACK_1_ADC_E,
	DIAG_FEEDBACK_2_ADC_E,
	DIAG_FEEDBACK_3_ADC_E,

	TOTAL_ADC_INPUT_E
} ADC_Input_Sig_En_t;

typedef void (*ReadInputs_Fptr_t)(UserInputSig_En_t);
typedef void (*WriteOutput_Fptr_t)(UserOutputSig_En_t);
typedef enum
{
	ADC_E,
	GPIO_E,
	EXT_INT_E,
	PWM_E
}InputChannel_En_t;

typedef struct
{
	UserInputSig_En_t	UserInputSig_En;
	InputChannel_En_t	InputChannel_En;
	ReadInputs_Fptr_t  	ReadInputs_Fptr;
	uint16_t		ReadInputVal_u16;
	uint16_t	 	DebounceCounter_u16;
	uint16_t		DebounceCountLimit;
	int8_t 	   		SignalState_s8;
}InputSigConf_St_t;

typedef struct
{
	UserOutputSig_En_t	UserOutputSig_En;
	InputChannel_En_t	OutputChannel_En;
	WriteOutput_Fptr_t	WriteOutput_Fptr;
	uint16_t 			WriteOutputVal_u16;
}OutputSigConf_St_t;

typedef struct 
{
	bool Neutral_Int_flag;
	bool Eco_Int_flag;
	bool Sports_Int_flag;
	bool Reverse_Int_flag;

	/* data */
}IntcSigConf_St_t;

typedef enum
{
	REVERSE_MODE_E = 0,
	NEUTRAL_MODE_E,
	ECONOMY_MODE_E,
	SPORTS_MODE_E,
	SAFE_MODE_E // todo Manjesh check
}DrivingModes_En_t;

typedef struct 
{
	bool Neutral_TurnOn_b;
	bool Neutral_Pressed_First_b;
	uint32_t Timer_u32;
}Date_update_st_t;

typedef enum
{
	BUTTON_RELEASED_E,
	BUTTON_PRESSED_E,
}UserButtonState_En_t;

typedef enum
{
	UIB_REGEN_SWITCH_E,
	UIB_NEUTRAL_SWITCH_E,
	TOTAL_UIB ,
}UIB_Multi_En_t;

typedef struct 
{
	UserButtonState_En_t UserButtonState_En;
	uint32_t TimerCaptureValue_u32;
	bool LongPressed_b;
}UserButton_St_t;
/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern InputSigConf_St_t	InputSigConf_aSt[TOTAL_USER_INPUT_SIGNALS_E];
extern OutputSigConf_St_t	OutputSigConf_aSt[TOTAL_USER_OUTPUT_SIGNALS_E];
extern IntcSigConf_St_t IntcSigConf_St;
extern uint8_t interrupt_buffer_8 ;
//extern IntcSigConf_St_t IntcSigConf_St;
//extern uint16_t		ADC_Result[TOTAL_ADC_INPUT_E+1];
void ReadInputThroghADC(UserInputSig_En_t);
void ReadInputThroghGPIO(UserInputSig_En_t);
void ReadInputThroghINT(UserInputSig_En_t);
void ReadInputThroghPWM(UserInputSig_En_t);

void UpdateSigToDataBank(void);
void Set_Drive_Mode_Counters(UserInputSig_En_t);
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En);

void WriteVcuOutputs(void);
void WriteOutputs(UserOutputSig_En_t	Signal_En);

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadUserInput(void);




#endif /* DATA_AQUIRE_H */
