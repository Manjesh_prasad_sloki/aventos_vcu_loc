#include "DataAquire.h"
#include "DataBank.h"
#include "Config_PORT.h"
#include "Config_ADCA0.h"
#include "adc_user.h"
#include "intc_user.h"
//#include "can_if.h"
#include "cil_can_conf.h"
#include "Config_RTCA0.h"
#include "Config_STBC.h"
#include "communicator.h"



/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


Date_update_st_t Date_update_st;



IntcSigConf_St_t IntcSigConf_St =
{
	true,
	false,
	false,
	false
};

UserButton_St_t UserButton_St[TOTAL_UIB] = 
{
	{BUTTON_PRESSED_E,0,false},
	{BUTTON_RELEASED_E,0,false},
};

 InputSigConf_St_t  InputSigConf_aSt[TOTAL_USER_INPUT_SIGNALS_E]= 
{ 
	{ KILL_SWITCH_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	HIGH_BEAM_DEBOUNCE_COUNT, 	        0x00 },
	{ LEFT_TURN_SWITCH_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	LIGHT_SWITCH_DEBOUNCE_COUNT,        0x00 },
	{ HED_LIGHT_HIGH, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	LEFT_TURN_DEBOUNCE_COUNT, 	        0x00 },
	{ EXTRA_1_E, 		      	ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	RIGHT_TURN_DEBOUNCE_COUNT, 	        0x00 },
	{ REGEN_SWITCH_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	BRAKE_DEBOUNCE_COUNT, 		        0x00 },
	{ KICK_STAND_SWITCH_E, 	        PWM_E, 	    &ReadInputThroghADC, 	0, 	0, 	BRAKE_DEBOUNCE_COUNT, 		        0x00 },
	{ BRAKE_SWITCH_E, 		    ADC_E,      &ReadInputThroghADC, 	0, 	0, 	EXTRA_1_DEBOUNCE_COUNT, 	        0x00 },
	{ RIGHT_TURN_SWITCH_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	BRAKE_DEBOUNCE_COUNT,    	        0x00 },
	{ DIAG_FEEDBACK_1_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	DIAG_FEEDBACK_1_DEBOUNCE_COUNT,    	        0x00 },
	{ DIAG_FEEDBACK_2_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	DIAG_FEEDBACK_2_DEBOUNCE_COUNT,    	        0x00 },
	{ DIAG_FEEDBACK_3_E, 		ADC_E, 	    &ReadInputThroghADC, 	0, 	0, 	DIAG_FEEDBACK_3_DEBOUNCE_COUNT,    	        0x00 },

	{ MODE_SELECT_NEUTRAL_E, 	ADC_E,        &ReadInputThroghINT, 	0, 	0, 	REGEN_DEBOUNCE_COUNT, 	        0x00 },
	{ MODE_SELECT_ECO_E, 	  	EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODE_SELECT_NEUTRAL_DEBOUNCE_COUNT, 	    0x00 },
	{ MODE_SELECT_SPORTS_E, 	EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODE_SELECT_ECO_DEBOUNCE_COUNT,     0x00 },
	{ MODE_SELECT_REVERSE_E, 	EXT_INT_E, 	&ReadInputThroghINT, 	0, 	0, 	MODE_SELECT_SPORTS_DEBOUNCE_COUNT, 	    0x00 },
    	{ EXTRA_2_E, 	  		EXT_INT_E, 	&ReadInputThroghPWM, 	0, 	0, 	MODE_SELECT_REVERSE_DEBOUNCE_COUNT, 	    0x00 }
};

 OutputSigConf_St_t OutputSigConf_aSt[TOTAL_USER_OUTPUT_SIGNALS_E] =
	 {
		 {OUTPUT_RELAY1_GND_E, GPIO_E, &WriteOutputs, 0},
		 {OUTPUT_RELAY2_GND_E, GPIO_E, &WriteOutputs, 0},
		 {OUTPUT_RELAY3_GND_E, GPIO_E, &WriteOutputs,  0},
		 {OUTPUT_TORQUE_ZERO_E, GPIO_E, &WriteOutputs, 0},
		 {OUTPUT_MOTOR_KILL_E, GPIO_E, &WriteOutputs,  0}

 };
 DrivingModes_En_t DrivingModes_En = NEUTRAL_MODE_E;

 uint16_t ADC_Result[TOTAL_ADC_INPUT_E + 1] = {0};

 int16_t Drive_Mode_Counter_s16 = 0;
 uint8_t interrupt_buffer_8 = 0;

 /***********************************************************************************************************************
  * Function Name: ReadUserInput
  * Description  : This function reads the User Input Signals and Update the Signal State into the Data-Bank.
  * Arguments    : None
  * Return Value : None
  ***********************************************************************************************************************/
 void ReadUserInput(void)
 {
	 uint16_t SigCount_u16 = 0;
	 
	 for(SigCount_u16 = INPUT_SIG_START_E; SigCount_u16 < TOTAL_USER_INPUT_SIGNALS_E; SigCount_u16++)
	 {
		 InputSigConf_aSt[SigCount_u16].ReadInputs_Fptr((UserInputSig_En_t)SigCount_u16);
	 }
	 
	 UserInputSig_St.Key_SwitchState_u8 = 0x01U;
	 
	 UpdateSigToDataBank();
	 
	return;
}
uint8_t Read_Throttle_LS_u8 = 0;
uint8_t Read_Throttle_HS_u8 = 0;


/***********************************************************************************************************************
* Function Name: WriteVcuOutputs
* Description  : This function reads the User Input Signals and Update the Signal State into the Data-Bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void WriteVcuOutputs(void)
{
	uint16_t SigCount_u16 = 0;
	 
	 for(SigCount_u16 = OUTPUT_SIG_START_E; SigCount_u16 < TOTAL_USER_OUTPUT_SIGNALS_E; SigCount_u16++)
	 {
		 OutputSigConf_aSt[SigCount_u16].WriteOutput_Fptr((UserInputSig_En_t)SigCount_u16);
	 }
	 
	 //UpdateSigToDataBank();
	 
	return;
}
/***********************************************************************************************************************
* Function Name: ReadInputThroghADC
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghADC(UserInputSig_En_t	Signal_En)
{
	static bool Read_ADC_Result_b = true;
	//static bool Read_Throttle_b = false;
      //  static bool Regen_toggle_b = true; 
	//static  bool   timer_value_capture_b = true;
	//static uint32_t  timer_value_32 = 0 ; 
	//static  bool   Regen_On_idicator_b = false; 
	//static  bool   B_Low_On_idicator_b = false;


	//static uint8_t Read_Throttle_LS_u8 = 0;
	//static uint8_t Read_Throttle_HS_u8 = 0;
	
	
	if(Read_ADC_Result_b)
	{
		R_Config_ADCA0_ScanGroup1_OperationOn();
		while(Group1_Scan_Completed_b == false); /*Wait untill convertion completes*/
		Group1_Scan_Completed_b = false;
		R_Config_ADCA0_ScanGroup1_GetResult(ADC_Result);
		Read_ADC_Result_b = false;
	}
	else
	{
		;
	}
	
	switch(Signal_En)
	{
		case HED_LIGHT_HIGH:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[HEDLIGHT_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
        }
		
		case LEFT_TURN_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[LEFT_TURN_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
		}
		
		case RIGHT_TURN_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[RIGHT_TURN_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
		}
		
		case BRAKE_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[BRAKE_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}
			Read_ADC_Result_b = TRUE;
			break;
		}
		
		case EXTRA_1_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[EXTRA_1_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
			//Read_ADC_Result_b = true;
		}
		case KILL_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[KILL_SWITCH_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
			//Read_ADC_Result_b = true;
		}
        case KICK_STAND_SWITCH_E:
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[KICK_SWITCH_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
				InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
			}
			else
			{
				; /*TBD*/
			}

			break;
			//Read_ADC_Result_b = true;
		}
        case REGEN_SWITCH_E:
		{	 
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[REGEN_SWITCH_ADC_E];
			if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 >= MIN_ADC_VALUE_FOR_TURN_ON)
			{
				if(BUTTON_RELEASED_E == UserButton_St[UIB_REGEN_SWITCH_E].UserButtonState_En )
				{
					UserButton_St[UIB_REGEN_SWITCH_E].TimerCaptureValue_u32 = GET_VCU_TIME_MS()+3000;
					UserButton_St[UIB_REGEN_SWITCH_E].UserButtonState_En = BUTTON_PRESSED_E;
				}

				if(GET_VCU_TIME_MS() > UserButton_St[UIB_REGEN_SWITCH_E].TimerCaptureValue_u32) 
				{
					UserButton_St[UIB_REGEN_SWITCH_E].LongPressed_b = true;

				}
			}
			else if (InputSigConf_aSt[Signal_En].ReadInputVal_u16 <= MIN_ADC_VALUE_FOR_TURN_OFF)
			{
			
				if( ( false == UserButton_St[UIB_REGEN_SWITCH_E].LongPressed_b ) && ( BUTTON_PRESSED_E == UserButton_St[UIB_REGEN_SWITCH_E].UserButtonState_En ))
				{
					UserButton_St[UIB_REGEN_SWITCH_E].UserButtonState_En = BUTTON_RELEASED_E;
					
					if (TURNED_ON_E == InputSigConf_aSt[Signal_En].SignalState_s8 )
					{
						InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_OFF_E;
					}
					else
					{
						InputSigConf_aSt[Signal_En].SignalState_s8 = TURNED_ON_E;
					}
				}
				else if(true == UserButton_St[UIB_REGEN_SWITCH_E].LongPressed_b)
				{
					UserButton_St[UIB_REGEN_SWITCH_E].LongPressed_b = false;
					UserButton_St[UIB_REGEN_SWITCH_E].UserButtonState_En = BUTTON_RELEASED_E;
				}
				else
				{

				}
				
			}
			else
			{
				; /*TBD*/
			}


			 break;
		}
		case DIAG_FEEDBACK_1_E :
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[DIAG_FEEDBACK_1_E];
			break;
		}
		case DIAG_FEEDBACK_2_E :
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[DIAG_FEEDBACK_2_E];
			break;
		}
		case DIAG_FEEDBACK_3_E :
		{
			InputSigConf_aSt[Signal_En].ReadInputVal_u16 = ADC_Result[DIAG_FEEDBACK_3_E];
			break;
		}
		default:
		{
			
		}
	}

	return;
}



/***********************************************************************************************************************
* Function Name: ReadInputThroghINT
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghINT(UserInputSig_En_t	Signal_En)
{
	 
	switch(Signal_En)
	{
		case MODE_SELECT_NEUTRAL_E:
		{
			if(true == IntcSigConf_St.Neutral_Int_flag)
			{
				IntcSigConf_St.Neutral_Int_flag = false;
				InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8 = true;
				InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8 = false;
				
			}
			else
			{
				InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8 = false;/* code */
			}
			
			/*todo:dileepabs*/
			break;
		}
		
		case MODE_SELECT_ECO_E:
		{
			if(true == IntcSigConf_St.Eco_Int_flag)
			{
				IntcSigConf_St.Eco_Int_flag = false;
				InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8 = true;
				InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8 = false;
				
			}
			else
			{
				InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8 = false;
			}
			break;
		}
		
		case MODE_SELECT_SPORTS_E:
		{
			if(true == IntcSigConf_St.Sports_Int_flag)
			{
				IntcSigConf_St.Sports_Int_flag = false;
				InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8 = true;
				InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8 = false;
				
			}
			else
			{
				InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8 = false;/* code */
			}
			break;
		}
        case MODE_SELECT_REVERSE_E:
        {
			if(true == IntcSigConf_St.Reverse_Int_flag)
			{
				IntcSigConf_St.Reverse_Int_flag= false;
				InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8 = false;
				InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8 = true;
				
			}
			else
			{
				InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8 = false;/* code */
			}
            break;
        }
		
		default:
		{
			;
		}
	}
	
	return;
}

/***********************************************************************************************************************
* Function Name: ReadInputThroghPWM
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadInputThroghPWM(UserInputSig_En_t	Signal_En)
{
	// todo
}
/***********************************************************************************************************************
* Function Name: WriteOutputs
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void WriteOutputs(UserOutputSig_En_t	Signal_En)
{
	//p9_3 = 1;
	//PORT.PM9 = _PORT_Pn3_OUTPUT_HIGH;
	// PORT.P10 |= _PORT_Pn4_OUTPUT_HIGH;
	// PORT.P10 |= _PORT_Pn3_OUTPUT_HIGH;
	// PORT.P9  |= _PORT_Pn3_OUTPUT_HIGH;
	 //PORT.P9  |= _PORT_Pn2_OUTPUT_HIGH;
	 //PORT.P8  |= _PORT_Pn6_OUTPUT_HIGH;
	 //PORT.P9  &= _PORT_Pn3_OUTPUT_LOW;
	 //PORT.P9   = _PORT_Pn2_OUTPUT_LOW;
	 //PORT.P8  &= _PORT_Pn6_OUTPUT_LOW;
	switch (Signal_En) // todo
	{
		case OUTPUT_RELAY1_GND_E:
		{
			if( OutputSigConf_aSt[Signal_En].WriteOutputVal_u16 == 1)
			{
				
			} // todo Manjesh
			break;
		}
		case OUTPUT_RELAY2_GND_E:
		{
			OutputSigConf_aSt[Signal_En].WriteOutputVal_u16 = 0;
			break;
		}
		case OUTPUT_RELAY3_GND_E:
		{
			OutputSigConf_aSt[Signal_En].WriteOutputVal_u16 = 0;
			break;
		}
		case OUTPUT_TORQUE_ZERO_E:
		{
			OutputSigConf_aSt[Signal_En].WriteOutputVal_u16 = 0;
			if( TURNED_ON_E == UserOutputSig_St.Output_Torque_u8)
			{
			   PORT.P10 |= _PORT_Pn3_OUTPUT_HIGH;
			}
			else
			{
				UserOutputSig_St.Output_Torque_u8 = 0;
				 PORT.P10 |= _PORT_Pn3_OUTPUT_LOW;
			}
			break;
		}
		case OUTPUT_MOTOR_KILL_E:
		{
			OutputSigConf_aSt[Signal_En].WriteOutputVal_u16 = UserOutputSig_St.Output_Motor_Kill_U8;
			if( TURNED_ON_E == UserOutputSig_St.Output_Motor_Kill_U8)
			{
			   UserOutputSig_St.Output_Motor_Kill_U8 = 0;
			   PORT.P10 |= _PORT_Pn4_OUTPUT_HIGH;
			}
			break;
		}
	}
}

/***********************************************************************************************************************
* Function Name: UpdateSigToDataBank
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t test_u32 =0;

void UpdateSigToDataBank(void)
{
//	int8_t ModeSelPos_s8 		= 0;

	UserInputSig_St.HighBeam_SwitchState_u8     =  InputSigConf_aSt[HED_LIGHT_HIGH].SignalState_s8;
	UserInputSig_St.LeftTurn_SwitchState_u8     =   InputSigConf_aSt[LEFT_TURN_SWITCH_E].SignalState_s8;
	UserInputSig_St.RightTurn_SwitchState_u8    =  InputSigConf_aSt[RIGHT_TURN_SWITCH_E].SignalState_s8;
	UserInputSig_St.Brake_SwitchState_Front_u8  =   InputSigConf_aSt[BRAKE_SWITCH_E].SignalState_s8;
	UserInputSig_St.Fall_SwitchState_u8         =   TURNED_OFF_E; // Manjesh todo remove
	UserInputSig_St.ReGen_SwitchState_u8        =   InputSigConf_aSt[REGEN_SWITCH_E].SignalState_s8;
	UserInputSig_St.Kill_SwitchState_u8 	   =    InputSigConf_aSt[KILL_SWITCH_E].SignalState_s8;
	UserInputSig_St.KickStand_SwitchState_u8   =    InputSigConf_aSt[KICK_STAND_SWITCH_E].SignalState_s8;
	if (true == UserButton_St[UIB_REGEN_SWITCH_E].LongPressed_b)
	{
		UserInputSig_St.B_Low_SwitchState_u8 = TURNED_ON_E;
	}

	if(true == InputSigConf_aSt[MODE_SELECT_NEUTRAL_E].SignalState_s8)
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
	}
	else if (true == InputSigConf_aSt[MODE_SELECT_ECO_E].SignalState_s8)
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Sports_Mode_Sel_u8  =  TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
	}
	else if(true == InputSigConf_aSt[MODE_SELECT_SPORTS_E].SignalState_s8)
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_ON_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;
	}
	else if(true == InputSigConf_aSt[MODE_SELECT_REVERSE_E].SignalState_s8)
	{
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;
	}
	else
	{

	}	
	return;
}


/***********************************************************************************************************************
* Function Name: Clear_Drive_Mode
* Description  : 
* Arguments    : DrivingModes_En_t DrivingModes_En
* Return Value : None
***********************************************************************************************************************/
void Clear_Drive_Mode(DrivingModes_En_t DrivingModes_En)
{
	switch (DrivingModes_En)
	{
	case NEUTRAL_MODE_E:
	{
		Drive_Mode_Counter_s16 = NEUTRAL_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8  = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		break;
	}
	case ECONOMY_MODE_E:
	{
		Drive_Mode_Counter_s16 = ECONOMY_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		break;
	}
	case SPORTS_MODE_E:
	{
		Drive_Mode_Counter_s16 = SPORTS_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_ON_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		break;
	}
	case REVERSE_MODE_E:
	{
		Drive_Mode_Counter_s16 = REVERSE_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_ON_E;

		break;
	}
	case SAFE_MODE_E: // todo Manjesh Check
	{
		Drive_Mode_Counter_s16 = REVERSE_MODE_E;
		UserInputSig_St.Neutral_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Economy_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Sports_Mode_Sel_u8 = TURNED_OFF_E;
		UserInputSig_St.Reverse_Mode_Sel_u8 = TURNED_OFF_E;

		break;
	}

	default:
	{
		break;
	}
	}
}
rtc_counter_value_t rtc_time;
void CANSched_Debug_TxMsgCallback(void)
{
	static bool flag = false;
	CAN_MessageFrame_St_t Can_Applidata_St;
//	Can_Applidata_St.DataBytes_au8[0] = (uint8_t)(ADC_Result[8]>>8U);
//	Can_Applidata_St.DataBytes_au8[1] = (uint8_t)ADC_Result[8];
//	Can_Applidata_St.DataBytes_au8[2] = (uint8_t)(ADC_Result[7]>>8U);
//	Can_Applidata_St.DataBytes_au8[3] = (uint8_t)ADC_Result[7];
//	Can_Applidata_St.DataBytes_au8[4] = (uint8_t)0;
//	Can_Applidata_St.DataBytes_au8[5] = (uint8_t)UserInputSig_St.Throttle_u8;
//	Can_Applidata_St.DataBytes_au8[6] = InputSigConf_aSt[EXTRA_4_E].SignalState_s8;
//	Can_Applidata_St.DataBytes_au8[7] = InputSigConf_aSt[EXTRA_3_E].SignalState_s8;//UserInputSig_St.Throttle_u8;
//	R_Config_RTCA0_Get_CounterDirectValue(&rtc_time);
	//BCD to Decimal conversion for RTC.
//	Can_Applidata_St.DataBytes_au8[0] = (uint8_t)((((rtc_time.sec & 0x70) >> 4) * 10) + (rtc_time.sec % 16));
//	Can_Applidata_St.DataBytes_au8[1] = (uint8_t)((((rtc_time.min & 0x70) >> 4) * 10) + (rtc_time.min % 16));
//	Can_Applidata_St.DataBytes_au8[2] = (uint8_t)((((rtc_time.hour & 0x70) >> 4) * 10) + (rtc_time.hour % 16));
	Can_Applidata_St.DataBytes_au8[3] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[4] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[5] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[6] = (uint8_t)0;
	Can_Applidata_St.DataBytes_au8[7] = (uint8_t)0;

	Can_Applidata_St.DataLength_u8 = 8;
	Can_Applidata_St.MessageType_u8 = STD_E;
	//CIL_CAN_Tx_AckMsg(CIL_TEST3_TX_E, Can_Applidata_St);
	if(((Can_Applidata_St.DataBytes_au8[0]) == 30) && (flag == false))
	{
		flag = true;
		//R_Config_STBC_Prepare_Stop_Mode();
		//R_Config_STBC_Start_Stop_Mode();
		EI();
	}

}

