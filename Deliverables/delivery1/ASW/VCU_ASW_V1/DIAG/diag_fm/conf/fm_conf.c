/***************************************************************************************************
 *    FILENAME    :  fm_conf.c
 *
 *    DESCRIPTION : Contains the configuration data for the fault management
 *                  Level 1 and 2. Application user should configure this
 *                  file prior to the compilation
 *
 *    $Id         : $
 *
 ***************************************************************************************************
 * Revision history
 *
 * Ver Author       Date       Description
 * 1   Sloki          25/09/2008
 ***************************************************************************************************
*/

/*
 **************************************************************************************************
 *    Includes
 **************************************************************************************************
*/
#include "fm.h"
#include "fm_level1.h"
#include "fm_conf.h"
//#include "config_40.h"
//#include "fm_conf.h"
/*
 ***************************************************************************************************
 *    Defines
 ***************************************************************************************************
*/

//#ifndef (TOTAL_FRZFRM_MANUF_E)
//	#error ***Enum table for freezeframe must have TOTAL_FRZFRM_MANUF_E ***
//#endif
//
//#ifndef (NUM_OF_FAULTPATHS1_E)
//	#error ***Enum table for faultpaths must have TOTAL_FRZFRM_MANUF_E ***
//#endif

#if (TOTAL_FRZFRM_MANUF_E != NUM_OF_FAULTPATHS_E)
#error ***Both Freezeframe Enum entries and Total Faultpath entries should match.***
#endif


/*
 **************************************************************************************************
 *    Variables
 **************************************************************************************************
*/


/*
 **************************************************************************************************
 *    "CONFIGURATION DATA TO BE FILLED BY APPLICATION USER"
 *                    FAULT ENTRY CONFIGURATION
 **************************************************************************************************
*/
const FM_APPL_FLTCONF_St_t APPL_FAULTCONF_aSt[NUM_OF_FAULTPATHS_E] =
{
	/*
		FM configuration
	*/
	/***************************************************************************************/
	/*                                                                                                                             */
	// DTC 1: control module configuration configuration
	{
		// Fault Path 
		FM_FP_TEST1_E,
		// Data for debounce table  
		{3,-3,0,3},
		//Fault priority
		FM_PRIO_3_E,
		//Readiness group supported
		RDY_EGR_GRP,
		//Functional unit
		FM_FUN_NW_E,
		//Entries for this fault path
		///CNTRL_MOD_CONFIG_FRZFRM_ENTRIES,
		//Error mask
		FM_ALL_ERR,
		//Flag to indicate its OBD relavent
		FALSE,
		//Flag to indicate GPL activation required or not
		FALSE,
	},

	// DTC 2: Control Module Input Power "A" Under Voltage
	{
		FM_FP_TEST2_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		//CNTRL_MOD_IP_PWR_A_UNDER_FRZFRM_ENTRIES,
		FM_MAX_ERR,
		TRUE,
		FALSE,
	},

	// DTC 3: Control Module Input Power "A" Over Voltage
	{
		FM_FP_TEST3_E,
		{0,-1,0,10},   	
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		//CNTRL_MOD_IP_PWR_A_OVER_FRZFRM_ENTRIES,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},

	//DTC 4: Vehicle Communication Bus A
	 {
		FM_FP_TEST4_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},

	// DTC 5: Vehicle Communication Bus A
	{
		FM_FP_TEST5_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},

	// DTC 6: Invalid Data Received From Antilock Braking System/Electronic Stability Program (ABS/ESP) Control Module
	{
		FM_FP_TEST6_E,
		{0,-1,0,10},      
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},

	 // DTC 7: Invalid Data Received From ECM/PCM  
	 {
		 FM_FP_TEST7_E,
		 {0,-1,0,10},
		 FM_PRIO_3_E,
		 RDY_EGR_GRP,
		 FM_FUN_NW_E,
		 //ECM_PCM_INV_DATA_FRZFRM_ENTRIES  , 
		 FM_MAX_ERR,
		 FALSE,
		 FALSE,
	 },

	 // DTC 8: Invalid Data Received From HVAC Control Module
	{
		FM_FP_TEST8_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},

	 // DTC 9: Invalid Data Received From TCM
	 {
		FM_FP_TEST9_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	 },

	// DTC 10: Lost Communication With Antilock Braking System/Electronic Stability Program (ABS/ESP) Control Module
	{
		FM_FP_TEST10_E,
		{0,-1,0,10},
		FM_PRIO_3_E,
		RDY_EGR_GRP,
		FM_FUN_NW_E,
		FM_MAX_ERR,
		FALSE,
		FALSE,
	},
};

uint16_t RPM_N_u16 = 0;
