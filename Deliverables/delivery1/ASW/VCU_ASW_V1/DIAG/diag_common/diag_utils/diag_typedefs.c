/******************************************************************************
 *    FILENAME    : diag_typedefs.c
 *    DESCRIPTION : source file contains definitions of timer counter.
 ******************************************************************************
 * Revision history
 *
 * Ver Author       Date               Description
 * 1   Sloki     21/11/2020		   Initial version
 ******************************************************************************
*/


#include "diag_typedefs.h"


volatile UINT32 TS_time_ms_u32 = 0;
