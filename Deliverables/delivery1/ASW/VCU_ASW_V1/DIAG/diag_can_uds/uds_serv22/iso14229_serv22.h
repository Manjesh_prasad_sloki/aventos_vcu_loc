/******************************************************************************
 *    FILENAME    : uds_serv22.h
 *    DESCRIPTION : Header file for UDS service - READ DATA BY IDENTIFIER(0x22).
 ******************************************************************************
 * Revision history
 *  
 * Ver Author          Date                     Description
 * 1   Sloki     10/01/2019		   Initial version
 ******************************************************************************
*/  

#ifndef _UDS_SERV22_H_
#define	_UDS_SERV22_H_

#ifdef	__cplusplus
 "C" {
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "uds_conf.h"
#include "iso14229_serv27.h"
#include "iso14229_serv2C.h"
    
/* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    /* ************************************************************************** */

#define MAX_DID_REQ                     0x20u
#define SERV22_SEESION_CHECK_BIT_POS    0u

#define MAX_DIDSUPPORTED_FOR_2C         0x14
/*
 **************************************************************************************************
 *    Export Variables
 **************************************************************************************************
 */
extern uint32_t Serv22_Index_u32;


/**
*  FUNCTION NAME : iso14229_serv22
*  FILENAME      : iso14229_serv22.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributor table.
*  @brief        : This function will process the service_22 requests
*  @return       : Type of response.                     
*/   
extern  UDS_Serv_resptype_En_t iso14229_serv22(UDS_Serv_St_t*);
 

 /**
*  FUNCTION NAME : UDS_Serv22_Timeout
*  FILENAME      : uds_serv22.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
extern void UDS_Serv22_Timeout(void);
 
#ifdef	__cplusplus
}
#endif
 
 
#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
