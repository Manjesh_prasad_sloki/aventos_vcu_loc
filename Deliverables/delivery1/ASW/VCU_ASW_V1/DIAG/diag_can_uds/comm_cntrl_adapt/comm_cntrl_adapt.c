/******************************************************************************
 *    FILENAME    : comm_cntrl_adapt.c
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

#include "comm_cntrl_adapt.h"

static bool IVN_Tx_En_flag_b; 
static bool IVN_Rx_En_flag_b; 

void NormalCommMsg_Adapt(bool Rx_Status,  bool Tx_Status)
{
    IVN_Tx_En_flag_b = Tx_Status;
	IVN_Rx_En_flag_b = Rx_Status;
}
void NWMngmntCommMsg_Adapt(bool Rx_Status,  bool Tx_Status)
{
    
}
void NWMngmnt_NormalCommMsg_Adapt(bool Rx_Status,  bool Tx_Status)
{
    IVN_Tx_En_flag_b = Tx_Status;
	IVN_Rx_En_flag_b = Rx_Status;
}

bool Get_IVN_Tx_Sts(void)
{
	return IVN_Tx_En_flag_b;
}

bool Get_IVN_Rx_Sts(void)
{
	return IVN_Rx_En_flag_b;
}