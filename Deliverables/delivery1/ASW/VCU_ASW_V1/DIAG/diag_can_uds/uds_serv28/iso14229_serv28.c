/******************************************************************************
 *    FILENAME    : uds_serv28.c
 *    DESCRIPTION : Service description for UDS service - Communication Control.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       21/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv28.h"
#include "diag_adapt.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* @Summary : - this boolean for control type */

bool ENABLE_TX_b = TRUE;
bool ENABLE_RX_b = TRUE;
bool DISABLE_TX_b = FALSE;
bool DISABLE_RX_b = FALSE;

Comm_Config_St_t Comm_Config_aSt[TOTAL_COMM_TYPE_E - 1] = 
{
    {NORMAL_COMM_MSG_E,                         &NormalCommMsg_Adapt         },
    {NETWORK_MANAGEMENT_COMM_MSG_E,             &NWMngmntCommMsg_Adapt       },
    {NETWORK_MANAGEMENT_AND_NORMAL_COMM_MSG_E,  &NWMngmnt_NormalCommMsg_Adapt},
};

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : CommunicationMessage_Fn
*  FILENAME      : uds_serv28.c
*  @param1       : uint8_t communicationType_u8 - communication type parameter.
*  @param2       : bool Rx_Status - control type RX status.
*  @param3       : bool Tx_Status - control type TX status.
*  @brief        : This function will process the communication type.
*  @return       : None.
**/
void CommunicationMessage_Fn(uint8_t communicationType_u8, bool Rx_Status, bool Tx_Status)
{
    uint8_t communicationStatus_u8 = communicationType_u8 - 1;
    (Comm_Config_aSt[communicationStatus_u8].COMM_MSG_Fptr)(Rx_Status, Tx_Status);
}

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : iso14229_serv28
*  FILENAME      : iso14229_serv28.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_28 requests
*  @return       : Type of response.
**/
UDS_Serv_resptype_En_t iso14229_serv28(UDS_Serv_St_t*  UDS_Serv_pSt)
{
    uint16_t numbytes_req_u16 = UDS_Serv_pSt->RxLen_u16;
    uint8_t communicationType_u8 = UDS_Serv_pSt->RxBuff_pu8[2];
    uint8_t controlType_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    uint8_t SuppressPosRes_u8  = (UDS_Serv_pSt->RxBuff_pu8[1] & 0x80);
    bool NoResponse_b          = TRUE;
    UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
    
    if(SERV_28_MIN_LEN != numbytes_req_u16)
    {
        /* Check whether length should be valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_COMMUNICATIONCONTROL;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH; /* 0x13 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if((!communicationType_u8) ||(TOTAL_COMM_TYPE_E <= communicationType_u8))
    {
        /* Check whether the CommunicationType is valid or not*/
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_COMMUNICATIONCONTROL;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        controlType_u8 = (controlType_u8 & 0x7F);
        switch(controlType_u8)
        {
            case ENABLE_RX_AND_TX_SUB_ID_E:
            {
                CommunicationMessage_Fn(communicationType_u8, ENABLE_RX_b, ENABLE_TX_b);
                UDS_Serv_pSt->TxLen_u16  = 1u;
                UDS_Serv_pSt->TxBuff_pu8[1] = controlType_u8;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                COMMUNICATION_Status_au8[0] = controlType_u8;
				break;
            }
            case ENABLE_RX_AND_DISABLE_TX_SUB_ID_E:
            {
                CommunicationMessage_Fn(communicationType_u8, ENABLE_RX_b, DISABLE_TX_b);
                UDS_Serv_pSt->TxLen_u16  = 1u;
                UDS_Serv_pSt->TxBuff_pu8[1] = controlType_u8;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                COMMUNICATION_Status_au8[0] = controlType_u8;
				break;
            }
            case DISABLE_RX_AND_ENABLE_TX_SUB_ID_E:
            {
                CommunicationMessage_Fn(communicationType_u8, DISABLE_RX_b, ENABLE_TX_b);
                UDS_Serv_pSt->TxLen_u16  = 1u;
                UDS_Serv_pSt->TxBuff_pu8[1] = controlType_u8;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                COMMUNICATION_Status_au8[0] = controlType_u8;
				break;
            }
            case DISABLE_RX_AND_TX_SUB_ID_E:
            {
                CommunicationMessage_Fn(communicationType_u8, DISABLE_RX_b, DISABLE_TX_b);
                UDS_Serv_pSt->TxLen_u16  = 1u;
                UDS_Serv_pSt->TxBuff_pu8[1] = controlType_u8;
                Serv_resptype_En = UDS_SERV_RESP_POS_E;
                COMMUNICATION_Status_au8[0] = controlType_u8;
				break;
            }
            default:
            {
                /* Check whether length should be valid or not */
                NoResponse_b = FALSE;
                UDS_Serv_pSt->TxLen_u16 = 3u;
                UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
                UDS_Serv_pSt->TxBuff_pu8[1] = SID_COMMUNICATIONCONTROL;
                UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED; /* 0x12 */
                Serv_resptype_En = UDS_SERV_RESP_NEG_E;
            }
        }
        if(SuppressPosRes_u8 && NoResponse_b)
        {
           Serv_resptype_En = UDS_SERV_RESP_NORESP_E;
        }
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : UDS_Serv28_Timeout
*  FILENAME      : uds_serv28.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv28_Timeout(void)
{
    NWMngmnt_NormalCommMsg_Adapt(TRUE, TRUE);		/* Rx Enable, Tx Enable */
	return;
}

/* *****************************************************************************
 End of File
*/
