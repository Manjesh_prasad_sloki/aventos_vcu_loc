/******************************************************************************
 *    FILENAME    : uds_serv23.c
 *    DESCRIPTION : Service description for UDS service - Read Memory By Address.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "iso14229_serv23.h"
#include "iso14229_serv3D.h"


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/** 
*  FUNCTION NAME : iso14229_serv23
*  FILENAME      : iso14229_serv23.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_23 requests
*  @return       : Type of response.
**/

UDS_Serv_resptype_En_t iso14229_serv23(UDS_Serv_St_t*  UDS_Serv_pSt)
{
    uint32_t i = 0u;
    uint32_t memoryAddress_u32 = 0x00u;
    uint16_t memorySize_u16 = 0x00u;
    uint8_t format_identifier_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    uint8_t memoryAddress_len_u8 = format_identifier_u8 & 0x0F;
    uint8_t memorySize_len_u8 = ((format_identifier_u8 & 0xF0) >> 4);
    UDS_Serv_resptype_En_t Serv_resptype_En = UDS_SERV_RESP_UNKNOWN_E;
    
    for(i = 0 ; i < memoryAddress_len_u8 ; i++) /* Memory Address */
    {
        memoryAddress_u32 = ((memoryAddress_u32 << 8) | UDS_Serv_pSt->RxBuff_pu8[i+2]);
    }
    for(i = memoryAddress_len_u8 ; i < (memoryAddress_len_u8 + memorySize_len_u8) ; i++) /* Memory Size */
    {
        memorySize_u16 = ((memorySize_u16 << 8) | UDS_Serv_pSt->RxBuff_pu8[i+2]);
    }
    
    if(SERV_23_MIN_LEN > UDS_Serv_pSt->RxLen_u16)
    {
        /* Check whether length should be valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_RMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH; /* 0x13 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if((MEM_ADDRESS_MAX_LEN < memoryAddress_len_u8) || (MEM_SIZE_MAX_LEN < memorySize_len_u8))
    {
        /* Check whether the memory address length and memory size length is valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_RMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if(!(MemoryAddr_Check(memoryAddress_u32, memorySize_u16)))
    {
        /* Check whether the memory start and end address is valid or not */
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_RMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else if(FALSE == Check_SecurityClearance())
	{
        UDS_Serv_pSt->TxLen_u16 = 3u;
        UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
        UDS_Serv_pSt->TxBuff_pu8[1] = SID_RMBA;
        UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED; /* 0x33 */
        Serv_resptype_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        UDS_Serv_pSt->TxLen_u16 = (memorySize_u16 + 1);
        UDS_Serv_pSt->TxBuff_pu8[1] = format_identifier_u8;
        /*for(i = 0 ; i < memorySize_u16 ; i++)		// For Flash Memory Read	
        {
            UDS_Serv_pSt->TxBuff_pu8[i+2] = (*(uint8_t*)memoryAddress_u32);
            memoryAddress_u32 = memoryAddress_u32 + 1;
        }*/
		
		if(TRUE == Read23_Ser_NVM((U2)memoryAddress_u32, (U1)memorySize_u16, &(UDS_Serv_pSt->TxBuff_pu8[2]))) {
			Serv_resptype_En = UDS_SERV_RESP_POS_E;
		} else {
			UDS_Serv_pSt->TxLen_u16 = 3u;
			UDS_Serv_pSt->TxBuff_pu8[0] = 0x7F;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_RMBA;
			UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE; /* 0x31 */
			Serv_resptype_En = UDS_SERV_RESP_NEG_E;
		}
    }
    return Serv_resptype_En;
}

/**
*  FUNCTION NAME : UDS_Serv23_Timeout
*  FILENAME      : uds_serv23.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void UDS_Serv23_Timeout(void)
{
    return;
}

/* *****************************************************************************
 End of File
*/
