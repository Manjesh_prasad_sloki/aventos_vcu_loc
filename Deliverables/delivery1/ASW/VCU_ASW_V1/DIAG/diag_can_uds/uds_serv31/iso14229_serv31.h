/******************************************************************************
 *    FILENAME    : uds_serv10.h
 *    DESCRIPTION : Header file for UDS service - Diagnostic Session Control.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Arun	       20/06/2019		   Initial version
 ******************************************************************************
*/ 

#ifndef _UDS_SERV31_H_
#define	_UDS_SERV31_H_

#ifdef	__cplusplus
 "C" {
#endif

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.*/
#include "uds_conf.h"
#include "iso14229_serv27.h"
  
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

#define SERV31_ROUTINE_NOTSTARTED		0x00
#define SERV31_ROUTINE_INPROGRESS		0x08
#define SERV31_ROUTINE_COMPLETED		0x01

/*
 ****************************************************************************************
 *    al Variables
 ****************************************************************************************
*/ 

 extern const ServID_Ser31_St_t Service31_ServIDaSt[TOTAL_ROUTINES_E];


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Data Types                                                        */
/* ************************************************************************** */
/* ************************************************************************** */



/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

extern uint8_t Serv31_PreconditionCheck(void);
extern UDS_Serv_resptype_En_t iso14229_serv31(UDS_Serv_St_t*  UDS_Serv_pSt);
extern void Serv31_Routine_Complete(Serv31_RoutineList_En_t Routine_Index_t);
extern void ClearRoutineResult(void);

/* Routine Start Functions */
extern void ServiceInfoReset_Routine(void);
extern void DispAllWarning_TXT_Routine(void);
extern void SetGaugeMAX_Routine(void);
extern void SetGaugeMIN_Routine(void);
extern void SetWarningLampOFF_Routine(void);
extern void SetWarningLampON_Routine(void);
extern void AutomaticLearning_Routine(void);
extern void OdometerReset_Routine(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ISO14229_SER31_H */
