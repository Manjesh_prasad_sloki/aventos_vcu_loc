﻿/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : mcu_can_signals.h
|    Project        : VCU
|    Module         : motor signals data
|    Description    : This is the generated file from the DBC2CH Tool for motor.
|                     controller
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 04/05/2021     Sandeep K Y            Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/

#ifndef MCU_CAN_SIGNALS_H
#define MCU_CAN_SIGNALS_H

/*******************************************************************************
 *  Includes
 ******************************************************************************/
#include <stdint.h>
#include "r_cg_macrodriver.h"

/*******************************************************************************
 *  Define & Macros
 ******************************************************************************/
#define MCU_0X100_MOTOR_SPEED_LIMIT_MASK0  8U


#define MCU_0X150_MOTOR_SPEED_MASK0  8U
#define MCU_0X150_RIDE_MODE_ACTUAL_MASK0  2U


#define MCU_0X250_MOTOR_TEMP_MASK0  8U
#define MCU_0X250_CONTROLLER_TEMP_MASK0  8U




#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @MCU_0X100 CAN Message                                   (256) */
#define MCU_0X100_ID                                            (256U)
#define MCU_0X100_IDE                                           (0U)
#define MCU_0X100_DLC                                           (8U)


#define MCU_0X100_MOTOR_SPEED_LIMITFACTOR                                   (0.305176)
#define MCU_0X100_CANID_MOTOR_SPEED_LIMIT_STARTBIT                           (0)
#define MCU_0X100_CANID_MOTOR_SPEED_LIMIT_OFFSET                             (0)
#define MCU_0X100_CANID_MOTOR_SPEED_LIMIT_MIN                                (-10000)
#define MCU_0X100_CANID_MOTOR_SPEED_LIMIT_MAX                                (9999.7)
#define MCU_0X100_MAX_REGENERATIONFACTOR                                   (0.392)
#define MCU_0X100_CANID_MAX_REGENERATION_STARTBIT                           (24)
#define MCU_0X100_CANID_MAX_REGENERATION_OFFSET                             (0)
#define MCU_0X100_CANID_MAX_REGENERATION_MIN                                (0)
#define MCU_0X100_CANID_MAX_REGENERATION_MAX                                (99.96)
#define MCU_0X100_RIDE_MODE_REQUESTFACTOR                                   (1)
#define MCU_0X100_CANID_RIDE_MODE_REQUEST_STARTBIT                           (56)
#define MCU_0X100_CANID_RIDE_MODE_REQUEST_OFFSET                             (0)
#define MCU_0X100_CANID_RIDE_MODE_REQUEST_MIN                                (0)
#define MCU_0X100_CANID_RIDE_MODE_REQUEST_MAX                                (7)

                       
typedef struct
{
  int16_t Motor_Speed_Limit;
  uint8_t Max_Regeneration;
  uint8_t Ride_mode_Request;
}
VCU_MCU_0x100_Tx_t;


/* def @MCU_0X150 CAN Message                                   (336) */
#define MCU_0X150_ID                                            (336U)
#define MCU_0X150_IDE                                           (0U)
#define MCU_0X150_DLC                                           (8U)


#define MCU_0X150_MOTOR_SPEEDFACTOR                                   (0.305176)
#define MCU_0X150_CANID_MOTOR_SPEED_STARTBIT                           (0)
#define MCU_0X150_CANID_MOTOR_SPEED_OFFSET                             (0)
#define MCU_0X150_CANID_MOTOR_SPEED_MIN                                (-10000)
#define MCU_0X150_CANID_MOTOR_SPEED_MAX                                (9999.7)
#define MCU_0X150_RIDE_MODE_ACTUALFACTOR                                   (1)
#define MCU_0X150_CANID_RIDE_MODE_ACTUAL_STARTBIT                           (58)
#define MCU_0X150_CANID_RIDE_MODE_ACTUAL_OFFSET                             (0)
#define MCU_0X150_CANID_RIDE_MODE_ACTUAL_MIN                                (0)
#define MCU_0X150_CANID_RIDE_MODE_ACTUAL_MAX                                (7)


typedef struct
{
  int16_t Motor_Speed;
  uint8_t Ride_Mode_Actual;
}
MCU_VCU_0x150_Rx_t;


/* def @MCU_0X250 CAN Message                                   (592) */
#define MCU_0X250_ID                                            (592U)
#define MCU_0X250_IDE                                           (0U)
#define MCU_0X250_DLC                                           (8U)


#define MCU_0X250_MOTOR_TEMPFACTOR                                   (0.390625)
#define MCU_0X250_CANID_MOTOR_TEMP_STARTBIT                           (0)
#define MCU_0X250_CANID_MOTOR_TEMP_OFFSET                             (0)
#define MCU_0X250_CANID_MOTOR_TEMP_MIN                                (0)
#define MCU_0X250_CANID_MOTOR_TEMP_MAX                                (199.609)
#define MCU_0X250_CONTROLLER_TEMPFACTOR                                   (0.292969)
#define MCU_0X250_CANID_CONTROLLER_TEMP_STARTBIT                           (16)
#define MCU_0X250_CANID_CONTROLLER_TEMP_OFFSET                             (0)
#define MCU_0X250_CANID_CONTROLLER_TEMP_MIN                                (-40.1368)
#define MCU_0X250_CANID_CONTROLLER_TEMP_MAX                                (149.707)
#define MCU_0X250_MOTOR_ROTATION_NUMBER_MASK0  			24U
#define MCU_0X250_MOTOR_ROTATION_NUMBER_MASK1  			16U
#define MCU_0X250_MOTOR_ROTATION_NUMBER_MASK2  			8U

#define MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_STARTBIT                           (32)
#define MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_OFFSET                             (0)
#define MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_MIN                                (0)
#define MCU_0X250_CANID_MOTOR_ROTATION_NUMBER_MAX                                (4294967295)

typedef struct
{
  int16_t Motor_Temp;
  int16_t Controller_Temp;
  uint32_t Motor_Rotation_Number;
}
MCU_VCU_0x250_Rx_t;


/* def @MCU_0X400 CAN Message                                   (1024) */
#define MCU_0X400_ID                                            (1024U)
#define MCU_0X400_IDE                                           (0U)
#define MCU_0X400_DLC                                           (2U)


#define MCU_0X400_BROADCAS_RATEFACTOR                                   (4)
#define MCU_0X400_CANID_BROADCAS_RATE_STARTBIT                           (0)
#define MCU_0X400_CANID_BROADCAS_RATE_OFFSET                             (0)
#define MCU_0X400_CANID_BROADCAS_RATE_MIN                                (0)
#define MCU_0X400_CANID_BROADCAS_RATE_MAX                                (1020)
#define MCU_0X400_BROADCAST_MODEFACTOR                                   (1)
#define MCU_0X400_CANID_BROADCAST_MODE_STARTBIT                           (8)
#define MCU_0X400_CANID_BROADCAST_MODE_OFFSET                             (0)
#define MCU_0X400_CANID_BROADCAST_MODE_MIN                                (0)
#define MCU_0X400_CANID_BROADCAST_MODE_MAX                                (1)


typedef struct
{
  uint8_t Broadcas_Rate;
  int8_t Broadcast_Mode;
}
VCU_MCU_0x400_Tx_t;


 extern uint32_t Deserialize_VCU_MCU_0x100_Tx(VCU_MCU_0x100_Tx_t* message, const uint8_t* data);
 extern uint32_t Deserialize_MCU_VCU_0x150_Rx(MCU_VCU_0x150_Rx_t* message, const uint8_t* data);
 extern uint32_t Deserialize_MCU_VCU_0x250_Rx(MCU_VCU_0x250_Rx_t* message, const uint8_t* data);
 extern uint32_t Deserialize_VCU_MCU_0x400_Tx(VCU_MCU_0x400_Tx_t* message, const uint8_t* data);
 
 
 extern uint32_t Serialize_VCU_MCU_0x400_Tx(VCU_MCU_0x400_Tx_t* message, uint8_t* data);
 extern uint32_t Serialize_VCU_MCU_0x100_Tx(VCU_MCU_0x100_Tx_t* message, uint8_t* data);
 #endif /* MCU_CAN_SIGNALS_H */
